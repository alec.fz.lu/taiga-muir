// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim.h"
#include "Vtaiga_sim__Syms.h"

VL_INLINE_OPT void Vtaiga_sim::_sequent__TOP__8(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_sequent__TOP__8\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d;
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n) 
               & 1U);
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n) 
               & 1U);
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n;
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock)
               ? (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d)
               : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock)
               ? (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d)
               : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q)));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n;
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill) 
             | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q)))) {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q)))) {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o 
        = ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q))))
            ? (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q) 
                         >> (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)))))
            : 0U);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o 
        = ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q))))
            ? (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q) 
                         >> (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)))))
            : 0U);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3c3fffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3dffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q) 
                                     >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q))))) 
              << 0x29U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x18000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3bffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x40000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3c3fffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3dffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q) 
                                     >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q))))) 
              << 0x29U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x18000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3bffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x40000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0xfffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
                                << 1U) | (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))))) 
              << 0x2cU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0xfffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
                                << 1U) | (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))))) 
              << 0x2cU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[0U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[1U];
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[2U];
        }
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] = 0U;
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fe000000000ULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3fefffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o) 
                                     >> 8U)))) << 0x24U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xbadcab1e0ULL | (0x3ff00000000fULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xcULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fdfffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x2000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = ((0x3ffffffffffdULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
               | ((QData)((IData)((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q))))) 
                  << 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fe000000000ULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3fefffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o) 
                                     >> 8U)))) << 0x24U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xbadcab1e0ULL | (0x3ff00000000fULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xcULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fdfffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x2000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = ((0x3ffffffffffdULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
               | ((QData)((IData)((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q))))) 
                  << 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xbfffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o)) 
              << 0x36U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xbfffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o)) 
              << 0x36U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            >> 0x1aU) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U] 
            >> 1U) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U] 
            >> 7U) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0x7fffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o)) 
              << 0x37U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q)))) {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
            >> 0xeU) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0x7fffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o)) 
              << 0x37U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q)))) {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 1U;
        }
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U]) 
           | (0xfffffffeU & ((0xfffffffcU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
        = ((1U & ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                  | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                       & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                      & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                     >> 0x1fU))) | (0xfffffffeU & (
                                                   (2U 
                                                    & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                                                   | (0xfffffffcU 
                                                      & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
              | (0xfffffffeU & ((2U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
                                | (0x3cU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U])))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0x3ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | (0xfc000000U & ((0xf8000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x19U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x1aU))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
        = ((0x3ffffffU & ((0x3ffffffU & ((0x2000000U 
                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                             << 0x19U)) 
                                         | (0x1ffffffU 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                               >> 7U)))) 
                          | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                               & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                              & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                             >> 6U))) | (0xfc000000U 
                                         & ((0x4000000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x19U)) 
                                            | (0xf8000000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U]) 
           | ((0x3ffffffU & ((0x2000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                            << 0x19U)) 
                             | (0x1ffffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                              >> 7U)))) 
              | (0xfc000000U & ((0x4000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                               << 0x19U)) 
                                | (0x78000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                  << 0x19U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0x7ffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | (0xfff80000U & ((0xfff00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x12U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x13U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
        = ((0x7ffffU & ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)) 
                                     | (0x3ffffU & 
                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                         >> 0xeU)))) 
                        | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                            & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                           >> 0xdU))) | (0xfff80000U 
                                         & ((0x80000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x12U)) 
                                            | (0xfff00000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                        << 0x12U)) 
                           | (0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                          >> 0xeU)))) 
              | (0xfff80000U & ((0x80000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                             << 0x12U)) 
                                | (0xf00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                << 0x12U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U]) 
           | (0xfffffffeU & ((0xfffffffcU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
        = ((1U & ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                  | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                       & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                      & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                     >> 0x1fU))) | (0xfffffffeU & (
                                                   (2U 
                                                    & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                                                   | (0xfffffffcU 
                                                      & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
              | (0xfffffffeU & ((2U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
                                | (0x3cU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U])))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0x3ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | (0xfc000000U & ((0xf8000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x19U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x1aU))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
        = ((0x3ffffffU & ((0x3ffffffU & ((0x2000000U 
                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                             << 0x19U)) 
                                         | (0x1ffffffU 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                               >> 7U)))) 
                          | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                               & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                              & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                             >> 6U))) | (0xfc000000U 
                                         & ((0x4000000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x19U)) 
                                            | (0xf8000000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U]) 
           | ((0x3ffffffU & ((0x2000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                            << 0x19U)) 
                             | (0x1ffffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                              >> 7U)))) 
              | (0xfc000000U & ((0x4000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                               << 0x19U)) 
                                | (0x78000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                  << 0x19U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0x7ffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | (0xfff80000U & ((0xfff00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x12U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x13U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
        = ((0x7ffffU & ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)) 
                                     | (0x3ffffU & 
                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                         >> 0xeU)))) 
                        | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                            & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                           >> 0xdU))) | (0xfff80000U 
                                         & ((0x80000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x12U)) 
                                            | (0xfff00000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                        << 0x12U)) 
                           | (0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                          >> 0xeU)))) 
              | (0xfff80000U & ((0x80000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                             << 0x12U)) 
                                | (0xf00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                << 0x12U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n)) 
               | ((3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1 = 1U;
        if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1) 
                      << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2 = 1U;
            if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                    = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                        & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                       | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2) 
                          << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
            = (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n)) 
               | ((3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1 = 1U;
        if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1) 
                      << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2 = 1U;
            if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                    = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                        & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                       | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2) 
                          << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
            = (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0x3fffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | (0xffffc000U & ((0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 0xdU)) 
                             | (0x4000U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                           << 0xeU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
        = ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                   << 0xdU)) | (0x1fffU 
                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x13U)))) 
           | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                         << 0xdU)) 
                             | (0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                               << 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                      << 0xdU)) | (0x1fffU 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                      >> 0x13U)))) 
              | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                            << 0xdU)) 
                                | (0x1ff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                                 << 0xdU))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (0xffffff80U & ((0xffffff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 6U)) 
                             | (0x80U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                         << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
        = ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                               << 6U)) | (0x3fU & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x1aU)))) 
           | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                       << 6U)) | (0xffffff00U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                     << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0xfffc0000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                  << 6U)) | (0x3fU 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                >> 0x1aU)))) 
              | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                          << 6U)) | 
                                (0x3ff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 6U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
        = ((0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                            << 0x1fU) | (0x7ffffffeU 
                                         & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                            >> 1U)))) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                    >> 2U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                  >> 1U)) | (0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 0x1fU) 
                                            | (0x7ffffffeU 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                  >> 1U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                  >> 1U)) | (0x7feU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                       >> 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | (0xffffff80U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                           << 0x20U) 
                                          | (0xfffffffffffffffeULL 
                                             & (QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                         & (0U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0xffffc000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | ((0x7fU & ((IData)(((0x7ffffffffeULL & 
                                  (((QData)((IData)(
                                                    vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                    << 0x20U) | (0xfffffffffffffffeULL 
                                                 & (QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                 | (QData)((IData)(
                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                      & (1U 
                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                     & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                    & (0U 
                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                        >> 0x19U)) | (0xffffff80U & 
                                      ((IData)((((0x7ffffffffeULL 
                                                  & (((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                                      << 0x20U) 
                                                     | (0xfffffffffffffffeULL 
                                                        & (QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                                 | (QData)((IData)(
                                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                      & (1U 
                                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                     & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                                    & (0U 
                                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                                >> 0x20U)) 
                                       << 7U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
        = (IData)(((0x7ffffffffeULL & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                        << 0x20U) | 
                                       (0xfffffffffffffffeULL 
                                        & (QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                   | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                        & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                       & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                      & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0xffffff80U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (IData)((((0x7ffffffffeULL & (((QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                            << 0x20U) 
                                           | (0xfffffffffffffffeULL 
                                              & (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                       | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                            & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                           & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                          & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                      >> 0x20U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | (0xfe000000U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                           << 0x20U) 
                                          | (0xfffffffffffffffeULL 
                                             & (QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                         & (2U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
        = ((0x1ffffffU & ((IData)(((0x7ffffffffeULL 
                                    & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                        << 0x20U) | 
                                       (0xfffffffffffffffeULL 
                                        & (QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                   | (QData)((IData)(
                                                     ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                        & (1U 
                                                           != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                       & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                      & (2U 
                                                         == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                          >> 7U)) | (0xfe000000U & 
                                     ((IData)((((0x7ffffffffeULL 
                                                 & (((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                                     << 0x20U) 
                                                    | (0xfffffffffffffffeULL 
                                                       & (QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                                | (QData)((IData)(
                                                                  ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                     & (1U 
                                                                        != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                    & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                                   & (2U 
                                                                      == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                               >> 0x20U)) 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0x3fffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | (0xffffc000U & ((0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 0xdU)) 
                             | (0x4000U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                           << 0xeU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
        = ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                   << 0xdU)) | (0x1fffU 
                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x13U)))) 
           | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                         << 0xdU)) 
                             | (0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                               << 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                      << 0xdU)) | (0x1fffU 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                      >> 0x13U)))) 
              | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                            << 0xdU)) 
                                | (0x1ff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                                 << 0xdU))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (0xffffff80U & ((0xffffff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 6U)) 
                             | (0x80U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                         << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
        = ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                               << 6U)) | (0x3fU & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x1aU)))) 
           | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                       << 6U)) | (0xffffff00U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                     << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0xfffc0000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                  << 6U)) | (0x3fU 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                >> 0x1aU)))) 
              | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                          << 6U)) | 
                                (0x3ff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 6U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
        = ((0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                            << 0x1fU) | (0x7ffffffeU 
                                         & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                            >> 1U)))) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                    >> 2U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                  >> 1U)) | (0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 0x1fU) 
                                            | (0x7ffffffeU 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                  >> 1U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                  >> 1U)) | (0x7feU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                       >> 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | (0xffffff80U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                           << 0x39U) 
                                          | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                              << 0x19U) 
                                             | (0x1fffffffffffffeULL 
                                                & ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                   >> 7U))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                             >> 7U)) 
                                                         & (0U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0xffffc000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | ((0x7fU & ((IData)(((0x7ffffffffeULL & 
                                  (((QData)((IData)(
                                                    vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                    << 0x39U) | (((QData)((IData)(
                                                                  vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                                  << 0x19U) 
                                                 | (0x1fffffffffffffeULL 
                                                    & ((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                       >> 7U))))) 
                                 | (QData)((IData)(
                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                      & (1U 
                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                     & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                        >> 7U)) 
                                                    & (0U 
                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                        >> 0x19U)) | (0xffffff80U & 
                                      ((IData)((((0x7ffffffffeULL 
                                                  & (((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                                      << 0x39U) 
                                                     | (((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                                         << 0x19U) 
                                                        | (0x1fffffffffffffeULL 
                                                           & ((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                              >> 7U))))) 
                                                 | (QData)((IData)(
                                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                      & (1U 
                                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                     & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                                        >> 7U)) 
                                                                    & (0U 
                                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                                >> 0x20U)) 
                                       << 7U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
        = (IData)(((0x7ffffffffeULL & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                        << 0x39U) | 
                                       (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                         << 0x19U) 
                                        | (0x1fffffffffffffeULL 
                                           & ((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                              >> 7U))))) 
                   | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                        & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                       & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                          >> 7U)) & 
                                      (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0xffffff80U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (IData)((((0x7ffffffffeULL & (((QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                            << 0x39U) 
                                           | (((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                               << 0x19U) 
                                              | (0x1fffffffffffffeULL 
                                                 & ((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                    >> 7U))))) 
                       | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                            & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                           & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                              >> 7U)) 
                                          & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                      >> 0x20U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | (0xfe000000U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                           << 0x39U) 
                                          | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                              << 0x19U) 
                                             | (0x1fffffffffffffeULL 
                                                & ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                   >> 7U))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                             >> 7U)) 
                                                         & (2U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
        = ((0x1ffffffU & ((IData)(((0x7ffffffffeULL 
                                    & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                        << 0x39U) | 
                                       (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                         << 0x19U) 
                                        | (0x1fffffffffffffeULL 
                                           & ((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                              >> 7U))))) 
                                   | (QData)((IData)(
                                                     ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                        & (1U 
                                                           != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                       & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                          >> 7U)) 
                                                      & (2U 
                                                         == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                          >> 7U)) | (0xfe000000U & 
                                     ((IData)((((0x7ffffffffeULL 
                                                 & (((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                                     << 0x39U) 
                                                    | (((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                                        << 0x19U) 
                                                       | (0x1fffffffffffffeULL 
                                                          & ((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                             >> 7U))))) 
                                                | (QData)((IData)(
                                                                  ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                     & (1U 
                                                                        != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                    & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                                       >> 7U)) 
                                                                   & (2U 
                                                                      == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                               >> 0x20U)) 
                                      << 0x19U)));
}

VL_INLINE_OPT void Vtaiga_sim::_combo__TOP__10(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_combo__TOP__10\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*127:0*/ __Vtemp630[4];
    WData/*95:0*/ __Vtemp631[3];
    WData/*191:0*/ __Vtemp632[6];
    WData/*127:0*/ __Vtemp634[4];
    WData/*95:0*/ __Vtemp635[3];
    WData/*191:0*/ __Vtemp636[6];
    WData/*95:0*/ __Vtemp654[3];
    WData/*95:0*/ __Vtemp655[3];
    WData/*95:0*/ __Vtemp656[3];
    WData/*95:0*/ __Vtemp657[3];
    // Body
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst 
        = ((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst 
        = ((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
        = ((0U >= (1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out)))
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__unit_data_array
           [(1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out))]
            : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array
        [(1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))];
    vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress) 
            & (IData)(vlTOPp->ddr_axi_wready_taiga)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T 
        = ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->ddr_axi_rvalid_muir));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x6fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_ready) 
               & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop))) 
              << 4U));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
        = ((0xffffffe0U & ((IData)((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                                     << 0x20U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out)))) 
                           << 5U)) | (0x1fU & (IData)(
                                                      (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                       >> 3U))));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
        = ((0x1fU & ((IData)((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                               << 0x20U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out)))) 
                     >> 0x1bU)) | (0xffffffe0U & ((IData)(
                                                          ((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out))) 
                                                           >> 0x20U)) 
                                                  << 5U)));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
        = (0x1fU & ((IData)(((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                               << 0x20U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out))) 
                             >> 0x20U)) >> 0x1bU));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop = (
                                                   (((IData)(vlTOPp->taiga_sim__DOT__axi_arvalid) 
                                                     & (IData)(vlTOPp->ddr_axi_arready_taiga)) 
                                                    & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write))) 
                                                   | ((IData)(vlTOPp->taiga_sim__DOT__axi_awvalid) 
                                                      & (IData)(vlTOPp->ddr_axi_awready_taiga)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_dme_wr_0_ack 
        = ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
           & (IData)(vlTOPp->ddr_axi_bvalid_muir));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_4 
        = (((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
            & (IData)(vlTOPp->ddr_axi_wready_muir)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
        = ((0x7ffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U]) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
                             << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[1U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[2U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[3U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[4U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[5U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
        = ((0xfffff800U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U]) 
           | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U]);
    vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni 
        = (1U & (~ (IData)(vlTOPp->rst)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump 
        = (((0x1bU == (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
                                >> 2U))) | (0x19U == 
                                            (0x1fU 
                                             & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
                                                >> 2U)))) 
           | (0x18U == (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
                                 >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
        = ((0xffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data) 
           | (0xffff0000U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
        = ((0xffff0000U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data) 
           | (0xffffU & ((0x20U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                          ? (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data 
                             >> 0x10U) : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
        = ((0xffffff00U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data) 
           | (0xffU & ((0x10U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                        ? (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
                           >> 8U) : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) 
           & (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall 
        = (((0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid)) 
           & (~ (IData)((0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_unit_stall 
        = (((~ (IData)((0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                               & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready))))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid)) 
           & (0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[0U] 
        = ((0x3eU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [0U]) | (1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                             >> 2U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage)) 
                           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready))));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2 
        = VL_LTS_IQQ(1,33,33, (((QData)((IData)((1U 
                                                 & ((~ 
                                                     (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                      >> 4U)) 
                                                    & (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                       >> 4U))))) 
                                << 0x20U) | (QData)((IData)(
                                                            ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                              << 0x1bU) 
                                                             | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                                >> 5U))))), 
                     (((QData)((IData)((1U & ((~ (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                  >> 4U)) 
                                              & (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                 >> 4U))))) 
                       << 0x20U) | (QData)((IData)(
                                                   ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                       >> 5U))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_wrap_out 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_4) 
           & (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count)));
    __Vtemp630[3U] = ((0xffU & ((0xffU & ((IData)((QData)((IData)(
                                                                  ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                    << 0x1bU) 
                                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                      >> 5U))))) 
                                          >> 0x13U)) 
                                | ((IData)(((QData)((IData)(
                                                            (0x8000000U 
                                                             | ((0x70000U 
                                                                 & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                | (1U 
                                                                   & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]))))) 
                                            >> 0x20U)) 
                                   >> 0x18U))) | (0xffffff00U 
                                                  & ((0x1f00U 
                                                      & ((IData)((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                                << 0x1bU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                                >> 5U))))) 
                                                         >> 0x13U)) 
                                                     | (0xffffe000U 
                                                        & ((IData)(
                                                                   ((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                                << 0x1bU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                                >> 5U)))) 
                                                                    >> 0x20U)) 
                                                           << 0xdU)))));
    __Vtemp631[1U] = ((1U & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                         << 0xeU) 
                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                           >> 0x12U)))) 
                                       << 0x1fU) | (QData)((IData)(
                                                                   (0x200000U 
                                                                    | ((0x1c00U 
                                                                        & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                            << 0xdU) 
                                                                           | (0x1c00U 
                                                                              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U)))) 
                                                                       | (1U 
                                                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                             >> 0x13U)))))))) 
                             >> 0x1fU)) | (0xfffffffeU 
                                           & ((IData)(
                                                      ((((QData)((IData)(
                                                                         ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                           << 0xeU) 
                                                                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                             >> 0x12U)))) 
                                                         << 0x1fU) 
                                                        | (QData)((IData)(
                                                                          (0x200000U 
                                                                           | ((0x1c00U 
                                                                               & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                                << 0xdU) 
                                                                                | (0x1c00U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U)))) 
                                                                              | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U))))))) 
                                                       >> 0x20U)) 
                                              << 1U)));
    __Vtemp632[2U] = ((0xfffffffeU & ((IData)(((0xfffffffff0ULL 
                                                & (((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                                    << 0x28U) 
                                                   | (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                                       << 8U) 
                                                      | (0xfffffffff0ULL 
                                                         & ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                            >> 0x18U))))) 
                                               | (QData)((IData)(
                                                                 (8U 
                                                                  | (3U 
                                                                     & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                         << 8U) 
                                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                           >> 0x18U)))))))) 
                                      << 1U)) | (1U 
                                                 & ((IData)(
                                                            ((((QData)((IData)(
                                                                               ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                                << 0xeU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                                >> 0x12U)))) 
                                                               << 0x1fU) 
                                                              | (QData)((IData)(
                                                                                (0x200000U 
                                                                                | ((0x1c00U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                                << 0xdU) 
                                                                                | (0x1c00U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U)))) 
                                                                                | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U))))))) 
                                                             >> 0x20U)) 
                                                    >> 0x1fU)));
    __Vtemp632[3U] = ((1U & ((IData)(((0xfffffffff0ULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                           << 0x28U) 
                                          | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                              << 8U) 
                                             | (0xfffffffff0ULL 
                                                & ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                   >> 0x18U))))) 
                                      | (QData)((IData)(
                                                        (8U 
                                                         | (3U 
                                                            & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                << 8U) 
                                                               | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                  >> 0x18U)))))))) 
                             >> 0x1fU)) | (0xfffffffeU 
                                           & ((0xfffffe00U 
                                               & ((IData)((QData)((IData)(
                                                                          (0x8000000U 
                                                                           | ((0x70000U 
                                                                               & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                              | (1U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])))))) 
                                                  << 9U)) 
                                              | ((IData)(
                                                         (((0xfffffffff0ULL 
                                                            & (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                                                << 0x28U) 
                                                               | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                                                   << 8U) 
                                                                  | (0xfffffffff0ULL 
                                                                     & ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                                        >> 0x18U))))) 
                                                           | (QData)((IData)(
                                                                             (8U 
                                                                              | (3U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                                << 8U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                                >> 0x18U))))))) 
                                                          >> 0x20U)) 
                                                 << 1U))));
    __Vtemp632[4U] = ((1U & ((1U & ((IData)((QData)((IData)(
                                                            (0x8000000U 
                                                             | ((0x70000U 
                                                                 & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                | (1U 
                                                                   & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])))))) 
                                    >> 0x17U)) | ((IData)(
                                                          (((0xfffffffff0ULL 
                                                             & (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                                                 << 0x28U) 
                                                                | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                                                    << 8U) 
                                                                   | (0xfffffffff0ULL 
                                                                      & ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                                         >> 0x18U))))) 
                                                            | (QData)((IData)(
                                                                              (8U 
                                                                               | (3U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                                << 8U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                                >> 0x18U))))))) 
                                                           >> 0x20U)) 
                                                  >> 0x1fU))) 
                      | (0xfffffffeU & ((0x1feU & ((IData)((QData)((IData)(
                                                                           (0x8000000U 
                                                                            | ((0x70000U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                               | (1U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])))))) 
                                                   >> 0x17U)) 
                                        | (0xfffffe00U 
                                           & ((0xffffc000U 
                                               & ((IData)((QData)((IData)(
                                                                          ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                            << 0x1bU) 
                                                                           | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                              >> 5U))))) 
                                                  << 0xeU)) 
                                              | ((IData)(
                                                         ((QData)((IData)(
                                                                          (0x8000000U 
                                                                           | ((0x70000U 
                                                                               & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                              | (1U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]))))) 
                                                          >> 0x20U)) 
                                                 << 9U))))));
    __Vtemp632[5U] = ((1U & (((IData)((QData)((IData)(
                                                      ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                        << 0x1bU) 
                                                       | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                          >> 5U))))) 
                              >> 0x12U) | ((IData)(
                                                   ((QData)((IData)(
                                                                    (0x8000000U 
                                                                     | ((0x70000U 
                                                                         & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                        | (1U 
                                                                           & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]))))) 
                                                    >> 0x20U)) 
                                           >> 0x17U))) 
                      | (0xfffffffeU & (__Vtemp630[3U] 
                                        << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
        = ((0xfffffffeU & ((IData)((((QData)((IData)(
                                                     ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                       << 0xeU) 
                                                      | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                         >> 0x12U)))) 
                                     << 0x1fU) | (QData)((IData)(
                                                                 (0x200000U 
                                                                  | ((0x1c00U 
                                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                          << 0xdU) 
                                                                         | (0x1c00U 
                                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                               >> 0x13U)))) 
                                                                     | (1U 
                                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                           >> 0x13U)))))))) 
                           << 1U)) | (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                            >> 0x12U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
        = __Vtemp631[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
        = __Vtemp632[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
        = __Vtemp632[3U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[4U] 
        = __Vtemp632[4U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
        = __Vtemp632[5U];
    __Vtemp634[1U] = ((0xffffff00U & ((IData)((QData)((IData)(
                                                              (0x8000000U 
                                                               | ((0x70000U 
                                                                   & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                       << 0x15U) 
                                                                      | (0x1f0000U 
                                                                         & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                            >> 0xbU)))) 
                                                                  | (1U 
                                                                     & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                        >> 0xbU))))))) 
                                      << 8U)) | (IData)(
                                                        (((0xfffffffff0ULL 
                                                           & (((QData)((IData)(
                                                                               vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U])) 
                                                               << 0x3dU) 
                                                              | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U])) 
                                                                  << 0x1dU) 
                                                                 | (0x1ffffffffffffff0ULL 
                                                                    & ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU])) 
                                                                       >> 3U))))) 
                                                          | (QData)((IData)(
                                                                            (8U 
                                                                             | (3U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                                << 0x1dU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
                                                                                >> 3U))))))) 
                                                         >> 0x20U)));
    __Vtemp634[2U] = ((0xffU & ((IData)((QData)((IData)(
                                                        (0x8000000U 
                                                         | ((0x70000U 
                                                             & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                 << 0x15U) 
                                                                | (0x1f0000U 
                                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                      >> 0xbU)))) 
                                                            | (1U 
                                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                  >> 0xbU))))))) 
                                >> 0x18U)) | (0xffffff00U 
                                              & ((0xffffe000U 
                                                  & ((IData)((QData)((IData)(
                                                                             ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                               << 0x10U) 
                                                                              | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                >> 0x10U))))) 
                                                     << 0xdU)) 
                                                 | ((IData)(
                                                            ((QData)((IData)(
                                                                             (0x8000000U 
                                                                              | ((0x70000U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                << 0x15U) 
                                                                                | (0x1f0000U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                                >> 0xbU)))) 
                                                                                | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                                >> 0xbU)))))) 
                                                             >> 0x20U)) 
                                                    << 8U))));
    __Vtemp634[3U] = ((0xffU & ((0xffU & ((IData)((QData)((IData)(
                                                                  ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                    << 0x10U) 
                                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                      >> 0x10U))))) 
                                          >> 0x13U)) 
                                | ((IData)(((QData)((IData)(
                                                            (0x8000000U 
                                                             | ((0x70000U 
                                                                 & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                     << 0x15U) 
                                                                    | (0x1f0000U 
                                                                       & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                          >> 0xbU)))) 
                                                                | (1U 
                                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                      >> 0xbU)))))) 
                                            >> 0x20U)) 
                                   >> 0x18U))) | (0xffffff00U 
                                                  & ((0x1f00U 
                                                      & ((IData)((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                                << 0x10U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                >> 0x10U))))) 
                                                         >> 0x13U)) 
                                                     | (0xffffe000U 
                                                        & ((IData)(
                                                                   ((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                                << 0x10U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                >> 0x10U)))) 
                                                                    >> 0x20U)) 
                                                           << 0xdU)))));
    __Vtemp635[1U] = ((1U & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                         << 3U) 
                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                           >> 0x1dU)))) 
                                       << 0x1fU) | (QData)((IData)(
                                                                   (0x200000U 
                                                                    | ((0x1c00U 
                                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                           << 2U)) 
                                                                       | (1U 
                                                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                             >> 0x1eU)))))))) 
                             >> 0x1fU)) | (0xfffffffeU 
                                           & ((IData)(
                                                      ((((QData)((IData)(
                                                                         ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                                           << 3U) 
                                                                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                             >> 0x1dU)))) 
                                                         << 0x1fU) 
                                                        | (QData)((IData)(
                                                                          (0x200000U 
                                                                           | ((0x1c00U 
                                                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                                << 2U)) 
                                                                              | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                                >> 0x1eU))))))) 
                                                       >> 0x20U)) 
                                              << 1U)));
    __Vtemp636[2U] = ((0xfffffffeU & ((IData)(((0xfffffffff0ULL 
                                                & (((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U])) 
                                                    << 0x3dU) 
                                                   | (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U])) 
                                                       << 0x1dU) 
                                                      | (0x1ffffffffffffff0ULL 
                                                         & ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU])) 
                                                            >> 3U))))) 
                                               | (QData)((IData)(
                                                                 (8U 
                                                                  | (3U 
                                                                     & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                         << 0x1dU) 
                                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
                                                                           >> 3U)))))))) 
                                      << 1U)) | (1U 
                                                 & ((IData)(
                                                            ((((QData)((IData)(
                                                                               ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                                                << 3U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                                >> 0x1dU)))) 
                                                               << 0x1fU) 
                                                              | (QData)((IData)(
                                                                                (0x200000U 
                                                                                | ((0x1c00U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                                << 2U)) 
                                                                                | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                                >> 0x1eU))))))) 
                                                             >> 0x20U)) 
                                                    >> 0x1fU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
        = ((0xfffffffeU & ((IData)((((QData)((IData)(
                                                     ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                       << 3U) 
                                                      | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                         >> 0x1dU)))) 
                                     << 0x1fU) | (QData)((IData)(
                                                                 (0x200000U 
                                                                  | ((0x1c00U 
                                                                      & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                         << 2U)) 
                                                                     | (1U 
                                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                           >> 0x1eU)))))))) 
                           << 1U)) | (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                            >> 0x1dU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
        = __Vtemp635[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
        = __Vtemp636[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
        = ((1U & ((IData)(((0xfffffffff0ULL & (((QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U])) 
                                                << 0x3dU) 
                                               | (((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U])) 
                                                   << 0x1dU) 
                                                  | (0x1ffffffffffffff0ULL 
                                                     & ((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU])) 
                                                        >> 3U))))) 
                           | (QData)((IData)((8U | 
                                              (3U & 
                                               ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                 << 0x1dU) 
                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
                                                   >> 3U)))))))) 
                  >> 0x1fU)) | (0xfffffffeU & (__Vtemp634[1U] 
                                               << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[4U] 
        = ((1U & (__Vtemp634[1U] >> 0x1fU)) | (0xfffffffeU 
                                               & (__Vtemp634[2U] 
                                                  << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
        = ((1U & (__Vtemp634[2U] >> 0x1fU)) | (0xfffffffeU 
                                               & (__Vtemp634[3U] 
                                                  << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[1U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[2U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[3U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[3U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[4U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[4U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U]) 
           | (0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[5U]));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U]) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                             << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[6U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[7U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[8U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[9U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xaU] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xbU] 
        = ((0x1ffffffU & ((0x1fc0000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[5U] 
                                         >> 7U)) | 
                          (0x3ffffU & ((0x1ffc000U 
                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
                                           << 0xeU)) 
                                       | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
                                          >> 0x12U))))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xcU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xdU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xeU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xfU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x10U] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
        = ((0xfffff800U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U]) 
           | (0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                        >> 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
        = ((0x7ffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U]) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x12U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x13U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x14U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x15U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x16U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x17U] 
        = (0xfU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                              << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                         >> 0x19U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result) 
             & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid)) 
            & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
               >> 4U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst 
        = (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
           | ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result) 
                & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid)) 
               & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
                  >> 3U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][0U] 
        = ((0x100U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
            ? ((0x80U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data
                : ((0x40U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                    ? (0xffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)
                    : (0xffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)))
            : ((0x80U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data
                : ((0x40U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                    ? VL_EXTENDS_II(32,16, (0xffffU 
                                            & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data))
                    : VL_EXTENDS_II(32,8, (0xffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc 
        = ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold)) 
                 & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                        >> 2U)) | (0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                                          & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid) 
           & (0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to 
        = (((- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready));
    if (((((((((1U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U])) 
               | (0U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
              | (4U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
             | (0xcU == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
            | (8U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
           | (0x10U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
          | (0x14U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
         | (0x18U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U])))) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result 
            = ((1U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                ? ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                 >> 5U)) : ((0U == 
                                             (0x1fU 
                                              & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                             ? (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                  << 0x1bU) 
                                                 | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                    >> 5U)) 
                                                + (
                                                   (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                      >> 5U)))
                                             : ((4U 
                                                 == 
                                                 (0x1fU 
                                                  & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                 ? 
                                                (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                     >> 5U)) 
                                                 ^ 
                                                 ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                     >> 5U)))
                                                 : 
                                                ((0xcU 
                                                  == 
                                                  (0x1fU 
                                                   & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                  ? 
                                                 (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                      >> 5U)) 
                                                  & ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                        >> 5U)))
                                                  : 
                                                 ((8U 
                                                   == 
                                                   (0x1fU 
                                                    & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                   ? 
                                                  (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       >> 5U)) 
                                                   | ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                         >> 5U)))
                                                   : 
                                                  ((0x10U 
                                                    == 
                                                    (0x1fU 
                                                     & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                    ? 
                                                   ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                                                     ? 
                                                    ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                        >> 5U))
                                                     : 
                                                    ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                        >> 5U)))
                                                    : 
                                                   ((0x14U 
                                                     == 
                                                     (0x1fU 
                                                      & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                     ? 
                                                    ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                                                      ? 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                         >> 5U))
                                                      : 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                         >> 5U)))
                                                     : 
                                                    ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                                                      ? 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                         >> 5U))
                                                      : 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                         >> 5U))))))))));
    } else {
        if ((0x1cU == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result 
                = ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                    ? ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                        << 0x1bU) | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                     >> 5U)) : ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                 << 0x1bU) 
                                                | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                   >> 5U)));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 0U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp 
        = ((0x100U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                      << 8U)) | (0xffU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
                                           << 8U) | 
                                          (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
                                           >> 0x18U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop = 0U;
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                     >> 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
            >> 1U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
            >> 9U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 0U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp 
        = ((0x100U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                      << 8U)) | (0xffU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
                                           << 8U) | 
                                          (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
                                           >> 0x18U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop = 0U;
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                     >> 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
            >> 1U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
            >> 9U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched 
        = ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
             & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_branch)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override)
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc
            : ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush)
                ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                                 >> 5U)) : (((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
                                             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)))
                                             ? ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_return)
                                                 ? 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram
                                                [vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index]
                                                 : 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc
                                                [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way])
                                             : ((IData)(4U) 
                                                + vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.push 
        = ((((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
               & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                  [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                  >> 2U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush))) 
             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
            | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst 
        = (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_pc_id_base 
        = (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)
                  ? ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U])
                      ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                          << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                       >> 3U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id))
                  : ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)
                      ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id)
                      : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = (0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j = 0U;
    {
        while (VL_GTS_III(1,32,32, 0U, vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) {
            if (((5U >= (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) 
                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                    [0U] >> (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[0U] 
                    = (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
                goto __Vlabel1;
            }
            vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j 
                = ((IData)(1U) + vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
        }
        __Vlabel1: ;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = ((0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [0U]) | ((QData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                       [0U]) & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                                                [0U] 
                                                >> 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                [0U])))) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = ((0x1ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [0U]) | ((QData)((IData)(((0x11U >= (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [0U])))
                                       ? (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
                                                [0U] 
                                                >> 
                                                (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [0U]))))
                                       : 0U))) << 0x21U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = ((0xf00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [0U]) | (IData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                      [0U]) ? vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd
                                     [0U][vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                     [0U]] : 0U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                         [0U] >> 0x20U)));
    if (VL_LIKELY((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                   [0U]))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[0U] 
            = (((~ ((IData)(1U) << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                    [0U])) & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                [0U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1) 
                         << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                         [0U]));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = (0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
           [1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[1U] = 4U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j = 0U;
    {
        while (VL_GTS_III(1,32,32, 4U, vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) {
            if (((5U >= (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) 
                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                    [1U] >> (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[1U] 
                    = (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
                goto __Vlabel2;
            }
            vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j 
                = ((IData)(1U) + vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
        }
        __Vlabel2: ;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = ((0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [1U]) | ((QData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                       [1U]) & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                                                [1U] 
                                                >> 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                [1U])))) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = ((0x1ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [1U]) | ((QData)((IData)(((0x11U >= (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [1U])))
                                       ? (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
                                                [1U] 
                                                >> 
                                                (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [1U]))))
                                       : 0U))) << 0x21U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = ((0xf00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [1U]) | (IData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                      [1U]) ? vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd
                                     [1U][vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                     [1U]] : 0U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                         [1U] >> 0x20U)));
    if (VL_LIKELY((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                   [1U]))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[1U] 
            = (((~ ((IData)(1U) << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                    [1U])) & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1) 
                         << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                         [1U]));
    }
    VL_EXTEND_WI(71,8, __Vtemp654, (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask));
    VL_SHIFTL_WWI(71,71,6, __Vtemp655, __Vtemp654, 
                  (0x38U & ((IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                     >> 3U)) << 3U)));
    __Vtemp656[0U] = __Vtemp655[0U];
    __Vtemp656[1U] = __Vtemp655[1U];
    __Vtemp656[2U] = (0x7fU & __Vtemp655[2U]);
    VL_EXTEND_WW(72,71, __Vtemp657, __Vtemp656);
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] = 0xffffffffU;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] = 0xffffffffU;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[2U] = 0xffU;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_0;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_1;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_2;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_4;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_5;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_6;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_7;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_8;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_9;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_10;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_11;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_12;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_13;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
            = (IData)((((QData)((IData)(vlTOPp->ddr_axi_rdata_muir)) 
                        << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
            = (IData)(((((QData)((IData)(vlTOPp->ddr_axi_rdata_muir)) 
                         << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))) 
                       >> 0x20U));
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            = __Vtemp657[0U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            = __Vtemp657[1U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[2U] 
            = __Vtemp657[2U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
            >> 2U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
              >> 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_flush_required 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
            >> 5U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__potential_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__potential_branch_exception 
        = (1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc 
                  >> 1U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                            >> 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
        = ((0xffffffc7U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           | (0xfffffff8U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_csr_r) 
                              << 5U) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_fence) 
                                         << 4U) | (0x3ffffff8U 
                                                   & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                                       >> 2U) 
                                                      & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ifence_r) 
                                                         << 3U)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
        = (((QData)((IData)(((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                >> 1U)))) << 0x28U) 
           | (((QData)((IData)(((0x1000U & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U])
                                 ? 6U : 4U))) << 0x23U) 
              | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address)) 
                  << 3U) | (QData)((IData)((7U & ((
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                     >> 3U))))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                   >> 1U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr))) 
                 & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                        >> 0x1fU)) | ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
                                      & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.new_request) 
                                         | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__mmu_request_complete))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                = (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o));
        } else {
            if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                    = (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q) 
                                 - (IData)(1U)));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp;
        if ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
                = (((~ ((IData)(0x1ffU) << (0xfU & 
                                            ((IData)(9U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1) 
                      << (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)))));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
        if ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1) 
                      << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
        = ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q) 
              >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                = (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o));
        } else {
            if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                    = (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q) 
                                 - (IData)(1U)));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp;
        if ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
                = (((~ ((IData)(0x1ffU) << (0xfU & 
                                            ((IData)(9U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1) 
                      << (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)))));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
        if ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1) 
                      << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
        = ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q) 
              >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__new_index 
        = (7U & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
                    & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
                       >> 3U)) ? vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram
                   [vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__read_index]
                    : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index)) 
                  + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.push)) 
                 - ((((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
                        & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_return)) 
                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush))) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override))) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request)) 
                    & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count) 
                  >> 3U) & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                            [1U] >> 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance 
        = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                  [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                           [1U] >> 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_105 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen)) 
           & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
              | (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid 
        = ((((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd)) 
           & (0U != (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xaU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
              >> 9U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[0U] 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd) 
            & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
               >> 9U)) & (0U != (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                           << 0x16U) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                             >> 0xaU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state;
    if ((0U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 1U;
    } else {
        if ((1U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 2U;
        } else {
            if ((2U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
                                   >> 0x3fU)))) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 3U;
                }
            } else {
                if ((3U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                    if ((1U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                         >> 0x28U)) 
                                | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__potential_branch_exception)) 
                               | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__system_op_or_exception_complete)))) {
                        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 5U;
                    }
                } else {
                    if ((4U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                        if ((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter)) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 3U;
                        }
                    } else {
                        if ((5U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                            if (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_is_idle) {
                                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 3U;
                            }
                        } else {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 0U;
                        }
                    }
                }
            }
        }
    }
    if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                       >> 0x28U)))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
            = ((0x107ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
               | ((QData)((IData)((0x1fU & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                                    >> 0x23U))))) 
                  << 0x23U));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
            = ((0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
               | ((QData)((IData)((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                           >> 3U)))) 
                  << 3U));
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__illegal_instruction) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                = (0x1000000000ULL | (0x107ffffffffULL 
                                      & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception));
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                = ((0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                   | ((QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U] 
                                        << 0x1aU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
                                        >> 6U)))) << 3U));
        } else {
            if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                               >> 0x28U)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                    = ((0x107ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                       | ((QData)((IData)((0x1fU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                                            >> 0x23U))))) 
                          << 0x23U));
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                    = ((0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                       | ((QData)((IData)((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                                   >> 3U)))) 
                          << 3U));
            } else {
                if ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U])) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = ((0x107ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                           | ((QData)((IData)(((0U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                ? 8U
                                                : (
                                                   (1U 
                                                    == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                    ? 9U
                                                    : 
                                                   ((3U 
                                                     == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                     ? 0xbU
                                                     : 8U))))) 
                              << 0x23U));
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = (0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception);
                } else {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = (0x1800000000ULL | (0x107ffffffffULL 
                                              & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception));
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = (0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception);
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
        = ((0xffffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
           | ((QData)((IData)((1U & ((((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                         >> 5U) & (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                                    >> 2U) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                                      >> 1U))) 
                                       | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                                  >> 0x28U))) 
                                      | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                                 >> 0x28U))) 
                                     | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__illegal_instruction))))) 
              << 0x28U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.push 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
              >> 0xdU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
              >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue) 
           & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                 >> 0xcU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x37ffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x80000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x37ffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x80000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance 
        = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                  [0U]) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance)));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet
        [1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rd_addr 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid)
            ? (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.data_out)
            : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid) 
            | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot) 
           | ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot) 
           & (- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
               & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot))) 
              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                 == (0xfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out) 
              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                  + (1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting))) 
                 - (1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0xff8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (0xfffffffeU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                              & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot) 
                                     >> 1U)) << 1U)) 
                             & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                                 == (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes) 
                                             >> 4U))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | (0xfffffffeU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | (((IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                        >> 1U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                   >> 3U) + (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting) 
                                   >> 1U))) - (1U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed) 
                                                >> 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0xfc7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1) 
              << 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (0xfffffffcU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                              & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot) 
                                     >> 2U)) << 2U)) 
                             & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                                 == (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes) 
                                             >> 8U))) 
                                << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | (0xfffffffcU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request) 
                                << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | (((IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                        >> 2U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
              << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                   >> 6U) + (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting) 
                                   >> 2U))) - (1U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed) 
                                                >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0xe3fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1) 
              << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (0xfffffff8U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                              & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot) 
                                     >> 3U)) << 3U)) 
                             & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                                 == (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes) 
                                             >> 0xcU))) 
                                << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | (0xfffffff8U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request) 
                                << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | (((IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                        >> 3U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
              << 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                   >> 9U) + (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting) 
                                   >> 3U))) - (1U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed) 
                                                >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0x1ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1) 
              << 9U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop)) 
          & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
            = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o) 
                  << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U] 
        = ((0xffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U]) 
           | (0xffff0000U & ((IData)((((QData)((IData)(
                                                       ((0x3c00U 
                                                         & ((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x2aU)) 
                                                            << 0xaU)) 
                                                        | ((0xcU 
                                                            & ((IData)(
                                                                       (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                        >> 0x27U)) 
                                                               << 2U)) 
                                                           | (1U 
                                                              & (IData)(
                                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                         >> 0x25U))))))) 
                                       << 0x2aU) | 
                                      (((QData)((IData)(
                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                         >> 4U))) 
                                        << 4U) | (QData)((IData)(
                                                                 (0xcU 
                                                                  & ((IData)(
                                                                             (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                              >> 2U)) 
                                                                     << 2U))))))) 
                             << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[4U] 
        = ((0xffffU & ((IData)((((QData)((IData)(((0x3c00U 
                                                   & ((IData)(
                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                               >> 0x2aU)) 
                                                      << 0xaU)) 
                                                  | ((0xcU 
                                                      & ((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 0x27U)) 
                                                         << 2U)) 
                                                     | (1U 
                                                        & (IData)(
                                                                  (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                   >> 0x25U))))))) 
                                 << 0x2aU) | (((QData)((IData)(
                                                               (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                >> 4U))) 
                                               << 4U) 
                                              | (QData)((IData)(
                                                                (0xcU 
                                                                 & ((IData)(
                                                                            (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                             >> 2U)) 
                                                                    << 2U))))))) 
                       >> 0x10U)) | (0xffff0000U & 
                                     ((IData)(((((QData)((IData)(
                                                                 ((0x3c00U 
                                                                   & ((IData)(
                                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                               >> 0x2aU)) 
                                                                      << 0xaU)) 
                                                                  | ((0xcU 
                                                                      & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x27U)) 
                                                                         << 2U)) 
                                                                     | (1U 
                                                                        & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x25U))))))) 
                                                 << 0x2aU) 
                                                | (((QData)((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 4U))) 
                                                    << 4U) 
                                                   | (QData)((IData)(
                                                                     (0xcU 
                                                                      & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 2U)) 
                                                                         << 2U)))))) 
                                               >> 0x20U)) 
                                      << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U] 
        = ((0xffffff00U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U]) 
           | (0xffffU & ((IData)(((((QData)((IData)(
                                                    ((0x3c00U 
                                                      & ((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 0x2aU)) 
                                                         << 0xaU)) 
                                                     | ((0xcU 
                                                         & ((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x27U)) 
                                                            << 2U)) 
                                                        | (1U 
                                                           & (IData)(
                                                                     (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                      >> 0x25U))))))) 
                                    << 0x2aU) | (((QData)((IData)(
                                                                  (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                   >> 4U))) 
                                                  << 4U) 
                                                 | (QData)((IData)(
                                                                   (0xcU 
                                                                    & ((IData)(
                                                                               (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 2U)) 
                                                                       << 2U)))))) 
                                  >> 0x20U)) >> 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop)) 
          & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
            = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o) 
                  << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U] 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U]) 
           | (0xff000000U & ((IData)((((QData)((IData)(
                                                       ((0x3c00U 
                                                         & ((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x2aU)) 
                                                            << 0xaU)) 
                                                        | ((0xcU 
                                                            & ((IData)(
                                                                       (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                        >> 0x27U)) 
                                                               << 2U)) 
                                                           | (1U 
                                                              & (IData)(
                                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                         >> 0x25U))))))) 
                                       << 0x2aU) | 
                                      (((QData)((IData)(
                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                         >> 4U))) 
                                        << 4U) | (QData)((IData)(
                                                                 (0xcU 
                                                                  & ((IData)(
                                                                             (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                              >> 2U)) 
                                                                     << 2U))))))) 
                             << 0x18U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[9U] 
        = ((0xffffffU & ((IData)((((QData)((IData)(
                                                   ((0x3c00U 
                                                     & ((IData)(
                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                 >> 0x2aU)) 
                                                        << 0xaU)) 
                                                    | ((0xcU 
                                                        & ((IData)(
                                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                    >> 0x27U)) 
                                                           << 2U)) 
                                                       | (1U 
                                                          & (IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x25U))))))) 
                                   << 0x2aU) | (((QData)((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 4U))) 
                                                 << 4U) 
                                                | (QData)((IData)(
                                                                  (0xcU 
                                                                   & ((IData)(
                                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                               >> 2U)) 
                                                                      << 2U))))))) 
                         >> 8U)) | (0xff000000U & ((IData)(
                                                           ((((QData)((IData)(
                                                                              ((0x3c00U 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x2aU)) 
                                                                                << 0xaU)) 
                                                                               | ((0xcU 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x27U)) 
                                                                                << 2U)) 
                                                                                | (1U 
                                                                                & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x25U))))))) 
                                                              << 0x2aU) 
                                                             | (((QData)((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 4U))) 
                                                                 << 4U) 
                                                                | (QData)((IData)(
                                                                                (0xcU 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 2U)) 
                                                                                << 2U)))))) 
                                                            >> 0x20U)) 
                                                   << 0x18U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[0xaU] 
        = (0xffffffU & ((IData)(((((QData)((IData)(
                                                   ((0x3c00U 
                                                     & ((IData)(
                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                 >> 0x2aU)) 
                                                        << 0xaU)) 
                                                    | ((0xcU 
                                                        & ((IData)(
                                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                    >> 0x27U)) 
                                                           << 2U)) 
                                                       | (1U 
                                                          & (IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x25U))))))) 
                                   << 0x2aU) | (((QData)((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 4U))) 
                                                 << 4U) 
                                                | (QData)((IData)(
                                                                  (0xcU 
                                                                   & ((IData)(
                                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                               >> 2U)) 
                                                                      << 2U)))))) 
                                 >> 0x20U)) >> 8U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[1U] 
        = (7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                         [1U] >> 0x21U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[1U] 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                         [1U] >> 0x20U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x7fffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | ((QData)((IData)((7U & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                                    [0U] 
                                                    >> 0x21U))))) 
                     << 0x27U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x3bfffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | ((QData)((IData)(((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                               [0U] 
                                               >> 0x20U)) 
                                      & (0U != (0x3fU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
                                                           [0U] 
                                                           >> 0x20U))))))) 
                     << 0x26U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x3ff00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                    [0U])));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x7fffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | ((QData)((IData)((7U & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                                    [1U] 
                                                    >> 0x21U))))) 
                     << 0x27U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x3bfffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | ((QData)((IData)(((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                               [1U] 
                                               >> 0x20U)) 
                                      & (0U != (0x3fU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
                                                           [1U] 
                                                           >> 0x20U))))))) 
                     << 0x26U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x3ff00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                    [1U])));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x3c0ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | ((QData)((IData)((0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                << 0x16U) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                  >> 0xaU))))) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x3c0ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | ((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table
                                     [(7U & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                                     [1U] 
                                                     >> 0x21U)))])) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] : vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[1U] 
        = (0x3fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                            [1U] >> 0x20U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[1U] 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                         [1U] >> 0x26U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] : vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U]);
}

VL_INLINE_OPT void Vtaiga_sim::_sequent__TOP__12(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_sequent__TOP__12\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[0U] 
        = ((0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                            << 7U) | (0x7cU & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                               >> 0x19U)))) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[1U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                  >> 0x19U)) | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                                                << 7U) 
                                               | (0x7cU 
                                                  & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                                                     >> 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[2U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                  >> 0x19U)) | (0x3cU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U] 
                                          << 7U) | 
                                         (0x7cU & (
                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                                                   >> 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[0U] 
        = ((0xfffffffcU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[1U] 
        = ((3U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U]) 
           | (0xfffffffcU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U]));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[2U] 
        = ((3U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0x3cU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[0U] 
        = ((0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                            << 0x1aU) | (0x3fffffcU 
                                         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U] 
                                            >> 6U)))) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[1U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                  >> 6U)) | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
                                             << 0x1aU) 
                                            | (0x3fffffcU 
                                               & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                                                  >> 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[2U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
                  >> 6U)) | (0xffcU & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
                                       >> 6U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[0U] 
        = ((0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                            << 0x13U) | (0x7fffcU & 
                                         (vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
                                          >> 0xdU)))) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[1U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                  >> 0xdU)) | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                               << 0x13U) 
                                              | (0x7fffcU 
                                                 & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                                                    >> 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[2U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                  >> 0xdU)) | (0xffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                                          << 0x13U) 
                                         | (0x7fffcU 
                                            & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                               >> 0xdU)))));
}

void Vtaiga_sim::_eval(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_eval\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__4(vlSymsp);
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
        vlTOPp->_sequent__TOP__6(vlSymsp);
        vlTOPp->_sequent__TOP__7(vlSymsp);
    }
    if ((((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk))) 
         | ((~ (IData)(vlTOPp->__VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni)) 
            & (IData)(vlTOPp->__Vclklast__TOP____VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni)))) {
        vlTOPp->_sequent__TOP__8(vlSymsp);
        vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux._sequent__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__3(vlSymsp);
        vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux._sequent__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__3(vlSymsp);
    }
    vlTOPp->_combo__TOP__10(vlSymsp);
    vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux._combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__5(vlSymsp);
    vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux._combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux__6(vlSymsp);
    vlTOPp->_settle__TOP__2(vlSymsp);
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
        vlTOPp->_sequent__TOP__12(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni 
        = vlTOPp->__VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni;
    vlTOPp->__VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni 
        = vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni;
}

VL_INLINE_OPT QData Vtaiga_sim::_change_request(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_change_request\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    return (vlTOPp->_change_request_1(vlSymsp));
}

VL_INLINE_OPT QData Vtaiga_sim::_change_request_1(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_change_request_1\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    __req |= ((vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni)
         | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8])|| (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[10] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[10]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[11] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[11]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[12] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[12]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[13] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[13]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[14] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[14]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[15] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[15]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[16] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[16]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[17] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[17])
         | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0])|| (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[10] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[10])|| (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[11] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[11]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[12] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[12]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[13] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[13]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[14] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[14]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[15] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[15]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[16] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[16]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[17] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[17])
         | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo__pop));
    VL_DEBUG_IF( if(__req && ((vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni))) VL_DBG_MSGF("        CHANGE: /localssd/taiga-project/taiga/test_benches/verilator/axi_xbar/axi_src/axi_lite_xbar.sv:36: taiga_sim.__Vcellinp__taiga_axi_bus__rst_ni\n"); );
    VL_DEBUG_IF( if(__req && ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9])|| (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[10] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[10]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[11] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[11]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[12] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[12]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[13] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[13]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[14] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[14]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[15] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[15]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[16] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[16]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[17] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[17]))) VL_DBG_MSGF("        CHANGE: /localssd/taiga-project/taiga/test_benches/verilator/axi_xbar/axi_src/axi_lite_demux.sv:49: taiga_sim.taiga_axi_bus.__Vcellout__gen_slv_port_demux[0].i_axi_lite_demux__mst_reqs_o\n"); );
    VL_DEBUG_IF( if(__req && ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9])|| (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[10] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[10]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[11] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[11]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[12] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[12]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[13] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[13]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[14] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[14]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[15] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[15]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[16] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[16]) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[17] ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[17]))) VL_DBG_MSGF("        CHANGE: /localssd/taiga-project/taiga/test_benches/verilator/axi_xbar/axi_src/axi_lite_demux.sv:49: taiga_sim.taiga_axi_bus.__Vcellout__gen_slv_port_demux[1].i_axi_lite_demux__mst_reqs_o\n"); );
    VL_DEBUG_IF( if(__req && ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop ^ vlTOPp->__Vchglast__TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo__pop))) VL_DBG_MSGF("        CHANGE: /localssd/taiga-project/taiga/core/interfaces.sv:127: pop\n"); );
    // Final
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni 
        = vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni;
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U];
    vlTOPp->__Vchglast__TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo__pop 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop;
    return __req;
}

#ifdef VL_DEBUG
void Vtaiga_sim::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((clk & 0xfeU))) {
        Verilated::overWidthError("clk");}
    if (VL_UNLIKELY((rst & 0xfeU))) {
        Verilated::overWidthError("rst");}
    if (VL_UNLIKELY((ddr_axi_awready_muir & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_awready_muir");}
    if (VL_UNLIKELY((ddr_axi_wready_muir & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_wready_muir");}
    if (VL_UNLIKELY((ddr_axi_bvalid_muir & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_bvalid_muir");}
    if (VL_UNLIKELY((ddr_axi_bresp_muir & 0xfcU))) {
        Verilated::overWidthError("ddr_axi_bresp_muir");}
    if (VL_UNLIKELY((ddr_axi_arready_muir & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_arready_muir");}
    if (VL_UNLIKELY((ddr_axi_rvalid_muir & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_rvalid_muir");}
    if (VL_UNLIKELY((ddr_axi_rresp_muir & 0xfcU))) {
        Verilated::overWidthError("ddr_axi_rresp_muir");}
    if (VL_UNLIKELY((ddr_axi_rlast_muir & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_rlast_muir");}
    if (VL_UNLIKELY((ddr_axi_rid_muir & 0xc0U))) {
        Verilated::overWidthError("ddr_axi_rid_muir");}
    if (VL_UNLIKELY((ddr_axi_awready_taiga & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_awready_taiga");}
    if (VL_UNLIKELY((ddr_axi_wready_taiga & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_wready_taiga");}
    if (VL_UNLIKELY((ddr_axi_bvalid_taiga & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_bvalid_taiga");}
    if (VL_UNLIKELY((ddr_axi_bresp_taiga & 0xfcU))) {
        Verilated::overWidthError("ddr_axi_bresp_taiga");}
    if (VL_UNLIKELY((ddr_axi_arready_taiga & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_arready_taiga");}
    if (VL_UNLIKELY((ddr_axi_rvalid_taiga & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_rvalid_taiga");}
    if (VL_UNLIKELY((ddr_axi_rresp_taiga & 0xfcU))) {
        Verilated::overWidthError("ddr_axi_rresp_taiga");}
    if (VL_UNLIKELY((ddr_axi_rlast_taiga & 0xfeU))) {
        Verilated::overWidthError("ddr_axi_rlast_taiga");}
    if (VL_UNLIKELY((ddr_axi_rid_taiga & 0xc0U))) {
        Verilated::overWidthError("ddr_axi_rid_taiga");}
    if (VL_UNLIKELY((bus_axi_arready & 0xfeU))) {
        Verilated::overWidthError("bus_axi_arready");}
    if (VL_UNLIKELY((bus_axi_awready & 0xfeU))) {
        Verilated::overWidthError("bus_axi_awready");}
    if (VL_UNLIKELY((bus_axi_bresp & 0xfcU))) {
        Verilated::overWidthError("bus_axi_bresp");}
    if (VL_UNLIKELY((bus_axi_bvalid & 0xfeU))) {
        Verilated::overWidthError("bus_axi_bvalid");}
    if (VL_UNLIKELY((bus_axi_rresp & 0xfcU))) {
        Verilated::overWidthError("bus_axi_rresp");}
    if (VL_UNLIKELY((bus_axi_rvalid & 0xfeU))) {
        Verilated::overWidthError("bus_axi_rvalid");}
    if (VL_UNLIKELY((bus_axi_wready & 0xfeU))) {
        Verilated::overWidthError("bus_axi_wready");}
    if (VL_UNLIKELY((addr & 0xc0000000U))) {
        Verilated::overWidthError("addr");}
    if (VL_UNLIKELY((be & 0xf0U))) {
        Verilated::overWidthError("be");}
    if (VL_UNLIKELY((rnw & 0xfeU))) {
        Verilated::overWidthError("rnw");}
    if (VL_UNLIKELY((is_amo & 0xfeU))) {
        Verilated::overWidthError("is_amo");}
    if (VL_UNLIKELY((amo_type_or_burst_size & 0xe0U))) {
        Verilated::overWidthError("amo_type_or_burst_size");}
    if (VL_UNLIKELY((sub_id & 0xfcU))) {
        Verilated::overWidthError("sub_id");}
    if (VL_UNLIKELY((request_push & 0xfeU))) {
        Verilated::overWidthError("request_push");}
    if (VL_UNLIKELY((inv_ack & 0xfeU))) {
        Verilated::overWidthError("inv_ack");}
    if (VL_UNLIKELY((wr_data_push & 0xfeU))) {
        Verilated::overWidthError("wr_data_push");}
    if (VL_UNLIKELY((rd_data_ack & 0xfeU))) {
        Verilated::overWidthError("rd_data_ack");}
}
#endif  // VL_DEBUG
