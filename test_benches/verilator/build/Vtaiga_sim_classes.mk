# Verilated -*- Makefile -*-
# DESCRIPTION: Verilator output: Make include file with class lists
#
# This file lists generated Verilated files, for including in higher level makefiles.
# See Vtaiga_sim.mk for the caller.

### Switches...
# C11 constructs required?  0/1 (always on now)
VM_C11 = 1
# Coverage output mode?  0/1 (from --coverage)
VM_COVERAGE = 0
# Parallel builds?  0/1 (from --output-split)
VM_PARALLEL_BUILDS = 1
# Threaded output mode?  0/1/N threads (from --threads)
VM_THREADS = 0
# Tracing output mode?  0/1 (from --trace/--trace-fst)
VM_TRACE = 0
# Tracing output mode in FST format?  0/1 (from --trace-fst)
VM_TRACE_FST = 0
# Tracing threaded output mode?  0/1/N threads (from --trace-thread)
VM_TRACE_THREADS = 0
# Separate FST writer thread? 0/1 (from --trace-fst with --trace-thread > 0)
VM_TRACE_FST_WRITER_THREAD = 0

### Object file lists...
# Generated module classes, fast-path, compile with highest optimization
VM_CLASSES_FAST += \
	Vtaiga_sim \
	Vtaiga_sim__1 \
	Vtaiga_sim__2 \
	Vtaiga_sim___024unit \
	Vtaiga_sim_axi_lite_mux__pi5 \
	Vtaiga_sim_l2_memory_interface \
	Vtaiga_sim_l2_arbitration_interface \
	Vtaiga_sim_l2_requester_interface \
	Vtaiga_sim_branch_predictor_interface \
	Vtaiga_sim_unit_writeback_interface \
	Vtaiga_sim_ras_interface \
	Vtaiga_sim_tlb_interface \
	Vtaiga_sim_renamer_interface \
	Vtaiga_sim_register_file_issue_interface \
	Vtaiga_sim_mmu_interface \
	Vtaiga_sim_ls_sub_unit_interface__pi8 \
	Vtaiga_sim_ls_sub_unit_interface__pi7 \
	Vtaiga_sim_fetch_sub_unit_interface \
	Vtaiga_sim_axi_interface \
	Vtaiga_sim_l1_arbiter_request_interface \
	Vtaiga_sim_l1_arbiter_return_interface \
	Vtaiga_sim_load_store_queue_interface \
	Vtaiga_sim_unsigned_division_interface \
	Vtaiga_sim_fifo_interface__D2a \
	Vtaiga_sim_fifo_interface__D47 \
	Vtaiga_sim_fifo_interface__D9 \
	Vtaiga_sim_fifo_interface__D6 \
	Vtaiga_sim_fifo_interface__D3 \
	Vtaiga_sim_fifo_interface__D5 \
	Vtaiga_sim_fifo_interface__D23 \
	Vtaiga_sim_fifo_interface__D7 \
	Vtaiga_sim_fifo_interface__D2c \
	Vtaiga_sim_fifo_interface__D22 \
	Vtaiga_sim_fifo_interface__D1e \
	Vtaiga_sim_fifo_interface__D20 \
	Vtaiga_sim_fifo_interface__D2b \

# Generated module classes, non-fast-path, compile with low/medium optimization
VM_CLASSES_SLOW += \
	Vtaiga_sim__Slow \
	Vtaiga_sim__1__Slow \
	Vtaiga_sim___024unit__Slow \
	Vtaiga_sim_axi_lite_mux__pi5__Slow \
	Vtaiga_sim_l2_memory_interface__Slow \
	Vtaiga_sim_l2_arbitration_interface__Slow \
	Vtaiga_sim_l2_requester_interface__Slow \
	Vtaiga_sim_branch_predictor_interface__Slow \
	Vtaiga_sim_unit_writeback_interface__Slow \
	Vtaiga_sim_ras_interface__Slow \
	Vtaiga_sim_tlb_interface__Slow \
	Vtaiga_sim_renamer_interface__Slow \
	Vtaiga_sim_register_file_issue_interface__Slow \
	Vtaiga_sim_mmu_interface__Slow \
	Vtaiga_sim_ls_sub_unit_interface__pi8__Slow \
	Vtaiga_sim_ls_sub_unit_interface__pi7__Slow \
	Vtaiga_sim_fetch_sub_unit_interface__Slow \
	Vtaiga_sim_axi_interface__Slow \
	Vtaiga_sim_l1_arbiter_request_interface__Slow \
	Vtaiga_sim_l1_arbiter_return_interface__Slow \
	Vtaiga_sim_load_store_queue_interface__Slow \
	Vtaiga_sim_unsigned_division_interface__Slow \
	Vtaiga_sim_fifo_interface__D2a__Slow \
	Vtaiga_sim_fifo_interface__D47__Slow \
	Vtaiga_sim_fifo_interface__D9__Slow \
	Vtaiga_sim_fifo_interface__D6__Slow \
	Vtaiga_sim_fifo_interface__D3__Slow \
	Vtaiga_sim_fifo_interface__D5__Slow \
	Vtaiga_sim_fifo_interface__D23__Slow \
	Vtaiga_sim_fifo_interface__D7__Slow \
	Vtaiga_sim_fifo_interface__D2c__Slow \
	Vtaiga_sim_fifo_interface__D22__Slow \
	Vtaiga_sim_fifo_interface__D1e__Slow \
	Vtaiga_sim_fifo_interface__D20__Slow \
	Vtaiga_sim_fifo_interface__D2b__Slow \

# Generated support classes, fast-path, compile with highest optimization
VM_SUPPORT_FAST += \

# Generated support classes, non-fast-path, compile with low/medium optimization
VM_SUPPORT_SLOW += \
	Vtaiga_sim__Syms \

# Global classes, need linked once per executable, fast-path, compile with highest optimization
VM_GLOBAL_FAST += \
	verilated \

# Global classes, need linked once per executable, non-fast-path, compile with low/medium optimization
VM_GLOBAL_SLOW += \


# Verilated -*- Makefile -*-
