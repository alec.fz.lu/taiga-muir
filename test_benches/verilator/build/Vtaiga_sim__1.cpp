// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim.h"
#include "Vtaiga_sim__Syms.h"

VL_INLINE_OPT void Vtaiga_sim::_sequent__TOP__7(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_sequent__TOP__7\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*255:0*/ __Vtemp352[8];
    WData/*255:0*/ __Vtemp353[8];
    WData/*255:0*/ __Vtemp355[8];
    WData/*255:0*/ __Vtemp356[8];
    WData/*255:0*/ __Vtemp359[8];
    WData/*255:0*/ __Vtemp360[8];
    WData/*255:0*/ __Vtemp362[8];
    WData/*255:0*/ __Vtemp364[8];
    WData/*95:0*/ __Vtemp365[3];
    WData/*95:0*/ __Vtemp379[3];
    WData/*95:0*/ __Vtemp380[3];
    WData/*95:0*/ __Vtemp383[3];
    WData/*95:0*/ __Vtemp384[3];
    WData/*95:0*/ __Vtemp387[3];
    WData/*95:0*/ __Vtemp388[3];
    WData/*95:0*/ __Vtemp391[3];
    WData/*95:0*/ __Vtemp392[3];
    WData/*95:0*/ __Vtemp395[3];
    WData/*95:0*/ __Vtemp396[3];
    WData/*95:0*/ __Vtemp399[3];
    WData/*95:0*/ __Vtemp400[3];
    WData/*95:0*/ __Vtemp403[3];
    WData/*95:0*/ __Vtemp404[3];
    WData/*95:0*/ __Vtemp407[3];
    WData/*95:0*/ __Vtemp408[3];
    WData/*95:0*/ __Vtemp411[3];
    WData/*95:0*/ __Vtemp412[3];
    WData/*95:0*/ __Vtemp415[3];
    WData/*95:0*/ __Vtemp416[3];
    WData/*95:0*/ __Vtemp419[3];
    WData/*95:0*/ __Vtemp420[3];
    WData/*95:0*/ __Vtemp423[3];
    WData/*95:0*/ __Vtemp424[3];
    WData/*255:0*/ __Vtemp499[8];
    WData/*255:0*/ __Vtemp500[8];
    WData/*95:0*/ __Vtemp518[3];
    // Body
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = 1U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = 0U;
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o = 0U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = 1U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = 0U;
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result 
        = (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid) 
            & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
               >> 2U)) & (~ ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
                             >> 1U)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state = 1U;
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state = 1U;
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state;
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3fff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (7U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out) 
                           >> 1U)));
    vlTOPp->ddr_axi_wlast_muir = (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_10 
        = (0xffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt)));
    vlTOPp->ddr_axi_awvalid_muir = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_11 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_12 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_13 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_17 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->ddr_axi_arvalid_taiga = vlTOPp->taiga_sim__DOT__axi_arvalid;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__delayed_abort_complete 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
            >> 2U) & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__delayed_abort_complete 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
            >> 2U) & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__state];
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_lr 
        = ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                    >> 7U)) & (2U == (0x1fU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                       >> 2U)))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store 
        = (1U & ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                             >> 8U))) | ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                  >> 7U)) 
                                         & (2U != (0x1fU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                              >> 2U)))))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc 
        = ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                    >> 7U)) & (3U == (0x1fU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                       >> 2U)))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                  >> 4U) & (~ (IData)((0U != (0xfU 
                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count)))))));
    vlTOPp->taiga_sim__DOT__axi_wvalid = (((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress) 
                                           & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                                              >> 4U)) 
                                          & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete)));
    vlTOPp->taiga_sim__DOT__axi_wlast = ((((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__on_last_burst) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress)) 
                                          & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                                             >> 4U)) 
                                         & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                  >> 4U) & (~ (IData)((0U != (0xfU 
                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count)))))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_write_ready = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_write_ready = 0U;
        } else {
            if ((((IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                           >> 8U)) & (IData)(vlTOPp->ddr_axi_rvalid_taiga)) 
                 & ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_count) 
                    == (3U & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                      >> 0xeU)))))) {
                vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_write_ready = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_3 
        = (0xfU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count)));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_mask;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_490 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_493 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_499 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_505 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_506 
        = (4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_507 
        = (5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_509 
        = (6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_r_ready 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate)) 
           & (6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406 
        = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & ((1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
              & ((2U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                 & (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R
                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29)
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                            : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R
                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29)
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data 
                                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R
                                    : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29)
                                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                                        : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R
                                : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29)
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                                    : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R));
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | ((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                             >> 0x21U))) == (7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | (((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                              >> 0x21U))) == (7U & 
                                              ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed) 
                                               >> 3U))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | (((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                              >> 0x21U))) == (7U & 
                                              ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed) 
                                               >> 6U))) 
              << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | (((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                              >> 0x21U))) == (7U & 
                                              ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed) 
                                               >> 9U))) 
              << 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__new_entry 
        = (((QData)((IData)((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))))) 
            << 0x23U) | (((QData)((IData)((0x7fffU 
                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                              >> 0x11U)))) 
                          << 0x14U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__immu.upper_physical_address))));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__pc 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc;
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__Vfuncout 
        = (0x1fffffU & (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__pc 
                        >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
           | ((0x3fffffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                            [0U] >> 5U)) == (0x200000U 
                                             | vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__Vfuncout)));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__pc 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc;
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__Vfuncout 
        = (0x1fffffU & (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__pc 
                        >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
           | (((0x3fffffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                             [1U] >> 5U)) == (0x200000U 
                                              | vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__Vfuncout)) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                    >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                    >> 0xcU));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.new_request 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress)) 
                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                    >> 0x1fU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr))
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb
           [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest]
            : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue
           [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xffffff00U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xffff00ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xff00U & (((1U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                >> 8U))))
                           ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment
                           : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                              >> 8U)) << 8U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xff00ffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xff0000U & (((2U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                  >> 8U))))
                             ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment
                             : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                                >> 0x10U)) << 0x10U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xff000000U & (((2U == (3U & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                    >> 8U))))
                               ? (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                                  >> 8U) : ((3U == 
                                             (3U & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                            >> 8U))))
                                             ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment
                                             : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                                                >> 0x18U))) 
                             << 0x18U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_10 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_62 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__value)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_10 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_62 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__value)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles = 0U;
        } else {
            if ((2U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_3;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_47;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_49;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_8));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_15) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_R_data = 1ULL;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_53;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_11)
                                        : ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29)) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_11))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_13) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_R_data 
                = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                    : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_0;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_1;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_2;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_4;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_5;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_6;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_7;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_8;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_9;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_10;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_11;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_12;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_13;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU] 
            = (IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_15)) 
                        << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU] 
            = (IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_15)) 
                         << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))) 
                       >> 0x20U));
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[1U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[1U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[2U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[2U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[3U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[3U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[4U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[4U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[5U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[5U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[6U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[6U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[7U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[7U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[8U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[8U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[9U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[9U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xaU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xaU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xbU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xbU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xcU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xcU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xdU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xdU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xeU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xeU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xfU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xfU]);
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_data 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag
        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0];
    if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_block_dirty) 
         & (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)))) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag___05FT_447_addr_pipe_0 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7___05FT_14_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7___05FT_24_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7___05FT_34_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7___05FT_44_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7___05FT_54_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7___05FT_64_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7___05FT_74_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7___05FT_84_addr_pipe_0 
        = (0xffU & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                    - (IData)(1U)));
    __Vtemp352[0U] = 1U;
    __Vtemp352[1U] = 0U;
    __Vtemp352[2U] = 0U;
    __Vtemp352[3U] = 0U;
    __Vtemp352[4U] = 0U;
    __Vtemp352[5U] = 0U;
    __Vtemp352[6U] = 0U;
    __Vtemp352[7U] = 0U;
    VL_SHIFTL_WWI(256,256,8, __Vtemp353, __Vtemp352, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[0U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[0U] 
           | __Vtemp353[0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[1U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[1U] 
           | __Vtemp353[1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[2U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[2U] 
           | __Vtemp353[2U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[3U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[3U] 
           | __Vtemp353[3U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[4U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[4U] 
           | __Vtemp353[4U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[5U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[5U] 
           | __Vtemp353[5U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[6U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[6U] 
           | __Vtemp353[6U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[7U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[7U] 
           | __Vtemp353[7U]);
    __Vtemp355[0U] = 1U;
    __Vtemp355[1U] = 0U;
    __Vtemp355[2U] = 0U;
    __Vtemp355[3U] = 0U;
    __Vtemp355[4U] = 0U;
    __Vtemp355[5U] = 0U;
    __Vtemp355[6U] = 0U;
    __Vtemp355[7U] = 0U;
    VL_SHIFTL_WWI(256,256,8, __Vtemp356, __Vtemp355, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[0U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U] 
           | __Vtemp356[0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[1U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U] 
           | __Vtemp356[1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[2U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U] 
           | __Vtemp356[2U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[3U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U] 
           | __Vtemp356[3U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[4U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U] 
           | __Vtemp356[4U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[5U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U] 
           | __Vtemp356[5U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[6U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U] 
           | __Vtemp356[6U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[7U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U] 
           | __Vtemp356[7U]);
    __Vtemp359[0U] = 1U;
    __Vtemp359[1U] = 0U;
    __Vtemp359[2U] = 0U;
    __Vtemp359[3U] = 0U;
    __Vtemp359[4U] = 0U;
    __Vtemp359[5U] = 0U;
    __Vtemp359[6U] = 0U;
    __Vtemp359[7U] = 0U;
    VL_SHIFTL_WWI(256,256,8, __Vtemp360, __Vtemp359, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[0U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U]) 
           | __Vtemp360[0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[1U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U]) 
           | __Vtemp360[1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[2U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U]) 
           | __Vtemp360[2U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[3U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U]) 
           | __Vtemp360[3U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[4U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U]) 
           | __Vtemp360[4U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[5U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U]) 
           | __Vtemp360[5U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[6U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U]) 
           | __Vtemp360[6U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[7U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U]) 
           | __Vtemp360[7U]);
    VL_SHIFTR_WWI(256,256,8, __Vtemp362, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[0U] 
        = __Vtemp362[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[1U] 
        = __Vtemp362[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[2U] 
        = __Vtemp362[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[3U] 
        = __Vtemp362[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[4U] 
        = __Vtemp362[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[5U] 
        = __Vtemp362[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[6U] 
        = __Vtemp362[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[7U] 
        = __Vtemp362[7U];
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_addr = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_addr 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid)
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data
                    : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid)
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data
                        : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle 
        = ((~ (IData)(vlTOPp->rst)) & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted_1 
           > vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted);
    if ((0x40U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr))) {
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b 
            = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                     >> 4U));
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor)
                ? vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted
                : (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                           >> 2U)));
    } else {
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b 
            = (1U & ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero)) 
                     & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                        >> 5U)));
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a 
            = ((- (IData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero))) 
               | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp);
    }
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__Vfuncout 
        = (((- (IData)((IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b))) 
            ^ vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a) 
           + (IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__.rd 
        = vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__Vfuncout;
    if ((1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                           >> 2U)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                      >> 2U)))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[0U];
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[1U];
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[2U];
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request 
        = (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
              >> 3U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict))) 
            | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__ready_for_issue_from_lsq));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 3U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict))) 
                 & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full) 
                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception;
    vlTOPp->taiga_sim__DOT__cpu__DOT__exception_id 
        = (7U & ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                >> 0x28U))) ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception)
                  : ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                    >> 0x28U))) ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception)
                      : ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                          << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                       >> 3U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
        = (((QData)((IData)(((0x100000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U])
                              ? (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                           << 0xbU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                             >> 0x15U)))
                              : ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                  << 0x1aU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[1U] 
                                               >> 6U))))) 
            << 0x10U) | (QData)((IData)(((0xfff0U & 
                                          ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                            << 0xaU) 
                                           | (0x3f0U 
                                              & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                 >> 0x16U)))) 
                                         | ((0xcU & 
                                             ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                               << 0x10U) 
                                              | (0xfffcU 
                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                    >> 0x10U)))) 
                                            | (((0U 
                                                 == 
                                                 (0x1fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                                      << 0xbU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                        >> 0x15U)))) 
                                                << 1U) 
                                               | (0U 
                                                  == 
                                                  (0x1fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                                       << 0x13U) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                         >> 0xdU))))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold 
        = (1U & ((((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                     >> 5U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__second_cycle_flush)) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr)) 
                  | ((((1U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state) 
                       | (2U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state)) 
                      | (4U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state)) 
                     | (5U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state))) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__potential_branch_exception)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_mul_op = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
                                                   & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                      >> 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_div_op = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
                                                   & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                      >> 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_branch_or_jump_op 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
           & (((0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                    << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                 >> 0xfU)))) 
               | (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                      << 0x11U) | (
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   >> 0xfU))))) 
              | (0x18U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                     << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                  >> 0xfU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_load_op = 
        ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
         & ((0U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                              << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                           >> 0xfU)))) 
            | (0xbU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                  << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                               >> 0xfU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_store_op = 
        ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
         & (8U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                             << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                          >> 0xfU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_metadata 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result) 
            << 1U) | (1U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
                            >> 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete 
        = (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid) 
            & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result))) 
           | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__arb_request 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests
        [vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i];
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v = 0U;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v) 
           | ((IData)(1U) << (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__.push 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
            & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.push 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
            & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store)) 
           & (~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v) 
                 >> 1U)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match)) 
           | (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address
              [0U] == (0x3fffffffU & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                              >> 0xdU)))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc) 
              | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store) 
                 & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match)) 
           | ((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address
               [1U] == (0x3fffffffU & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                               >> 0xdU)))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation)) 
           | (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc) 
               << 1U) | (0xfffffffeU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store) 
                                         << 1U) & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match)))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance = 
        ((0U != (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests)) 
         & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full)));
    vlTOPp->ddr_axi_wvalid_taiga = vlTOPp->taiga_sim__DOT__axi_wvalid;
    vlTOPp->ddr_axi_wlast_taiga = vlTOPp->taiga_sim__DOT__axi_wlast;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 5U) & (~ ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                                >> 6U))) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 5U) & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                             >> 6U)) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.push 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))) 
                 & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done 
        = ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
             >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))) 
           & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__burst_count) 
              == (0x1fU & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                           >> 1U))));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_count 
        = vlTOPp->__Vdly__taiga_sim__DOT__l2_to_mem__DOT__read_count;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->ddr_axi_rready_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_r_ready;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[1U] = 0U;
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[1U] = 1U;
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[0U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array
           [1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_368 
        = ((0xdU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU]
            : ((0xcU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU]
                : ((0xbU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU]
                    : ((0xaU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU]
                        : ((9U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U]
                            : ((8U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U]
                                : ((7U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U]
                                    : ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U]
                                        : ((5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U]
                                            : ((4U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U]
                                                : (
                                                   (3U 
                                                    == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                    ? 
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U]
                                                    : 
                                                   ((2U 
                                                     == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                     ? 
                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U]
                                                     : 
                                                    ((1U 
                                                      == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                      ? 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U]
                                                      : 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U])))))))))))));
    VL_SHIFTR_WWI(256,256,8, __Vtemp364, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty 
        = (1U & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[0U] 
                 & __Vtemp364[0U]));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[0U] 
           & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_data 
              == (0x3ffffffffffffULL & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                        >> 0xeU))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift 
            = (0x1fU & (0x1eU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated)
                         ? ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                            >> 1U) : ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                                      >> 1U)));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated)
                         ? ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                            - (IData)(2U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated)
                         ? (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                             >> 1U) - (IData)(1U)) : 
                        ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                         >> 1U)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__.rd;
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x77U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (8U & (((~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                      [0U]) | (~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                               [1U])) << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x7bU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (4U & (((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid)) 
                     | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop)) 
                    << 2U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.new_request 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
            >> 0x1fU) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_ack 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)));
    __Vtemp365[0U] = (IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)) 
                               << 0x2bU) | (((QData)((IData)(
                                                             (1U 
                                                              & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected))))) 
                                             << 0x2aU) 
                                            | (((QData)((IData)(
                                                                ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                  ? 0U
                                                                  : 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                             >> 4U)))))) 
                                                << 0x26U) 
                                               | (((QData)((IData)(
                                                                   (7U 
                                                                    & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                        ? (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 7U))
                                                                        : (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                                >> 1U)))))) 
                                                   << 0x23U) 
                                                  | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data)) 
                                                      << 3U) 
                                                     | (QData)((IData)(
                                                                       (7U 
                                                                        & (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 4U)))))))))));
    __Vtemp365[1U] = ((0xfffff000U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                        ? (IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                   >> 0xaU))
                                        : (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                   >> 8U))) 
                                      << 0xcU)) | (IData)(
                                                          ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)) 
                                                             << 0x2bU) 
                                                            | (((QData)((IData)(
                                                                                (1U 
                                                                                & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected))))) 
                                                                << 0x2aU) 
                                                               | (((QData)((IData)(
                                                                                ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                                 ? 0U
                                                                                 : 
                                                                                (0xfU 
                                                                                & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                                >> 4U)))))) 
                                                                   << 0x26U) 
                                                                  | (((QData)((IData)(
                                                                                (7U 
                                                                                & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                                 ? (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 7U))
                                                                                 : (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                                >> 1U)))))) 
                                                                      << 0x23U) 
                                                                     | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data)) 
                                                                         << 3U) 
                                                                        | (QData)((IData)(
                                                                                (7U 
                                                                                & (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 4U)))))))))) 
                                                           >> 0x20U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
        = __Vtemp365[0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
        = __Vtemp365[1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
        = (0xfffU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                       ? (IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                  >> 0xaU)) : (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                       >> 8U))) 
                     >> 0x14U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
        = (((QData)((IData)((((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex 
                               >> 1U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken_ex)) 
                             & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r)))) 
            << 0x28U) | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex)) 
                          << 3U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jmp_id))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[(7U 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                                >> 4U)) 
                                                                                >> 5U))] 
        = (((~ ((IData)(1U) << (0x1fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
            & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[
            (7U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                            >> 4U)) >> 5U))]) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r) 
                                                  & ((3U 
                                                      != 
                                                      (3U 
                                                       & (IData)(
                                                                 (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                  >> 0xeU)))) 
                                                     & (1U 
                                                        == 
                                                        (3U 
                                                         & (IData)(
                                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                    >> 0xcU)))))) 
                                                 << 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
        = ((1U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                 >> 2U)))) ? (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 0x10U))
            : ((2U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                     >> 2U)))) ? (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r 
                                                  | (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 0x10U)))
                : ((3U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                         >> 2U)))) ? 
                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r 
                    & (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                  >> 0x10U)))) : (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 0x10U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs
        [((0x18U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                             >> 0xcU)) << 3U)) | (7U 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))];
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r) 
           & ((3U != (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                    >> 0xeU)))) & (3U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 0xcU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr 
        = vlTOPp->__Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)) 
           | (2U & ((0xfffffffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state)) 
                    | (0x3ffffffeU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                      >> 2U)))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_request 
        = ((0xffffffffff8ULL & (vlTOPp->taiga_sim__DOT__l2_arb__DOT__arb_request 
                                << 1U)) | (QData)((IData)(
                                                          (((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i) 
                                                            << 2U) 
                                                           | (3U 
                                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__arb_request))))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 4U) & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v)) 
                 & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 4U) & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v) 
                             >> 1U)) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort 
        = ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc) 
           & ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation) 
                  >> (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id))) 
              | (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation) 
                  >> (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id)) 
                 & (~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match) 
                       >> (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id))))));
    vlTOPp->ddr_axi_awaddr_taiga = (0xfffffffcU & ((IData)(
                                                           (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                            >> 0xeU)) 
                                                   << 2U));
    vlTOPp->ddr_axi_araddr_taiga = (0xfffffff0U & ((IData)(
                                                           (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                            >> 0x10U)) 
                                                   << 4U));
    vlTOPp->ddr_axi_arid_taiga = (7U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out));
    vlTOPp->ddr_axi_arlen_taiga = (0x1fU & (IData)(
                                                   (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                    >> 3U)));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write 
        = ((IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                    >> 8U)) & ((2U != (0x1fU & (IData)(
                                                       (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                        >> 3U)))) 
                               | (3U != (0x1fU & (IData)(
                                                         (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                          >> 3U))))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_mask = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_mask 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid)
                    ? 0U : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid)
                             ? 0U : 0xffU));
        }
    }
    if (VL_UNLIKELY(((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_47) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [COMPUTE] [binaryOp_add11] [Pred: %1#] [In(0): 0x%x] [In(1) 0x%x] [Out: 0x%x] [OpCode: add] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control),
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__cycleCount));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_24;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R 
        = ((IData)(vlTOPp->rst) ? 0ULL : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22)
                                           ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control)
                                               ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                                               : 0ULL)
                                           : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                                               ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39)
                                                   ? 0ULL
                                                   : 
                                                  ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control)
                                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                                                    : 0ULL))
                                               : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control)
                                                   ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out
                                                   : 0ULL))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_if 
        = ((0x18U & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                     [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                     << 3U)) | (((IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) 
                                 << 2U) | ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)
                                            : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_branch 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                 [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_return 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                 [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                 >> 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_3 
        = ((IData)(1U) + vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_8 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_R_data 
           + vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_R_data);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3___05FT_14_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2___05FT_14_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1___05FT_14_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0___05FT_14_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7___05FT_14_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6___05FT_14_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5___05FT_14_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4___05FT_14_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3___05FT_24_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2___05FT_24_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1___05FT_24_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0___05FT_24_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7___05FT_24_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6___05FT_24_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5___05FT_24_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4___05FT_24_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3___05FT_34_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2___05FT_34_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1___05FT_34_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0___05FT_34_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7___05FT_34_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6___05FT_34_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5___05FT_34_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4___05FT_34_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7___05FT_44_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6___05FT_44_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5___05FT_44_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4___05FT_44_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3___05FT_44_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2___05FT_44_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1___05FT_44_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0___05FT_44_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7___05FT_44_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6___05FT_44_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5___05FT_44_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4___05FT_44_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3___05FT_44_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2___05FT_44_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1___05FT_44_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0___05FT_44_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3___05FT_54_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2___05FT_54_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1___05FT_54_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0___05FT_54_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7___05FT_54_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6___05FT_54_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5___05FT_54_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4___05FT_54_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3___05FT_64_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2___05FT_64_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1___05FT_64_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0___05FT_64_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7___05FT_64_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6___05FT_64_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5___05FT_64_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4___05FT_64_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3___05FT_74_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2___05FT_74_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1___05FT_74_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0___05FT_74_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7___05FT_74_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6___05FT_74_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5___05FT_74_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4___05FT_74_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7___05FT_84_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6___05FT_84_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5___05FT_84_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4___05FT_84_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3___05FT_84_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2___05FT_84_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1___05FT_84_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0___05FT_84_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7___05FT_84_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6___05FT_84_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5___05FT_84_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4___05FT_84_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3___05FT_84_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2___05FT_84_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1___05FT_84_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0___05FT_84_addr_pipe_0])))))) 
                   >> 0x20U));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count = 0U;
    } else {
        if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_11;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14) {
                VL_EXTEND_WQ(68,64, __Vtemp379, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data);
                VL_SHIFTL_WWI(68,68,32, __Vtemp380, __Vtemp379, 3U);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data 
                    = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                       + (((QData)((IData)(__Vtemp380[1U])) 
                           << 0x20U) | (QData)((IData)(
                                                       __Vtemp380[0U]))));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14) {
                    VL_EXTEND_WQ(68,64, __Vtemp383, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data);
                    VL_SHIFTL_WWI(68,68,32, __Vtemp384, __Vtemp383, 3U);
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data 
                        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                           + (((QData)((IData)(__Vtemp384[1U])) 
                               << 0x20U) | (QData)((IData)(
                                                           __Vtemp384[0U]))));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14) {
                            VL_EXTEND_WQ(68,64, __Vtemp387, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data);
                            VL_SHIFTL_WWI(68,68,32, __Vtemp388, __Vtemp387, 3U);
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data 
                                = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                                   + (((QData)((IData)(
                                                       __Vtemp388[1U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   __Vtemp388[0U]))));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14) {
                        VL_EXTEND_WQ(68,64, __Vtemp391, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data);
                        VL_SHIFTL_WWI(68,68,32, __Vtemp392, __Vtemp391, 3U);
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data 
                            = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                               + (((QData)((IData)(
                                                   __Vtemp392[1U])) 
                                   << 0x20U) | (QData)((IData)(
                                                               __Vtemp392[0U]))));
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14) {
                VL_EXTEND_WQ(68,64, __Vtemp395, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data);
                VL_SHIFTL_WWI(68,68,32, __Vtemp396, __Vtemp395, 3U);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data 
                    = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                       + (((QData)((IData)(__Vtemp396[1U])) 
                           << 0x20U) | (QData)((IData)(
                                                       __Vtemp396[0U]))));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14) {
                    VL_EXTEND_WQ(68,64, __Vtemp399, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data);
                    VL_SHIFTL_WWI(68,68,32, __Vtemp400, __Vtemp399, 3U);
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data 
                        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                           + (((QData)((IData)(__Vtemp400[1U])) 
                               << 0x20U) | (QData)((IData)(
                                                           __Vtemp400[0U]))));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14) {
                            VL_EXTEND_WQ(68,64, __Vtemp403, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data);
                            VL_SHIFTL_WWI(68,68,32, __Vtemp404, __Vtemp403, 3U);
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data 
                                = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                                   + (((QData)((IData)(
                                                       __Vtemp404[1U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   __Vtemp404[0U]))));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14) {
                        VL_EXTEND_WQ(68,64, __Vtemp407, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data);
                        VL_SHIFTL_WWI(68,68,32, __Vtemp408, __Vtemp407, 3U);
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data 
                            = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                               + (((QData)((IData)(
                                                   __Vtemp408[1U])) 
                                   << 0x20U) | (QData)((IData)(
                                                               __Vtemp408[0U]))));
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                VL_EXTEND_WQ(68,64, __Vtemp411, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data);
                VL_SHIFTL_WWI(68,68,32, __Vtemp412, __Vtemp411, 3U);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data 
                    = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                       + (((QData)((IData)(__Vtemp412[1U])) 
                           << 0x20U) | (QData)((IData)(
                                                       __Vtemp412[0U]))));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                    VL_EXTEND_WQ(68,64, __Vtemp415, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data);
                    VL_SHIFTL_WWI(68,68,32, __Vtemp416, __Vtemp415, 3U);
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data 
                        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                           + (((QData)((IData)(__Vtemp416[1U])) 
                               << 0x20U) | (QData)((IData)(
                                                           __Vtemp416[0U]))));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                            VL_EXTEND_WQ(68,64, __Vtemp419, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data);
                            VL_SHIFTL_WWI(68,68,32, __Vtemp420, __Vtemp419, 3U);
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data 
                                = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                                   + (((QData)((IData)(
                                                       __Vtemp420[1U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   __Vtemp420[0U]))));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                        VL_EXTEND_WQ(68,64, __Vtemp423, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data);
                        VL_SHIFTL_WWI(68,68,32, __Vtemp424, __Vtemp423, 3U);
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data 
                            = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                               + (((QData)((IData)(
                                                   __Vtemp424[1U])) 
                                   << 0x20U) | (QData)((IData)(
                                                               __Vtemp424[0U]))));
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start 
        = (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
            & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress))) 
           & (~ (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
                 >> 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x100U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 1U : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x200U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 2U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x400U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x800U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 4U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x1000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 5U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x2000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 6U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x4000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 7U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x8000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 8U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x10000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 9U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x20000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xaU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x40000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xbU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x80000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xcU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x100000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xdU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x200000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xeU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x400000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xfU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x800000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x10U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x1000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x11U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x2000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x12U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x4000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x13U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x8000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x14U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x10000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x15U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x20000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x16U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x40000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x17U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x80000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x18U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((1U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x19U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((2U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1aU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((4U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1bU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((8U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1cU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x10U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1dU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x20U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1eU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x40U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1fU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x100U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 1U : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x200U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 2U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x400U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x800U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 4U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x1000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 5U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x2000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 6U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x4000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 7U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x8000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 8U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x10000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 9U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x20000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xaU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x40000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xbU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x80000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xcU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x100000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xdU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x200000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xeU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x400000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xfU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x800000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x10U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x1000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x11U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x2000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x12U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x4000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x13U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x8000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x14U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x10000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x15U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x20000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x16U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x40000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x17U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x80000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x18U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((1U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x19U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((2U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1aU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((4U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1bU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((8U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1cU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x10U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1dU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x20U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1eU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x40U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1fU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot) 
           | ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot) 
           & (- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_ack))));
    vlTOPp->data_bram_addr = (0x3fffffffU & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                                              << 0x12U) 
                                             | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                                >> 0xeU)));
    vlTOPp->data_bram_be = (0xfU & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                                     << 0x1aU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                                  >> 6U)));
    vlTOPp->data_bram_data_in = ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                  << 0x1dU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
                                               >> 3U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.push 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
           & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
              >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in)) 
           | ((0x1c0U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                         << 3U)) | ((0x30U & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                                               << 0x18U) 
                                              | (0xfffff0U 
                                                 & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                                    >> 8U)))) 
                                    | (0xeU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
                                               << 1U)))));
    vlTOPp->__Vfunc_address_range_check__3__addr = 
        ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
          << 0x14U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                       >> 0xcU));
    vlTOPp->__Vfunc_address_range_check__3__Vfuncout 
        = (8U == (0xfU & (vlTOPp->__Vfunc_address_range_check__3__addr 
                          >> 0x1cU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match)) 
           | (IData)(vlTOPp->__Vfunc_address_range_check__3__Vfuncout));
    vlTOPp->__Vfunc_address_range_check__4__addr = 
        ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
          << 0x14U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                       >> 0xcU));
    vlTOPp->__Vfunc_address_range_check__4__Vfuncout 
        = (6U == (0xfU & (vlTOPp->__Vfunc_address_range_check__4__addr 
                          >> 0x1cU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match)) 
           | ((IData)(vlTOPp->__Vfunc_address_range_check__4__Vfuncout) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level = 3U;
    if (((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
         | (0U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)))) {
        if ((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg 
                   >> (0x1fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                        >> 0x23U)))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr 
        = (((((((((0x301U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                >> 4U)))) 
                  | (0xf11U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                  >> 4U))))) 
                 | (0xf12U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
                | (0xf13U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                >> 4U))))) 
               | (0xf14U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                               >> 4U))))) 
              | (0x300U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                              >> 4U))))) 
             | (0x302U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                             >> 4U))))) 
            | (0x303U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                            >> 4U)))))
            ? ((0x301U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                             >> 4U))))
                ? 0x40141100U : ((0xf11U == (0xfffU 
                                             & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U))))
                                  ? 0U : ((0xf12U == 
                                           (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))
                                           ? 0U : (
                                                   (0xf13U 
                                                    == 
                                                    (0xfffU 
                                                     & (IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                >> 4U))))
                                                    ? 0U
                                                    : 
                                                   ((0xf14U 
                                                     == 
                                                     (0xfffU 
                                                      & (IData)(
                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                 >> 4U))))
                                                     ? 0U
                                                     : 
                                                    ((0x300U 
                                                      == 
                                                      (0xfffU 
                                                       & (IData)(
                                                                 (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                  >> 4U))))
                                                      ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus
                                                      : 
                                                     ((0x302U 
                                                       == 
                                                       (0xfffU 
                                                        & (IData)(
                                                                  (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                   >> 4U))))
                                                       ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg
                                                       : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mideleg)))))))
            : (((((((((0x304U == (0xfffU & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                    >> 4U)))) 
                      | (0x305U == (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U))))) 
                     | (0x306U == (0xfffU & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                     >> 4U))))) 
                    | (0x340U == (0xfffU & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                    >> 4U))))) 
                   | (0x341U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                   >> 4U))))) 
                  | (0x342U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                  >> 4U))))) 
                 | (0x343U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
                | (0x344U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                >> 4U)))))
                ? ((0x304U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))
                    ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg
                    : ((0x305U == (0xfffU & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                     >> 4U))))
                        ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec
                        : ((0x306U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))
                            ? 0U : ((0x340U == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))
                                     ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out
                                     : ((0x341U == 
                                         (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                         ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc
                                         : ((0x342U 
                                             == (0xfffU 
                                                 & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                             ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause
                                             : ((0x343U 
                                                 == 
                                                 (0xfffU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                                 ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtval
                                                 : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip)))))))
                : ((((((((((0x3efU <= (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U)))) 
                           & (0x3a0U >= (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                          | (0xb00U == (0xfffU & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                         | (0xb02U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                        | ((0xb03U <= (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U)))) 
                           & (0xb1fU >= (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U)))))) 
                       | (0xb80U == (0xfffU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))) 
                      | (0xb82U == (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U))))) 
                     | ((0xb83U <= (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U)))) 
                        & (0xb9fU >= (0xfffU & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U)))))) 
                    | ((0x320U <= (0xfffU & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                     >> 4U)))) 
                       & (0x33fU >= (0xfffU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))))
                    ? (((0x3efU <= (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U)))) 
                        & (0x3a0U >= (0xfffU & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U)))))
                        ? 0U : ((0xb00U == (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))
                                 ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle)
                                 : ((0xb02U == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))
                                     ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret)
                                     : (((0xb03U <= 
                                          (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U)))) 
                                         & (0xb1fU 
                                            >= (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U)))))
                                         ? 0U : ((0xb80U 
                                                  == 
                                                  (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                                  ? 
                                                 (1U 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
                                                             >> 0x20U)))
                                                  : 
                                                 ((0xb82U 
                                                   == 
                                                   (0xfffU 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 4U))))
                                                   ? 
                                                  (1U 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
                                                              >> 0x20U)))
                                                   : 0U))))))
                    : (((((((((0x100U == (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U)))) 
                              | (0x102U == (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))) 
                             | (0x103U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))) 
                            | (0x104U == (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))) 
                           | (0x105U == (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                          | (0x106U == (0xfffU & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                         | (0x140U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                        | (0x141U == (0xfffU & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U)))))
                        ? ((0x100U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))
                            ? (0xc0122U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus)
                            : ((0x102U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                ? 0U : ((0x103U == 
                                         (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                         ? 0U : ((0x104U 
                                                  == 
                                                  (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                                  ? 
                                                 (0x222U 
                                                  & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg)
                                                  : 
                                                 ((0x105U 
                                                   == 
                                                   (0xfffU 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 4U))))
                                                   ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__stvec
                                                   : 
                                                  ((0x106U 
                                                    == 
                                                    (0xfffU 
                                                     & (IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                >> 4U))))
                                                    ? 0U
                                                    : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out))))))
                        : (((((((((0x142U == (0xfffU 
                                              & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U)))) 
                                  | (0x143U == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                                 | (0x144U == (0xfffU 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                                | (0x180U == (0xfffU 
                                              & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                               | (1U == (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                              | (2U == (0xfffU & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                             | (3U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                            | (0xc00U == (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U)))))
                            ? ((0x142U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out
                                : ((0x143U == (0xfffU 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))
                                    ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out
                                    : ((0x144U == (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                        ? (0x222U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip)
                                        : ((0x180U 
                                            == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))
                                            ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp
                                            : ((1U 
                                                == 
                                                (0xfffU 
                                                 & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                                ? 0U
                                                : (
                                                   (2U 
                                                    == 
                                                    (0xfffU 
                                                     & (IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                >> 4U))))
                                                    ? 0U
                                                    : 
                                                   ((3U 
                                                     == 
                                                     (0xfffU 
                                                      & (IData)(
                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                 >> 4U))))
                                                     ? 0U
                                                     : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle))))))))
                            : ((0xc01U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle)
                                : ((0xc02U == (0xfffU 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))
                                    ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret)
                                    : (((0xc03U <= 
                                         (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U)))) 
                                        & (0xc1fU >= 
                                           (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U)))))
                                        ? 0U : ((0xc80U 
                                                 == 
                                                 (0xfffU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                                 ? 
                                                (1U 
                                                 & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
                                                            >> 0x20U)))
                                                 : 
                                                ((0xc81U 
                                                  == 
                                                  (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                                  ? 
                                                 (1U 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
                                                             >> 0x20U)))
                                                  : 
                                                 ((0xc82U 
                                                   == 
                                                   (0xfffU 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 4U))))
                                                   ? 
                                                  (1U 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
                                                              >> 0x20U)))
                                                   : 0U)))))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[(7U 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                                >> 4U)) 
                                                                                >> 5U))] 
        = (((~ ((IData)(1U) << (0x1fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
            & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[
            (7U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                            >> 4U)) >> 5U))]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write) 
                                                 << 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ls_is_idle));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__second_cycle_flush 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_flush_required;
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
            = ((0x7fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U]) 
               | (0xff800000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                 << 0x14U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
            = (0x7fffffU & ((0x700000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] 
                                          << 0x14U)) 
                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               >> 0xcU)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
            = ((0x7fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U]) 
               | (0xff800000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                 << 0x14U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
            = ((0xff800000U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U]) 
               | (0x7fffffU & ((0x700000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                             << 0x14U)) 
                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                  >> 0xcU))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
            = ((0xff8fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U]) 
               | (0x700000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                               << 5U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
            = ((0xfff01fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U]) 
               | (0xfe000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                              << 0xaU)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
            = ((0xffffff07U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U]) 
               | (0xf8U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                            << 0x11U) | (0x1fff8U & 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                          >> 0xfU)))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
            = ((0xffffe0ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U]) 
               | (0x1f00U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                              << 0x11U) | (0x1ff00U 
                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 0xfU)))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xe07fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0x1f800000U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr) 
                                 << 0x17U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0x1fffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0xe0000000U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr) 
                                 << 0x17U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
            = ((0xfffffff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U]) 
               | (7U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr) 
                        >> 9U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xff9fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0xffe00000U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group) 
                                 << 0x15U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xffe0ffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0x1f0000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                               << 6U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xffff03ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0xfffffc00U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rd_addr) 
                                 << 0xaU)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xfffffdffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0x200U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
                            << 9U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xffffffc7U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0x38U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U]));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0x100U & ((~ ((((((0xdU == (0x1fU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1bU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 5U)))) 
                                    | (5U == (0x1fU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1bU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 5U))))) 
                                   | (0x1bU == (0x1fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 5U))))) 
                                  | (3U == (0x1fU & 
                                            ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1bU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U))))) 
                                 | ((0x1cU == (0x1fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 5U)))) 
                                    & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 0x11U))) 
                                | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op))) 
                            << 8U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xffffff7fU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0xffffff80U & ((((((0x18U == (0x1fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 5U)))) 
                                     | (8U == (0x1fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 5U))))) 
                                    | (0xcU == (0x1fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 5U))))) 
                                   | (0xbU == (0x1fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 5U))))) 
                                  | (2U == (0x1fU & 
                                            ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1bU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U))))) 
                                 << 7U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = ((0xffffffbfU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
               | (0xffffffc0U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd) 
                                 << 6U)));
    }
    if (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
            = (0xfffffffbU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]);
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                = ((0xfffffffbU & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U]) 
                   | (4U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]));
        }
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__new_attr = 
        (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id) 
          << 6U) | ((0x3eU & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                       >> 2U)) << 1U)) 
                    | (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.push 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
            & (~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                          >> 8U)))) & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort)));
    if (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write) {
        vlTOPp->ddr_axi_wstrb_taiga = 0xfU;
        vlTOPp->ddr_axi_wdata_taiga = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result_r;
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count = 0U;
    } else {
        vlTOPp->ddr_axi_wstrb_taiga = (0xfU & (IData)(
                                                      (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                       >> 0xaU)));
        vlTOPp->ddr_axi_wdata_taiga = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out;
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count 
            = (0x1fU & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                >> 3U)));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data
        [0U][0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data
        [1U][0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_15) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[1U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[2U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[3U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[4U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[5U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[6U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[7U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[7U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[8U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[9U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xaU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xbU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xcU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xdU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xeU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xfU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[7U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404 
        = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))
               ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit)) 
                  & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty))
               : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                  & ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99)) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405 
        = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))
               ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit)) 
                  & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty)))
               : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))
                   ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99)) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty)))
                   : ((3U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                      & ((4U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                         & (5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta 
        = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB) 
                    - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match) 
            >> 1U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.new_request 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[1U] = 0U;
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[1U] = 1U;
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[0U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array
           [1U]);
    if (VL_LIKELY(VL_ONEHOT0_I(((4U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                                >> 0x28U)) 
                                       << 2U)) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                                                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret)))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__next_privilege_level 
            = (3U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret))
                      ? ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret)
                          ? (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                             >> 0xbU) : (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                               >> 8U)))
                      : ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                        >> 0x28U)))
                          ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level)
                          : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))));
    } else {
        if (VL_UNLIKELY(Verilated::assertOn())) {
            VL_WRITEF("[%0t] %%Error: csr_regs.sv:222: Assertion failed in %Ntaiga_sim.cpu.gc_unit_block.csr_registers.genblk1: 'unique if' statement violated\n",
                      64,VL_TIME_UNITED_Q(1),vlSymsp->name());
            VL_STOP_MT("/localssd/taiga-project/taiga/core/csr_regs.sv", 222, "");
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] = 
        ((7U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U]) 
         | (0xfffffff8U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id) 
                           << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] = 
        ((0xfffffffbU & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]) 
         | (0xfffffffcU & ((IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetched_count))) 
                           << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] = 
        ((0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]) 
         | vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table
         [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] = 
        ((7U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]) 
         | (0xfffffff8U & ((IData)((((QData)((IData)(
                                                     vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                                     [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                                 [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])))) 
                           << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] = 
        ((7U & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                          [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                          << 0x20U) | (QData)((IData)(
                                                      vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                      [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])))) 
                >> 0x1dU)) | (0xfffffff8U & ((IData)(
                                                     ((((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                                                        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                                                        << 0x20U) 
                                                       | (QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                                         [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id]))) 
                                                      >> 0x20U)) 
                                             << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] = 
        ((0x38U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U]) 
         | (7U & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                             [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                         [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id]))) 
                           >> 0x20U)) >> 0x1dU)));
    vlTOPp->ddr_axi_awlen_taiga = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_7));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_50;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_R_data 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__data_R_data;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_R_data 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__data_R_data;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17)
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
           | ((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                  [0U] >> 0x14U))) 
              == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                        >> 0x11U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
           | (((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                   [1U] >> 0x14U))) 
               == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                         >> 0x11U)))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_352 
        = ((0xdU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xdU]
            : ((0xcU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xcU]
                : ((0xbU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xbU]
                    : ((0xaU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xaU]
                        : ((9U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[9U]
                            : ((8U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[8U]
                                : ((7U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[7U]
                                    : ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[6U]
                                        : ((5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[5U]
                                            : ((4U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[4U]
                                                : (
                                                   (3U 
                                                    == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                    ? 
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[3U]
                                                    : 
                                                   ((2U 
                                                     == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                     ? 
                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[2U]
                                                     : 
                                                    ((1U 
                                                      == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                      ? 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[1U]
                                                      : 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0U])))))))))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_11 
        = (0xffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_512) {
            if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 1U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_513) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_block_dirty) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 2U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_514) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 3U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_515) {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 4U;
                        }
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_517) {
                            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_wrap_out) {
                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 5U;
                            }
                        } else {
                            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_518) {
                                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_dme_wr_0_ack) {
                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = 1U;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_11)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_11))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_11))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data 
                    = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                        : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data 
                            = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data 
                        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                            : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_15)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_15))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_15))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_17;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_11)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_11))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_11))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data 
                    = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                        : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data 
                            = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data 
                        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                            : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_15)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_15))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_15))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_17;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_11)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_11))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_11))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data 
                    = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                        : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data 
                            = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data 
                        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
                            : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138);
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_15)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_15))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_15))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_17;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire = (0x1fU 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire = (0x3cU 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire = ((0x23U 
                                                 & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                                                   [0U] 
                                                   << 2U));
    if ((0U < (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[0U] 
            = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                      [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [0U]]) & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                                [0U])));
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[0U] 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                [0U]] & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                         [0U])) & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                                      >> 5U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[0U] 
            = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired
               [0U] | vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
               [0U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire) 
               & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
               [0U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__retire = 
            ((0x3cU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
             | (3U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                      + vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
                      [0U])));
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
            [0U]) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = ((0x23U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
                   | (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [0U] << 2U));
        }
    }
    if (((1U < (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count)) 
         & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[1U] 
            = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                      [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [1U]]) & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                                [1U])));
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[1U] 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                [1U]] & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                         [1U])) & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                                      >> 5U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[1U] 
            = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired
               [1U] | vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
               [1U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire) 
               & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
               [1U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__retire = 
            ((0x3cU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
             | (3U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                      + vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
                      [1U])));
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
            [1U]) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = ((0x23U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
                   | (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [1U] << 2U));
        }
    }
    vlTOPp->data_bram_en = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.new_request;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in 
        = ((0x1feU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus;
    if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__next_privilege_level))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xffffffdfU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | (((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                    ? (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                             >> 1U)) : 0U) << 5U));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = (0xfffffffdU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | (0x100U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level) 
                            << 8U)));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xffffff7fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | (0x80U & (((3U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                             ? (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                      >> 3U)) : ((1U 
                                                  == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                  ? 
                                                 (1U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                     >> 1U))
                                                  : 0U)) 
                           << 7U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = (0xfffffff7U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xffffe7ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level) 
                  << 0xbU));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op 
        = ((0x20000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
            ? ((0x10000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                ? ((0x8000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                    ? 2U : 1U) : ((0x8000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                                   ? 3U : 0U)) : 3U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op 
        = ((0x20U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
            ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_link 
        = ((1U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                             << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                         >> 0x12U)))) 
           | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 0x12U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_link 
        = ((1U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                             << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                          >> 0xaU)))) 
           | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xaU)))));
    if (VL_LIKELY(VL_ONEHOT0_I(((2U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 5U)) | (1U 
                                                  & ((~ 
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 6U)) 
                                                     & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 5U))))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset 
            = (0x1fffffU & ((1U & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 6U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U)))
                             ? VL_EXTENDS_II(21,12, 
                                             (0xfffU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 9U) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 0x17U))))
                             : ((0x40U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                                 ? ((0x100000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x12U)) 
                                    | ((0xff000U & 
                                        ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1dU) 
                                         | (0x1ffff000U 
                                            & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 3U)))) 
                                       | ((0x800U & 
                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xcU)) 
                                          | (0x7feU 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 9U) 
                                                | (0x1feU 
                                                   & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 0x17U)))))))
                                 : VL_EXTENDS_II(21,13, 
                                                 ((0x1000U 
                                                   & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0xaU)) 
                                                  | ((0x800U 
                                                      & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         << 1U)) 
                                                     | ((0x7e0U 
                                                         & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                             << 9U) 
                                                            | (0x1e0U 
                                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                                  >> 0x17U)))) 
                                                        | (0x1eU 
                                                           & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                               << 0x16U) 
                                                              | (0x3ffffeU 
                                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                                    >> 0xaU)))))))))));
    } else {
        if (VL_UNLIKELY(Verilated::assertOn())) {
            VL_WRITEF("[%0t] %%Error: decode_and_issue.sv:437: Assertion failed in %Ntaiga_sim.cpu.decode_and_issue_block: 'unique if' statement violated\n",
                      64,VL_TIME_UNITED_Q(1),vlSymsp->name());
            VL_STOP_MT("/localssd/taiga-project/taiga/core/decode_and_issue.sv", 437, "");
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match 
        = ((0U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                              << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 0x17U))))
            ? (1U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
            : ((1U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                  << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0x17U))))
                ? (2U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                : ((2U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                      << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x17U))))
                    ? (4U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                    : ((0x102U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 9U) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x17U))))
                        ? (8U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                        : ((0x302U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 9U) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 0x17U))))
                            ? (0x10U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                            : ((0x120U == (0xfe0U & 
                                           ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                             << 9U) 
                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 0x17U))))
                                ? (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                                : 0U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk11__DOT__illegal_op_check__DOT__base_legal 
        = (((((((((((((((((((((((((((((((((((((((((
                                                   ((((0x63U 
                                                       == 
                                                       (0x707fU 
                                                        & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                            << 0x1dU) 
                                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                              >> 3U)))) 
                                                      | (0x1063U 
                                                         == 
                                                         (0x707fU 
                                                          & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                              << 0x1dU) 
                                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                                >> 3U))))) 
                                                     | (0x4063U 
                                                        == 
                                                        (0x707fU 
                                                         & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                             << 0x1dU) 
                                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                               >> 3U))))) 
                                                    | (0x5063U 
                                                       == 
                                                       (0x707fU 
                                                        & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                            << 0x1dU) 
                                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                              >> 3U))))) 
                                                   | (0x6063U 
                                                      == 
                                                      (0x707fU 
                                                       & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                           << 0x1dU) 
                                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                             >> 3U))))) 
                                                  | (0x7063U 
                                                     == 
                                                     (0x707fU 
                                                      & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                          << 0x1dU) 
                                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                            >> 3U))))) 
                                                 | (0x67U 
                                                    == 
                                                    (0x707fU 
                                                     & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                         << 0x1dU) 
                                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                           >> 3U))))) 
                                                | (0x6fU 
                                                   == 
                                                   (0x7fU 
                                                    & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                        << 0x1dU) 
                                                       | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                          >> 3U))))) 
                                               | (0x37U 
                                                  == 
                                                  (0x7fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U))))) 
                                              | (0x17U 
                                                 == 
                                                 (0x7fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1dU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 3U))))) 
                                             | (0x13U 
                                                == 
                                                (0x707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                            | (0x1013U 
                                               == (0xfc00707fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U))))) 
                                           | (0x2013U 
                                              == (0x707fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1dU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 3U))))) 
                                          | (0x3013U 
                                             == (0x707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                         | (0x4013U 
                                            == (0x707fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                                        | (0x5013U 
                                           == (0xfc00707fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))))) 
                                       | (0x40005013U 
                                          == (0xfc00707fU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                                      | (0x6013U == 
                                         (0x707fU & 
                                          ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                            << 0x1dU) 
                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 3U))))) 
                                     | (0x7013U == 
                                        (0x707fU & 
                                         ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
                                    | (0x33U == (0xfe00707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                   | (0x40000033U == 
                                      (0xfe00707fU 
                                       & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
                                  | (0x1033U == (0xfe00707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                 | (0x2033U == (0xfe00707fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                                | (0x3033U == (0xfe00707fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))))) 
                               | (0x4033U == (0xfe00707fU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                              | (0x5033U == (0xfe00707fU 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x1dU) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 3U))))) 
                             | (0x40005033U == (0xfe00707fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                            | (0x6033U == (0xfe00707fU 
                                           & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1dU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U))))) 
                           | (0x7033U == (0xfe00707fU 
                                          & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1dU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 3U))))) 
                          | (3U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                << 0x1dU) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 3U))))) 
                         | (0x1003U == (0x707fU & (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                        | (0x2003U == (0x707fU & ((
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))))) 
                       | (0x4003U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                      | (0x5003U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x1dU) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 3U))))) 
                     | (0x23U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1dU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 3U))))) 
                    | (0x1023U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1dU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U))))) 
                   | (0x2023U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1dU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 3U))))) 
                  | (0xfU == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1dU) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 3U))))) 
                 | (0x100fU == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                            << 0x1dU) 
                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 3U))))) 
                | (0x1073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
               | (0x2073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1dU) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 3U))))) 
              | (0x3073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 0x1dU) 
                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 3U))))) 
             | (0x5073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1dU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 3U))))) 
            | (0x6073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x1dU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 3U))))) | 
           (0x7073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x3fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((0x18U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U)))) 
                | (0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x1bU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 5U))))) | 
               (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U))))) 
              << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x7eU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | (((0xcU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U)))) 
               & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                     >> 0x1cU))) | (((((4U == (0x1fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 5U)))) 
                                       | (5U == (0x1fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 5U))))) 
                                      | (0xdU == (0x1fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 5U))))) 
                                     | (0x1bU == (0x1fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 5U))))) 
                                    | (0x19U == (0x1fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 5U)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x7dU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((0U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                  << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 5U)))) 
                | (8U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U))))) 
               | (0xbU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U))))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x5fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | (((0x1cU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
               | (3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U))))) 
              << 5U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x6fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((2U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                  << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 5U)))) 
                & (2U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                 << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 0xfU))))) 
               & (0x40U == (0x7fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                      << 4U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x1cU))))) 
              << 4U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr 
        = ((0x3e0U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr)) 
           | (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                        << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                    >> 0x12U))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr 
        = ((0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr)) 
           | (0x3e0U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                         << 0xeU) | (0x3fe0U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x12U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op 
        = ((0xcU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 5U)))) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
              >> 0x1cU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op 
        = ((0x1cU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 5U)))) 
           & (0U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                            << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                         >> 0xfU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[0U] 
        = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                  << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                               >> 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[0U] 
        = (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                     << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                  >> 0xaU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
        = ((0x3fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U]) 
           | (0xffffffc0U & ((IData)((((QData)((IData)(
                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                                       << 0x29U) | 
                                      (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                        << 9U) | ((QData)((IData)(
                                                                  vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                                  >> 0x17U)))) 
                             << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U] 
        = ((0x3fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                               << 0x29U) | (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                             << 9U) 
                                            | ((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                               >> 0x17U)))) 
                     >> 0x1aU)) | (0xffffffc0U & ((IData)(
                                                          ((((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                                                             << 0x29U) 
                                                            | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                                                << 9U) 
                                                               | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                                                  >> 0x17U))) 
                                                           >> 0x20U)) 
                                                  << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[4U] 
        = (0x3fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                               << 0x29U) | (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                             << 9U) 
                                            | ((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                               >> 0x17U))) 
                             >> 0x20U)) >> 0x1aU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
        = ((0xc7ffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U]) 
           | (0x38000000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0x3ffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xfc000000U & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                                         << 9U) 
                                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                           >> 0x17U)))) 
                                       << 1U) | (QData)((IData)(
                                                                (1U 
                                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                    >> 2U)))))) 
                             << 0x1aU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
        = ((0xf8000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U]) 
           | ((0x3ffffffU & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                                         << 9U) 
                                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                           >> 0x17U)))) 
                                       << 1U) | (QData)((IData)(
                                                                (1U 
                                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                    >> 2U)))))) 
                             >> 6U)) | (0xfc000000U 
                                        & ((IData)(
                                                   ((((QData)((IData)(
                                                                      ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                                                        << 9U) 
                                                                       | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                          >> 0x17U)))) 
                                                      << 1U) 
                                                     | (QData)((IData)(
                                                                       (1U 
                                                                        & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                           >> 2U))))) 
                                                    >> 0x20U)) 
                                           << 0x1aU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xfc7fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xff800000U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__br_use_signed) 
                              << 0x19U) | ((0x1000000U 
                                            & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                               << 8U)) 
                                           | (0x800000U 
                                              & (((~ 
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                    >> 0x10U)) 
                                                  << 0x17U) 
                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                    << 8U)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[0U] 
        = ((0x3fff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [0U]) | (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                            << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                         >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xfffe3fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0x1c000U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                           << 0x1aU) | (0x3ffc000U 
                                        & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                           >> 6U)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group[0U] 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 0x15U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group[1U] 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 0x16U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr[0U] 
        = (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                     << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                               >> 0x17U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr[1U] 
        = (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                     << 3U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                               >> 0x1dU)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_24 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_47 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
        = ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
           | (0xfffU & vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
        = (0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address);
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                            [0U]) << 0xcU))));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                            [1U]) << 0xcU))));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request 
        = (1U & (((((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                        >> 0x1fU)) | ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
                                      & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.new_request) 
                                         | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__mmu_request_complete)))) 
                   & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count) 
                         >> 3U))) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_hold))) 
                 & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__exception_pending))));
    vlTOPp->ddr_axi_wdata_muir = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode)
                                   ? ((0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                       ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xfU]
                                       : ((0xeU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                           ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xeU]
                                           : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_352))
                                   : ((0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                       ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU]
                                       : ((0xeU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                           ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU]
                                           : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_368)));
    VL_SHIFTR_WWI(256,256,8, __Vtemp499, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v, (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count));
    VL_SHIFTR_WWI(256,256,8, __Vtemp500, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d, (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_block_dirty 
        = (1U & (__Vtemp499[0U] & __Vtemp500[0U]));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret_next 
        = (0x1ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
                             + (QData)((IData)((3U 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.potential_push 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear) 
                  & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index) 
                        >> 5U))) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                                    >> 5U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
        [1U];
    if (VL_LIKELY(VL_ONEHOT0_I(((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret)) 
                                 << 2U) | ((2U & ((IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                                           >> 0x28U)) 
                                                  << 1U)) 
                                           | (1U & 
                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U] 
                                               | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U]))))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_new 
            = ((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U] 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U]))
                ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                    & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write)
                           ? 0xe19aaU : 0xc0122U))) 
                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
                      & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write)
                          ? 0xe19aaU : 0xc0122U))) : 
               ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                               >> 0x28U))) ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception
                 : (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                     | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret))
                     ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return
                     : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus)));
    } else {
        if (VL_UNLIKELY(Verilated::assertOn())) {
            VL_WRITEF("[%0t] %%Error: csr_regs.sv:278: Assertion failed in %Ntaiga_sim.cpu.gc_unit_block.csr_registers.genblk1: 'unique if' statement violated\n",
                      64,VL_TIME_UNITED_Q(1),vlSymsp->name());
            VL_STOP_MT("/localssd/taiga-project/taiga/core/csr_regs.sv", 278, "");
        }
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group 
        = ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group)) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group
           [(0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr))]);
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group 
        = ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group)) 
           | (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group
              [(0x1fU & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr) 
                         >> 5U))] << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table
        [(0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr))];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr 
        = ((0xfc0U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table
        [(0x1fU & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr) 
                   >> 5U))];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr 
        = ((0x3fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1) 
              << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x73U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op) 
                & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                      >> 0x11U))) << 3U) | (0x1fffcU 
                                            & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op) 
                                                << 2U) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 0xfU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd 
        = (1U & (~ ((((0x18U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1bU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 5U)))) 
                      | (8U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1bU) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 5U))))) 
                     | (3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 0x1bU) 
                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 5U))))) 
                    | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__data_R_data = 0ULL;
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__data_R_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_bits_data;
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__data_R_data = 0ULL;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_R_data 
           + vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_R_data);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__data_R_data = 0ULL;
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__data_R_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_bits_data;
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__data_R_data = 0ULL;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address
            : vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc);
    vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                    >> 6U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_512 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_513 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_514 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_515 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_517 
        = (4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_518 
        = (5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid 
            = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405)
                : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405)
                    : ((3U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
                       & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405))));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid 
            = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404)
                : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404)
                    : ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404))));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
           & (0xffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
            : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
                : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
                    : ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
                        : ((4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_17 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_46;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11)
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_17 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_48;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11)
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_17 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_51;
        }
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_log_value_ready) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [PHI] [phiindvars_iv6] [Pred: %1#] [Out: %20#] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_39,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control),
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__cycleCount));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_44) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control)
                        ? 1U : 2U);
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_52) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state = 0U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_59) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state = 0U;
                    }
                }
            }
        }
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_log_value_ready) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control))) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [PHI] [phiindvars_iv6] [Pred: %1#] [Out: %20#] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_39,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control),
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__cycleCount));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13)
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13)))));
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5)
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5)))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID;
                        }
                    }
                } else {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_6;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17)
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21)
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25)
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25)))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control;
                        }
                    }
                } else {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_7;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9))
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9)))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19) {
                vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control;
                        }
                    }
                } else {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_11;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13))
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29))
                                            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_0_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_1_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_2_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45)
                                            ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50)
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID = 0U;
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_47;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control = 0U;
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID = 0U;
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_92;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)))) {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_56;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)))) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_84;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = 0U;
                    }
                } else {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = 1U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = 0U;
                        }
                    } else {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_76;
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = 0U;
                    }
                }
            } else {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_32;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control)
                        ? 1U : 2U);
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state = 1U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state = 2U;
                        }
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state = 0U;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_bits_data 
        = ((7U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                 >> 3U)))) ? (((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU])) 
                                               << 0x20U) 
                                              | (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU])))
            : ((6U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                     >> 3U)))) ? (((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU])) 
                                                   << 0x20U) 
                                                  | (QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU])))
                : ((5U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                         >> 3U)))) ? 
                   (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU])) 
                     << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU])))
                    : ((4U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                             >> 3U))))
                        ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U])))
                        : ((3U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                 >> 3U))))
                            ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U])))
                            : ((2U == (7U & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                     >> 3U))))
                                ? (((QData)((IData)(
                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U])) 
                                    << 0x20U) | (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U])))
                                : ((1U == (7U & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                         >> 3U))))
                                    ? (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U])) 
                                        << 0x20U) | (QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U])))
                                    : (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U])) 
                                        << 0x20U) | (QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U]))))))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R));
    vlTOPp->instruction_bram_addr = (0x3fffffffU & 
                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                                      >> 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next)) 
           | ((((IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) 
                << 3U) << 1U) | ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
                                   & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_branch)) 
                                  << 3U) | (((8U == 
                                              (0xfU 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                                                  >> 0x1cU))) 
                                             << 2U) 
                                            | (2U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                                >> 5U))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.new_request 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
           & (8U == (0xfU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                             >> 0x1cU))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___GEN_5 
        = (((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
            & ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap))) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_done));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_w_valid 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_34 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_36 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_38 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55 
        = (((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_0) 
              | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20)) 
             & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_1) 
                | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21))) 
            & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_2) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22))) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_3) 
              | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data
            : 0ULL);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[0U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->instruction_bram_en = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.new_request;
    vlTOPp->ddr_axi_wvalid_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_w_valid;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_44 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_50 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_51 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_82 
        = ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state)) 
             & (1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) 
            & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_44 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_50 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_51 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_82 
        = ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state)) 
             & (1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) 
            & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_44 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_52 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_59 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
            : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                ? 0ULL : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_log_value_ready 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_42 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_43 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_44 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_45 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41) {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = 0U;
                        }
                    } else {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = 1U;
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_44) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_38;
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_38) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41) {
                        if (((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb__DOT___T)) 
                             & (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)))) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state = 1U;
                        }
                    } else {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state = 2U;
                    }
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_44) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_wr_mem_0_MemResp_valid) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_47) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state = 0U;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_56 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_84 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0)) 
           & (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_0_0) 
               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_1_0)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_2_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0 
        = ((~ (IData)(vlTOPp->rst)) & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_33));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7)
                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control)
                        : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7)
                                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control)
                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7)
                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control)
                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___GEN_8;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)
                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID)
                        : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)
                                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID)
                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)
                            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID)
                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control 
                    = (0U != ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
                               ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R
                               : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29)
                                   ? (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21))
                                   : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control 
                            = (0U != ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
                                       ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R
                                       : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29)
                                           ? (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21))
                                           : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control 
                        = (0U != ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
                                   ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R
                                   : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29)
                                       ? (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21))
                                       : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R)));
                }
            }
        }
    }
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_4)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_4))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_4))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_12)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_12))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_12))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_8)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_8))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_8))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__true_output)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__true_output))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__true_output))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_17;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0 = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_true_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_13)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_13))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_13))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID 
        = ((IData)(vlTOPp->rst) ? 0U : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                         ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id)
                                         : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                             ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)
                                                 ? 0U
                                                 : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id))
                                             : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__false_output)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__false_output))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__false_output))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_18;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0 = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_false_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_15)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_15))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_15))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_19;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data = 0ULL;
    } else {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_0;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data = 0ULL;
    } else {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_1;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data = 0ULL;
    } else {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_2;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired
        [1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [0U]][0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [1U]][1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_control) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_293 
        = (((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state)) 
            & (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_32 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released) 
                    | (((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids)) 
                        == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                        [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                       [0U]))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released) 
                    | (((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids)) 
                        == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                        [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                       [1U]))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (2U & ((0xfffffffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 3U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [0U]) << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (2U & ((0xfffffffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 3U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [1U]) << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (4U & ((0xfffffffcU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 6U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [0U]) << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (4U & ((0xfffffffcU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 6U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [1U]) << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (8U & ((0xfffffff8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 9U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [0U]) << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (8U & ((0xfffffff8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 9U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [1U]) << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U] 
        = ((0xfffffffcU & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           << 2U)) | (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                             << 0xcU) 
                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                               >> 0x14U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[1U] 
        = ((3U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U])))) 
                  >> 0x1eU)) | (0xfffffffcU & ((IData)(
                                                       ((((QData)((IData)(
                                                                          vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                          [0U])) 
                                                          << 0x20U) 
                                                         | (QData)((IData)(
                                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                           [1U]))) 
                                                        >> 0x20U)) 
                                               << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[2U] 
        = (3U & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U]))) 
                          >> 0x20U)) >> 0x1eU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
        = ((0x3fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           | (0xffffffc0U & ((IData)((((QData)((IData)(
                                                       vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                       [0U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U])))) 
                             << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[1U] 
        = ((0x3fU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                               [0U])) 
                               << 0x20U) | (QData)((IData)(
                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                           [1U])))) 
                     >> 0x1aU)) | (0xffffffc0U & ((IData)(
                                                          ((((QData)((IData)(
                                                                             vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                             [0U])) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(
                                                                              vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                              [1U]))) 
                                                           >> 0x20U)) 
                                                  << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U]) 
           | (0x3fU & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                  [0U])) 
                                  << 0x20U) | (QData)((IData)(
                                                              vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                              [1U]))) 
                                >> 0x20U)) >> 0x1aU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
        = ((0xfffffff8U & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           << 3U)) | ((6U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                              << 0xdU) 
                                             | (0x1ffeU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   >> 0x13U)))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_result_valid) 
                                         & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs1_addr) 
                                             == (0x1fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                       >> 3U)))) 
                                            & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs2_addr) 
                                               == (0x1fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                       << 0x18U) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                         >> 8U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
        = ((7U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U])))) 
                  >> 0x1dU)) | (0xfffffff8U & ((IData)(
                                                       ((((QData)((IData)(
                                                                          vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                          [0U])) 
                                                          << 0x20U) 
                                                         | (QData)((IData)(
                                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                           [1U]))) 
                                                        >> 0x20U)) 
                                               << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
        = (7U & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U]))) 
                          >> 0x20U)) >> 0x1dU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
        = ((0x3fffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U]) 
           | (0xc0000000U & ((IData)((((QData)((IData)(
                                                       vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                       [0U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U])))) 
                             << 0x1eU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
        = ((0x3fffffffU & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           >> 2U)) | (0xc0000000U & 
                                      ((IData)(((((QData)((IData)(
                                                                  vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                  [0U])) 
                                                  << 0x20U) 
                                                 | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U]))) 
                                                >> 0x20U)) 
                                       << 0x1eU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
        = (0x3fffffffU & ((IData)(((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U]))) 
                                   >> 0x20U)) >> 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0x1fffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xe0000000U & ((IData)((((QData)((IData)(
                                                       vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                       [0U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U])))) 
                             << 0x1dU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
        = ((0x1fffffffU & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           >> 3U)) | (0xe0000000U & 
                                      ((IData)(((((QData)((IData)(
                                                                  vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                  [0U])) 
                                                  << 0x20U) 
                                                 | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U]))) 
                                                >> 0x20U)) 
                                       << 0x1dU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[2U] 
        = (0x1fffffffU & ((IData)(((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U]))) 
                                   >> 0x20U)) >> 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_use_regfile)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
           [1U] : vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs2_r);
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_use_regfile)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
           [0U] : vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs1_r);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb__DOT___T 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_38 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_44 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_47 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_1) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_io_out_valid) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate = 1U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_2) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_req_ready) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_3) {
                    if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid) 
                         & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4))) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate = 0U;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_76 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_32));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_10 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_33 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___GEN_8 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_fire_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_6)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_6))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_6))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_10;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_true_R_0) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_false_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__true_output 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__false_output 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_8 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID;
        }
    }
    if (VL_UNLIKELY(((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_47) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [COMPUTE] [icmp_exitcond15] [Pred: %1#] [In(0): 0x%x] [In(1) 0x%x] [Out: 0x%x] [OpCode: eq] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control),
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_R_data,
                   64,(QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21)),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__cycleCount);
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_24;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R 
        = ((IData)(vlTOPp->rst) ? 0ULL : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22)
                                           ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control)
                                               ? (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21))
                                               : 0ULL)
                                           : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
                                               ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39)
                                                   ? 0ULL
                                                   : 
                                                  ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control)
                                                    ? (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21))
                                                    : 0ULL))
                                               : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control)
                                                   ? (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21))
                                                   : 0ULL))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_0 = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_0 
                = (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_2));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_1 = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_1 
                = (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_3));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_2 = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_2 
                = (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_4));
        }
    }
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b 
        = (1U & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                     >> 1U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
                                >> 2U)));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__a 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
            << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
                         >> 3U));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__Vfuncout 
        = (((- (IData)((IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b))) 
            ^ vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__a) 
           + (IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend 
        = vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__Vfuncout;
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b 
        = (1U & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                     >> 1U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
                                >> 2U)));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__a 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
            << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                         >> 3U));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__Vfuncout 
        = (((- (IData)((IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b))) 
            ^ vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__a) 
           + (IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor 
        = vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__Vfuncout;
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
        = (((QData)((IData)((1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
                                    >> 0x1dU) & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                                 >> 0x19U))))) 
            << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
                                          << 2U) | 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                                          >> 0x1eU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
        = (((QData)((IData)((1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                                    >> 0x1dU) & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                                 >> 0x19U))))) 
            << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                                          << 2U) | 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                          >> 0x1eU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
              >> 0x1aU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0xffffff8fU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (0xfffffff0U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_subtract) 
                              << 6U) | ((0x20U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data 
                                                   >> 0x1aU) 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                     >> 0x10U))) 
                                        | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_lshift) 
                                           << 4U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (0xffffff80U & ((0xfffff000U & ((IData)(
                                                     (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                       << 0x20U) 
                                                      | (QData)((IData)(
                                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                        [0U])))) 
                                             << 0xcU)) 
                             | (0xf80U & (((0x40000U 
                                            & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])
                                            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                           [1U] : (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                    << 0x18U) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                      >> 8U))) 
                                          << 7U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
        = ((0x7fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                               << 0x20U) | (QData)((IData)(
                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                           [0U])))) 
                     >> 0x14U)) | (0xffffff80U & ((0xf80U 
                                                   & ((IData)(
                                                              (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                                << 0x20U) 
                                                               | (QData)((IData)(
                                                                                vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U])))) 
                                                      >> 0x14U)) 
                                                  | (0xfffff000U 
                                                     & ((IData)(
                                                                ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                                   << 0x20U) 
                                                                  | (QData)((IData)(
                                                                                vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U]))) 
                                                                 >> 0x20U)) 
                                                        << 0xcU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U] 
        = ((0x7fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                << 0x20U) | (QData)((IData)(
                                                            vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                            [0U]))) 
                              >> 0x20U)) >> 0x14U)) 
           | (0xffffff80U & ((0xffffe000U & ((IData)(
                                                     (((QData)((IData)(
                                                                       (1U 
                                                                        & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                            [0U] 
                                                                            >> 0x1fU) 
                                                                           & (~ 
                                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                               >> 0x14U)))))) 
                                                       << 0x20U) 
                                                      | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data)))) 
                                             << 0xdU)) 
                             | ((0x1000U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data 
                                             >> 0x13U) 
                                            & ((~ (
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   >> 0x14U)) 
                                               << 0xcU))) 
                                | (0xf80U & ((IData)(
                                                     ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                        << 0x20U) 
                                                       | (QData)((IData)(
                                                                         vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                         [0U]))) 
                                                      >> 0x20U)) 
                                             >> 0x14U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U] 
        = ((0x7fU & ((IData)((((QData)((IData)((1U 
                                                & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                    [0U] 
                                                    >> 0x1fU) 
                                                   & (~ 
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                       >> 0x14U)))))) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data)))) 
                     >> 0x13U)) | (0xffffff80U & ((0x1f80U 
                                                   & ((IData)(
                                                              (((QData)((IData)(
                                                                                (1U 
                                                                                & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U] 
                                                                                >> 0x1fU) 
                                                                                & (~ 
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                                >> 0x14U)))))) 
                                                                << 0x20U) 
                                                               | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data)))) 
                                                      >> 0x13U)) 
                                                  | (0xffffe000U 
                                                     & ((IData)(
                                                                ((((QData)((IData)(
                                                                                (1U 
                                                                                & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U] 
                                                                                >> 0x1fU) 
                                                                                & (~ 
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                                >> 0x14U)))))) 
                                                                   << 0x20U) 
                                                                  | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data))) 
                                                                 >> 0x20U)) 
                                                        << 0xdU)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [0U]][0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [1U]][1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_req_ready 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_io_out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb__DOT___T) 
           | ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state)) 
              & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R) 
                 & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40) 
                    & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen) 
           == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_tag_reg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid 
        = (((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
            | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg) 
              & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_true_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_false_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_6 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_7 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_6 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_7 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_12 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state;
    __Vtemp518[0U] = ((0xffffff80U & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                                                << 0x20U) 
                                               | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor)))) 
                                      << 7U)) | ((0x40U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                     << 4U)) 
                                                 | ((0x20U 
                                                     & (((~ 
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                           >> 1U)) 
                                                         << 5U) 
                                                        & ((vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
                                                            ^ 
                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U]) 
                                                           << 3U))) 
                                                    | ((0x10U 
                                                        & (((~ 
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                              >> 1U)) 
                                                            << 4U) 
                                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
                                                              << 2U))) 
                                                       | ((8U 
                                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                              << 3U)) 
                                                          | (7U 
                                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                 << 0x1dU) 
                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                   >> 3U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[0U] 
        = __Vtemp518[0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[1U] 
        = ((0x7fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor)))) 
                     >> 0x19U)) | (0xffffff80U & ((IData)(
                                                          ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor))) 
                                                           >> 0x20U)) 
                                                  << 7U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[2U] 
        = (0x7fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor))) 
                             >> 0x20U)) >> 0x19U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x8000U & ((0x8000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 0xfU) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x20U))) 
                                                  << 0xfU))) 
                         | ((0x8000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xfU)) | 
                            ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                         >> 0x20U))) 
                             << 0xfU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xbfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x4000U & (((0x1c000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xeU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1eU))) 
                                         << 0xeU)) 
                                       & ((0xc000U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xfU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x1fU))) 
                                             << 0xeU)))) 
                          | (0xc000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xfU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1fU))) 
                                         << 0xeU)))) 
                         | (((0x1c000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xeU)) 
                             | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                            >> 0x1eU))) 
                                << 0xeU)) & ((0xc000U 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                          >> 2U)) 
                                                 >> 0xfU)) 
                                             | ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 0x1fU))) 
                                                << 0xeU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xdfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x2000U & (((0x3e000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xdU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1cU))) 
                                         << 0xdU)) 
                                       & ((0x1e000U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xeU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x1dU))) 
                                             << 0xdU)))) 
                          | (0x1e000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xeU) 
                                         & ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x1dU))) 
                                            << 0xdU)))) 
                         | (((0x3e000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xdU)) 
                             | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                            >> 0x1cU))) 
                                << 0xdU)) & ((0x1e000U 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                          >> 2U)) 
                                                 >> 0xeU)) 
                                             | ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 0x1dU))) 
                                                << 0xdU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xefffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x1000U & (((0x7f000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xcU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1aU))) 
                                         << 0xcU)) 
                                       & ((0x3f000U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xdU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x1bU))) 
                                             << 0xcU)))) 
                          | (0x3f000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xdU) 
                                         & ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x1bU))) 
                                            << 0xcU)))) 
                         | (((0x7f000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xcU)) 
                             | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                            >> 0x1aU))) 
                                << 0xcU)) & ((0x3f000U 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                          >> 2U)) 
                                                 >> 0xdU)) 
                                             | ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 0x1bU))) 
                                                << 0xcU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xf7ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x800U & (((0xff800U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xbU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x18U))) 
                                        << 0xbU)) & 
                                      ((0x7f800U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xcU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x19U))) 
                                          << 0xbU)))) 
                         | (0x7f800U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xcU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x19U))) 
                                         << 0xbU)))) 
                        | (((0xff800U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xbU)) 
                            | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                           >> 0x18U))) 
                               << 0xbU)) & ((0x7f800U 
                                             & ((IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                         >> 2U)) 
                                                >> 0xcU)) 
                                            | ((~ (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                           >> 0x19U))) 
                                               << 0xbU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfbffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x400U & (((0x1ffc00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xaU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x16U))) 
                                         << 0xaU)) 
                                       & ((0xffc00U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xbU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x17U))) 
                                             << 0xaU)))) 
                         | (0xffc00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xbU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x17U))) 
                                         << 0xaU)))) 
                        | (((0x1ffc00U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xaU)) 
                            | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                           >> 0x16U))) 
                               << 0xaU)) & ((0xffc00U 
                                             & ((IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                         >> 2U)) 
                                                >> 0xbU)) 
                                            | ((~ (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                           >> 0x17U))) 
                                               << 0xaU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfdffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x200U & (((0x3ffe00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 9U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x14U))) 
                                                   << 9U)) 
                                       & ((0x1ffe00U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xaU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x15U))) 
                                             << 9U)))) 
                         | (0x1ffe00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xaU) 
                                         & ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x15U))) 
                                            << 9U)))) 
                        | (((0x3ffe00U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 9U)) | 
                            ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                         >> 0x14U))) 
                             << 9U)) & ((0x1ffe00U 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 0xaU)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x15U))) 
                                           << 9U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfeffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x100U & (((0x7fff00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 8U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x12U))) 
                                                   << 8U)) 
                                       & ((0x3fff00U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 9U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x13U))) 
                                             << 8U)))) 
                         | (0x3fff00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 9U) & 
                                         ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x13U))) 
                                          << 8U)))) 
                        | (((0x7fff00U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 8U)) | 
                            ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                         >> 0x12U))) 
                             << 8U)) & ((0x3fff00U 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 9U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x13U))) 
                                           << 8U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xff7fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x80U & (((0xffff80U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 7U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x10U))) 
                                                  << 7U)) 
                                      & ((0x7fff80U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 8U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x11U))) 
                                            << 7U)))) 
                        | (0x7fff80U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 8U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x11U))) 
                                                   << 7U)))) 
                       | (((0xffff80U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 7U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0x10U))) 
                            << 7U)) & ((0x7fff80U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 8U)) | 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x11U))) 
                                        << 7U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xffbfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x40U & (((0x1ffffc0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 6U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xeU))) 
                                                   << 6U)) 
                                       & ((0xffffc0U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 7U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0xfU))) 
                                             << 6U)))) 
                        | (0xffffc0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 7U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xfU))) 
                                                   << 6U)))) 
                       | (((0x1ffffc0U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 6U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0xeU))) 
                            << 6U)) & ((0xffffc0U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 7U)) | 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0xfU))) 
                                        << 6U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xffdfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x20U & (((0x3ffffe0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 5U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xcU))) 
                                                   << 5U)) 
                                       & ((0x1ffffe0U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 6U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0xdU))) 
                                             << 5U)))) 
                        | (0x1ffffe0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 6U) & 
                                         ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xdU))) 
                                          << 5U)))) 
                       | (((0x3ffffe0U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 5U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0xcU))) 
                            << 5U)) & ((0x1ffffe0U 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 6U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xdU))) 
                                          << 5U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xffefU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x10U & (((0x7fffff0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 4U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xaU))) 
                                                   << 4U)) 
                                       & ((0x3fffff0U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 5U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0xbU))) 
                                             << 4U)))) 
                        | (0x3fffff0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 5U) & 
                                         ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xbU))) 
                                          << 4U)))) 
                       | (((0x7fffff0U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 4U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0xaU))) 
                            << 4U)) & ((0x3fffff0U 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 5U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xbU))) 
                                          << 4U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfff7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (8U & (((0xffffff8U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 3U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 8U))) 
                                                << 3U)) 
                                    & ((0x7fffff8U 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 4U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 9U))) 
                                          << 3U)))) 
                     | (0x7fffff8U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 4U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 9U))) 
                                                 << 3U)))) 
                    | (((0xffffff8U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 3U)) | ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 8U))) 
                                                  << 3U)) 
                       & ((0x7fffff8U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 4U)) | 
                          ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                       >> 9U))) << 3U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfffbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (4U & (((0x1ffffffcU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 2U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 6U))) 
                                                 << 2U)) 
                                     & ((0xffffffcU 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 3U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 7U))) 
                                           << 2U)))) 
                     | (0xffffffcU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 3U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 7U))) 
                                                 << 2U)))) 
                    | (((0x1ffffffcU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 2U)) | (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 6U))) 
                                                   << 2U)) 
                       & ((0xffffffcU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 3U)) | 
                          ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                       >> 7U))) << 2U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfffdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (2U & (((0x3ffffffeU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 1U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 4U))) 
                                                 << 1U)) 
                                     & ((0x1ffffffeU 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 2U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 5U))) 
                                           << 1U)))) 
                     | (0x1ffffffeU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 2U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 5U))) 
                                                  << 1U)))) 
                    | (((0x3ffffffeU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 1U)) | (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 4U))) 
                                                   << 1U)) 
                       & ((0x1ffffffeU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 2U)) | 
                          ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                       >> 5U))) << 1U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (1U & (((0x7fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      & (~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 2U)))) 
                                     & ((0x3fffffffU 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 1U)) 
                                        | (~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 3U)))))) 
                     | (0x3fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 1U) & (~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 3U)))))) 
                    | (((0x7fffffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U))) 
                        | (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                      >> 2U)))) & (
                                                   (0x3fffffffU 
                                                    & ((IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                                >> 2U)) 
                                                       >> 1U)) 
                                                   | (~ (IData)(
                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                                 >> 3U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in 
        = (1U & ((0x20000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U])
                  ? ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a) 
                       | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b))) 
                      & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                  >> 1U)) | (~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 1U))))) 
                     | ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                 >> 1U)) & (~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 1U)))))
                  : ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a) 
                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b)) 
                      | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a)) 
                         & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b)))) 
                     & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                  >> 1U)) & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 1U))) 
                        | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                       >> 1U))) & (~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 1U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x8000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                   >> 2U)) >> 0xfU) 
                         & ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0x20U))) 
                            << 0xfU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xbfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x4000U & ((0x1c000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xeU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1eU))) 
                                        << 0xeU)) & 
                                      ((0xc000U & ((IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                            >> 2U)) 
                                                   >> 0xfU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x1fU))) 
                                          << 0xeU)))) 
                         | (0xc000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xfU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1fU))) 
                                        << 0xeU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xdfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x2000U & ((0x3e000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xdU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1cU))) 
                                        << 0xdU)) & 
                                      ((0x1e000U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xeU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x1dU))) 
                                          << 0xdU)))) 
                         | (0x1e000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xeU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1dU))) 
                                         << 0xdU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xefffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x1000U & ((0x7f000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xcU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1aU))) 
                                        << 0xcU)) & 
                                      ((0x3f000U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xdU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x1bU))) 
                                          << 0xcU)))) 
                         | (0x3f000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xdU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1bU))) 
                                         << 0xcU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xf7ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x800U & ((0xff800U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 0xbU) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x18U))) 
                                                   << 0xbU)) 
                                     & ((0x7f800U & 
                                         ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xcU)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x19U))) 
                                           << 0xbU)))) 
                        | (0x7f800U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xcU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x19U))) 
                                        << 0xbU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfbffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x400U & ((0x1ffc00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xaU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x16U))) 
                                        << 0xaU)) & 
                                      ((0xffc00U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xbU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x17U))) 
                                          << 0xaU)))) 
                        | (0xffc00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xbU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x17U))) 
                                        << 0xaU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfdffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x200U & ((0x3ffe00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 9U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x14U))) 
                                                  << 9U)) 
                                      & ((0x1ffe00U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 0xaU)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x15U))) 
                                            << 9U)))) 
                        | (0x1ffe00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xaU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x15U))) 
                                         << 9U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfeffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x100U & ((0x7fff00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 8U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x12U))) 
                                                  << 8U)) 
                                      & ((0x3fff00U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 9U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x13U))) 
                                            << 8U)))) 
                        | (0x3fff00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 9U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x13U))) 
                                                   << 8U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xff7fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x80U & ((0xffff80U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 7U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 0x10U))) 
                                                 << 7U)) 
                                     & ((0x7fff80U 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 8U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x11U))) 
                                           << 7U)))) 
                       | (0x7fff80U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 8U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x11U))) 
                                                  << 7U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xffbfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x40U & ((0x1ffffc0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 6U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xeU))) 
                                                  << 6U)) 
                                      & ((0xffffc0U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 7U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0xfU))) 
                                            << 6U)))) 
                       | (0xffffc0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 7U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xfU))) 
                                                  << 6U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xffdfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x20U & ((0x3ffffe0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 5U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xcU))) 
                                                  << 5U)) 
                                      & ((0x1ffffe0U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 6U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0xdU))) 
                                            << 5U)))) 
                       | (0x1ffffe0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 6U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xdU))) 
                                                   << 5U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xffefU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x10U & ((0x7fffff0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 4U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xaU))) 
                                                  << 4U)) 
                                      & ((0x3fffff0U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 5U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0xbU))) 
                                            << 4U)))) 
                       | (0x3fffff0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 5U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xbU))) 
                                                   << 4U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfff7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (8U & ((0xffffff8U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                              >> 2U)) 
                                     >> 3U) & ((~ (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                           >> 8U))) 
                                               << 3U)) 
                                   & ((0x7fffff8U & 
                                       ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 4U)) | (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 9U))) 
                                                   << 3U)))) 
                    | (0x7fffff8U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 4U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 9U))) 
                                                << 3U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfffbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (4U & ((0x1ffffffcU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 2U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 6U))) 
                                                << 2U)) 
                                    & ((0xffffffcU 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 3U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 7U))) 
                                          << 2U)))) 
                    | (0xffffffcU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 3U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 7U))) 
                                                << 2U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfffdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (2U & ((0x3ffffffeU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 1U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 4U))) 
                                                << 1U)) 
                                    & ((0x1ffffffeU 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 2U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 5U))) 
                                          << 1U)))) 
                    | (0x1ffffffeU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 2U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 5U))) 
                                                 << 1U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (1U & ((0x7fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                              >> 2U)) 
                                     & (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                   >> 2U)))) 
                                    & ((0x3fffffffU 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 1U)) 
                                       | (~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 3U)))))) 
                    | (0x3fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 1U) & (~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 3U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x8000U & ((0xffff8000U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
                         ^ (0xf8000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                        >> 0xcU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
        = (0x7fffffffU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                   >> 2U)) & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b 
        = (0x7fffffffU & ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                      >> 2U))) & (~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 2U)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop 
        = (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
              & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jal_jalr_ex))) 
             & (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                           >> 0x28U)))) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                                           >> 2U)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
              >> 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
        = ((0xffffffe0U & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                                     << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex)))) 
                           << 5U)) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken_ex) 
                                       << 4U) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
                                                  << 3U) 
                                                 | ((4U 
                                                     & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jal_jalr_ex)) 
                                                        << 2U)) 
                                                    | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return) 
                                                        << 1U) 
                                                       | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_call))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
        = ((0x1fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex)))) 
                     >> 0x1bU)) | (0xffffffe0U & ((IData)(
                                                          ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex))) 
                                                           >> 0x20U)) 
                                                  << 5U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
        = (0x1fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex))) 
                             >> 0x20U)) >> 0x1bU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
           & ((0x7fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                               << 4U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                         >> 0x1cU))) 
              != (0x7fffffffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex 
                                 >> 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result 
        = (0x1ffffffffULL & ((((0x3fffffffeULL & ((
                                                   (8U 
                                                    & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                    ? 
                                                   ((4U 
                                                     & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                     ? 
                                                    (((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                      << 0x13U) 
                                                     | ((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                        >> 0xdU))
                                                     : 
                                                    ((((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x13U) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                         >> 0xdU)) 
                                                     & (((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                         << 0x34U) 
                                                        | (((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                            << 0x14U) 
                                                           | ((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                              >> 0xcU)))))
                                                    : 
                                                   ((4U 
                                                     & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                     ? 
                                                    ((((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x13U) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                         >> 0xdU)) 
                                                     | (((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                         << 0x34U) 
                                                        | (((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                            << 0x14U) 
                                                           | ((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                              >> 0xcU))))
                                                     : 
                                                    ((((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x13U) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                         >> 0xdU)) 
                                                     ^ 
                                                     (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x34U) 
                                                      | (((QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                          << 0x14U) 
                                                         | ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                            >> 0xcU)))))) 
                                                  << 1U)) 
                               | (QData)((IData)((1U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                     >> 6U))))) 
                              + ((((8U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                    ? ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                        ? (0x1ffffffffULL 
                                           & ((((QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                << 0x34U) 
                                               | (((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                   << 0x14U) 
                                                  | ((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                     >> 0xcU))) 
                                              ^ (- (QData)((IData)(
                                                                   (1U 
                                                                    & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                       >> 6U)))))))
                                        : 0ULL) : 0ULL) 
                                  << 1U) | (QData)((IData)(
                                                           (1U 
                                                            & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                               >> 6U)))))) 
                             >> 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict 
        = (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse
           [0U] & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                   >> 8U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict 
        = (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse
           [1U] & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                   >> 7U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_1 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_2 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_3 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_io_out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid 
        = ((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid)) 
           & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid 
        = ((((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid)) 
           & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_wr_mem_0_MemResp_valid 
        = ((((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid)) 
           & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_6 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_fire_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control = 0U;
    } else {
        if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_2;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_1_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_2_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_8_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_7));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_14) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_R_data = 0x400ULL;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_54;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_R_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_12) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_R_data 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R
                    : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_19);
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_2 = 0U;
    } else {
        if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11) 
             & (8U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr)))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_2 
                = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
                    << 0x19U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U] 
                                 >> 7U));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_3 = 0U;
    } else {
        if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11) 
             & (0xcU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr)))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_3 
                = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
                    << 0x19U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U] 
                                 >> 7U));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_13 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_29 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_31 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_33 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_408 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
           & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0);
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_4 = 0U;
    } else {
        if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11) 
             & (0x10U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr)))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_4 
                = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
                    << 0x19U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U] 
                                 >> 7U));
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x8000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xfU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0x10U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xbfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x4000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xeU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0xfU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xdfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x2000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xdU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0xeU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xefffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x1000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xcU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0xdU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xf7ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x800U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 0xbU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                     >> 0xcU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfbffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x400U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 0xaU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                     >> 0xbU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfdffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x200U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 9U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                    | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                   >> 0xaU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfeffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x100U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 8U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                    | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                   >> 9U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xff7fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x80U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 7U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 8U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xffbfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x40U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 6U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 7U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xffdfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x20U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 5U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 6U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xffefU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x10U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 4U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 5U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfff7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (8U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                     >> 3U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                               >> 4U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfffbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (4U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                     >> 2U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                               >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfffdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (2U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                     >> 1U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                               >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                     | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                    & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                        | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                       >> 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x8000U & ((0xffff8000U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
                         ^ (0xf8000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                        >> 0xcU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way 
        = (3U & ((- (IData)((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                                   >> 3U)))) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = ((0x7ffffe3U & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
           | (0x1cU & (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                       << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = ((0x7fffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
           | ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex))
               ? ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex))
                   ? ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                       ? 3U : 2U) : ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                                      ? 3U : 1U)) : 
              ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex))
                ? ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                    ? 2U : 0U) : ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                                   ? 1U : 0U))));
    if ((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                  >> 2U)))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
            = ((0x7fffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
               | ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                   ? 3U : 0U));
    }
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__pc 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
            << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                         >> 5U));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__Vfuncout 
        = (0x1fffffU & (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__pc 
                        >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = ((0x400001fU & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
           | (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__Vfuncout 
              << 5U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
        = ((2U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
            ? (IData)((0x7fffffffffffffffULL & (((0x10U 
                                                  & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                  ? 
                                                 ((QData)((IData)(
                                                                  ((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
                                                                    << 0x14U) 
                                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                      >> 0xcU)))) 
                                                  << 0x1fU)
                                                  : 
                                                 (((QData)((IData)(
                                                                   (0x7fffffffU 
                                                                    & (- (IData)(
                                                                                (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                                >> 5U))))))) 
                                                   << 0x20U) 
                                                  | (QData)((IData)(
                                                                    ((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
                                                                      << 0x14U) 
                                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                        >> 0xcU)))))) 
                                                >> 
                                                (0x1fU 
                                                 & (((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
                                                      << 0x19U) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                        >> 7U)) 
                                                    ^ 
                                                    (- (IData)(
                                                               (1U 
                                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                   >> 4U)))))))))
            : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
        = ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result) 
           | (0xfffffffeU & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
                             & ((- (IData)((1U & (~ 
                                                  vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
        = ((0xfffffffeU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result) 
           | (1U & ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                     ? (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result 
                                >> 0x20U)) : vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready 
        = (0x7fU & (- (IData)((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict)) 
                                     & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready 
        = ((0x7dU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready)) 
           | (2U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict)) 
                    << 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xfffff0ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xffffff00U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict) 
                              << 0xbU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table
                                          [(0x1fU & 
                                            ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                              << 0x18U) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                >> 8U)))] 
                                          << 8U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_25 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_25 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_38 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_wr_mem_0_MemResp_valid) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_taskID)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_8_bits_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_taskID)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_39 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19))
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_taskID)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID)
                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_19 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_18 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_80 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_24 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_47 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken 
        = (1U & (((1U & (((0x20000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U])
                           ? ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a) 
                                << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in)) 
                              + (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b) 
                                  << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in)))
                           : ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a) 
                                << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in)) 
                              + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in))) 
                         >> 0x10U)) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                       >> 0x17U)) | 
                 (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                  >> 0x18U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way 
        = ((- (IData)((1U & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                                 >> 2U)) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                                             >> 4U) 
                                            ^ (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
                                               >> 1U)))))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_fetch_id_base 
        = (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)
                  ? ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U])
                      ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                          << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                       >> 3U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id))
                  : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_id_stall 
        = (1U & (((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                      >> 2U)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count) 
                                 >> 3U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__illegal_instruction 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__illegal_instruction_pattern_r) 
             & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                >> 2U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold))) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
             & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                >> 2U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                           >> 6U)) & (0U != (0x1fU 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                 << 0x10U) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                   >> 0x10U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)) 
           | (8U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                      << 2U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state)) 
                    & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
                       << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid 
        = (1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                   >> 2U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold))) 
                 & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid 
        = (1U & (((((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                     >> 0xdU) & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                    >> 0xcU))) & ((IData)(
                                                          (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                           [0U] 
                                                           >> 1U)) 
                                                  | ((IData)(
                                                             (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                              [0U] 
                                                              >> 3U)) 
                                                     & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                        >> 0x13U)))) 
                  & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                             [0U] >> 6U))) | ((((~ 
                                                 ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                   >> 0xdU) 
                                                  & (~ 
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                      >> 0xcU)))) 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                           [0U] 
                                                           >> 2U))) 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                          [0U] 
                                                          >> 6U))) 
                                              & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                         [0U] 
                                                         >> 7U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x7dU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (2U & ((((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                          >> 0x1fU)) | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress))) 
                     << 1U) & ((0xffffeU & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                            >> 0xcU)) 
                               | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full)) 
                                  << 1U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
        = (((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[2U] 
             << 3U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                       >> 0x1dU)) + VL_EXTENDS_II(32,12, 
                                                  (0xfffU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                                       << 0xfU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                         >> 0x11U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_10 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_1 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_81 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_80) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_14) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_129 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) 
           & (0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_R_data 
           == vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_R_data);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_19 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31)
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out
            : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_31 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_62 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr = 0xffffU;
    } else {
        if (((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
             & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
                >> 7U))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr 
                = (0xffU & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0xbU] 
                             << 0x14U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0xaU] 
                                          >> 0xcU)));
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc 
        = (((0x800000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U])
             ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
                 << 2U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                           >> 0x1eU)) : ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                          << 5U) | 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                          >> 0x1bU))) 
           + ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken)
               ? VL_EXTENDS_II(32,21, (0x1fffffU & 
                                       vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]))
               : 4U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_instruction_stall 
        = (1U & (((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_id_stall)) 
                  & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                        >> 2U))) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index 
            = (0x1fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index));
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_data 
            = (0x1fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)
                         ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                             << 0x10U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                          >> 0x10U))
                         : ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                             << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                          >> 0xaU))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_data 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)
                ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_phys_addr)
                : (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.data_out));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
           & (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__.request) 
              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__.request;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | (0xfffffffeU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
                                 << 1U)) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy)) 
                                            << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                    >> 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | (0xfffffffcU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
                                 << 2U)) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy)) 
                                            << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                    >> 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | (0xfffffff8U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
                                 << 3U)) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy)) 
                                            << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                    >> 3U)));
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
        = (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                  << 0x1fU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                               >> 1U)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
            = (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                      << 0xaU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                                  >> 0x16U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
            = (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                      << 0x15U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                   >> 0xbU)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
            = (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U]);
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
        = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                     << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                                  >> 3U)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                                   >> 0x18U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                         << 0x13U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                      >> 0xdU)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                         << 0x1eU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                                      >> 2U)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
        = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                    << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                                 >> 0xaU)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
            = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                        << 1U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                                  >> 0x1fU)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
            = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                        << 0xcU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                    >> 0x14U)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
            = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                        << 0x17U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                                     >> 9U)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                 >> 8U));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                     >> 0x1dU));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                     >> 0x12U));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                     >> 7U));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
        = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                           << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                                        >> 0xeU)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
            = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                               << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                                            >> 3U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
            = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                               << 8U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                         >> 0x18U)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
            = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                               << 0x13U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                                            >> 0xdU)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                 >> 9U));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                     >> 0x1eU));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                     >> 0x13U));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                     >> 8U));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be = 0U;
    if ((0U == (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                       << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                    >> 0xeU))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
               | ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address)));
    } else {
        if ((1U == (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                           << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                        >> 0xeU))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be 
                = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
                   | ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address)));
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be 
                = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
                   | ((IData)(1U) << (1U | (2U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address))));
        } else {
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be = 0xfU;
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__new_entry 
        = (((QData)((IData)((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))))) 
            << 0x23U) | (((QData)((IData)((0x7fffU 
                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                              >> 0x11U)))) 
                          << 0x14U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.upper_physical_address))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr 
        = (1U & ((1U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                             >> 0xeU))))
                  ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address
                  : ((5U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                    << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                 >> 0xeU))))
                      ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address
                      : ((2U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                        << 0x12U) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                        >> 0xeU)))) 
                         & (0U != (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                    >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                    >> 0xcU));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_0 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_2 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_28 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_30 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_32 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_34 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_4 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_12 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control 
        = vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state;
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
        if (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress) 
             & (~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                    >> 2U) & (~ (IData)((0U != (3U 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking))))))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 2U;
        }
    } else {
        if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
            if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 4U;
            }
        } else {
            if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
                if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid) 
                     & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                           >> 2U)))) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state 
                        = ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                             [0U])) 
                                  | ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                 [0U] 
                                                 >> 1U))) 
                                     & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                [0U] 
                                                >> 2U)))))
                            ? 0x40U : ((1U & ((IData)(
                                                      vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                      [0U]) 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                          [0U] 
                                                          >> 1U)) 
                                                 | (IData)(
                                                           (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                            [0U] 
                                                            >> 3U)))))
                                        ? (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid) 
                                            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid))
                                            ? 0x20U
                                            : 0x40U)
                                        : 8U));
                }
            } else {
                if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
                    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 0x10U;
                    }
                } else {
                    if ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
                        if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid) 
                             & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                                   >> 2U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state 
                                = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid) 
                                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid))
                                    ? 0x20U : 0x40U);
                        }
                    } else {
                        if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                    >> 5U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                              >> 6U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 1U;
                        }
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state;
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
        if (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress) 
             & (~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                    >> 2U) & (~ (IData)((0U != (3U 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking))))))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 2U;
        }
    } else {
        if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
            if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 4U;
            }
        } else {
            if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
                if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid) 
                     & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                           >> 2U)))) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state 
                        = ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                             [0U])) 
                                  | ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                 [0U] 
                                                 >> 1U))) 
                                     & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                [0U] 
                                                >> 2U)))))
                            ? 0x40U : ((1U & ((IData)(
                                                      vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                      [0U]) 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                          [0U] 
                                                          >> 1U)) 
                                                 | (IData)(
                                                           (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                            [0U] 
                                                            >> 3U)))))
                                        ? (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid) 
                                            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid))
                                            ? 0x20U
                                            : 0x40U)
                                        : 8U));
                }
            } else {
                if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
                    if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 0x10U;
                    }
                } else {
                    if ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
                        if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid) 
                             & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                                   >> 2U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state 
                                = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid) 
                                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid))
                                    ? 0x20U : 0x40U);
                        }
                    } else {
                        if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                    >> 5U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                              >> 6U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 1U;
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 1U;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7fffffffffcULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | (IData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7ffffffff83ULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size)) 
                     << 2U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7ffffffe1ffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be)) 
                     << 9U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7ffffffff7fULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo)) 
                     << 7U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x1fffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr)) 
                     << 0xdU));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7fffffffeffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw)) 
                     << 8U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_6 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_fire_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0) 
            | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1) 
              | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_47 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_92 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T) {
            if ((0x80U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U])) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate = 1U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_1) {
                if ((1U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U])) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_2) {
                    if ((0x80000000U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U])) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate = 0U;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data
        [0U][0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data
        [1U][0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_57 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_42 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_43 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_44 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_45 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_46 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_47 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_48 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_49 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_50 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_51 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_52 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_53 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_54 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_55 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_56 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_16)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_1 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_2 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xfffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0xfffff000U 
                                                     & (((0U 
                                                          == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
                                                         << 0xfU) 
                                                        | ((0x4000U 
                                                            & ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate)) 
                                                               << 0xeU)) 
                                                           | (((1U 
                                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
                                                               << 0xdU) 
                                                              | ((2U 
                                                                  == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
                                                                 << 0xcU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
           | ((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                  [0U] >> 0x14U))) 
              == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                        >> 0x11U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
           | (((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                   [1U] >> 0x14U))) 
               == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                         >> 0x11U)))) 
              << 1U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
        = ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
           | (0xfffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
        = (0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address);
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                            [0U]) << 0xcU))));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                            [1U]) << 0xcU))));
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address
            : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address);
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (1U & (((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                      >> 2U) ^ (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                >> 6U)) ^ (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                           >> 0xaU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (2U & (((0x3ffffffeU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                     >> 2U)) ^ (0x3fffffeU 
                                                & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                                   >> 6U))) 
                    ^ (0x3ffffeU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                    >> 0xaU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (4U & ((0x3ffffffcU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                    >> 2U)) ^ (0x3fffffcU 
                                               & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                                  >> 6U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (8U & ((0x3ffffff8U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                    >> 2U)) ^ (0x3fffff8U 
                                               & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                                  >> 6U)))));
}
