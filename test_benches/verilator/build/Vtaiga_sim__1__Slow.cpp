// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim.h"
#include "Vtaiga_sim__Syms.h"

void Vtaiga_sim::_initial__TOP__3(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_initial__TOP__3\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    IData/*31:0*/ __Vilp;
    // Body
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1fU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1eU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1dU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1cU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1bU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1aU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x19U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x18U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x17U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x16U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x15U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x14U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x13U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x12U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x11U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x10U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xfU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xeU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xdU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xcU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xbU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xaU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[9U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[8U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[7U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[6U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[5U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[4U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[3U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[2U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[1U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1fU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1eU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1dU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1cU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1bU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1aU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x19U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x18U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x17U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x16U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x15U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x14U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x13U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x12U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x11U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x10U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xfU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xeU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xdU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xcU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xbU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xaU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[9U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[8U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[7U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[6U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[5U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[4U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[3U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[2U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[1U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1fU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1eU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1dU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1cU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1bU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x1aU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x19U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x18U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x17U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x16U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x15U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x14U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x13U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x12U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x11U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0x10U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xfU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xeU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xdU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xcU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xbU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0xaU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[9U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[8U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[7U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[6U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[5U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[4U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[3U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[2U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[1U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[0U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1fU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1eU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1dU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1cU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1bU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x1aU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x19U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x18U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x17U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x16U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x15U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x14U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x13U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x12U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x11U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0x10U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xfU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xeU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xdU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xcU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xbU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0xaU] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[9U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[8U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[7U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[6U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[5U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[4U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[3U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[2U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[1U] = 0ULL;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[0U] = 0ULL;
    __Vilp = 0U;
    while ((__Vilp <= 0x3fU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x3fU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__last_unit = 0U;
    __Vilp = 0U;
    while ((__Vilp <= 0x1ffU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x1ffU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x1ffU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x1ffU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index = 0U;
    __Vilp = 0U;
    while ((__Vilp <= 0x3fU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x3fU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x3fU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    __Vilp = 0U;
    while ((__Vilp <= 0x3fU)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vilp] = 0U;
        __Vilp = ((IData)(1U) + __Vilp);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index = 0U;
}

void Vtaiga_sim::_settle__TOP__9(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_settle__TOP__9\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*95:0*/ __Vtemp602[3];
    // Body
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[0U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[0U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
        [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr
        [1U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data
        [0U][0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data
        [1U][0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [0U]][0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [1U]][1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[0U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [0U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[1U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data
           [1U][1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
           | ((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                  [0U] >> 0x14U))) 
              == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                        >> 0x11U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
           | (((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                   [1U] >> 0x14U))) 
               == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                         >> 0x11U)))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U] 
        = ((0xfffffffcU & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           << 2U)) | (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                             << 0xcU) 
                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                               >> 0x14U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[1U] 
        = ((3U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U])))) 
                  >> 0x1eU)) | (0xfffffffcU & ((IData)(
                                                       ((((QData)((IData)(
                                                                          vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                          [0U])) 
                                                          << 0x20U) 
                                                         | (QData)((IData)(
                                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                           [1U]))) 
                                                        >> 0x20U)) 
                                               << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[2U] 
        = (3U & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U]))) 
                          >> 0x20U)) >> 0x1eU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
        = ((0x3fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           | (0xffffffc0U & ((IData)((((QData)((IData)(
                                                       vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                       [0U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U])))) 
                             << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[1U] 
        = ((0x3fU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                               [0U])) 
                               << 0x20U) | (QData)((IData)(
                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                           [1U])))) 
                     >> 0x1aU)) | (0xffffffc0U & ((IData)(
                                                          ((((QData)((IData)(
                                                                             vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                             [0U])) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(
                                                                              vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                              [1U]))) 
                                                           >> 0x20U)) 
                                                  << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U]) 
           | (0x3fU & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                  [0U])) 
                                  << 0x20U) | (QData)((IData)(
                                                              vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                              [1U]))) 
                                >> 0x20U)) >> 0x1aU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
        = ((0xfffffff8U & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           << 3U)) | ((6U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                              << 0xdU) 
                                             | (0x1ffeU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   >> 0x13U)))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_result_valid) 
                                         & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs1_addr) 
                                             == (0x1fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                       >> 3U)))) 
                                            & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs2_addr) 
                                               == (0x1fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                       << 0x18U) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                         >> 8U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
        = ((7U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U])))) 
                  >> 0x1dU)) | (0xfffffff8U & ((IData)(
                                                       ((((QData)((IData)(
                                                                          vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                          [0U])) 
                                                          << 0x20U) 
                                                         | (QData)((IData)(
                                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                           [1U]))) 
                                                        >> 0x20U)) 
                                               << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
        = (7U & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                            [0U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                        [1U]))) 
                          >> 0x20U)) >> 0x1dU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
        = ((0x3fffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U]) 
           | (0xc0000000U & ((IData)((((QData)((IData)(
                                                       vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                       [0U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U])))) 
                             << 0x1eU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
        = ((0x3fffffffU & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           >> 2U)) | (0xc0000000U & 
                                      ((IData)(((((QData)((IData)(
                                                                  vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                  [0U])) 
                                                  << 0x20U) 
                                                 | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U]))) 
                                                >> 0x20U)) 
                                       << 0x1eU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
        = (0x3fffffffU & ((IData)(((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U]))) 
                                   >> 0x20U)) >> 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0x1fffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xe0000000U & ((IData)((((QData)((IData)(
                                                       vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                       [0U])) 
                                       << 0x20U) | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U])))) 
                             << 0x1dU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
        = ((0x1fffffffU & ((IData)((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U])))) 
                           >> 3U)) | (0xe0000000U & 
                                      ((IData)(((((QData)((IData)(
                                                                  vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                  [0U])) 
                                                  << 0x20U) 
                                                 | (QData)((IData)(
                                                                   vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                   [1U]))) 
                                                >> 0x20U)) 
                                       << 0x1dU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[2U] 
        = (0x1fffffffU & ((IData)(((((QData)((IData)(
                                                     vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                     [0U])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                 [1U]))) 
                                   >> 0x20U)) >> 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_use_regfile)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
           [1U] : vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs2_r);
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_use_regfile)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
           [0U] : vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs1_r);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[1U][1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
        [1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
        = ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
           | (0xfffU & vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
        = (0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address);
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                            [0U]) << 0xcU))));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry
                                            [1U]) << 0xcU))));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request 
        = (1U & (((((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                        >> 0x1fU)) | ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
                                      & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.new_request) 
                                         | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__mmu_request_complete)))) 
                   & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count) 
                         >> 3U))) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_hold))) 
                 & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__exception_pending))));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b 
        = (1U & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                     >> 1U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
                                >> 2U)));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__a 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
            << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
                         >> 3U));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__Vfuncout 
        = (((- (IData)((IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b))) 
            ^ vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__a) 
           + (IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend 
        = vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__Vfuncout;
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b 
        = (1U & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                     >> 1U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
                                >> 2U)));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__a 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U] 
            << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                         >> 3U));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__Vfuncout 
        = (((- (IData)((IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b))) 
            ^ vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__a) 
           + (IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor 
        = vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__Vfuncout;
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
        = (((QData)((IData)((1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
                                    >> 0x1dU) & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                                 >> 0x19U))))) 
            << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
                                          << 2U) | 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                                          >> 0x1eU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
        = (((QData)((IData)((1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                                    >> 0x1dU) & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                                 >> 0x19U))))) 
            << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                                          << 2U) | 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                          >> 0x1eU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
              >> 0x1aU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0xffffff8fU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (0xfffffff0U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_subtract) 
                              << 6U) | ((0x20U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data 
                                                   >> 0x1aU) 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                     >> 0x10U))) 
                                        | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_lshift) 
                                           << 4U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (0xffffff80U & ((0xfffff000U & ((IData)(
                                                     (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                       << 0x20U) 
                                                      | (QData)((IData)(
                                                                        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                        [0U])))) 
                                             << 0xcU)) 
                             | (0xf80U & (((0x40000U 
                                            & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])
                                            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                           [1U] : (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                    << 0x18U) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                      >> 8U))) 
                                          << 7U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
        = ((0x7fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                               << 0x20U) | (QData)((IData)(
                                                           vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                           [0U])))) 
                     >> 0x14U)) | (0xffffff80U & ((0xf80U 
                                                   & ((IData)(
                                                              (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                                << 0x20U) 
                                                               | (QData)((IData)(
                                                                                vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U])))) 
                                                      >> 0x14U)) 
                                                  | (0xfffff000U 
                                                     & ((IData)(
                                                                ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                                   << 0x20U) 
                                                                  | (QData)((IData)(
                                                                                vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U]))) 
                                                                 >> 0x20U)) 
                                                        << 0xcU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U] 
        = ((0x7fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                << 0x20U) | (QData)((IData)(
                                                            vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                            [0U]))) 
                              >> 0x20U)) >> 0x14U)) 
           | (0xffffff80U & ((0xffffe000U & ((IData)(
                                                     (((QData)((IData)(
                                                                       (1U 
                                                                        & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                            [0U] 
                                                                            >> 0x1fU) 
                                                                           & (~ 
                                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                               >> 0x14U)))))) 
                                                       << 0x20U) 
                                                      | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data)))) 
                                             << 0xdU)) 
                             | ((0x1000U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data 
                                             >> 0x13U) 
                                            & ((~ (
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   >> 0x14U)) 
                                               << 0xcU))) 
                                | (0xf80U & ((IData)(
                                                     ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data)) 
                                                        << 0x20U) 
                                                       | (QData)((IData)(
                                                                         vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                         [0U]))) 
                                                      >> 0x20U)) 
                                             >> 0x14U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U] 
        = ((0x7fU & ((IData)((((QData)((IData)((1U 
                                                & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                    [0U] 
                                                    >> 0x1fU) 
                                                   & (~ 
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                       >> 0x14U)))))) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data)))) 
                     >> 0x13U)) | (0xffffff80U & ((0x1f80U 
                                                   & ((IData)(
                                                              (((QData)((IData)(
                                                                                (1U 
                                                                                & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U] 
                                                                                >> 0x1fU) 
                                                                                & (~ 
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                                >> 0x14U)))))) 
                                                                << 0x20U) 
                                                               | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data)))) 
                                                      >> 0x13U)) 
                                                  | (0xffffe000U 
                                                     & ((IData)(
                                                                ((((QData)((IData)(
                                                                                (1U 
                                                                                & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.data
                                                                                [0U] 
                                                                                >> 0x1fU) 
                                                                                & (~ 
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                                >> 0x14U)))))) 
                                                                   << 0x20U) 
                                                                  | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data))) 
                                                                 >> 0x20U)) 
                                                        << 0xdU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire = (0x1fU 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire = (0x3cU 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire = ((0x23U 
                                                 & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                                                   [0U] 
                                                   << 2U));
    if ((0U < (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[0U] 
            = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                      [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [0U]]) & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                                [0U])));
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[0U] 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                [0U]] & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                         [0U])) & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                                      >> 5U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[0U] 
            = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired
               [0U] | vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
               [0U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire) 
               & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
               [0U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__retire = 
            ((0x3cU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
             | (3U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                      + vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
                      [0U])));
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
            [0U]) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = ((0x23U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
                   | (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [0U] << 2U));
        }
    }
    if (((1U < (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count)) 
         & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[1U] 
            = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                      [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [1U]]) & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                                [1U])));
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[1U] 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table
                [vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                [1U]] & (~ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse
                         [1U])) & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                                      >> 5U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[1U] 
            = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired
               [1U] | vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
               [1U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire) 
               & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
               [1U]);
        vlTOPp->taiga_sim__DOT__cpu__DOT__retire = 
            ((0x3cU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
             | (3U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                      + vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
                      [1U])));
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired
            [1U]) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire));
            vlTOPp->taiga_sim__DOT__cpu__DOT__retire 
                = ((0x23U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)) 
                   | (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                      [1U] << 2U));
        }
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [0U]][0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set
        [vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group
        [1U]][1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.physical_address
            : vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc);
    vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                    >> 6U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched 
        = ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
             & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_branch)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)));
    __Vtemp602[0U] = ((0xffffff80U & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                                                << 0x20U) 
                                               | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor)))) 
                                      << 7U)) | ((0x40U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                     << 4U)) 
                                                 | ((0x20U 
                                                     & (((~ 
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                           >> 1U)) 
                                                         << 5U) 
                                                        & ((vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
                                                            ^ 
                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[1U]) 
                                                           << 3U))) 
                                                    | ((0x10U 
                                                        & (((~ 
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                              >> 1U)) 
                                                            << 4U) 
                                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[2U] 
                                                              << 2U))) 
                                                       | ((8U 
                                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__div_inputs[0U] 
                                                              << 3U)) 
                                                          | (7U 
                                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                 << 0x1dU) 
                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                   >> 3U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[0U] 
        = __Vtemp602[0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[1U] 
        = ((0x7fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor)))) 
                     >> 0x19U)) | (0xffffff80U & ((IData)(
                                                          ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor))) 
                                                           >> 0x20U)) 
                                                  << 7U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs[2U] 
        = (0x7fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor))) 
                             >> 0x20U)) >> 0x19U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x8000U & ((0x8000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 0xfU) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x20U))) 
                                                  << 0xfU))) 
                         | ((0x8000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xfU)) | 
                            ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                         >> 0x20U))) 
                             << 0xfU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xbfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x4000U & (((0x1c000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xeU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1eU))) 
                                         << 0xeU)) 
                                       & ((0xc000U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xfU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x1fU))) 
                                             << 0xeU)))) 
                          | (0xc000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xfU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1fU))) 
                                         << 0xeU)))) 
                         | (((0x1c000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xeU)) 
                             | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                            >> 0x1eU))) 
                                << 0xeU)) & ((0xc000U 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                          >> 2U)) 
                                                 >> 0xfU)) 
                                             | ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 0x1fU))) 
                                                << 0xeU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xdfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x2000U & (((0x3e000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xdU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1cU))) 
                                         << 0xdU)) 
                                       & ((0x1e000U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xeU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x1dU))) 
                                             << 0xdU)))) 
                          | (0x1e000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xeU) 
                                         & ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x1dU))) 
                                            << 0xdU)))) 
                         | (((0x3e000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xdU)) 
                             | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                            >> 0x1cU))) 
                                << 0xdU)) & ((0x1e000U 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                          >> 2U)) 
                                                 >> 0xeU)) 
                                             | ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 0x1dU))) 
                                                << 0xdU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xefffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x1000U & (((0x7f000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xcU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1aU))) 
                                         << 0xcU)) 
                                       & ((0x3f000U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xdU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x1bU))) 
                                             << 0xcU)))) 
                          | (0x3f000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xdU) 
                                         & ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x1bU))) 
                                            << 0xcU)))) 
                         | (((0x7f000U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xcU)) 
                             | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                            >> 0x1aU))) 
                                << 0xcU)) & ((0x3f000U 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                          >> 2U)) 
                                                 >> 0xdU)) 
                                             | ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 0x1bU))) 
                                                << 0xcU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xf7ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x800U & (((0xff800U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xbU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x18U))) 
                                        << 0xbU)) & 
                                      ((0x7f800U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xcU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x19U))) 
                                          << 0xbU)))) 
                         | (0x7f800U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xcU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x19U))) 
                                         << 0xbU)))) 
                        | (((0xff800U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xbU)) 
                            | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                           >> 0x18U))) 
                               << 0xbU)) & ((0x7f800U 
                                             & ((IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                         >> 2U)) 
                                                >> 0xcU)) 
                                            | ((~ (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                           >> 0x19U))) 
                                               << 0xbU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfbffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x400U & (((0x1ffc00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xaU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x16U))) 
                                         << 0xaU)) 
                                       & ((0xffc00U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xbU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x17U))) 
                                             << 0xaU)))) 
                         | (0xffc00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xbU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x17U))) 
                                         << 0xaU)))) 
                        | (((0x1ffc00U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xaU)) 
                            | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                           >> 0x16U))) 
                               << 0xaU)) & ((0xffc00U 
                                             & ((IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                         >> 2U)) 
                                                >> 0xbU)) 
                                            | ((~ (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                           >> 0x17U))) 
                                               << 0xaU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfdffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x200U & (((0x3ffe00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 9U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x14U))) 
                                                   << 9U)) 
                                       & ((0x1ffe00U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 0xaU)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x15U))) 
                                             << 9U)))) 
                         | (0x1ffe00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xaU) 
                                         & ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x15U))) 
                                            << 9U)))) 
                        | (((0x3ffe00U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 9U)) | 
                            ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                         >> 0x14U))) 
                             << 9U)) & ((0x1ffe00U 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 0xaU)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x15U))) 
                                           << 9U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfeffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x100U & (((0x7fff00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 8U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x12U))) 
                                                   << 8U)) 
                                       & ((0x3fff00U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 9U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0x13U))) 
                                             << 8U)))) 
                         | (0x3fff00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 9U) & 
                                         ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x13U))) 
                                          << 8U)))) 
                        | (((0x7fff00U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 8U)) | 
                            ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                         >> 0x12U))) 
                             << 8U)) & ((0x3fff00U 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 9U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x13U))) 
                                           << 8U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xff7fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x80U & (((0xffff80U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 7U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x10U))) 
                                                  << 7U)) 
                                      & ((0x7fff80U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 8U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x11U))) 
                                            << 7U)))) 
                        | (0x7fff80U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 8U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x11U))) 
                                                   << 7U)))) 
                       | (((0xffff80U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 7U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0x10U))) 
                            << 7U)) & ((0x7fff80U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 8U)) | 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x11U))) 
                                        << 7U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xffbfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x40U & (((0x1ffffc0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 6U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xeU))) 
                                                   << 6U)) 
                                       & ((0xffffc0U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 7U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0xfU))) 
                                             << 6U)))) 
                        | (0xffffc0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 7U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xfU))) 
                                                   << 6U)))) 
                       | (((0x1ffffc0U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 6U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0xeU))) 
                            << 6U)) & ((0xffffc0U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 7U)) | 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0xfU))) 
                                        << 6U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xffdfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x20U & (((0x3ffffe0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 5U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xcU))) 
                                                   << 5U)) 
                                       & ((0x1ffffe0U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 6U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0xdU))) 
                                             << 5U)))) 
                        | (0x1ffffe0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 6U) & 
                                         ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xdU))) 
                                          << 5U)))) 
                       | (((0x3ffffe0U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 5U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0xcU))) 
                            << 5U)) & ((0x1ffffe0U 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 6U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xdU))) 
                                          << 5U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xffefU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (0x10U & (((0x7fffff0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 4U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xaU))) 
                                                   << 4U)) 
                                       & ((0x3fffff0U 
                                           & ((IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                       >> 2U)) 
                                              >> 5U)) 
                                          | ((~ (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                         >> 0xbU))) 
                                             << 4U)))) 
                        | (0x3fffff0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 5U) & 
                                         ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xbU))) 
                                          << 4U)))) 
                       | (((0x7fffff0U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 4U)) | 
                           ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0xaU))) 
                            << 4U)) & ((0x3fffff0U 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 5U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0xbU))) 
                                          << 4U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfff7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (8U & (((0xffffff8U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 3U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 8U))) 
                                                << 3U)) 
                                    & ((0x7fffff8U 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 4U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 9U))) 
                                          << 3U)))) 
                     | (0x7fffff8U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 4U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 9U))) 
                                                 << 3U)))) 
                    | (((0xffffff8U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 3U)) | ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 8U))) 
                                                  << 3U)) 
                       & ((0x7fffff8U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 4U)) | 
                          ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                       >> 9U))) << 3U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfffbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (4U & (((0x1ffffffcU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 2U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 6U))) 
                                                 << 2U)) 
                                     & ((0xffffffcU 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 3U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 7U))) 
                                           << 2U)))) 
                     | (0xffffffcU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 3U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 7U))) 
                                                 << 2U)))) 
                    | (((0x1ffffffcU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 2U)) | (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 6U))) 
                                                   << 2U)) 
                       & ((0xffffffcU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 3U)) | 
                          ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                       >> 7U))) << 2U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfffdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (2U & (((0x3ffffffeU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 1U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 4U))) 
                                                 << 1U)) 
                                     & ((0x1ffffffeU 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 2U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 5U))) 
                                           << 1U)))) 
                     | (0x1ffffffeU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 2U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 5U))) 
                                                  << 1U)))) 
                    | (((0x3ffffffeU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 1U)) | (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 4U))) 
                                                   << 1U)) 
                       & ((0x1ffffffeU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 2U)) | 
                          ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                       >> 5U))) << 1U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b 
        = ((0xfffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b)) 
           | (1U & (((0x7fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      & (~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 2U)))) 
                                     & ((0x3fffffffU 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 1U)) 
                                        | (~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 3U)))))) 
                     | (0x3fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 1U) & (~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 3U)))))) 
                    | (((0x7fffffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U))) 
                        | (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                      >> 2U)))) & (
                                                   (0x3fffffffU 
                                                    & ((IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                                >> 2U)) 
                                                       >> 1U)) 
                                                   | (~ (IData)(
                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                                 >> 3U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in 
        = (1U & ((0x20000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U])
                  ? ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a) 
                       | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b))) 
                      & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                  >> 1U)) | (~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 1U))))) 
                     | ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                 >> 1U)) & (~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 1U)))))
                  : ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a) 
                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b)) 
                      | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a)) 
                         & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b)))) 
                     & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                  >> 1U)) & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 1U))) 
                        | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                       >> 1U))) & (~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 1U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x8000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                   >> 2U)) >> 0xfU) 
                         & ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                        >> 0x20U))) 
                            << 0xfU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xbfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x4000U & ((0x1c000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xeU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1eU))) 
                                        << 0xeU)) & 
                                      ((0xc000U & ((IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                            >> 2U)) 
                                                   >> 0xfU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x1fU))) 
                                          << 0xeU)))) 
                         | (0xc000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xfU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1fU))) 
                                        << 0xeU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xdfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x2000U & ((0x3e000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xdU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1cU))) 
                                        << 0xdU)) & 
                                      ((0x1e000U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xeU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x1dU))) 
                                          << 0xdU)))) 
                         | (0x1e000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xeU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1dU))) 
                                         << 0xdU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xefffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x1000U & ((0x7f000U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xcU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x1aU))) 
                                        << 0xcU)) & 
                                      ((0x3f000U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xdU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x1bU))) 
                                          << 0xcU)))) 
                         | (0x3f000U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xdU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x1bU))) 
                                         << 0xcU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xf7ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x800U & ((0xff800U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 0xbU) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x18U))) 
                                                   << 0xbU)) 
                                     & ((0x7f800U & 
                                         ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                   >> 2U)) 
                                          >> 0xcU)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x19U))) 
                                           << 0xbU)))) 
                        | (0x7f800U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xcU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x19U))) 
                                        << 0xbU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfbffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x400U & ((0x1ffc00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xaU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x16U))) 
                                        << 0xaU)) & 
                                      ((0xffc00U & 
                                        ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xbU)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 0x17U))) 
                                          << 0xaU)))) 
                        | (0xffc00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 0xbU) & 
                                       ((~ (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                    >> 0x17U))) 
                                        << 0xaU))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfdffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x200U & ((0x3ffe00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 9U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x14U))) 
                                                  << 9U)) 
                                      & ((0x1ffe00U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 0xaU)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x15U))) 
                                            << 9U)))) 
                        | (0x1ffe00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 0xaU) & 
                                        ((~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 0x15U))) 
                                         << 9U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfeffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x100U & ((0x7fff00U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 8U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x12U))) 
                                                  << 8U)) 
                                      & ((0x3fff00U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 9U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0x13U))) 
                                            << 8U)))) 
                        | (0x3fff00U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 9U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0x13U))) 
                                                   << 8U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xff7fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x80U & ((0xffff80U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 7U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 0x10U))) 
                                                 << 7U)) 
                                     & ((0x7fff80U 
                                         & ((IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                     >> 2U)) 
                                            >> 8U)) 
                                        | ((~ (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                       >> 0x11U))) 
                                           << 7U)))) 
                       | (0x7fff80U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 8U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0x11U))) 
                                                  << 7U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xffbfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x40U & ((0x1ffffc0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 6U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xeU))) 
                                                  << 6U)) 
                                      & ((0xffffc0U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 7U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0xfU))) 
                                            << 6U)))) 
                       | (0xffffc0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 7U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xfU))) 
                                                  << 6U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xffdfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x20U & ((0x3ffffe0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 5U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xcU))) 
                                                  << 5U)) 
                                      & ((0x1ffffe0U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 6U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0xdU))) 
                                            << 5U)))) 
                       | (0x1ffffe0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 6U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xdU))) 
                                                   << 5U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xffefU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x10U & ((0x7fffff0U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 4U) & ((~ (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                              >> 0xaU))) 
                                                  << 4U)) 
                                      & ((0x3fffff0U 
                                          & ((IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                      >> 2U)) 
                                             >> 5U)) 
                                         | ((~ (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                        >> 0xbU))) 
                                            << 4U)))) 
                       | (0x3fffff0U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                  >> 2U)) 
                                         >> 5U) & (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 0xbU))) 
                                                   << 4U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfff7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (8U & ((0xffffff8U & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                              >> 2U)) 
                                     >> 3U) & ((~ (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                           >> 8U))) 
                                               << 3U)) 
                                   & ((0x7fffff8U & 
                                       ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                 >> 2U)) 
                                        >> 4U)) | (
                                                   (~ (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                               >> 9U))) 
                                                   << 3U)))) 
                    | (0x7fffff8U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 4U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 9U))) 
                                                << 3U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfffbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (4U & ((0x1ffffffcU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 2U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 6U))) 
                                                << 2U)) 
                                    & ((0xffffffcU 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 3U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 7U))) 
                                          << 2U)))) 
                    | (0xffffffcU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 3U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 7U))) 
                                                << 2U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfffdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (2U & ((0x3ffffffeU & ((((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                               >> 2U)) 
                                      >> 1U) & ((~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 4U))) 
                                                << 1U)) 
                                    & ((0x1ffffffeU 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 2U)) 
                                       | ((~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 5U))) 
                                          << 1U)))) 
                    | (0x1ffffffeU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 2U) & ((~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 5U))) 
                                                 << 1U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0xfffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (1U & ((0x7fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                              >> 2U)) 
                                     & (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                   >> 2U)))) 
                                    & ((0x3fffffffU 
                                        & ((IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                    >> 2U)) 
                                           >> 1U)) 
                                       | (~ (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                     >> 3U)))))) 
                    | (0x3fffffffU & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                                >> 2U)) 
                                       >> 1U) & (~ (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                            >> 3U))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
           | (0x8000U & ((0xffff8000U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a)) 
                         ^ (0xf8000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                        >> 0xcU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
        = (0x7fffffffU & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                   >> 2U)) & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                      >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b 
        = (0x7fffffffU & ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a 
                                      >> 2U))) & (~ (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b 
                                                             >> 2U)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop 
        = (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
              & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jal_jalr_ex))) 
             & (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                           >> 0x28U)))) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                                           >> 2U)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
              >> 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
        = ((0xffffffe0U & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                                     << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex)))) 
                           << 5U)) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken_ex) 
                                       << 4U) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
                                                  << 3U) 
                                                 | ((4U 
                                                     & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jal_jalr_ex)) 
                                                        << 2U)) 
                                                    | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return) 
                                                        << 1U) 
                                                       | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_call))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
        = ((0x1fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex)))) 
                     >> 0x1bU)) | (0xffffffe0U & ((IData)(
                                                          ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex))) 
                                                           >> 0x20U)) 
                                                  << 5U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
        = (0x1fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex)) 
                               << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex))) 
                             >> 0x20U)) >> 0x1bU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
           & ((0x7fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                               << 4U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                         >> 0x1cU))) 
              != (0x7fffffffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex 
                                 >> 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result 
        = (0x1ffffffffULL & ((((0x3fffffffeULL & ((
                                                   (8U 
                                                    & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                    ? 
                                                   ((4U 
                                                     & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                     ? 
                                                    (((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                      << 0x13U) 
                                                     | ((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                        >> 0xdU))
                                                     : 
                                                    ((((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x13U) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                         >> 0xdU)) 
                                                     & (((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                         << 0x34U) 
                                                        | (((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                            << 0x14U) 
                                                           | ((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                              >> 0xcU)))))
                                                    : 
                                                   ((4U 
                                                     & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                     ? 
                                                    ((((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x13U) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                         >> 0xdU)) 
                                                     | (((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                         << 0x34U) 
                                                        | (((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                            << 0x14U) 
                                                           | ((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                              >> 0xcU))))
                                                     : 
                                                    ((((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x13U) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                         >> 0xdU)) 
                                                     ^ 
                                                     (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                       << 0x34U) 
                                                      | (((QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                          << 0x14U) 
                                                         | ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                            >> 0xcU)))))) 
                                                  << 1U)) 
                               | (QData)((IData)((1U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                     >> 6U))))) 
                              + ((((8U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                    ? ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                        ? (0x1ffffffffULL 
                                           & ((((QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[3U])) 
                                                << 0x34U) 
                                               | (((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[2U])) 
                                                   << 0x14U) 
                                                  | ((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U])) 
                                                     >> 0xcU))) 
                                              ^ (- (QData)((IData)(
                                                                   (1U 
                                                                    & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                       >> 6U)))))))
                                        : 0ULL) : 0ULL) 
                                  << 1U) | (QData)((IData)(
                                                           (1U 
                                                            & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                               >> 6U)))))) 
                             >> 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret_next 
        = (0x1ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
                             + (QData)((IData)((3U 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.potential_push 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear) 
                  & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index) 
                        >> 5U))) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                                    >> 5U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict 
        = (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse
           [0U] & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                   >> 8U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict 
        = (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.inuse
           [1U] & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                   >> 7U));
    vlTOPp->instruction_bram_addr = (0x3fffffffU & 
                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                                      >> 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next)) 
           | ((((IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) 
                << 3U) << 1U) | ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
                                   & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_branch)) 
                                  << 3U) | (((8U == 
                                              (0xfU 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                                                  >> 0x1cU))) 
                                             << 2U) 
                                            | (2U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                                >> 5U))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.new_request 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
           & (8U == (0xfU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                             >> 0x1cU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x8000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xfU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0x10U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xbfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x4000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xeU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0xfU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xdfffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x2000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xdU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0xeU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xefffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x1000U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                           | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                          >> 0xcU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                       | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                      >> 0xdU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xf7ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x800U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 0xbU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                     >> 0xcU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfbffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x400U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 0xaU) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                     >> 0xbU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfdffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x200U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 9U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                    | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                   >> 0xaU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfeffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x100U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                          | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                         >> 8U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                    | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                   >> 9U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xff7fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x80U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 7U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 8U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xffbfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x40U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 6U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 7U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xffdfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x20U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 5U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 6U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xffefU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x10U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                         | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                        >> 4U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                   | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                                  >> 5U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfff7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (8U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                     >> 3U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                               >> 4U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfffbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (4U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                     >> 2U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                               >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfffdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (2U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                     >> 1U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                                | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                               >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0xfffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                     | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                    & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a 
                        | vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b) 
                       >> 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a 
        = ((0x7fffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
           | (0x8000U & ((0xffff8000U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a)) 
                         ^ (0xf8000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                        >> 0xcU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way 
        = (3U & ((- (IData)((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                                   >> 3U)))) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = ((0x7ffffe3U & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
           | (0x1cU & (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                       << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = ((0x7fffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
           | ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex))
               ? ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex))
                   ? ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                       ? 3U : 2U) : ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                                      ? 3U : 1U)) : 
              ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex))
                ? ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                    ? 2U : 0U) : ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                                   ? 1U : 0U))));
    if ((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                  >> 2U)))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
            = ((0x7fffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
               | ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U])
                   ? 3U : 0U));
    }
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__pc 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
            << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                         >> 5U));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__Vfuncout 
        = (0x1fffffU & (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__pc 
                        >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = ((0x400001fU & vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry) 
           | (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__Vfuncout 
              << 5U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override)
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc
            : ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush)
                ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                                 >> 5U)) : (((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
                                             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)))
                                             ? ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_return)
                                                 ? 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram
                                                [vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index]
                                                 : 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc
                                                [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way])
                                             : ((IData)(4U) 
                                                + vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.push 
        = ((((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
               & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                  [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                  >> 2U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush))) 
             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
        = ((2U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
            ? (IData)((0x7fffffffffffffffULL & (((0x10U 
                                                  & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                                                  ? 
                                                 ((QData)((IData)(
                                                                  ((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
                                                                    << 0x14U) 
                                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                      >> 0xcU)))) 
                                                  << 0x1fU)
                                                  : 
                                                 (((QData)((IData)(
                                                                   (0x7fffffffU 
                                                                    & (- (IData)(
                                                                                (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                                >> 5U))))))) 
                                                   << 0x20U) 
                                                  | (QData)((IData)(
                                                                    ((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
                                                                      << 0x14U) 
                                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                        >> 0xcU)))))) 
                                                >> 
                                                (0x1fU 
                                                 & (((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[1U] 
                                                      << 0x19U) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                        >> 7U)) 
                                                    ^ 
                                                    (- (IData)(
                                                               (1U 
                                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
                                                                   >> 4U)))))))))
            : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
        = ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result) 
           | (0xfffffffeU & (vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
                             & ((- (IData)((1U & (~ 
                                                  vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result 
        = ((0xfffffffeU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result) 
           | (1U & ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U])
                     ? (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result 
                                >> 0x20U)) : vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result)));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids_retired
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready 
        = (0x7fU & (- (IData)((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict)) 
                                     & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready 
        = ((0x7dU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready)) 
           | (2U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict)) 
                    << 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xfffff0ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xffffff00U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict) 
                              << 0xbU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table
                                          [(0x1fU & 
                                            ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                              << 0x18U) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                >> 8U)))] 
                                          << 8U))));
    vlTOPp->instruction_bram_en = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.new_request;
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken 
        = (1U & (((1U & (((0x20000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U])
                           ? ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a) 
                                << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in)) 
                              + (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b) 
                                  << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in)))
                           : ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a) 
                                << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in)) 
                              + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in))) 
                         >> 0x10U)) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                       >> 0x17U)) | 
                 (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                  >> 0x18U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way 
        = ((- (IData)((1U & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                                 >> 2U)) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex) 
                                             >> 4U) 
                                            ^ (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
                                               >> 1U)))))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
            | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst 
        = (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_fetch_id_base 
            = (7U & ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U])
                      ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                          << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                       >> 3U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_pc_id_base 
            = (7U & ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U])
                      ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                          << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                       >> 3U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id)));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_fetch_id_base 
            = (7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id));
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_pc_id_base 
            = (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)
                      ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id)
                      : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst 
        = (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
           | ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result) 
                & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid)) 
               & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
                  >> 3U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__new_index 
        = (7U & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
                    & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
                       >> 3U)) ? vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram
                   [vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__read_index]
                    : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index)) 
                  + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.push)) 
                 - ((((((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
                        & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_return)) 
                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush))) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override))) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request)) 
                    & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_id_stall 
        = (1U & (((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                      >> 2U)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count) 
                                 >> 3U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__illegal_instruction 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__illegal_instruction_pattern_r) 
             & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                >> 2U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold))) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
             & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                >> 2U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                           >> 6U)) & (0U != (0x1fU 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                 << 0x10U) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                   >> 0x10U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)) 
           | (8U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                      << 2U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state)) 
                    & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
                       << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid 
        = (1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                   >> 2U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold))) 
                 & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid 
        = (1U & (((((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                     >> 0xdU) & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                    >> 0xcU))) & ((IData)(
                                                          (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                           [0U] 
                                                           >> 1U)) 
                                                  | ((IData)(
                                                             (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                              [0U] 
                                                              >> 3U)) 
                                                     & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                        >> 0x13U)))) 
                  & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                             [0U] >> 6U))) | ((((~ 
                                                 ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                   >> 0xdU) 
                                                  & (~ 
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                      >> 0xcU)))) 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                           [0U] 
                                                           >> 2U))) 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                          [0U] 
                                                          >> 6U))) 
                                              & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                         [0U] 
                                                         >> 7U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x7dU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (2U & ((((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                          >> 0x1fU)) | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress))) 
                     << 1U) & ((0xffffeU & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                            >> 0xcU)) 
                               | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full)) 
                                  << 1U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
        = (((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[2U] 
             << 3U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                       >> 0x1dU)) + VL_EXTENDS_II(32,12, 
                                                  (0xfffU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                                       << 0xfU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                         >> 0x11U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc 
        = (((0x800000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U])
             ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[3U] 
                 << 2U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[2U] 
                           >> 0x1eU)) : ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                                          << 5U) | 
                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                          >> 0x1bU))) 
           + ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken)
               ? VL_EXTENDS_II(32,21, (0x1fffffU & 
                                       vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]))
               : 4U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_instruction_stall 
        = (1U & (((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_id_stall)) 
                  & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                        >> 2U))) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index 
            = (0x1fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index));
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_data 
            = (0x1fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)
                         ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                             << 0x10U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                          >> 0x10U))
                         : ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                             << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                          >> 0xaU))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_data 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)
                ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_phys_addr)
                : (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.data_out));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
           & (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__.request) 
              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__.request;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | (0xfffffffeU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
                                 << 1U)) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy)) 
                                            << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                    >> 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | (0xfffffffcU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
                                 << 2U)) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy)) 
                                            << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                    >> 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks)) 
           | (0xfffffff8U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready) 
                                 << 3U)) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy)) 
                                            << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests) 
                    >> 3U)));
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
        = (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                  << 0x1fU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                               >> 1U)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
            = (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                      << 0xaU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                                  >> 0x16U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
            = (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                      << 0x15U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                   >> 0xbU)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id 
            = (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U]);
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
        = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                     << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                                  >> 3U)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                                   >> 0x18U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                         << 0x13U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                      >> 0xdU)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                         << 0x1eU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                                      >> 2U)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
        = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                    << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                                 >> 0xaU)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
            = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                        << 1U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                                  >> 0x1fU)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
            = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                        << 0xcU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                    >> 0x14U)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be 
            = (0xfU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                        << 0x17U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                                     >> 9U)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                 >> 8U));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                     >> 0x1dU));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                     >> 0x12U));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                     >> 7U));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
        = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
                           << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                                        >> 0xeU)));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
            = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                               << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[3U] 
                                            >> 3U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
            = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                               << 8U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                                         >> 0x18U)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr 
            = (0x3fffffffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                               << 0x13U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                                            >> 0xdU)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
                 >> 9U));
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
                     >> 0x1eU));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
                     >> 0x13U));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests))) {
        vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[0U] 
                     >> 8U));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall 
        = (((0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid)) 
           & (~ (IData)((0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_unit_stall 
        = (((~ (IData)((0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                               & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready))))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid)) 
           & (0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[0U] 
        = ((0x3eU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [0U]) | (1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                             >> 2U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage)) 
                           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be = 0U;
    if ((0U == (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                       << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                    >> 0xeU))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
               | ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address)));
    } else {
        if ((1U == (3U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                           << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                        >> 0xeU))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be 
                = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
                   | ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address)));
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be 
                = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
                   | ((IData)(1U) << (1U | (2U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address))));
        } else {
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be = 0xfU;
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__new_entry 
        = (((QData)((IData)((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))))) 
            << 0x23U) | (((QData)((IData)((0x7fffU 
                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                              >> 0x11U)))) 
                          << 0x14U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.upper_physical_address))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr 
        = (1U & ((1U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                             >> 0xeU))))
                  ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address
                  : ((5U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                    << 0x12U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                 >> 0xeU))))
                      ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address
                      : ((2U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                        << 0x12U) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                        >> 0xeU)))) 
                         & (0U != (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                    >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                    >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state;
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
        if (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress) 
             & (~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                    >> 2U) & (~ (IData)((0U != (3U 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking))))))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 2U;
        }
    } else {
        if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
            if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 4U;
            }
        } else {
            if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
                if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid) 
                     & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                           >> 2U)))) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state 
                        = ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                             [0U])) 
                                  | ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                 [0U] 
                                                 >> 1U))) 
                                     & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                [0U] 
                                                >> 2U)))))
                            ? 0x40U : ((1U & ((IData)(
                                                      vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                      [0U]) 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                          [0U] 
                                                          >> 1U)) 
                                                 | (IData)(
                                                           (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                            [0U] 
                                                            >> 3U)))))
                                        ? (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid) 
                                            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid))
                                            ? 0x20U
                                            : 0x40U)
                                        : 8U));
                }
            } else {
                if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
                    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 0x10U;
                    }
                } else {
                    if ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))) {
                        if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid) 
                             & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                                   >> 2U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state 
                                = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid) 
                                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid))
                                    ? 0x20U : 0x40U);
                        }
                    } else {
                        if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                    >> 5U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                              >> 6U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = 1U;
                        }
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state;
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
        if (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress) 
             & (~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                    >> 2U) & (~ (IData)((0U != (3U 
                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking))))))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 2U;
        }
    } else {
        if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
            if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 4U;
            }
        } else {
            if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
                if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid) 
                     & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                           >> 2U)))) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state 
                        = ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                             [0U])) 
                                  | ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                 [0U] 
                                                 >> 1U))) 
                                     & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                [0U] 
                                                >> 2U)))))
                            ? 0x40U : ((1U & ((IData)(
                                                      vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                      [0U]) 
                                              & ((IData)(
                                                         (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                          [0U] 
                                                          >> 1U)) 
                                                 | (IData)(
                                                           (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                            [0U] 
                                                            >> 3U)))))
                                        ? (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid) 
                                            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid))
                                            ? 0x20U
                                            : 0x40U)
                                        : 8U));
                }
            } else {
                if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
                    if ((8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks))) {
                        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 0x10U;
                    }
                } else {
                    if ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))) {
                        if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid) 
                             & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                                   >> 2U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state 
                                = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid) 
                                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid))
                                    ? 0x20U : 0x40U);
                        }
                    } else {
                        if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                    >> 5U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                              >> 6U)))) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 1U;
                        }
                    }
                }
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = 1U;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7fffffffffcULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | (IData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.sub_id)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7ffffffff83ULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.amo_type_or_burst_size)) 
                     << 2U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7ffffffe1ffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.be)) 
                     << 9U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7ffffffff7fULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.is_amo)) 
                     << 7U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x1fffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.addr)) 
                     << 0xdU));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[0U] 
        = ((0x7fffffffeffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rnw)) 
                     << 8U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released) 
                    | (((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids)) 
                        == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                        [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                       [0U]))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released) 
                    | (((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids)) 
                        == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                        [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                       [1U]))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (2U & ((0xfffffffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 3U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [0U]) << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (2U & ((0xfffffffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 3U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [1U]) << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (4U & ((0xfffffffcU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 6U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [0U]) << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (4U & ((0xfffffffcU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 6U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [1U]) << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = (7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (8U & ((0xfffffff8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 9U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [0U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [0U]) << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
           | (8U & ((0xfffffff8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    | ((((7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids) 
                                >> 9U)) == vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids
                         [1U]) & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired
                        [1U]) << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold)) 
                 & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                        >> 2U)) | (0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                                          & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid) 
           & (0U != ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to 
        = (((- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid))) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = (0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j = 0U;
    {
        while (VL_GTS_III(1,32,32, 0U, vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) {
            if (((5U >= (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) 
                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                    [0U] >> (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[0U] 
                    = (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
                goto __Vlabel1;
            }
            vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j 
                = ((IData)(1U) + vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
        }
        __Vlabel1: ;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = ((0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [0U]) | ((QData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                       [0U]) & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                                                [0U] 
                                                >> 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                [0U])))) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = ((0x1ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [0U]) | ((QData)((IData)(((0x11U >= (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [0U])))
                                       ? (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
                                                [0U] 
                                                >> 
                                                (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [0U]))))
                                       : 0U))) << 0x21U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[0U] 
        = ((0xf00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [0U]) | (IData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                      [0U]) ? vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd
                                     [0U][vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                     [0U]] : 0U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                         [0U] >> 0x20U)));
    if (VL_LIKELY((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                   [0U]))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[0U] 
            = (((~ ((IData)(1U) << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                    [0U])) & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                [0U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1) 
                         << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                         [0U]));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = (0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
           [1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[1U] = 4U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j = 0U;
    {
        while (VL_GTS_III(1,32,32, 4U, vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) {
            if (((5U >= (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)) 
                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                    [1U] >> (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[1U] 
                    = (7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
                goto __Vlabel2;
            }
            vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j 
                = ((IData)(1U) + vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j);
        }
        __Vlabel2: ;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = ((0xeffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [1U]) | ((QData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                       [1U]) & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
                                                [1U] 
                                                >> 
                                                vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                [1U])))) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = ((0x1ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [1U]) | ((QData)((IData)(((0x11U >= (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [1U])))
                                       ? (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
                                                [1U] 
                                                >> 
                                                (0x1fU 
                                                 & ((IData)(3U) 
                                                    * 
                                                    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                                    [1U]))))
                                       : 0U))) << 0x21U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[1U] 
        = ((0xf00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
            [1U]) | (IData)((IData)(((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                      [1U]) ? vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd
                                     [1U][vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                                     [1U]] : 0U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                         [1U] >> 0x20U)));
    if (VL_LIKELY((5U >= vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                   [1U]))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[1U] 
            = (((~ ((IData)(1U) << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                    [1U])) & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1) 
                         << vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel
                         [1U]));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr
        [0U]];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
            >> 2U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
              >> 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_flush_required 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
            >> 5U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__potential_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__potential_branch_exception 
        = (1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc 
                  >> 1U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                            >> 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
        = ((0xffffffc7U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           | (0xfffffff8U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_csr_r) 
                              << 5U) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_fence) 
                                         << 4U) | (0x3ffffff8U 
                                                   & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                                       >> 2U) 
                                                      & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ifence_r) 
                                                         << 3U)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
        = (((QData)((IData)(((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                >> 1U)))) << 0x28U) 
           | (((QData)((IData)(((0x1000U & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U])
                                 ? 6U : 4U))) << 0x23U) 
              | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address)) 
                  << 3U) | (QData)((IData)((7U & ((
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                     >> 3U))))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count) 
                  >> 3U) & (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                            [1U] >> 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance 
        = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                  [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                           [1U] >> 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data[0U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data[1U][0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid 
        = ((((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd)) 
           & (0U != (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xaU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
              >> 9U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[0U] 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd) 
            & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
               >> 9U)) & (0U != (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                           << 0x16U) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                             >> 0xaU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state;
    if ((0U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 1U;
    } else {
        if ((1U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 2U;
        } else {
            if ((2U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
                                   >> 0x3fU)))) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 3U;
                }
            } else {
                if ((3U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                    if ((1U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                         >> 0x28U)) 
                                | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__potential_branch_exception)) 
                               | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__system_op_or_exception_complete)))) {
                        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 5U;
                    }
                } else {
                    if ((4U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                        if ((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter)) {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 3U;
                        }
                    } else {
                        if ((5U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state)) {
                            if (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_is_idle) {
                                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 3U;
                            }
                        } else {
                            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 0U;
                        }
                    }
                }
            }
        }
    }
    if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                       >> 0x28U)))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
            = ((0x107ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
               | ((QData)((IData)((0x1fU & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                                    >> 0x23U))))) 
                  << 0x23U));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
            = ((0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
               | ((QData)((IData)((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                           >> 3U)))) 
                  << 3U));
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__illegal_instruction) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                = (0x1000000000ULL | (0x107ffffffffULL 
                                      & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception));
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                = ((0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                   | ((QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U] 
                                        << 0x1aU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
                                        >> 6U)))) << 3U));
        } else {
            if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                               >> 0x28U)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                    = ((0x107ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                       | ((QData)((IData)((0x1fU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                                            >> 0x23U))))) 
                          << 0x23U));
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                    = ((0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                       | ((QData)((IData)((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                                   >> 3U)))) 
                          << 3U));
            } else {
                if ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U])) {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = ((0x107ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
                           | ((QData)((IData)(((0U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                ? 8U
                                                : (
                                                   (1U 
                                                    == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                    ? 9U
                                                    : 
                                                   ((3U 
                                                     == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                     ? 0xbU
                                                     : 8U))))) 
                              << 0x23U));
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = (0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception);
                } else {
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = (0x1800000000ULL | (0x107ffffffffULL 
                                              & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception));
                    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                        = (0x1f800000007ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception);
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
        = ((0xffffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception) 
           | ((QData)((IData)((1U & ((((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                         >> 5U) & (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                                    >> 2U) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                                      >> 1U))) 
                                       | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                                                  >> 0x28U))) 
                                      | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                                 >> 0x28U))) 
                                     | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__illegal_instruction))))) 
              << 0x28U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance 
        = (1U & ((~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                  [0U]) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance)));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__wb_packet
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data
        [0U][0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data
        [1U][0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rd_addr 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid)
            ? (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.data_out)
            : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid) 
            | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[1U] 
        = (7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                         [1U] >> 0x21U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[1U] 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                         [1U] >> 0x20U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x7fffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | ((QData)((IData)((7U & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                                    [0U] 
                                                    >> 0x21U))))) 
                     << 0x27U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x3bfffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | ((QData)((IData)(((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                               [0U] 
                                               >> 0x20U)) 
                                      & (0U != (0x3fU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
                                                           [0U] 
                                                           >> 0x20U))))))) 
                     << 0x26U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x3ff00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                    [0U])));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x7fffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | ((QData)((IData)((7U & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                                    [1U] 
                                                    >> 0x21U))))) 
                     << 0x27U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x3bfffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | ((QData)((IData)(((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                               [1U] 
                                               >> 0x20U)) 
                                      & (0U != (0x3fU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
                                                           [1U] 
                                                           >> 0x20U))))))) 
                     << 0x26U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x3ff00000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                    [1U])));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[0U] 
        = ((0x3c0ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [0U]) | ((QData)((IData)((0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                << 0x16U) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                  >> 0xaU))))) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[1U] 
        = ((0x3c0ffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
            [1U]) | ((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table
                                     [(7U & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet
                                                     [1U] 
                                                     >> 0x21U)))])) 
                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
           | ((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                  [0U] >> 0x14U))) 
              == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                        >> 0x11U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
           | (((0xffffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                   [1U] >> 0x14U))) 
               == (0x8000U | (0x7fffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                         >> 0x11U)))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] : vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet
        [1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                   >> 1U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr))) 
                 & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                        >> 0x1fU)) | ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
                                      & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.new_request) 
                                         | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__mmu_request_complete))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
        = ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
           | (0xfffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
        = (0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address);
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                            [0U]) << 0xcU))));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address 
            = ((0xfffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
               | (0xfffff000U & ((0xfffff000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address) 
                                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry
                                            [1U]) << 0xcU))));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__commit_packet
        [1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.push 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
              >> 0xdU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
              >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.new_issue) 
           & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                 >> 0xcU)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp)
            ? vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.physical_address
            : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[1U] 
        = (0x3fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                            [1U] >> 0x20U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[1U] 
        = (1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                         [1U] >> 0x26U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot) 
           | ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot) 
           & (- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (1U & (((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                      >> 2U) ^ (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                >> 6U)) ^ (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                           >> 0xaU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (2U & (((0x3ffffffeU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                     >> 2U)) ^ (0x3fffffeU 
                                                & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                                   >> 6U))) 
                    ^ (0x3ffffeU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                    >> 0xaU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (4U & ((0x3ffffffcU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                    >> 2U)) ^ (0x3fffffcU 
                                               & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                                  >> 6U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash)) 
           | (8U & ((0x3ffffff8U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                    >> 2U)) ^ (0x3fffff8U 
                                               & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr 
                                                  >> 6U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)
            : vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[0U] 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use
           [0U] : vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
               & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot))) 
              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                 == (0xfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out) 
              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                  + (1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting))) 
                 - (1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0xff8U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (0xfffffffeU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                              & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot) 
                                     >> 1U)) << 1U)) 
                             & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                                 == (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes) 
                                             >> 4U))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | (0xfffffffeU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | (((IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                        >> 1U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                   >> 3U) + (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting) 
                                   >> 1U))) - (1U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed) 
                                                >> 1U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0xfc7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1) 
              << 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (0xfffffffcU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                              & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot) 
                                     >> 2U)) << 2U)) 
                             & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                                 == (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes) 
                                             >> 8U))) 
                                << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | (0xfffffffcU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request) 
                                << 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | (((IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                        >> 2U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
              << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                   >> 6U) + (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting) 
                                   >> 2U))) - (1U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed) 
                                                >> 2U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0xe3fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1) 
              << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts)) 
           | (0xfffffff8U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                              & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot) 
                                     >> 3U)) << 3U)) 
                             & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                                 == (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes) 
                                             >> 0xcU))) 
                                << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting)) 
           | (0xfffffff8U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts) 
                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request) 
                                << 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed)) 
           | (((IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                        >> 3U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
              << 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 
        = (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                   >> 9U) + (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting) 
                                   >> 3U))) - (1U & 
                                               ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed) 
                                                >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next 
        = ((0x1ffU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1) 
              << 9U));
}

void Vtaiga_sim::_eval_initial(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_eval_initial\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__3(vlSymsp);
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
    vlTOPp->__Vclklast__TOP____VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni 
        = vlTOPp->__VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni;
}

void Vtaiga_sim::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::final\n"); );
    // Variables
    Vtaiga_sim__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vtaiga_sim::_eval_settle(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_eval_settle\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__1(vlSymsp);
    vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux._settle__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__1(vlSymsp);
    vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux._settle__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux__2(vlSymsp);
    vlTOPp->_settle__TOP__2(vlSymsp);
    vlTOPp->_settle__TOP__9(vlSymsp);
}

void Vtaiga_sim::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_ctor_var_reset\n"); );
    // Body
    clk = VL_RAND_RESET_I(1);
    rst = VL_RAND_RESET_I(1);
    ddr_axi_awvalid_muir = VL_RAND_RESET_I(1);
    ddr_axi_awready_muir = VL_RAND_RESET_I(1);
    ddr_axi_awaddr_muir = VL_RAND_RESET_I(32);
    ddr_axi_awid_muir = VL_RAND_RESET_I(6);
    ddr_axi_awlen_muir = VL_RAND_RESET_I(8);
    ddr_axi_awsize_muir = VL_RAND_RESET_I(3);
    ddr_axi_awburst_muir = VL_RAND_RESET_I(2);
    ddr_axi_awcache_muir = VL_RAND_RESET_I(4);
    ddr_axi_wvalid_muir = VL_RAND_RESET_I(1);
    ddr_axi_wready_muir = VL_RAND_RESET_I(1);
    ddr_axi_wdata_muir = VL_RAND_RESET_I(32);
    ddr_axi_wstrb_muir = VL_RAND_RESET_I(4);
    ddr_axi_wlast_muir = VL_RAND_RESET_I(1);
    ddr_axi_wid_muir = VL_RAND_RESET_I(6);
    ddr_axi_bvalid_muir = VL_RAND_RESET_I(1);
    ddr_axi_bready_muir = VL_RAND_RESET_I(1);
    ddr_axi_bresp_muir = VL_RAND_RESET_I(2);
    ddr_axi_bid_muir = VL_RAND_RESET_I(6);
    ddr_axi_arvalid_muir = VL_RAND_RESET_I(1);
    ddr_axi_arready_muir = VL_RAND_RESET_I(1);
    ddr_axi_araddr_muir = VL_RAND_RESET_I(32);
    ddr_axi_arid_muir = VL_RAND_RESET_I(6);
    ddr_axi_arlen_muir = VL_RAND_RESET_I(8);
    ddr_axi_arsize_muir = VL_RAND_RESET_I(3);
    ddr_axi_arburst_muir = VL_RAND_RESET_I(2);
    ddr_axi_arcache_muir = VL_RAND_RESET_I(4);
    ddr_axi_rvalid_muir = VL_RAND_RESET_I(1);
    ddr_axi_rready_muir = VL_RAND_RESET_I(1);
    ddr_axi_rdata_muir = VL_RAND_RESET_I(32);
    ddr_axi_rresp_muir = VL_RAND_RESET_I(2);
    ddr_axi_rlast_muir = VL_RAND_RESET_I(1);
    ddr_axi_rid_muir = VL_RAND_RESET_I(6);
    ddr_axi_awvalid_taiga = VL_RAND_RESET_I(1);
    ddr_axi_awready_taiga = VL_RAND_RESET_I(1);
    ddr_axi_awaddr_taiga = VL_RAND_RESET_I(32);
    ddr_axi_awid_taiga = VL_RAND_RESET_I(6);
    ddr_axi_awlen_taiga = VL_RAND_RESET_I(8);
    ddr_axi_awsize_taiga = VL_RAND_RESET_I(3);
    ddr_axi_awburst_taiga = VL_RAND_RESET_I(2);
    ddr_axi_awcache_taiga = VL_RAND_RESET_I(4);
    ddr_axi_wvalid_taiga = VL_RAND_RESET_I(1);
    ddr_axi_wready_taiga = VL_RAND_RESET_I(1);
    ddr_axi_wdata_taiga = VL_RAND_RESET_I(32);
    ddr_axi_wstrb_taiga = VL_RAND_RESET_I(4);
    ddr_axi_wlast_taiga = VL_RAND_RESET_I(1);
    ddr_axi_wid_taiga = VL_RAND_RESET_I(6);
    ddr_axi_bvalid_taiga = VL_RAND_RESET_I(1);
    ddr_axi_bready_taiga = VL_RAND_RESET_I(1);
    ddr_axi_bresp_taiga = VL_RAND_RESET_I(2);
    ddr_axi_bid_taiga = VL_RAND_RESET_I(6);
    ddr_axi_arvalid_taiga = VL_RAND_RESET_I(1);
    ddr_axi_arready_taiga = VL_RAND_RESET_I(1);
    ddr_axi_araddr_taiga = VL_RAND_RESET_I(32);
    ddr_axi_arid_taiga = VL_RAND_RESET_I(6);
    ddr_axi_arlen_taiga = VL_RAND_RESET_I(8);
    ddr_axi_arsize_taiga = VL_RAND_RESET_I(3);
    ddr_axi_arburst_taiga = VL_RAND_RESET_I(2);
    ddr_axi_arcache_taiga = VL_RAND_RESET_I(4);
    ddr_axi_rvalid_taiga = VL_RAND_RESET_I(1);
    ddr_axi_rready_taiga = VL_RAND_RESET_I(1);
    ddr_axi_rdata_taiga = VL_RAND_RESET_I(32);
    ddr_axi_rresp_taiga = VL_RAND_RESET_I(2);
    ddr_axi_rlast_taiga = VL_RAND_RESET_I(1);
    ddr_axi_rid_taiga = VL_RAND_RESET_I(6);
    bus_axi_araddr = VL_RAND_RESET_I(32);
    bus_axi_arburst = VL_RAND_RESET_I(2);
    bus_axi_arcache = VL_RAND_RESET_I(4);
    bus_axi_arid = VL_RAND_RESET_I(6);
    bus_axi_arlen = VL_RAND_RESET_I(8);
    bus_axi_arlock = VL_RAND_RESET_I(1);
    bus_axi_arprot = VL_RAND_RESET_I(3);
    bus_axi_arqos = VL_RAND_RESET_I(4);
    bus_axi_arready = VL_RAND_RESET_I(1);
    bus_axi_arregion = VL_RAND_RESET_I(4);
    bus_axi_arsize = VL_RAND_RESET_I(3);
    bus_axi_arvalid = VL_RAND_RESET_I(1);
    bus_axi_awaddr = VL_RAND_RESET_I(32);
    bus_axi_awburst = VL_RAND_RESET_I(2);
    bus_axi_awcache = VL_RAND_RESET_I(4);
    bus_axi_awid = VL_RAND_RESET_I(6);
    bus_axi_awlen = VL_RAND_RESET_I(8);
    bus_axi_awlock = VL_RAND_RESET_I(1);
    bus_axi_awprot = VL_RAND_RESET_I(3);
    bus_axi_awqos = VL_RAND_RESET_I(4);
    bus_axi_awready = VL_RAND_RESET_I(1);
    bus_axi_awregion = VL_RAND_RESET_I(4);
    bus_axi_awsize = VL_RAND_RESET_I(3);
    bus_axi_awvalid = VL_RAND_RESET_I(1);
    bus_axi_bid = VL_RAND_RESET_I(6);
    bus_axi_bready = VL_RAND_RESET_I(1);
    bus_axi_bresp = VL_RAND_RESET_I(2);
    bus_axi_bvalid = VL_RAND_RESET_I(1);
    bus_axi_rdata = VL_RAND_RESET_I(32);
    bus_axi_rid = VL_RAND_RESET_I(6);
    bus_axi_rlast = VL_RAND_RESET_I(1);
    bus_axi_rready = VL_RAND_RESET_I(1);
    bus_axi_rresp = VL_RAND_RESET_I(2);
    bus_axi_rvalid = VL_RAND_RESET_I(1);
    bus_axi_wdata = VL_RAND_RESET_I(32);
    bus_axi_wlast = VL_RAND_RESET_I(1);
    bus_axi_wready = VL_RAND_RESET_I(1);
    bus_axi_wstrb = VL_RAND_RESET_I(4);
    bus_axi_wvalid = VL_RAND_RESET_I(1);
    bus_axi_wid = VL_RAND_RESET_I(6);
    addr = VL_RAND_RESET_I(30);
    be = VL_RAND_RESET_I(4);
    rnw = VL_RAND_RESET_I(1);
    is_amo = VL_RAND_RESET_I(1);
    amo_type_or_burst_size = VL_RAND_RESET_I(5);
    sub_id = VL_RAND_RESET_I(2);
    request_push = VL_RAND_RESET_I(1);
    request_full = VL_RAND_RESET_I(1);
    inv_addr = VL_RAND_RESET_I(30);
    inv_valid = VL_RAND_RESET_I(1);
    inv_ack = VL_RAND_RESET_I(1);
    con_result = VL_RAND_RESET_I(1);
    con_valid = VL_RAND_RESET_I(1);
    wr_data = VL_RAND_RESET_I(32);
    wr_data_push = VL_RAND_RESET_I(1);
    data_full = VL_RAND_RESET_I(1);
    rd_data = VL_RAND_RESET_I(32);
    rd_sub_id = VL_RAND_RESET_I(2);
    rd_data_valid = VL_RAND_RESET_I(1);
    rd_data_ack = VL_RAND_RESET_I(1);
    instruction_bram_addr = VL_RAND_RESET_I(30);
    instruction_bram_en = VL_RAND_RESET_I(1);
    instruction_bram_be = VL_RAND_RESET_I(4);
    instruction_bram_data_in = VL_RAND_RESET_I(32);
    instruction_bram_data_out = VL_RAND_RESET_I(32);
    data_bram_addr = VL_RAND_RESET_I(30);
    data_bram_en = VL_RAND_RESET_I(1);
    data_bram_be = VL_RAND_RESET_I(4);
    data_bram_data_in = VL_RAND_RESET_I(32);
    data_bram_data_out = VL_RAND_RESET_I(32);
    write_uart = VL_RAND_RESET_I(1);
    uart_byte = VL_RAND_RESET_I(8);
    instruction_issued = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<35; ++__Vi0) {
        taiga_events[__Vi0] = VL_RAND_RESET_I(1);
    }
    instruction_pc_dec = VL_RAND_RESET_I(32);
    instruction_data_dec = VL_RAND_RESET_I(32);
    taiga_sim__DOT__axi_awid = VL_RAND_RESET_I(6);
    taiga_sim__DOT__axi_awvalid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__axi_wlast = VL_RAND_RESET_I(1);
    taiga_sim__DOT__axi_wvalid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__axi_arvalid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__axi_bid_muir = VL_RAND_RESET_I(6);
    VL_RAND_RESET_W(99, taiga_sim__DOT__tr);
    VL_RAND_RESET_W(192, taiga_sim__DOT__xbar_rule_array);
    VL_RAND_RESET_W(370, taiga_sim__DOT__slv_ports_req_array);
    VL_RAND_RESET_W(112, taiga_sim__DOT__mst_ports_resp);
    VL_RAND_RESET_W(370, taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o);
    VL_RAND_RESET_W(112, taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o);
    taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni = VL_RAND_RESET_I(1);
    taiga_sim__DOT____Vlvbound1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__read_modify_write = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__address_phase_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__amo_result = VL_RAND_RESET_I(32);
    taiga_sim__DOT__l2_to_mem__DOT__amo_result_r = VL_RAND_RESET_I(32);
    taiga_sim__DOT__l2_to_mem__DOT__read_count = VL_RAND_RESET_I(2);
    taiga_sim__DOT__l2_to_mem__DOT__amo_write_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count = VL_RAND_RESET_I(5);
    VL_RAND_RESET_W(69, taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs);
    taiga_sim__DOT__l2_to_mem__DOT__write_in_progress = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__write_burst_count = VL_RAND_RESET_I(5);
    taiga_sim__DOT__l2_to_mem__DOT__on_last_burst = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2 = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__requests_in[__Vi0] = VL_RAND_RESET_Q(43);
    }
    taiga_sim__DOT__l2_arb__DOT__advance = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__arb_request = VL_RAND_RESET_Q(43);
    taiga_sim__DOT__l2_arb__DOT__mem_request = VL_RAND_RESET_Q(44);
    taiga_sim__DOT__l2_arb__DOT__reserv_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__reserv_lr = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__reserv_sc = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__reserv_store = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__requests[__Vi0] = VL_RAND_RESET_Q(43);
    }
    taiga_sim__DOT__l2_arb__DOT__reserv_request = VL_RAND_RESET_Q(43);
    taiga_sim__DOT__l2_arb__DOT__reserv_id = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__reserv_id_v = VL_RAND_RESET_I(2);
    taiga_sim__DOT__l2_arb__DOT__write_done = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__burst_count = VL_RAND_RESET_I(5);
    taiga_sim__DOT__l2_arb__DOT__new_attr = VL_RAND_RESET_I(7);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__input_data[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__return_data[__Vi0] = VL_RAND_RESET_Q(34);
    }
    taiga_sim__DOT__l2_arb__DOT__return_push = VL_RAND_RESET_I(2);
    taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort = VL_RAND_RESET_I(1);
    taiga_sim__DOT__l2_arb__DOT__rr__DOT__state = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_Q(44);
    }
    taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address[__Vi0] = VL_RAND_RESET_I(30);
    }
    taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation = VL_RAND_RESET_I(2);
    taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match = VL_RAND_RESET_I(2);
    taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation = VL_RAND_RESET_I(2);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(7);
    }
    taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(5);
    taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(5);
    taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(6);
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_Q(35);
    }
    taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_Q(43);
    }
    taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_Q(43);
    }
    taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    for (int __Vi0=0; __Vi0<16; ++__Vi0) {
        taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(4);
    taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(5);
    VL_RAND_RESET_W(69, taiga_sim__DOT__cpu__DOT__br_results);
    taiga_sim__DOT__cpu__DOT__branch_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__potential_branch_exception = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__br_exception = VL_RAND_RESET_Q(41);
    VL_RAND_RESET_W(119, taiga_sim__DOT__cpu__DOT__issue);
    VL_RAND_RESET_W(110, taiga_sim__DOT__cpu__DOT__alu_inputs);
    VL_RAND_RESET_W(93, taiga_sim__DOT__cpu__DOT__ls_inputs);
    VL_RAND_RESET_W(126, taiga_sim__DOT__cpu__DOT__branch_inputs);
    VL_RAND_RESET_W(66, taiga_sim__DOT__cpu__DOT__mul_inputs);
    VL_RAND_RESET_W(67, taiga_sim__DOT__cpu__DOT__div_inputs);
    VL_RAND_RESET_W(134, taiga_sim__DOT__cpu__DOT__gc_inputs);
    taiga_sim__DOT__cpu__DOT__ls_exception = VL_RAND_RESET_Q(41);
    taiga_sim__DOT__cpu__DOT__pc_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__pc_id_assigned = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__fetch_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_instruction = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__early_branch_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_metadata = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__decode_advance = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(70, taiga_sim__DOT__cpu__DOT__decode);
    taiga_sim__DOT__cpu__DOT__branch_metadata_if = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__branch_metadata_ex = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__retire = VL_RAND_RESET_I(6);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__retire_ids[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__retire_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__wb_packet[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__commit_packet[__Vi0] = VL_RAND_RESET_Q(42);
    }
    taiga_sim__DOT__cpu__DOT__exception_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__gc_init_clear = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_fetch_hold = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_issue_hold = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_issue_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_fetch_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_tlb_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_fetch_pc = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__ls_is_idle = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__illegal_instruction = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__instruction_issued = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_flush_required = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__wb_snoop = VL_RAND_RESET_Q(36);
    taiga_sim__DOT__cpu__DOT__tr_operand_stall = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_unit_stall = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_no_id_stall = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_no_instruction_stall = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_alu_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_branch_or_jump_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_load_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_store_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_mul_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_div_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_misc_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_rs1_forwarding_needed = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_rs2_forwarding_needed = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_rs1_and_rs2_forwarding_needed = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__tr_num_instructions_completing = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__tr_num_instructions_in_flight = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__tr_num_of_instructions_pending_writeback = VL_RAND_RESET_I(3);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellout__id_block__commit_packet[__Vi0] = VL_RAND_RESET_Q(42);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellinp__id_block__wb_packet[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit[__Vi0] = VL_RAND_RESET_Q(42);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table[__Vi0] = VL_RAND_RESET_I(5);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table[__Vi0] = VL_RAND_RESET_I(2);
    }
    taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__id_block__DOT__fetched_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__id_block__DOT__pre_issue_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__id_block__DOT__next_pc_id_base = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__id_block__DOT__next_fetch_id_base = VL_RAND_RESET_I(3);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[__Vi0] = VL_RAND_RESET_I(1);
    }
    taiga_sim__DOT__cpu__DOT__id_block__DOT__contiguous_retire = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__non_rd_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__rd_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[__Vi0] = VL_RAND_RESET_I(3);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<2; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[__Vi0][__Vi1] = VL_RAND_RESET_I(1);
        }
    }
    taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index = VL_RAND_RESET_I(3);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__fetch_block__DOT__unit_data_array[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__exception_pending = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry[__Vi0] = VL_RAND_RESET_I(27);
    }
    taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry = VL_RAND_RESET_I(27);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__read_data = VL_RAND_RESET_I(27);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__read_data = VL_RAND_RESET_I(27);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__read_data = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__read_data = VL_RAND_RESET_I(32);
    for (int __Vi0=0; __Vi0<512; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram[__Vi0] = VL_RAND_RESET_I(27);
    }
    for (int __Vi0=0; __Vi0<512; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram[__Vi0] = VL_RAND_RESET_I(27);
    }
    for (int __Vi0=0; __Vi0<512; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<512; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__ras_block__DOT__new_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(4);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group[__Vi0] = VL_RAND_RESET_I(1);
    }
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_wb_group = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index = VL_RAND_RESET_I(6);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_phys_addr = VL_RAND_RESET_I(6);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_data = VL_RAND_RESET_I(6);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1 = VL_RAND_RESET_I(6);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(6);
    }
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__read_index = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__inflight_count = VL_RAND_RESET_I(6);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_operands_ready = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_ready = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_conflict = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_conflict = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs1_data = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_rs2_data = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs2_r = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs1_r = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_use_regfile = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_use_regfile = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op_r = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_subtract = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_lshift = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_shifter_path = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_slt_path = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__ls_offset = VL_RAND_RESET_I(12);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_load_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_store_r = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_link = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_link = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_return = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_call = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__br_use_signed = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset = VL_RAND_RESET_I(21);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset_r = VL_RAND_RESET_I(21);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_csr_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__potential_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ecall = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ebreak = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ret = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_fence = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ifence_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__illegal_instruction_pattern_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs1_addr = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs2_addr = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_result_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk11__DOT__illegal_op_check__DOT__base_legal = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<2; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__register_file_block__DOT__rs_data_set[__Vi0][__Vi1] = VL_RAND_RESET_I(32);
        }
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<2; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[__Vi0][__Vi1] = VL_RAND_RESET_I(1);
        }
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__id_inuse_toggle_mem_set__in_use[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__0__KET____DOT__reg_group__data[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellout__register_file_gen__BRA__1__KET____DOT__reg_group__data[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___read_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<2; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_data[__Vi0][__Vi1] = VL_RAND_RESET_I(1);
        }
    }
    taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index = VL_RAND_RESET_I(6);
    for (int __Vi0=0; __Vi0<64; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<64; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<64; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<64; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<64; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<64; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken_ex = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__id_ex = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jal_jalr_ex = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jmp_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_call = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_a = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sign_extended_b = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_a = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__eq_b = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_a = VL_RAND_RESET_I(16);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_ls_b = VL_RAND_RESET_I(16);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__sub_eq_a = VL_RAND_RESET_I(16);
    taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__bc__DOT__carry_in = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__add_sub_result = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__alu_unit_block__DOT__result = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_switch_stall = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__ready_for_issue_from_lsq = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data = VL_RAND_RESET_I(32);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__last_unit = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unaligned_addr = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in = VL_RAND_RESET_I(9);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be = VL_RAND_RESET_I(4);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__genblk3__DOT__genblk1__DOT__axi_bus__data_out = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry = VL_RAND_RESET_Q(40);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_ack = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids_retired[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_Q(42);
    }
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes = VL_RAND_RESET_I(16);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed = VL_RAND_RESET_I(12);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids = VL_RAND_RESET_I(12);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count = VL_RAND_RESET_I(12);
    for (int __Vi0=0; __Vi0<4; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<4; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<4; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr[__Vi0] = VL_RAND_RESET_Q(40);
    }
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next = VL_RAND_RESET_I(12);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index_one_hot = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_request = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_load_waiting = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__waiting_load_completed = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr = VL_RAND_RESET_Q(40);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound1 = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound2 = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound3 = VL_RAND_RESET_I(3);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[__Vi0] = VL_RAND_RESET_I(1);
    }
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT__ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__arvalid_m__result = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__awvalid_m__result = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state = 0;
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state = 0;
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__second_cycle_flush = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__system_op_or_exception_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs = VL_RAND_RESET_Q(48);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception = VL_RAND_RESET_Q(41);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r = VL_RAND_RESET_Q(41);
    VL_RAND_RESET_W(134, taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__instruction_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_id = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter = VL_RAND_RESET_I(32);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs[__Vi0] = VL_RAND_RESET_I(32);
    }
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__next_privilege_level = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__stvec = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr = VL_RAND_RESET_I(32);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mideleg = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtval = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle_next = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret_next = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_new = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask = VL_RAND_RESET_I(32);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[__Vi0] = VL_RAND_RESET_I(18);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<6; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[__Vi0][__Vi1] = VL_RAND_RESET_I(32);
        }
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_sel[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unnamedblk1__DOT__unnamedblk2__DOT__j = 0;
    taiga_sim__DOT__cpu__DOT__writeback_block__DOT____Vlvbound1 = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(172, taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests);
    taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__acks = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__busy = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way = VL_RAND_RESET_I(2);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<1; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_data[__Vi0][__Vi1] = VL_RAND_RESET_Q(36);
        }
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__ram_entry[__Vi0] = VL_RAND_RESET_Q(36);
    }
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__new_entry = VL_RAND_RESET_Q(36);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tlb_write = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__mmu_request_complete = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr[__Vi0] = VL_RAND_RESET_I(5);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr[__Vi0] = VL_RAND_RESET_I(5);
    }
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[__Vi0] = VL_RAND_RESET_Q(36);
    }
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1 = VL_RAND_RESET_Q(36);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[__Vi0] = VL_RAND_RESET_Q(36);
    }
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1 = VL_RAND_RESET_Q(36);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__next_state = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__delayed_abort_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way = VL_RAND_RESET_I(2);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        for (int __Vi1=0; __Vi1<1; ++__Vi1) {
            taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_data[__Vi0][__Vi1] = VL_RAND_RESET_Q(36);
        }
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__ram_entry[__Vi0] = VL_RAND_RESET_Q(36);
    }
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__new_entry = VL_RAND_RESET_Q(36);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tlb_write = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__mmu_request_complete = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__0__KET____DOT__ram_block__ram_data_out[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr[__Vi0] = VL_RAND_RESET_I(5);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellout__lut_rams__BRA__1__KET____DOT__ram_block__ram_data_out[__Vi0] = VL_RAND_RESET_Q(36);
    }
    for (int __Vi0=0; __Vi0<1; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr[__Vi0] = VL_RAND_RESET_I(5);
    }
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[__Vi0] = VL_RAND_RESET_Q(36);
    }
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT____Vlvbound1 = VL_RAND_RESET_Q(36);
    for (int __Vi0=0; __Vi0<32; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[__Vi0] = VL_RAND_RESET_Q(36);
    }
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT____Vlvbound1 = VL_RAND_RESET_Q(36);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__next_state = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__access_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__delayed_abort_complete = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_result = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_done = VL_RAND_RESET_I(1);
    for (int __Vi0=0; __Vi0<8; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__read_index = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__result = VL_RAND_RESET_Q(64);
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id[__Vi0] = VL_RAND_RESET_I(3);
    }
    taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__rs1_r = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__rs2_r = VL_RAND_RESET_Q(33);
    taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_dividend = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__unsigned_divisor = VL_RAND_RESET_I(32);
    VL_RAND_RESET_W(71, taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__fifo_inputs);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr = VL_RAND_RESET_I(7);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_done = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT____Vcellinp__in_progress_m__set = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__done_r = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count = VL_RAND_RESET_I(16);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR = VL_RAND_RESET_Q(34);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_1 = VL_RAND_RESET_Q(35);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_2 = VL_RAND_RESET_Q(35);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_3 = VL_RAND_RESET_Q(35);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_1 = VL_RAND_RESET_Q(34);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_2 = VL_RAND_RESET_Q(34);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_3 = VL_RAND_RESET_Q(34);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted_1 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(1110, taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs);
    VL_RAND_RESET_W(336, taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps);
    VL_RAND_RESET_W(740, taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs);
    VL_RAND_RESET_W(224, taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(555, taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o);
    taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o = VL_RAND_RESET_Q(56);
    VL_RAND_RESET_W(175, taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(555, taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o);
    taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o = VL_RAND_RESET_Q(56);
    VL_RAND_RESET_W(175, taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids = VL_RAND_RESET_I(3);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies = VL_RAND_RESET_I(3);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o = VL_RAND_RESET_I(2);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2 = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp = VL_RAND_RESET_Q(46);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1 = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids = VL_RAND_RESET_I(3);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies = VL_RAND_RESET_I(3);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o = VL_RAND_RESET_I(2);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2 = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(76, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(70, taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp = VL_RAND_RESET_Q(46);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q = VL_RAND_RESET_I(1);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q = VL_RAND_RESET_I(2);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1 = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q = VL_RAND_RESET_I(9);
    taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d = VL_RAND_RESET_I(9);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_w_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_r_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_dme_wr_0_ack = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_req_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_bits_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_3 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_0 = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_1 = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_2 = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_done = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT___GEN_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_29 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_31 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_33 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr = VL_RAND_RESET_I(16);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_1 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_2 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_3 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_4 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___GEN_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_10 = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_len = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_addr = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_len = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_addr = VL_RAND_RESET_I(32);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag[__Vi0] = VL_RAND_RESET_Q(50);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_data = VL_RAND_RESET_Q(50);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0 = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag___05FT_447_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7___05FT_14_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7___05FT_24_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7___05FT_34_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7___05FT_44_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7___05FT_54_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7___05FT_64_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7___05FT_74_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    for (int __Vi0=0; __Vi0<256; ++__Vi0) {
        taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7[__Vi0] = VL_RAND_RESET_I(8);
    }
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7___05FT_84_addr_pipe_0 = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state = VL_RAND_RESET_I(3);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_tag_reg = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_3 = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_7 = VL_RAND_RESET_I(4);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_wrap_out = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_11 = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97);
    VL_RAND_RESET_W(512, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__block_rmeta_tag = VL_RAND_RESET_Q(50);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_105 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202);
    VL_RAND_RESET_W(512, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata);
    VL_RAND_RESET_W(512, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_0 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_1 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_2 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_3 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_4 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_5 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_6 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_7 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_8 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_9 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_10 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_11 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_12 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_13 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_15 = VL_RAND_RESET_I(32);
    VL_RAND_RESET_W(512, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224 = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(72, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask);
    VL_RAND_RESET_W(512, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273);
    VL_RAND_RESET_W(256, taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_block_dirty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_352 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_368 = VL_RAND_RESET_I(32);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_490 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_493 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_499 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_505 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_506 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_507 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_509 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_512 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_408 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_513 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_514 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_515 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_517 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_518 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_wr_mem_0_MemResp_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_1_bits_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_2_bits_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_8_bits_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2_io_Out_0_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11_io_Out_0_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_io_out_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_addr = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_mask = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_tag = VL_RAND_RESET_I(8);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb__DOT___T = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field2_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field1_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_dataPtrs_field0_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_28 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_30 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_32 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_34 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_0_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_1_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_R_2_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_0_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_1_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_2_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_6 = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_30 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_31 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_32 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_33 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_34 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_36 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_38 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_40 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_47 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_56 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_76 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_84 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_92 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_293 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_fire_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_fire_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_16 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_20 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_42 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_43 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_44 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_45 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_46 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_47 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_48 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_49 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_50 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_51 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_52 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_53 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_54 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_55 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_56 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_57 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48 = VL_RAND_RESET_I(14);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_129 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_10 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___GEN_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__output_R_enable_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19 = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30 = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_44 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_39 = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_42 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_43 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_44 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_45 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_52 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_59 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138 = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_log_value_ready = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_7 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_10 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__value = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__data_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_44 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_50 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_51 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_62 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_82 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_7 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_10 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__value = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__data_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_44 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_50 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_25 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_51 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_62 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_82 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_7 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_data_R = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_24 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_47 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_7 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_12 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__value = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state = VL_RAND_RESET_I(2);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_38 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_44 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_38 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_47 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_62 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_8 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_19 = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_31 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_62 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_7 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_R_data = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_data_R = VL_RAND_RESET_Q(64);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_11 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_14 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_24 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_47 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_true_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_false_R_0 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_4 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_12 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__true_output = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__false_output = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_13 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_15 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_17 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_18 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_19 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_80 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_81 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__cycleCount = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_3 = VL_RAND_RESET_I(15);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_taskID = VL_RAND_RESET_I(5);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_control = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8 = VL_RAND_RESET_I(1);
    taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9 = VL_RAND_RESET_I(1);
    __Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__Vfuncout = VL_RAND_RESET_I(21);
    __Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__pc = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__Vfuncout = VL_RAND_RESET_I(21);
    __Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__pc = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__Vfuncout = VL_RAND_RESET_I(21);
    __Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__2__pc = VL_RAND_RESET_I(32);
    __Vfunc_address_range_check__3__Vfuncout = VL_RAND_RESET_I(1);
    __Vfunc_address_range_check__3__addr = VL_RAND_RESET_I(32);
    __Vfunc_address_range_check__4__Vfuncout = VL_RAND_RESET_I(1);
    __Vfunc_address_range_check__4__addr = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__a = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__5__b = VL_RAND_RESET_I(1);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__a = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__6__b = VL_RAND_RESET_I(1);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__Vfuncout = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a = VL_RAND_RESET_I(32);
    __Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__l2_to_mem__DOT__read_count = VL_RAND_RESET_I(2);
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = VL_RAND_RESET_I(2);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID = VL_RAND_RESET_I(5);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID = VL_RAND_RESET_I(5);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID = VL_RAND_RESET_I(5);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = VL_RAND_RESET_I(2);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = VL_RAND_RESET_I(2);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R = VL_RAND_RESET_Q(64);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid = VL_RAND_RESET_I(1);
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state = VL_RAND_RESET_I(1);
    __VinpClk__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni = VL_RAND_RESET_I(1);
    __Vchglast__TOP__taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni = VL_RAND_RESET_I(1);
    VL_RAND_RESET_W(555, __Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o);
    VL_RAND_RESET_W(555, __Vchglast__TOP__taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o);
    __Vchglast__TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo__pop = VL_RAND_RESET_I(1);
}
