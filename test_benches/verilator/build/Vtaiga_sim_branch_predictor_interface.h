// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtaiga_sim.h for the primary calling header

#ifndef _VTAIGA_SIM_BRANCH_PREDICTOR_INTERFACE_H_
#define _VTAIGA_SIM_BRANCH_PREDICTOR_INTERFACE_H_  // guard

#include "verilated_heavy.h"

//==========

class Vtaiga_sim__Syms;

//----------

VL_MODULE(Vtaiga_sim_branch_predictor_interface) {
  public:
    
    // LOCAL SIGNALS
    CData/*0:0*/ is_return;
    CData/*0:0*/ is_branch;
    
    // INTERNAL VARIABLES
  private:
    Vtaiga_sim__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Vtaiga_sim_branch_predictor_interface);  ///< Copying not allowed
  public:
    Vtaiga_sim_branch_predictor_interface(const char* name = "TOP");
    ~Vtaiga_sim_branch_predictor_interface();
    
    // INTERNAL METHODS
    void __Vconfigure(Vtaiga_sim__Syms* symsp, bool first);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
