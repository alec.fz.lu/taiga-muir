// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtaiga_sim.h for the primary calling header

#ifndef _VTAIGA_SIM_L2_REQUESTER_INTERFACE_H_
#define _VTAIGA_SIM_L2_REQUESTER_INTERFACE_H_  // guard

#include "verilated_heavy.h"

//==========

class Vtaiga_sim__Syms;

//----------

VL_MODULE(Vtaiga_sim_l2_requester_interface) {
  public:
    
    // LOCAL SIGNALS
    CData/*3:0*/ be;
    CData/*0:0*/ rnw;
    CData/*0:0*/ is_amo;
    CData/*4:0*/ amo_type_or_burst_size;
    CData/*1:0*/ sub_id;
    CData/*0:0*/ request_push;
    CData/*0:0*/ inv_valid;
    CData/*0:0*/ rd_data_valid;
    IData/*29:0*/ addr;
    
    // INTERNAL VARIABLES
  private:
    Vtaiga_sim__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Vtaiga_sim_l2_requester_interface);  ///< Copying not allowed
  public:
    Vtaiga_sim_l2_requester_interface(const char* name = "TOP");
    ~Vtaiga_sim_l2_requester_interface();
    
    // INTERNAL METHODS
    void __Vconfigure(Vtaiga_sim__Syms* symsp, bool first);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
