// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim_l2_memory_interface.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_CTOR_IMP(Vtaiga_sim_l2_memory_interface) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void Vtaiga_sim_l2_memory_interface::__Vconfigure(Vtaiga_sim__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

Vtaiga_sim_l2_memory_interface::~Vtaiga_sim_l2_memory_interface() {
}

void Vtaiga_sim_l2_memory_interface::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+          Vtaiga_sim_l2_memory_interface::_ctor_var_reset\n"); );
    // Body
    wr_data_read = VL_RAND_RESET_I(1);
}
