// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim_fifo_interface__D1e.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_CTOR_IMP(Vtaiga_sim_fifo_interface__D1e) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void Vtaiga_sim_fifo_interface__D1e::__Vconfigure(Vtaiga_sim__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

Vtaiga_sim_fifo_interface__D1e::~Vtaiga_sim_fifo_interface__D1e() {
}

void Vtaiga_sim_fifo_interface__D1e::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+                  Vtaiga_sim_fifo_interface__D1e::_ctor_var_reset\n"); );
    // Body
    push = VL_RAND_RESET_I(1);
    valid = VL_RAND_RESET_I(1);
}
