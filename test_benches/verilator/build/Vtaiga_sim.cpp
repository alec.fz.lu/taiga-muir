// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim.h"
#include "Vtaiga_sim__Syms.h"

//==========

void Vtaiga_sim::eval_step() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Vtaiga_sim::eval\n"); );
    Vtaiga_sim__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
#ifdef VL_DEBUG
    // Debug assertions
    _eval_debug_assertions();
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("/localssd/taiga-project/taiga/test_benches/verilator/taiga_sim.sv", 28, "",
                "Verilated model didn't converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Vtaiga_sim::_eval_initial_loop(Vtaiga_sim__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        _eval_settle(vlSymsp);
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("/localssd/taiga-project/taiga/test_benches/verilator/taiga_sim.sv", 28, "",
                "Verilated model didn't DC converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

VL_INLINE_OPT void Vtaiga_sim::_settle__TOP__2(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_settle__TOP__2\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.slv_resps_o[0U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.slv_resps_o[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[2U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.slv_resps_o[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U] 
        = ((0xffff0000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U]) 
           | vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.slv_resps_o[3U]);
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.mst_req_o[0U];
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.mst_req_o[1U];
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[2U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.mst_req_o[2U];
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[3U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.mst_req_o[3U];
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[4U] 
        = vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.mst_req_o[4U];
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U]) 
           | vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.mst_req_o[5U]);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U] 
        = ((0xffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U]) 
           | (0xffff0000U & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[0U] 
                             << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[4U] 
        = ((0xffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[0U] 
                       >> 0x10U)) | (0xffff0000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[1U] 
                                      << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U] 
        = ((0xffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[1U] 
                       >> 0x10U)) | (0xffff0000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[2U] 
                                      << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[6U] 
        = ((0xffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[2U] 
                       >> 0x10U)) | (0xffff0000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.slv_resps_o[3U] 
                                      << 0x10U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U]) 
           | (0xfe000000U & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[0U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[6U] 
        = ((0x1ffffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[0U] 
                          >> 7U)) | (0xfe000000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[1U] 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U] 
        = ((0x1ffffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[1U] 
                          >> 7U)) | (0xfe000000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[2U] 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U] 
        = ((0x1ffffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[2U] 
                          >> 7U)) | (0xfe000000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[3U] 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
        = ((0x1ffffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[3U] 
                          >> 7U)) | (0xfe000000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[4U] 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0xaU] 
        = ((0x1ffffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[4U] 
                          >> 7U)) | (0xfe000000U & 
                                     (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[5U] 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0xbU] 
        = (0x1ffffffU & (vlSymsp->TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.mst_req_o[5U] 
                         >> 7U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[0U] 
        = (IData)((0xffffffffffffffULL & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[1U])) 
                                           << 0x20U) 
                                          | (QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[0U])))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[1U] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[1U]) 
           | (IData)(((0xffffffffffffffULL & (((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[1U])) 
                                               << 0x20U) 
                                              | (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[0U])))) 
                      >> 0x20U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[1U] 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[1U]) 
           | (0xff000000U & ((IData)((0xffffffffffffffULL 
                                      & (((QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U])) 
                                          << 0x30U) 
                                         | (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[4U])) 
                                             << 0x10U) 
                                            | ((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                               >> 0x10U))))) 
                             << 0x18U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[2U] 
        = ((0xffffffU & ((IData)((0xffffffffffffffULL 
                                  & (((QData)((IData)(
                                                      vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U])) 
                                      << 0x30U) | (
                                                   ((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[4U])) 
                                                    << 0x10U) 
                                                   | ((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                                      >> 0x10U))))) 
                         >> 8U)) | (0xff000000U & ((IData)(
                                                           ((0xffffffffffffffULL 
                                                             & (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U])) 
                                                                 << 0x30U) 
                                                                | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[4U])) 
                                                                    << 0x10U) 
                                                                   | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                                                      >> 0x10U)))) 
                                                            >> 0x20U)) 
                                                   << 0x18U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U] 
        = ((0xffff0000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U]) 
           | (0xffffffU & ((IData)(((0xffffffffffffffULL 
                                     & (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U])) 
                                         << 0x30U) 
                                        | (((QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[4U])) 
                                            << 0x10U) 
                                           | ((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                              >> 0x10U)))) 
                                    >> 0x20U)) >> 8U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U] 
        = ((0xffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U]) 
           | (0xffffff00U & ((IData)((0xffffffffffffffULL 
                                      & (((QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                          << 0x28U) 
                                         | (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[2U])) 
                                             << 8U) 
                                            | ((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[1U])) 
                                               >> 0x18U))))) 
                             << 8U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[6U] 
        = ((0xffU & ((IData)((0xffffffffffffffULL & 
                              (((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                << 0x28U) | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[2U])) 
                                              << 8U) 
                                             | ((QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[1U])) 
                                                >> 0x18U))))) 
                     >> 0x18U)) | (0xffffff00U & ((IData)(
                                                          ((0xffffffffffffffULL 
                                                            & (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[3U])) 
                                                                << 0x28U) 
                                                               | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[2U])) 
                                                                   << 8U) 
                                                                  | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[1U])) 
                                                                     >> 0x18U)))) 
                                                           >> 0x20U)) 
                                                  << 8U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[7U] 
        = (IData)((0xffffffffffffffULL & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[6U])) 
                                           << 0x18U) 
                                          | ((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U])) 
                                             >> 8U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U]) 
           | (IData)(((0xffffffffffffffULL & (((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[6U])) 
                                               << 0x18U) 
                                              | ((QData)((IData)(
                                                                 vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_resps[5U])) 
                                                 >> 8U))) 
                      >> 0x20U)));
    vlTOPp->bus_axi_arvalid = (1U & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0U] 
                                     >> 1U));
    vlTOPp->bus_axi_araddr = vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[1U];
    vlTOPp->bus_axi_rready = (1U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[0U]);
    vlTOPp->bus_axi_awaddr = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U] 
                               << 0xdU) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[4U] 
                                           >> 0x13U));
    vlTOPp->bus_axi_awvalid = (1U & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[3U] 
                                     >> 0xeU));
    vlTOPp->bus_axi_wvalid = (1U & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[2U] 
                                    >> 7U));
    vlTOPp->bus_axi_wdata = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[3U] 
                              << 0x12U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[2U] 
                                           >> 0xeU));
    vlTOPp->bus_axi_wstrb = (0xfU & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[3U] 
                                      << 0x16U) | (
                                                   vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[2U] 
                                                   >> 0xaU)));
    vlTOPp->bus_axi_bready = (1U & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[2U] 
                                    >> 6U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___GEN_7 
        = (1U & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U] 
                  >> 0x1aU) | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
           & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U]);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies 
        = ((6U & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies)) 
           | (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[1U] 
                    >> 0x17U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies 
        = ((5U & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies)) 
           | (2U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U] 
                    >> 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies 
        = ((3U & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies)) 
           | (4U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U] 
                    >> 5U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies 
        = ((6U & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies)) 
           | (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[6U] 
                    >> 0x1fU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies 
        = ((5U & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies)) 
           | (2U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U] 
                    >> 0x16U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies 
        = ((3U & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies)) 
           | (4U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[0xaU] 
                    >> 0xdU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready 
        = ((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
           & ((0xa7U >= (0xffU & ((IData)(0x36U) + 
                                  ((IData)(0x38U) * 
                                   (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (7U & (((IData)(0x36U) + ((IData)(0x38U) 
                                           * (3U & 
                                              vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                        >> 5U))] >> (0x1fU & ((IData)(0x36U) 
                                              + ((IData)(0x38U) 
                                                 * 
                                                 (3U 
                                                  & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U])))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready 
        = ((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
           & ((0xa7U >= (0xffU & ((IData)(0x36U) + 
                                  ((IData)(0x38U) * 
                                   (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (0xfU & (((IData)(0xa8U) + (0xffU 
                                             & ((IData)(0x36U) 
                                                + ((IData)(0x38U) 
                                                   * 
                                                   (3U 
                                                    & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))))) 
                          >> 5U))] >> (0x1fU & ((IData)(0xa8U) 
                                                + (0xffU 
                                                   & ((IData)(0x36U) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * 
                                                       (3U 
                                                        & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U])))))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready 
        = (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
            & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
           & ((0xa7U >= (0xffU & ((IData)(0x35U) + 
                                  ((IData)(0x38U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (7U & (((IData)(0x35U) + ((IData)(0x38U) 
                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))) 
                        >> 5U))] >> (0x1fU & ((IData)(0x35U) 
                                              + ((IData)(0x38U) 
                                                 * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready 
        = (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
            & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
           & ((0xa7U >= (0xffU & ((IData)(0x35U) + 
                                  ((IData)(0x38U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (0xfU & (((IData)(0xa8U) + (0xffU 
                                             & ((IData)(0x35U) 
                                                + ((IData)(0x38U) 
                                                   * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))) 
                          >> 5U))] >> (0x1fU & ((IData)(0xa8U) 
                                                + (0xffU 
                                                   & ((IData)(0x35U) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
             & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d = 0U;
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            if ((1U & (~ ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
                          & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                             >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
             & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = 1U;
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            if ((1U & (~ ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
                          & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                             >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
             & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = 1U;
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
                 & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                    >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
             & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d = 0U;
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            if ((1U & (~ ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
                          & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                             >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_d = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
             & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = 1U;
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            if ((1U & (~ ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
                          & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                             >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__load_aw_lock = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
             & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = 1U;
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            if (((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])) 
                 & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_readies) 
                    >> (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xdfffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready)) 
              << 0x35U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
            >> 7U) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xdfffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready)) 
              << 0x35U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop 
        = (vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U] 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_w_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_ready));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n)) 
               | ((3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o 
            = (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_n)) 
               | ((3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o 
            = (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_n)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = ((0xfffffffeU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U]) 
           | (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
               & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
              & (0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0xfdffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | (0xfe000000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
                               & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
                              & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))) 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0xfffbffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | (0xfffc0000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
                               & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
                              & (2U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))) 
                             << 0x12U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xfffc0000000000ULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)
               ? 0ULL : ((0xa7U >= (0xffU & ((IData)(0x38U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))
                          ? (0x3ffffffffffULL & (((0U 
                                                   == 
                                                   (0x1fU 
                                                    & ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))
                                                   ? 0ULL
                                                   : 
                                                  ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                                   ((IData)(2U) 
                                                                    + 
                                                                    (7U 
                                                                     & (((IData)(0x38U) 
                                                                         * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)) 
                                                                        >> 5U)))])) 
                                                   << 
                                                   ((IData)(0x40U) 
                                                    - 
                                                    (0x1fU 
                                                     & ((IData)(0x38U) 
                                                        * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))) 
                                                 | (((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                                     ((IData)(1U) 
                                                                      + 
                                                                      (7U 
                                                                       & (((IData)(0x38U) 
                                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)) 
                                                                          >> 5U)))])) 
                                                     << 
                                                     ((0U 
                                                       == 
                                                       (0x1fU 
                                                        & ((IData)(0x38U) 
                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))
                                                       ? 0x20U
                                                       : 
                                                      ((IData)(0x20U) 
                                                       - 
                                                       (0x1fU 
                                                        & ((IData)(0x38U) 
                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))) 
                                                    | ((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                                       (7U 
                                                                        & (((IData)(0x38U) 
                                                                            * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)) 
                                                                           >> 5U))])) 
                                                       >> 
                                                       (0x1fU 
                                                        & ((IData)(0x38U) 
                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))))
                          : 0ULL)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
           & ((0xa7U >= (0xffU & ((IData)(0x2aU) + 
                                  ((IData)(0x38U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (7U & (((IData)(0x2aU) + ((IData)(0x38U) 
                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))) 
                        >> 5U))] >> (0x1fU & ((IData)(0x2aU) 
                                              + ((IData)(0x38U) 
                                                 * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = ((0xfffffffeU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U]) 
           | (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
               & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                  >> 0x19U)) & (0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0xfdffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | (0xfe000000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
                               << 0x19U) & vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U]) 
                             & ((1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)) 
                                << 0x19U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0xfffbffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | (0x1fc0000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
                              << 0x12U) & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                           >> 7U)) 
                            & ((2U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)) 
                               << 0x12U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xfffc0000000000ULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)
               ? 0ULL : ((0xa7U >= (0xffU & ((IData)(0x38U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))
                          ? (0x3ffffffffffULL & (((0U 
                                                   == 
                                                   (0x1fU 
                                                    & ((IData)(0xa8U) 
                                                       + 
                                                       (0xffU 
                                                        & ((IData)(0x38U) 
                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))))
                                                   ? 0ULL
                                                   : 
                                                  ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                                   ((IData)(2U) 
                                                                    + 
                                                                    (0xfU 
                                                                     & (((IData)(0xa8U) 
                                                                         + 
                                                                         (0xffU 
                                                                          & ((IData)(0x38U) 
                                                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))) 
                                                                        >> 5U)))])) 
                                                   << 
                                                   ((IData)(0x40U) 
                                                    - 
                                                    (0x1fU 
                                                     & ((IData)(0xa8U) 
                                                        + 
                                                        (0xffU 
                                                         & ((IData)(0x38U) 
                                                            * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))))) 
                                                 | (((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                                     ((IData)(1U) 
                                                                      + 
                                                                      (0xfU 
                                                                       & (((IData)(0xa8U) 
                                                                           + 
                                                                           (0xffU 
                                                                            & ((IData)(0x38U) 
                                                                               * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))) 
                                                                          >> 5U)))])) 
                                                     << 
                                                     ((0U 
                                                       == 
                                                       (0x1fU 
                                                        & ((IData)(0xa8U) 
                                                           + 
                                                           (0xffU 
                                                            & ((IData)(0x38U) 
                                                               * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))))
                                                       ? 0x20U
                                                       : 
                                                      ((IData)(0x20U) 
                                                       - 
                                                       (0x1fU 
                                                        & ((IData)(0xa8U) 
                                                           + 
                                                           (0xffU 
                                                            & ((IData)(0x38U) 
                                                               * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))))) 
                                                    | ((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                                       (0xfU 
                                                                        & (((IData)(0xa8U) 
                                                                            + 
                                                                            (0xffU 
                                                                             & ((IData)(0x38U) 
                                                                                * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))) 
                                                                           >> 5U))])) 
                                                       >> 
                                                       (0x1fU 
                                                        & ((IData)(0xa8U) 
                                                           + 
                                                           (0xffU 
                                                            & ((IData)(0x38U) 
                                                               * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))))))
                          : 0ULL)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)) 
           & ((0xa7U >= (0xffU & ((IData)(0x2aU) + 
                                  ((IData)(0x38U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (0xfU & (((IData)(0xa8U) + (0xffU 
                                             & ((IData)(0x2aU) 
                                                + ((IData)(0x38U) 
                                                   * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o))))) 
                          >> 5U))] >> (0x1fU & ((IData)(0xa8U) 
                                                + (0xffU 
                                                   & ((IData)(0x2aU) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_r_fifo__data_o)))))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0xffffffbfU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | (0xffffffc0U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
                               << 6U) & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
                             & ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)) 
                                << 6U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = ((0x7fffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U]) 
           | (0x80000000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
                               << 0x1fU) & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                            << 0x19U)) 
                             & ((1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)) 
                                << 0x1fU))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0xfeffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | (0xff000000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
                               << 0x18U) & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                            << 0x12U)) 
                             & ((2U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)) 
                                << 0x18U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xf007ffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)
                                ? 0U : ((0xa7U >= (0xffU 
                                                   & ((IData)(0x2bU) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))
                                         ? (0x1ffU 
                                            & (((0U 
                                                 == 
                                                 (0x1fU 
                                                  & ((IData)(0x2bU) 
                                                     + 
                                                     ((IData)(0x38U) 
                                                      * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))
                                                 ? 0U
                                                 : 
                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                 ((IData)(1U) 
                                                  + 
                                                  (7U 
                                                   & (((IData)(0x2bU) 
                                                       + 
                                                       ((IData)(0x38U) 
                                                        * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))) 
                                                      >> 5U)))] 
                                                 << 
                                                 ((IData)(0x20U) 
                                                  - 
                                                  (0x1fU 
                                                   & ((IData)(0x2bU) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))))) 
                                               | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                  (7U 
                                                   & (((IData)(0x2bU) 
                                                       + 
                                                       ((IData)(0x38U) 
                                                        * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))) 
                                                      >> 5U))] 
                                                  >> 
                                                  (0x1fU 
                                                   & ((IData)(0x2bU) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))))
                                         : 0U)))) << 0x2bU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
           & ((0xa7U >= (0xffU & ((IData)(0x34U) + 
                                  ((IData)(0x38U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (7U & (((IData)(0x34U) + ((IData)(0x38U) 
                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))) 
                        >> 5U))] >> (0x1fU & ((IData)(0x34U) 
                                              + ((IData)(0x38U) 
                                                 * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0xffffffbfU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | (0x40U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
                         << 6U) & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                                   >> 0x19U)) & ((0U 
                                                  == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)) 
                                                 << 6U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = ((0x7fffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U]) 
           | (0x80000000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
                               << 0x1fU) & vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U]) 
                             & ((1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)) 
                                << 0x1fU))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0xfeffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | (0x1000000U & ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
                              << 0x18U) & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                                           >> 7U)) 
                            & ((2U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)) 
                               << 0x18U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xf007ffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)
                                ? 0U : ((0xa7U >= (0xffU 
                                                   & ((IData)(0x2bU) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))
                                         ? (0x1ffU 
                                            & (((0U 
                                                 == 
                                                 (0x1fU 
                                                  & ((IData)(0xa8U) 
                                                     + 
                                                     (0xffU 
                                                      & ((IData)(0x2bU) 
                                                         + 
                                                         ((IData)(0x38U) 
                                                          * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))))
                                                 ? 0U
                                                 : 
                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                 ((IData)(1U) 
                                                  + 
                                                  (0xfU 
                                                   & (((IData)(0xa8U) 
                                                       + 
                                                       (0xffU 
                                                        & ((IData)(0x2bU) 
                                                           + 
                                                           ((IData)(0x38U) 
                                                            * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))) 
                                                      >> 5U)))] 
                                                 << 
                                                 ((IData)(0x20U) 
                                                  - 
                                                  (0x1fU 
                                                   & ((IData)(0xa8U) 
                                                      + 
                                                      (0xffU 
                                                       & ((IData)(0x2bU) 
                                                          + 
                                                          ((IData)(0x38U) 
                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))))))) 
                                               | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                                                  (0xfU 
                                                   & (((IData)(0xa8U) 
                                                       + 
                                                       (0xffU 
                                                        & ((IData)(0x2bU) 
                                                           + 
                                                           ((IData)(0x38U) 
                                                            * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))) 
                                                      >> 5U))] 
                                                  >> 
                                                  (0x1fU 
                                                   & ((IData)(0xa8U) 
                                                      + 
                                                      (0xffU 
                                                       & ((IData)(0x2bU) 
                                                          + 
                                                          ((IData)(0x38U) 
                                                           * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))))))
                                         : 0U)))) << 0x2bU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)) 
           & ((0xa7U >= (0xffU & ((IData)(0x34U) + 
                                  ((IData)(0x38U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))) 
              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[
                 (0xfU & (((IData)(0xa8U) + (0xffU 
                                             & ((IData)(0x34U) 
                                                + ((IData)(0x38U) 
                                                   * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o))))) 
                          >> 5U))] >> (0x1fU & ((IData)(0xa8U) 
                                                + (0xffU 
                                                   & ((IData)(0x34U) 
                                                      + 
                                                      ((IData)(0x38U) 
                                                       * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_b_fifo__data_o)))))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xfffbffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid)) 
              << 0x2aU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid) 
           & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xfffbffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid)) 
              << 0x2aU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_r_valid) 
           & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              >> 0x19U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xefffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid)) 
              << 0x34U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid) 
           & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
              >> 6U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xefffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid)) 
              << 0x34U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_b_valid) 
           & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              >> 0x1fU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__r_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[0U] 
        = (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o);
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U]) 
           | (IData)((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
                      >> 0x20U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U]) 
           | (0xff000000U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
                             << 0x18U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[2U] 
        = ((0xffffffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
                         >> 8U)) | (0xff000000U & ((IData)(
                                                           (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
                                                            >> 0x20U)) 
                                                   << 0x18U)));
    vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[3U] 
        = (0xffffffU & ((IData)((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
                                 >> 0x20U)) >> 8U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_pop))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__b_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__read_pointer_q;
        }
    }
}

VL_INLINE_OPT void Vtaiga_sim::_combo__TOP__4(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_combo__TOP__4\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->ddr_axi_bready_muir = vlTOPp->ddr_axi_bvalid_muir;
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__unit_data_array[0U] 
        = vlTOPp->instruction_bram_data_out;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array[0U] 
        = vlTOPp->data_bram_data_out;
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xfffffbffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfffffc00U 
                                                     & ((IData)(vlTOPp->bus_axi_rvalid) 
                                                        << 0xaU)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xfff00fffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfffff000U 
                                                     & (((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.bid) 
                                                         << 0xeU) 
                                                        | ((IData)(vlTOPp->bus_axi_bresp) 
                                                           << 0xcU))));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[0U] = ((1U 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[0U]) 
                                                  | (0xfffffffeU 
                                                     & ((IData)(
                                                                (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rid)) 
                                                                  << 0x23U) 
                                                                 | (((QData)((IData)(vlTOPp->bus_axi_rdata)) 
                                                                     << 3U) 
                                                                    | (QData)((IData)(
                                                                                (((IData)(vlTOPp->bus_axi_rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rlast))))))) 
                                                        << 1U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xfffffc00U 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | ((1U 
                                                      & ((IData)(
                                                                 (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rid)) 
                                                                   << 0x23U) 
                                                                  | (((QData)((IData)(vlTOPp->bus_axi_rdata)) 
                                                                      << 3U) 
                                                                     | (QData)((IData)(
                                                                                (((IData)(vlTOPp->bus_axi_rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rlast))))))) 
                                                         >> 0x1fU)) 
                                                     | (0xfffffffeU 
                                                        & ((IData)(
                                                                   ((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rid)) 
                                                                      << 0x23U) 
                                                                     | (((QData)((IData)(vlTOPp->bus_axi_rdata)) 
                                                                         << 3U) 
                                                                        | (QData)((IData)(
                                                                                (((IData)(vlTOPp->bus_axi_rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rlast)))))) 
                                                                    >> 0x20U)) 
                                                           << 1U))));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xff0fffffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfff00000U 
                                                     & (((IData)(vlTOPp->bus_axi_awready) 
                                                         << 0x17U) 
                                                        | (((IData)(vlTOPp->bus_axi_arready) 
                                                            << 0x16U) 
                                                           | (((IData)(vlTOPp->bus_axi_wready) 
                                                               << 0x15U) 
                                                              | ((IData)(vlTOPp->bus_axi_bvalid) 
                                                                 << 0x14U))))));
}

VL_INLINE_OPT void Vtaiga_sim::_sequent__TOP__6(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_sequent__TOP__6\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    CData/*0:0*/ __Vdly__taiga_sim__DOT__axi_arvalid;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__l2_to_mem__DOT__write_burst_count;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__burst_count;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count;
    CData/*3:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index;
    CData/*3:0*/ __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0;
    CData/*5:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*6:0*/ __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count;
    CData/*3:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index;
    CData/*3:0*/ __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count;
    CData/*3:0*/ __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0;
    CData/*3:0*/ __Vdly__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index;
    CData/*3:0*/ __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0;
    CData/*4:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0;
    CData/*1:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0;
    CData/*5:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v0;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v1;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0;
    CData/*2:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*5:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0;
    CData/*5:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0;
    CData/*5:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0;
    CData/*4:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0;
    CData/*5:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0;
    CData/*5:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*5:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*5:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*5:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    CData/*5:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0;
    CData/*5:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r;
    CData/*3:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count;
    CData/*2:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*1:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index;
    CData/*1:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0;
    CData/*3:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3;
    CData/*1:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    CData/*4:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    CData/*2:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index;
    CData/*2:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0;
    CData/*2:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0;
    CData/*0:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate;
    CData/*1:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0;
    CData/*7:0*/ __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0;
    CData/*7:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0;
    CData/*0:0*/ __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state;
    CData/*0:0*/ __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state;
    SData/*8:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0;
    SData/*8:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0;
    SData/*8:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0;
    SData/*8:0*/ __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0;
    SData/*15:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count;
    IData/*29:0*/ __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0;
    IData/*26:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0;
    IData/*26:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0;
    IData/*31:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter;
    IData/*31:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0;
    IData/*31:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r;
    WData/*95:0*/ __Vtemp250[3];
    WData/*95:0*/ __Vtemp251[3];
    WData/*95:0*/ __Vtemp252[3];
    WData/*95:0*/ __Vtemp253[3];
    WData/*95:0*/ __Vtemp254[3];
    WData/*95:0*/ __Vtemp256[3];
    WData/*95:0*/ __Vtemp257[3];
    WData/*95:0*/ __Vtemp258[3];
    WData/*95:0*/ __Vtemp259[3];
    WData/*95:0*/ __Vtemp260[3];
    WData/*95:0*/ __Vtemp262[3];
    WData/*95:0*/ __Vtemp263[3];
    WData/*95:0*/ __Vtemp264[3];
    WData/*95:0*/ __Vtemp265[3];
    WData/*95:0*/ __Vtemp266[3];
    WData/*95:0*/ __Vtemp313[3];
    WData/*95:0*/ __Vtemp314[3];
    WData/*95:0*/ __Vtemp315[3];
    CData/*31:0*/ __Vtemp319;
    QData/*43:0*/ __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    QData/*34:0*/ __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0;
    QData/*42:0*/ __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    QData/*41:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0;
    QData/*39:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0;
    QData/*63:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter;
    QData/*35:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    QData/*35:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    QData/*35:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    QData/*35:0*/ __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    QData/*33:0*/ __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR;
    QData/*49:0*/ __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0;
    // Body
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index;
    __Vdly__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0 = 0U;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index;
    __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index;
    __Vdly__taiga_sim__DOT__l2_to_mem__DOT__write_burst_count 
        = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_burst_count;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count;
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter;
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__burst_count 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__burst_count;
    __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count;
    vlTOPp->__Vdly__taiga_sim__DOT__l2_to_mem__DOT__read_count 
        = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_count;
    __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0 = 0U;
    __Vdly__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate;
    __Vdly__taiga_sim__DOT__axi_arvalid = vlTOPp->taiga_sim__DOT__axi_arvalid;
    vlTOPp->__Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr;
    __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state;
    __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R;
    __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0 = 0U;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0 = 0U;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state;
    if (VL_UNLIKELY((((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID:  0] [BB] [bb_entry1] [Out: %1#] [Cycle: %5#]\n",
                   1,((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7)
                       ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control)
                       : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_R_0_control)),
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__cycleCount));
    }
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0 = 0U;
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0 = 0U;
    if (VL_UNLIKELY((((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state)) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [CONST] [Vadd] [Pred: %1#] [Val: 0x0000000000000000] [Cycle: %5#]\n",
                   5,((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)
                       ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID)
                       : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID)),
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_control),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__cycleCount);
    }
    if (VL_UNLIKELY((((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state)) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [CONST] [Vadd] [Pred: %1#] [Val: 0x0000000000000001] [Cycle: %5#]\n",
                   5,((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)
                       ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_taskID)
                       : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_1_bits_taskID)),
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_control),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__cycleCount);
    }
    if (VL_UNLIKELY((((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state)) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [CONST] [Vadd] [Pred: %1#] [Val: 0x0000000000000400] [Cycle: %5#]\n",
                   5,((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)
                       ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_taskID)
                       : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_2_bits_taskID)),
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_control),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__cycleCount);
    }
    if (VL_UNLIKELY((((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state)) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID:  0] [UBR] [br_2] [Out: %1#] [Cycle: %5#]\n",
                   1,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_R_control,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__cycleCount));
    }
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0 = 0U;
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_81) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [CBR] [br_16] [Out: T:1 - F:0] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount));
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_81) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control))) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [CBR] [br_16] [Out: T:0 - F:1] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount));
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_80) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control))) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [CBR] [br_16] [Out: T:0 - F:0] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount));
    }
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 = 0U;
    __Vdly__taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3 = 0U;
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__done 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.done;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1 = 0U;
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0 = 0U;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state;
    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_taskID;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state;
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0 = 0U;
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"Ptrs: ");
    }
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"ptr(0): 0x%x, ",64,
                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_0);
    }
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"ptr(1): 0x%x, ",64,
                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_1);
    }
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"ptr(2): 0x%x, ",64,
                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__ptrs_2);
    }
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"\nVals: ");
    }
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"N/A");
    }
    if (VL_UNLIKELY((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
                      & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"\n");
    }
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state;
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_EXTEND_WQ(68,64, __Vtemp250, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_R_data);
        VL_EXTEND_WQ(68,64, __Vtemp251, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_R_0_data);
        VL_SHIFTL_WWI(68,68,32, __Vtemp252, __Vtemp251, 3U);
        VL_ADD_W(3, __Vtemp253, __Vtemp250, __Vtemp252);
        __Vtemp254[0U] = __Vtemp253[0U];
        __Vtemp254[1U] = __Vtemp253[1U];
        __Vtemp254[2U] = (0xfU & __Vtemp253[2U]);
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [GEP] [Gep_arrayidx7] [Pred: %1#][Out: 0x%x] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_control),
                   68,__Vtemp254,15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__cycleCount));
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_EXTEND_WQ(68,64, __Vtemp256, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_R_data);
        VL_EXTEND_WQ(68,64, __Vtemp257, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_R_0_data);
        VL_SHIFTL_WWI(68,68,32, __Vtemp258, __Vtemp257, 3U);
        VL_ADD_W(3, __Vtemp259, __Vtemp256, __Vtemp258);
        __Vtemp260[0U] = __Vtemp259[0U];
        __Vtemp260[1U] = __Vtemp259[1U];
        __Vtemp260[2U] = (0xfU & __Vtemp259[2U]);
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [GEP] [Gep_arrayidx29] [Pred: %1#][Out: 0x%x] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_control),
                   68,__Vtemp260,15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__cycleCount));
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_EXTEND_WQ(68,64, __Vtemp262, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_R_data);
        VL_EXTEND_WQ(68,64, __Vtemp263, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_R_0_data);
        VL_SHIFTL_WWI(68,68,32, __Vtemp264, __Vtemp263, 3U);
        VL_ADD_W(3, __Vtemp265, __Vtemp262, __Vtemp264);
        __Vtemp266[0U] = __Vtemp265[0U];
        __Vtemp266[1U] = __Vtemp265[1U];
        __Vtemp266[2U] = (0xfU & __Vtemp265[2U]);
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [GEP] [Gep_arrayidx412] [Pred: %1#][Out: 0x%x] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_control),
                   68,__Vtemp266,15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__cycleCount));
    }
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state;
    if (VL_UNLIKELY((((((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state)) 
                        & (1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) 
                       & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [STORE] [st_13] [Pred: %1#] [Iter: %5#] [Addr: %20#] [Data: %20#] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_control),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__value,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__cycleCount));
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_129) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [BB] bb_for_body5] [Mask: 0x%x]\n",
                   5,((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                      | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID)),
                   2,(((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1) 
                       << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0)));
    }
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_129) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate))) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] bb_for_body5: Output fired @ %5# -> 0 predicate\n",
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__cycleCount);
    }
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0 = 0U;
    __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0 = 0U;
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [ret_4] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__output_R_enable_taskID,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__cycleCount));
    }
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0 = 0U;
    if (VL_UNLIKELY((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_293) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [LOOP] [Loop_0] [RESTARTED] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount));
    }
    if (VL_UNLIKELY(((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_293) 
                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control))) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [LOOP] [Loop_0] [FIRED] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount));
    }
    if (VL_UNLIKELY(((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_293) 
                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control))) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [LOOP] [Loop_0] [FINAL] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_taskID,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount));
    }
    if (VL_UNLIKELY((((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5)) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [BB] [bb_for_cond_cleanup3] [Out: %1#] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_R_taskID,
                   1,((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30)
                       ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control)
                       : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control)),
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__cycleCount));
    }
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__divisor_is_zero 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero;
    __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r;
    __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0 = 0U;
    __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1 = 0U;
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 3U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.cpu.ras_block.read_index_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 3U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (7U 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.cpu.ras_block.read_index_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 3U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (7U 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.cpu.ras_block.read_index_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                       >> 4U)) | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                                  >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk4[0].inv_response_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                       >> 4U)) | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                                  >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk4[1].inv_response_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid)) 
                                   | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk5[1].returndata_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__.pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk2[1].input_data_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                       >> 4U)) | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                                  >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.mem_returndata.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 3U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__.push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                         >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.genblk4[0].inv_response_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                         >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.genblk4[1].inv_response_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push) 
                                       >> 1U)) | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid)) 
                                                  | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.genblk5[1].returndata_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push) 
                                       >> 1U)) | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid)) 
                                                  | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.genblk5[1].returndata_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->ddr_axi_rvalid_taiga)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count))))))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                         >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.mem_returndata.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->ddr_axi_rvalid_taiga)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count))))))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                         >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.mem_returndata.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__inflight_count) 
                                      >> 5U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: register_free_list.sv:90: Assertion failed in %Ntaiga_sim.cpu.renamer_block.free_list_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/register_free_list.sv", 90, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 5U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.data_attributes_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.potential_push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__inflight_count) 
                                           >> 5U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0x1fU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__inflight_count))))))) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: register_free_list.sv:88: Assertion failed in %Ntaiga_sim.cpu.renamer_block.free_list_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/register_free_list.sv", 88, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 5U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0x1fU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.data_attributes_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.input_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance)) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full)) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.input_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance)) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full)) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.input_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((0U != (0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr))) 
                                   | (0U == (0x3fU 
                                             & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: renamer.sv:160: Assertion failed in %Ntaiga_sim.cpu.renamer_block.rename_rs_zero_assertion[0]: rs zero renamed\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/renamer.sv", 160, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((0U != (0x1fU & 
                                           ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr) 
                                            >> 5U))) 
                                   | (0U == (0x3fU 
                                             & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr) 
                                                >> 6U))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: renamer.sv:160: Assertion failed in %Ntaiga_sim.cpu.renamer_block.rename_rs_zero_assertion[1]: rs zero renamed\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/renamer.sv", 160, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.push)) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full)) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.mem_data.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid)) 
                                   | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk5[0].returndata_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push)) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid)) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.genblk5[0].returndata_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push)) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid)) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.genblk5[0].returndata_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                                      >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.mem_data.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete)) 
                                   | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.cpu.fetch_block.attributes_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__.pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk2[0].input_data_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                                         >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))) 
                                       & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out)))) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full)) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.mem_data.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                               [0U] 
                                               >> 0x26U))) 
                                   | (0U != (0x3fU 
                                             & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                                        [0U] 
                                                        >> 0x20U)))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: register_file.sv:109: Assertion failed in %Ntaiga_sim.cpu.register_file_block.write_to_rd_zero_assertion[0]: write to register zero\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/register_file.sv", 109, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                               [1U] 
                                               >> 0x26U))) 
                                   | (0U != (0x3fU 
                                             & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                                        [1U] 
                                                        >> 0x20U)))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: register_file.sv:109: Assertion failed in %Ntaiga_sim.cpu.register_file_block.write_to_rd_zero_assertion[1]: write to register zero\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/register_file.sv", 109, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | (~ ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                               [0U] 
                                               >> 0x26U)) 
                                      & (0U == (0x3fU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                                           [0U] 
                                                           >> 0x20U))))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: register_bank.sv:65: Assertion failed in %Ntaiga_sim.cpu.register_file_block.register_file_gen[0].reg_group.write_to_zero_reg_assertion: Write to zero reg occured!\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/register_bank.sv", 65, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | (~ ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                               [1U] 
                                               >> 0x26U)) 
                                      & (0U == (0x3fU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                                           [1U] 
                                                           >> 0x20U))))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: register_bank.sv:65: Assertion failed in %Ntaiga_sim.cpu.register_file_block.register_file_gen[1].reg_group.write_to_zero_reg_assertion: Write to zero reg occured!\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/register_bank.sv", 65, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | (~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count) 
                                       >> 3U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: instruction_metadata_and_id_management.sv:309: Assertion failed in %Ntaiga_sim.cpu.id_block.pc_id_assigned_without_pc_id_available_assertion: ID assigned without any ID available\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/instruction_metadata_and_id_management.sv", 309, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__.pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk1[1].input_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 4U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.l2_arb.genblk1[0].input_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.l2_arb.genblk1[0].input_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.genblk1[0].input_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
             >> 5U) & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           & (0x18U == (0x7fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U] 
                                  << 1U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
                                            >> 0x1fU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
             >> 5U) & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           & (8U == (0x7fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U] 
                               << 1U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
                                         >> 0x1fU)))));
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                       >> 4U)) | ((~ 
                                                   (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count) 
                                                     >> 3U) 
                                                    & (~ (IData)(
                                                                 (0U 
                                                                  != 
                                                                  (7U 
                                                                   & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                                  | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
                                        & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store)) 
                                       & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v)))) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                         >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.genblk4[0].inv_response_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
                                        & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store)) 
                                       & (~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v) 
                                             >> 1U)))) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 4U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0xfU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                                         >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.genblk4[1].inv_response_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request)) 
                                   | (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: load_store_unit.sv:390: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.invalid_ls_address_assertion: invalid L/S address\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/load_store_unit.sv", 390, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst) 
                                | ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
                                   | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                                      >> 3U))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.push)) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 3U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (7U 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
                                        & (~ (IData)(
                                                     (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                      >> 8U)))) 
                                       & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort)))) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 5U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (0x1fU 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.l2_arb.data_attributes_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid)) 
                                   | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                       >> 2U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                                 >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: mmu.sv:196: Assertion failed in %Ntaiga_sim.cpu.genblk4.d_mmu.mmu_spurious_l1_response: mmu recieved response without a request\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/mmu.sv", 196, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid)) 
                                   | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                       >> 2U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                                 >> 4U)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: mmu.sv:196: Assertion failed in %Ntaiga_sim.cpu.genblk2.i_mmu.mmu_spurious_l1_response: mmu recieved response without a request\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/mmu.sv", 196, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
                                       | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                          >> 6U))) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid)) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.cpu.fetch_block.attributes_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
                                       | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                          >> 6U))) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid)) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.cpu.fetch_block.attributes_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((0U != (0x1fU & 
                                           ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                             << 0x16U) 
                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 0xaU)))) 
                                   | (0U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rd_addr)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: renamer.sv:157: Assertion failed in %Ntaiga_sim.cpu.renamer_block.rename_rd_zero_assertion: rd zero renamed\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/renamer.sv", 157, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | (~ ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                          >> 2U)) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: instruction_metadata_and_id_management.sv:313: Assertion failed in %Ntaiga_sim.cpu.id_block.decode_advanced_without_id_assertion: Decode advanced without ID\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/instruction_metadata_and_id_management.sv", 313, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)) 
                                       & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.new_request) 
                                          | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__mmu_request_complete)))) 
                                   | VL_ONEHOT_I((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: tlb_lut_ram.sv:163: Assertion failed in %Ntaiga_sim.cpu.genblk2.i_tlb.multiple_tag_hit_in_tlb: Multiple tag hits in TLB!\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/tlb_lut_ram.sv", 163, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)) 
                                       & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.new_request) 
                                          | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__mmu_request_complete)))) 
                                   | VL_ONEHOT_I((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: tlb_lut_ram.sv:163: Assertion failed in %Ntaiga_sim.cpu.genblk4.d_tlb.multiple_tag_hit_in_tlb: Multiple tag hits in TLB!\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/tlb_lut_ram.sv", 163, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress) 
                                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: mmu.sv:204: Assertion failed in %Ntaiga_sim.cpu.genblk2.i_mmu.mmu_tlb_state_mismatch: MMU and TLB state mismatch\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/mmu.sv", 204, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid)) 
                                   | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid) 
                                      & ((0U >= (1U 
                                                 & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out))) 
                                         & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid) 
                                            >> (1U 
                                                & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out))))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: fetch.sv:271: Assertion failed in %Ntaiga_sim.cpu.fetch_block.spurious_fetch_complete_assertion: Spurious fetch complete detected!\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/fetch.sv", 271, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)))) 
                                   | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.attributes_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.push)) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid)) 
                                      | (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.attributes_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
                                       & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                          >> 0xbU))) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid)) 
                                      | (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.attributes_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress) 
                                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: mmu.sv:204: Assertion failed in %Ntaiga_sim.cpu.genblk4.d_mmu.mmu_tlb_state_mismatch: MMU and TLB state mismatch\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/mmu.sv", 204, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop)) 
                                   | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:131: Assertion failed in %Ntaiga_sim.cpu.genblk8.div_unit_block.div_input_fifo.fifo_underflow_assertion: underflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 131, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                       >> 2U)) | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid)) 
                                                  | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:127: Assertion failed in %Ntaiga_sim.cpu.genblk8.div_unit_block.div_input_fifo.fifo_overflow_assertion: overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 127, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)))) 
                                   | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid) 
                                      & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid) 
                                         >> (1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out)))))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: load_store_unit.sv:385: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.spurious_load_complete_assertion: Spurious load complete detected!\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/load_store_unit.sv", 385, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                        >> 2U) & ((~ 
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                    >> 1U)) 
                                                  & (~ 
                                                     vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U])))) 
                                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: decode_and_issue.sv:616: Assertion failed in %Ntaiga_sim.cpu.decode_and_issue_block.invalid_fetch_address_assertion: invalid fetch address\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/decode_and_issue.sv", 616, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                         >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                   >> 4U)) 
                                       & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                          >> 4U))) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 3U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (7U 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst) 
                                | ((~ (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                         >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                   >> 1U)) 
                                       & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                          >> 1U))) 
                                   | ((~ (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                                           >> 3U) & 
                                          (~ (IData)(
                                                     (0U 
                                                      != 
                                                      (7U 
                                                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    if (Verilated::assertOn()) {
        if (VL_LIKELY((1U & (~ ((IData)(vlTOPp->rst) 
                                | ((~ (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                         >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                   >> 2U)) 
                                       & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                          >> 2U))) 
                                   | ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid)) 
                                      | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop)))))))) {
            if (VL_UNLIKELY(Verilated::assertOn())) {
                VL_WRITEF("[%0t] %%Error: taiga_fifo.sv:129: Assertion failed in %Ntaiga_sim.cpu.genblk8.div_unit_block.div_input_fifo.fifo_potenial_push_overflow_assertion: potential push overflow\n",
                          64,VL_TIME_UNITED_Q(1),vlSymsp->name());
                VL_STOP_MT("/localssd/taiga-project/taiga/core/taiga_fifo.sv", 129, "");
            }
        }
    }
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst)
            ? 0U : (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index) 
                          + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.push))));
    __Vdly__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst)
            ? 0U : (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index) 
                          + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_phys_addr 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table
            [vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index];
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush 
        = ((2U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state) 
           | (4U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_data;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst)
            ? 0U : (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__read_index) 
                          + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop))));
    if (vlTOPp->ddr_axi_rvalid_taiga) {
        __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0 
            = (((QData)((IData)((7U & (IData)(vlTOPp->ddr_axi_rid_taiga)))) 
                << 0x20U) | (QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)));
        __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__write_index;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT____Vcellinp__read_index_fifo__rst)
            ? 0U : (0xfU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__inflight_count) 
                             + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.pop)) 
                            - (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched))));
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.branch_fetched) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index;
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                             >> 5U));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0 
            = (0x1ffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                          << 0x19U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                       >> 7U)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                             >> 5U));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0 
            = (0x1ffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                          << 0x19U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                       >> 7U)));
    }
    if (((IData)(vlTOPp->ddr_axi_rvalid_taiga) & ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_count) 
                                                  == 
                                                  (3U 
                                                   & (IData)(
                                                             (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                              >> 0xeU)))))) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result_r 
            = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result;
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0 
            = (0x1ffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                          << 0x19U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                       >> 7U)));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0 
            = (0x1ffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                          << 0x19U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                       >> 7U)));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__write_index = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count = 0U;
        __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index = 0U;
        __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index = 0U;
        __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index = 0U;
        __Vdly__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__write_index;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__write_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__write_index) 
                       + (IData)(vlTOPp->ddr_axi_rvalid_taiga)));
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count) 
                        + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__.pop)));
        __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index) 
                       + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.push)));
        __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index) 
                        + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.push)));
        __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index) 
                        + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.potential_push)));
        __Vdly__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index) 
                       + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram[(0x1ffU 
                                                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                                                                                << 0x19U) 
                                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                                                                >> 7U)))] 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                             >> 5U));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__read_data 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram
            [(0x1ffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc 
                        >> 2U))];
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__target_update_way))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram[(0x1ffU 
                                                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                                                                                << 0x19U) 
                                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                                                                >> 7U)))] 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[0U] 
                             >> 5U));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__read_data 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram
            [(0x1ffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc 
                        >> 2U))];
    }
    __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index) 
                                                + (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_len = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_len = 0xfU;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_len = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_len = 0xfU;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way = 1U;
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way = 1U;
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way = 1U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way 
            = ((2U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way) 
                      << 1U)) | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way) 
                                       >> 1U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way 
            = ((2U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way) 
                      << 1U)) | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way) 
                                       >> 1U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way 
            = ((2U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way) 
                      << 1U)) | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way) 
                                       >> 1U)));
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count 
            = (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                         + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.valid)) 
                        - (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.push)));
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count 
            = (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                         + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.inv_valid)) 
                        - (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__.push)));
    }
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid 
        = (1U & ((~ (IData)(vlTOPp->rst)) & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push) 
                                              >> 1U) 
                                             | ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid) 
                                                & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.pop))))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index) 
                       + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__.pop)));
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index) 
                       + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__.pop)));
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__l2_to_mem__DOT__write_burst_count = 0U;
    } else {
        if (vlTOPp->ddr_axi_bvalid_taiga) {
            __Vdly__taiga_sim__DOT__l2_to_mem__DOT__write_burst_count = 0U;
        } else {
            if ((((IData)(vlTOPp->taiga_sim__DOT__axi_wvalid) 
                  & (IData)(vlTOPp->ddr_axi_wready_taiga)) 
                 & ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count) 
                    != (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_burst_count)))) {
                __Vdly__taiga_sim__DOT__l2_to_mem__DOT__write_burst_count 
                    = (0x1fU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_burst_count)));
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r = 0U;
    } else {
        if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r 
                = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta;
        }
    }
    __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                                  + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.pop)) 
                                                 - (IData)(vlTOPp->ddr_axi_rvalid_taiga))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_taskID 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_8_bits_taskID;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking = 0U;
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__read_index = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking 
            = (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
                     + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__delayed_abort_complete)));
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__read_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__read_index) 
                       + (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                                >> 4U))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ret 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op) 
               & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match) 
                    >> 2U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match) 
                              >> 3U)) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match) 
                                         >> 4U)));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ecall 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op) 
               & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ebreak 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op) 
               & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match) 
                  >> 1U));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x3fU & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__inflight_count) 
                                                   + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid)) 
                                                  - (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.potential_push)) 
                                                 - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback))));
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
        = ((0xfffffffffffffffeULL & __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter) 
           | (IData)((IData)((1U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state))));
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
        = ((1ULL & __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter) 
           | (0xfffffffffffffffeULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
                                       << 1U)));
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter 
        = ((0xfffffffeU & __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter) 
           | ((3U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state) 
              & (4U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state)));
    __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter 
        = ((1U & __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter) 
           | (0xfffffffeU & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter 
                             << 1U)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete = 0U;
    } else {
        if (vlTOPp->ddr_axi_bvalid_taiga) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete = 0U;
        } else {
            if (((IData)(vlTOPp->taiga_sim__DOT__axi_wlast) 
                 & (IData)(vlTOPp->ddr_axi_wready_taiga))) {
                vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete = 1U;
            }
        }
    }
    __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x3fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                                                  + (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done)) 
                                                 - (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.push))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__stvec = 0x80000000U;
    } else {
        if ((0x20U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U])) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__stvec 
                = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_17) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__l2_arb__DOT__burst_count = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done) {
            __Vdly__taiga_sim__DOT__l2_arb__DOT__burst_count = 0U;
        } else {
            if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                        >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))))) {
                __Vdly__taiga_sim__DOT__l2_arb__DOT__burst_count 
                    = (0x1fU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__burst_count)));
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_17) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_17) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip = 0U;
    } else {
        if ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U])) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip 
                = (0xaaaU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr);
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mideleg = 0U;
    } else {
        if ((8U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U])) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mideleg 
                = (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
                   & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask);
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_1) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate = 1U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_2) {
                if (vlTOPp->ddr_axi_arready_muir) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_3) {
                    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_r_ready) 
                          & (IData)(vlTOPp->ddr_axi_rvalid_muir)) 
                         & (IData)(vlTOPp->ddr_axi_rlast_muir))) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate = 0U;
                    }
                }
            }
        }
    }
    if (vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push) {
        __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [0U];
        __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index;
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
            = (0x100000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
            = (0xffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle);
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
            = ((0x100000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle) 
               | (IData)((IData)(((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U])
                                   ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr
                                   : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle_next)))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
            = ((0xffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle) 
               | ((QData)((IData)((1U & ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[4U])
                                          ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr
                                          : (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle_next 
                                                     >> 0x20U)))))) 
                  << 0x20U));
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index) 
                                                + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__.pop))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
            = (0x100000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
            = (0xffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret);
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
            = ((0x100000000ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret) 
               | (IData)((IData)(((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U])
                                   ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr
                                   : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret_next)))));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
            = ((0xffffffffULL & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret) 
               | ((QData)((IData)((1U & ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[4U])
                                          ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr
                                          : (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret_next 
                                                     >> 0x20U)))))) 
                  << 0x20U));
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index) 
                                                + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_16) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_56;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_45;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_55;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg = 0U;
    } else {
        if ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U])) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg 
                = (0xaaaU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr);
        } else {
            if ((0x10U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U])) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg 
                    = (0x222U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr);
            }
        }
    }
    __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                                  + (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop)) 
                                                 - (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_hold 
        = ((1U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state) 
           | (2U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__read_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__read_index) 
                                                  + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid)) 
                                                 - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_4) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_7;
        }
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__read_index) 
                                                 + (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_42;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__l2_to_mem__DOT__read_count = 0U;
    } else {
        if (((IData)(vlTOPp->ddr_axi_rvalid_taiga) 
             & ((IData)(vlTOPp->ddr_axi_rid_taiga) 
                == (7U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out))))) {
            vlTOPp->__Vdly__taiga_sim__DOT__l2_to_mem__DOT__read_count 
                = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_count)));
        }
    }
    if ((1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                 >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))) 
               & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out))))) {
        __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_data
            [(1U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                    >> 6U))];
        __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index;
    }
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid 
        = (1U & ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push) 
                                             | ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid) 
                                                & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rd_data_valid))))));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid)
                ? 0U : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid)
                         ? 1U : 2U));
    }
    __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                                                  + (IData)(vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read)) 
                                                 - (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.push))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_44;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_43;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance) {
            vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__state 
                = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i;
        }
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__read_index) 
                                                + (IData)(vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read))));
    if ((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U] 
               | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[2U]))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0 
            = ((0x18U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                  >> 0xcU)) << 3U)) 
               | (7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                >> 4U))));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8) {
            __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_12;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7) {
                __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R 
                    = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7) {
                        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R 
                            = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R 
                        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_11;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count) 
                                                 + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__.pop))));
    if ((1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U] 
                >> 3U) | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                  >> 0x28U))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtval 
            = ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                              >> 0x28U))) ? (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                                     >> 3U))
                : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause 
        = (0x8000001fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause);
    if ((1U & ((((0x80000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr)
                  ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM
                 [(0x1fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr)]
                  : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM
                 [(0x1fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr)]) 
                & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U] 
                   >> 2U)) | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                      >> 0x28U))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause 
            = ((0x7fffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause) 
               | (0x80000000U & (((~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                              >> 0x28U))) 
                                  << 0x1fU) & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause 
            = ((0xffffffe0U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause) 
               | (0x1fU & ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                          >> 0x28U)))
                            ? (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                       >> 0x23U)) : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__system_op_or_exception_complete 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                   >> 5U) & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                 >> 4U)) | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                            >> 3U))) 
                 | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                            >> 0x28U))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg = 0U;
    } else {
        if ((4U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U])) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg 
                = (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
                   & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation 
            = (2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation));
    } else {
        if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) {
            if ((1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation))) {
                vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation 
                    = (2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation));
            } else {
                if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_lr) {
                    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation 
                        = (1U | (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation 
            = (1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation));
    } else {
        if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) {
            if ((2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation))) {
                vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation 
                    = (1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation));
            } else {
                if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_lr) {
                    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation 
                        = (2U | (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation));
                }
            }
        }
    }
    __Vdly__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index 
        = ((IData)(vlTOPp->rst) ? 0U : (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index) 
                                              + (1U 
                                                 & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                                    >> 4U)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U] = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[0U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[1U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[2U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[3U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[4U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[5U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[6U]);
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U] 
                    = (~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[7U]);
            } else {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[0U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[1U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[2U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[3U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[4U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[5U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[6U];
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U] 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[7U];
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0xfU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__inflight_count) 
                                                 + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop)) 
                                                - (1U 
                                                   & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                                      >> 4U)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_11) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid) {
                __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate = 1U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_12) {
                if (vlTOPp->ddr_axi_awready_muir) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_13) {
                    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid) 
                          & (IData)(vlTOPp->ddr_axi_wready_muir)) 
                         & (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt)))) {
                        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate = 3U;
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_17) {
                        if (vlTOPp->ddr_axi_bvalid_muir) {
                            __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate = 0U;
                        }
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_15)
                                        : ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29)) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_15))));
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__axi_arvalid = 0U;
    } else {
        if (((IData)(vlTOPp->taiga_sim__DOT__axi_arvalid) 
             & (IData)(vlTOPp->ddr_axi_arready_taiga))) {
            __Vdly__taiga_sim__DOT__axi_arvalid = 0U;
        } else {
            if ((1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                         >> 4U) & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                           >> 9U))) 
                       & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__address_phase_complete))))) {
                __Vdly__taiga_sim__DOT__axi_arvalid = 1U;
            }
        }
    }
    vlTOPp->__Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr 
        = (1U & ((~ (IData)(vlTOPp->rst)) & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                                >> 5U) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
                                                  >> 5U)) 
                                              | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr)) 
                                             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete)))));
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push))) {
        vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.data_out 
            = (0x3ffffffffULL & vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out);
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push))) {
        vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.data_out 
            = (0x3ffffffffULL & vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
        = ((IData)(vlTOPp->rst) ? 0x1800U : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_new);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_2 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_3 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_1 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level 
        = ((IData)(vlTOPp->rst) ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__next_privilege_level));
    if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance) {
        __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_request;
        __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index;
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__read_index = 0U;
        __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__read_index 
            = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__read_index) 
                       + (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop)));
        __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle)
                ? 1U : ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate)
                         ? 0U : ((0xfffeU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count) 
                                             << 1U)) 
                                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle))));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_45;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3 = 0U;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_44;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2 = 0U;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_42;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0 = 0U;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_43;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1 = 0U;
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[1U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[1U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[2U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[2U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[3U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[3U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[4U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[4U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[5U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[5U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[6U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[6U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[7U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[7U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[8U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[8U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[9U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[9U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xaU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xaU];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xbU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xbU];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xcU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xcU];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xdU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xdU];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xeU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xeU];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xfU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xfU];
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R)));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R)));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 
                                = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R)));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R)));
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 
                                = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)));
                    }
                }
            }
        }
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_req_ready) 
         & (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)))) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_tag_reg 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_tag;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_17)
                                        : ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31)) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_17))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data = 0ULL;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data;
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data = 0ULL;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data;
                    }
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data = 0ULL;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__on_last_burst = 0U;
    } else {
        if (vlTOPp->ddr_axi_bvalid_taiga) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__on_last_burst = 0U;
        } else {
            if ((((~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress)) 
                  & (0U == (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count))) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress) 
                    & ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count) 
                       == (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_burst_count))))) {
                vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__on_last_burst = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_13)
                                        : ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31)) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_13))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                                 + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__.pop))));
    if ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
          & (~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                        >> 8U)))) & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort)))) {
        __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__new_attr;
        __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0x1fU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                                                  + (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop)) 
                                                 - (IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.request_push))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10;
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10;
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10;
                    }
                }
            }
        }
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
         & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_lr))) {
        __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0 
            = (0x3fffffffU & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                      >> 0xdU)));
        __Vdlyvset__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id;
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__axi_awvalid = 0U;
    } else {
        if ((1U & (((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                      & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count)) 
                     >> 4U) & ((~ (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                           >> 9U))) 
                               | (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_write_ready))) 
                   & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress))))) {
            vlTOPp->taiga_sim__DOT__axi_awvalid = 1U;
        } else {
            if (vlTOPp->ddr_axi_awready_taiga) {
                vlTOPp->taiga_sim__DOT__axi_awvalid = 0U;
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__exception_pending = 0U;
    } else {
        if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                    >> 6U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
                              & (8U != (0xfU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__translated_address 
                                                >> 0x1cU))))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__exception_pending = 1U;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12) {
                vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                    }
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12) {
                            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control 
                                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_R_control 
                            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                    }
                }
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_0 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_1 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_2 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_3 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_4 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_5 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_6 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((7U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_7 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((8U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_8 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((9U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_9 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0xaU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_10 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0xbU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_11 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0xcU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_12 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0xdU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_13 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0xeU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
        if ((0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_15 
                = vlTOPp->ddr_axi_rdata_muir;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R 
                    = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1) 
                        << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R 
                            = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1) 
                                << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0));
                    }
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R 
                                = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1) 
                                    << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R 
                            = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1) 
                                << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0));
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0 = 0U;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15)));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rename_valid) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table
            [vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index];
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0 
            = (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] 
                     >> 3U));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__potential_flush 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op) 
               | ((3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                     >> 0xfU)));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ifence_r 
            = ((3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                 << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 5U)))) 
               & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                  >> 0xfU));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_csr_r 
            = ((0x1cU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
               & (0U != (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 0xfU)))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_fence 
            = ((3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                 << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 5U)))) 
               & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                     >> 0xfU)));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__illegal_instruction_pattern_r = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__illegal_instruction_pattern_r 
                = (1U & (~ (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk11__DOT__illegal_op_check__DOT__base_legal) 
                               | ((((0x2000033U == 
                                     (0xfe00707fU & 
                                      ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1dU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 3U)))) | 
                                    (0x2001033U == 
                                     (0xfe00707fU & 
                                      ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1dU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 3U))))) 
                                   | (0x2002033U == 
                                      (0xfe00707fU 
                                       & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
                                  | (0x2003033U == 
                                     (0xfe00707fU & 
                                      ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1dU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 3U)))))) 
                              | ((((0x2004033U == (0xfe00707fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U)))) 
                                   | (0x2005033U == 
                                      (0xfe00707fU 
                                       & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
                                  | (0x2006033U == 
                                     (0xfe00707fU & 
                                      ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1dU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 3U))))) 
                                 | (0x2007033U == (0xfe00707fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U)))))) 
                             | (((0x30200073U == ((
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))) 
                                 | (0x73U == ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1dU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U)))) 
                                | (0x100073U == ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                            | (((0x10200073U == ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))) 
                                | (0x12000073U == (0xfe007fffU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U))))) 
                               | (0x10500073U == ((
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U)))))));
        }
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__flush_or_rst)) 
                 & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
                     | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                        >> 6U)) | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid) 
                                   & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete))))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_if;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pre_issue_count 
        = (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))
            ? 0U : (0xfU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pre_issue_count) 
                             + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned)) 
                            - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[0U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[1U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[2U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[3U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[4U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[5U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[6U] = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[7U] = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[0U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[0U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[1U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[1U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[2U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[2U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[3U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[3U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[4U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[4U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[5U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[5U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[6U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[6U];
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[7U] 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[7U];
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking 
        = ((IData)(vlTOPp->rst) ? 0U : (7U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
                                               - ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
                                                  & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                                      >> 2U) 
                                                     | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                                        >> 4U)))) 
                                              + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__delayed_abort_complete))));
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst)
            ? 0U : (0xfU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                             + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack)) 
                            - (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.push))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg 
        = ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0 
            = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rd_addr;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetched_count 
        = (((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))
            ? 0U : (0xfU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetched_count) 
                             + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete)) 
                            - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_switch_stall 
        = ((~ (IData)(vlTOPp->rst)) & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match) 
                                            != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__last_unit))) 
                                        | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_switch_stall)) 
                                       & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid)));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_metadata;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id;
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram[(0x1ffU 
                                                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                                                                                << 0x19U) 
                                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                                                                >> 7U)))] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__read_data 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram
            [(0x1ffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc 
                        >> 2U))];
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_update_way))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram[(0x1ffU 
                                                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[2U] 
                                                                                << 0x19U) 
                                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__br_results[1U] 
                                                                                >> 7U)))] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__read_data 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram
            [(0x1ffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc 
                        >> 2U))];
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id;
    }
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_data;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc_override 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_flush_required) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__second_cycle_flush)) 
                  | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_exception 
                             >> 0x28U))) | (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                                    >> 0x28U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT__ready 
        = (1U & ((IData)(vlTOPp->rst) | ((((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                                            >> 0xaU) 
                                           | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                                              >> 0x14U)) 
                                          | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT__ready)) 
                                         & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request)))));
    __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst)
            ? 0U : (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released) 
                     | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__newly_released)) 
                    & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0)));
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0)));
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 
                                = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0)));
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0)));
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count 
        = ((IData)(vlTOPp->rst) ? 0U : (0xfU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count) 
                                                 + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued)) 
                                                - (3U 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire)))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd) 
               & (0U != (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0xaU)))));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst)
            ? 0U : (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__read_index) 
                          + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack))));
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken_ex 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst)
            ? 0U : (3U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest) 
                          + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_ack))));
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__ras.push) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0 
            = ((IData)(4U) + vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__new_index;
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__immu.upper_physical_address 
            = ((0x3ffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__immu.upper_physical_address) 
               | (0xffc00U & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                       [0U] >> 0x14U)) 
                              << 0xaU)));
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__immu.upper_physical_address 
            = ((0xffc00U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__immu.upper_physical_address) 
               | (0x3ffU & ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))
                             ? (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                        [0U] >> 0xaU))
                             : (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                >> 0xcU))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id;
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tlb_write))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__new_entry;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 
            = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                        >> 0xcU));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tlb_write))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__new_entry;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 
            = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                        >> 0xcU));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset_r 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__br_use_signed 
            = (1U & (~ ((6U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x11U) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 0xfU)))) 
                        | (7U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 0x11U) 
                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 0xfU)))))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_call 
            = (((0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U)))) 
                | (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x1bU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 5U))))) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_link));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_return 
            = ((0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
               & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_link) 
                   & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_link))) 
                  | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_link) 
                      & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_link)) 
                     & ((0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 0x12U))) 
                        != (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                      << 0x16U) | (
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 0xaU)))))));
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.upper_physical_address 
            = ((0x3ffU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.upper_physical_address) 
               | (0xffc00U & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                       [0U] >> 0x14U)) 
                              << 0xaU)));
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.upper_physical_address 
            = ((0xffc00U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.upper_physical_address) 
               | (0x3ffU & ((0x10U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))
                             ? (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                        [0U] >> 0xaU))
                             : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                >> 0xcU))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__result 
            = VL_MULS_QQQ(64,64,64, VL_EXTENDS_QQ(64,33, vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__rs1_r), 
                          VL_EXTENDS_QQ(64,33, vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__rs2_r));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete;
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [1U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [1U];
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [0U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [0U];
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [1U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [1U];
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [0U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [0U];
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_call 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                     >> 0x16U));
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__pc_ex 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
                << 5U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                          >> 0x1bU));
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jal_jalr_ex 
            = (1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                      >> 0x18U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                   >> 0x17U)));
    }
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tlb_write))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__new_entry;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0 
            = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                        >> 0xcU));
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tlb_write))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__new_entry;
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0 
            = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                        >> 0xcU));
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex 
            = ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex) 
               | (0xfffffffeU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc));
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex 
            = ((0xfffffffeU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex) 
               | (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc 
                        & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                              >> 0x17U)))));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__read_index 
        = ((IData)(vlTOPp->rst) ? 0U : (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__read_index) 
                                              + (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.push) 
                                       | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid) 
                                          & (~ (IData)(
                                                       (0U 
                                                        != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)))))));
    __Vdly__taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r 
        = (1U & ((~ (IData)(vlTOPp->rst)) & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                              >> 6U) 
                                             | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r) 
                                                & (~ 
                                                   ((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                                                     >> 0x1aU) 
                                                    | (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
                                                               >> 0x28U))))))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes 
            = (((~ ((IData)(0xfU) << (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index) 
                                              << 2U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__hashes)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__addr_hash) 
                  << (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index) 
                              << 2U))));
    }
    if ((1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot)) 
                & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                           >> 0x20U))) & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released)) 
                                          | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot))))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0 
            = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0 = 1U;
    }
    if ((1U & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match) 
                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot)) 
                 >> 1U) & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                                   >> 0x20U))) & ((~ 
                                                   ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released) 
                                                    >> 1U)) 
                                                  | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot) 
                                                     >> 1U))))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1 
            = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1 = 1U;
    }
    if ((1U & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match) 
                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot)) 
                 >> 2U) & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                                   >> 0x20U))) & ((~ 
                                                   ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released) 
                                                    >> 2U)) 
                                                  | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot) 
                                                     >> 2U))))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2 
            = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2 = 1U;
    }
    if ((1U & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match) 
                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot)) 
                 >> 3U) & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                                   >> 0x20U))) & ((~ 
                                                   ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released) 
                                                    >> 3U)) 
                                                  | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot) 
                                                     >> 3U))))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3 
            = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3 = 1U;
    }
    if ((2U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
         [1U])) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__done_r = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_done) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__done_r = 1U;
        }
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.data_valid 
        = (1U & ((~ (IData)(vlTOPp->rst)) & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                                             >> 0xaU)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.data_valid 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.new_request) 
                                       & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                          >> 0xbU)));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp = 0U;
    } else {
        if ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate)) 
                   & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp 
                = ((7U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign))
                    ? (0xfffffffcU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.quotient 
                                      << 2U)) : ((6U 
                                                  == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign))
                                                  ? 
                                                 (1U 
                                                  | (0xfffffffcU 
                                                     & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.quotient 
                                                        << 2U)))
                                                  : 
                                                 ((4U 
                                                   == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign))
                                                   ? 
                                                  (2U 
                                                   | (0xfffffffcU 
                                                      & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.quotient 
                                                         << 2U)))
                                                   : 
                                                  (3U 
                                                   | (0xfffffffcU 
                                                      & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.quotient 
                                                         << 2U))))));
        }
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
               | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid)))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__.addr 
            = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state))
                ? ((0xfffff000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                                   << 0xcU)) | (0xffcU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                                   >> 0x14U)))
                : ((0xfffff000U & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                            [0U] >> 0xaU)) 
                                   << 0xcU)) | (0xffcU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                                   >> 0xaU))));
    }
    if (vlTOPp->rst) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__done = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
            vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__done = 0U;
        } else {
            if ((1U & (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count) 
                          >> (0xfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter))) 
                         | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early)) 
                        & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.done))) 
                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate))))) {
                vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__done = 1U;
            } else {
                if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.done) {
                    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__done = 0U;
                }
            }
        }
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
               | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid)))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__.addr 
            = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state))
                ? ((0xfffff000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                                   << 0xcU)) | (0xffcU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                                   >> 0x14U)))
                : ((0xfffff000U & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                            [0U] >> 0xaU)) 
                                   << 0xcU)) | (0xffcU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__virtual_address 
                                                   >> 0xaU))));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst)
            ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count_next));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0 
            = (0U != (3U & vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U]));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0 = 1U;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh
            [0U];
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1 = 1U;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst) {
        __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index = 0U;
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid = 0U;
    } else {
        __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index) 
                     + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid 
            = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_request_one_hot)) 
               & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot)));
    }
    if ((0x400U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U])) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__genblk3__DOT__genblk1__DOT__axi_bus__data_out 
            = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                << 0x1cU) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[0U] 
                             >> 4U));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT____Vcellinp__in_progress_m__set) 
                                       | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress) 
                                          & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_ack
                                                [1U] 
                                                >> 1U)))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__ls_offset 
            = (0xfffU & ((0x100U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                          ? ((0xfe0U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 9U) | (0x1e0U 
                                                   & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 0x17U)))) 
                             | (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x16U) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xaU))))
                          : ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                              << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 0x17U))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_store_r 
            = (8U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 5U))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_load_r 
            = ((0U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                 << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 5U)))) 
               | (0xbU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U)))));
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid 
        = (1U & ((~ (IData)(vlTOPp->rst)) & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                              >> 2U) 
                                             | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
                                                & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop))))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op_r 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_slt_path 
            = ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                   >> 5U)) & ((2U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                             << 0x11U) 
                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 0xfU)))) 
                              | (3U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x11U) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 0xfU))))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_shifter_path 
            = (1U & (~ ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                         >> 5U) | ((((((2U == (7U & 
                                               ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x11U) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 0xfU)))) 
                                       | (3U == (7U 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x11U) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 0xfU))))) 
                                      | (4U == (7U 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x11U) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 0xfU))))) 
                                     | (6U == (7U & 
                                               ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x11U) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 0xfU))))) 
                                    | (7U == (7U & 
                                              ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                << 0x11U) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 0xfU))))) 
                                   | (0U == (7U & (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x11U) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 0xfU))))))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_lshift 
            = (1U & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                        >> 0x11U)));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_subtract 
            = ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                   >> 5U)) & (((3U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x11U) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0xfU)))) 
                               | (2U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                << 0x11U) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 0xfU))))) 
                              | (((0U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x11U) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 0xfU)))) 
                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     >> 1U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 8U))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0 
            = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                     >> 3U));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0 = 1U;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
            [0U];
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1 = 1U;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound3 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                      << 0x18U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                   >> 8U)));
        if ((0xbU >= (0xfU & ((IData)(3U) * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed 
                = (((~ ((IData)(7U) << (0xfU & ((IData)(3U) 
                                                * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index))))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound3) 
                      << (0xfU & ((IData)(3U) * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)))));
        }
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request) 
         & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
               >> 0xbU)))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0 
            = ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                << 3U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                          >> 0x1dU));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0 
            = (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr)) 
                << 8U) | (QData)((IData)((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__be) 
                                           << 4U) | 
                                          ((0xeU & 
                                            ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                              << 0x13U) 
                                             | (0x7fffeU 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                   >> 0xdU)))) 
                                           | (1U & 
                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                               >> 0xbU)))))));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs2_use_regfile 
            = (1U & (~ (((((0xdU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1bU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
                           | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1bU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U))))) 
                          | (0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x1bU) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 5U))))) 
                         | (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                << 0x1bU) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U))))) 
                        | (4U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                            << 0x1bU) 
                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 5U)))))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs2_r 
            = (((0xdU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
                | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))))
                ? (0xfffff000U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x1dU) | (0x1ffff000U 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 3U))))
                : (((0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 0x1bU) 
                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 5U)))) 
                    | (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1bU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 5U)))))
                    ? 4U : VL_EXTENDS_II(32,12, (0xfffU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 9U) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 0x17U))))));
    }
    if (vlTOPp->rst) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v0 = 0U;
        __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v1 = 1U;
    } else {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v0 
            = (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                     [0U] + (3U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire))));
        __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v1 
            = (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
                     [1U] + (3U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_use_regfile 
            = (1U & (~ ((((0xdU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1bU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U)))) 
                          | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1bU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U))))) 
                         | (0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                << 0x1bU) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U))))) 
                        | (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1bU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_stage_ready) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pre_alu_rs1_r 
            = ((((5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U)))) 
                 | (0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1bU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 5U))))) 
                | (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x1bU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 5U))))) ? 
               ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] 
                 << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                              >> 3U)) : 0U);
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_13) {
            if ((1U & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0)) {
                if (((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid))) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = 1U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_29) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_31) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_done) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = 3U;
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_33) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__state = 0U;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_7) {
            __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___GEN_7;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate) {
                if ((0x2000000U & vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U])) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_40));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 = 0U;
                }
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_40));
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_38));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 = 0U;
                }
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_38));
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_36));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 = 0U;
                }
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_36));
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_34));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 = 0U;
                }
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_34));
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_30));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R = 0U;
                }
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_30));
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R 
                    = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_31));
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R = 0U;
                }
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_31));
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R = 0U;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_taskID)
                    : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID));
        }
    }
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [1U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [1U];
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [0U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [0U];
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [1U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [1U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [1U];
    __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle
           [0U] ^ vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory
           [vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
           [0U]]);
    __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT___toggle_addr
        [0U];
    if (VL_UNLIKELY(((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_82) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [LOAD] [ld_8] [Pred: %1#] [Iter: %5#] [Addr: %20#] [Data: %20#] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__value,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__data_R_data,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__cycleCount));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_44) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 = 0U;
                    }
                } else {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4)));
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_50) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_25;
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_44) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control) {
                    if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate))) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = 1U;
                    }
                } else {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = 2U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_50) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_51) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state = 0U;
                    }
                }
            }
        }
    }
    if (VL_UNLIKELY(((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_82) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [LOAD] [ld_10] [Pred: %1#] [Iter: %5#] [Addr: %20#] [Data: %20#] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control),
                   15,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__value,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__data_R_data,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__cycleCount));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_44) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 = 0U;
                    }
                } else {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4)));
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 = 0U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_50) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_25;
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0 = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_44) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control) {
                    if (((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid)) 
                         & (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)))) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = 1U;
                    }
                } else {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = 2U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_50) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = 2U;
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_51) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25) {
                        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state = 0U;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata = 0U;
    } else {
        if ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate)) 
                   & (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[5U] 
                      >> 0x1aU)))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata 
                = ((0x10U == (0xffU & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U] 
                                        << 7U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[6U] 
                                                  >> 0x19U))))
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_4
                    : ((0xcU == (0xffU & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U] 
                                           << 7U) | 
                                          (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[6U] 
                                           >> 0x19U))))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_3
                        : ((8U == (0xffU & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U] 
                                             << 7U) 
                                            | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[6U] 
                                               >> 0x19U))))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_2
                            : ((4U == (0xffU & ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U] 
                                                 << 7U) 
                                                | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[6U] 
                                                   >> 0x19U))))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_1
                                : ((0U == (0xffU & 
                                           ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[7U] 
                                             << 7U) 
                                            | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[6U] 
                                               >> 0x19U))))
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0
                                    : 0U)))));
        }
    }
    if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                       [1U] >> 0x26U)))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0 
            = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                      [1U]);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0 
            = (0x3fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                [1U] >> 0x20U)));
    }
    if ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                       [0U] >> 0x26U)))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0 
            = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                      [0U]);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0 
            = (0x3fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__register_file_block__commit
                                [0U] >> 0x20U)));
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_1) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_0;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state) {
                if ((1U & ((~ (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0) 
                                & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0)) 
                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0))) 
                           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg))))) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)));
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state)
                                            ? ((~ (IData)(
                                                          (0x3fffU 
                                                           == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48)))) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state)
                                            ? ((~ (IData)(
                                                          (0x3fffU 
                                                           == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48)))) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_57;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
                if ((0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48))) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0 
            = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)) 
                     & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback)
                         ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_wb_group)
                         : (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)))));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index;
    }
    __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index 
        = ((IData)(vlTOPp->rst) ? 0U : (0x3fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index) 
                                                 + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear))));
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.potential_push) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear)
                ? (0x20U | (0x1fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index)))
                : vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table
               [(7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__retire) 
                       >> 2U))]);
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index 
            = (7U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_105) 
         & (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)))) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_addr 
                                >> 6U)));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_addr = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498) {
            VL_EXTEND_WQ(65,64, __Vtemp313, (0xffffffffffffffc0ULL 
                                             & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg));
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_addr 
                = __Vtemp313[0U];
        }
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x12U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x10U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0xfU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0xeU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 5U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x1dU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0 
            = (0xffU & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
                        >> 8U));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x13U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x17U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 4U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x1cU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x16U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 3U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 7U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 2U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x1aU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x18U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x1fU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x15U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x1fU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0 
            = (0xffU & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
                        >> 0x18U));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U])) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x1bU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x11U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 8U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x1eU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 9U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 6U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 5U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x14U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x1eU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0 
            = (0xffU & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
                        >> 0x10U));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x1dU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x18U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0xdU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x1cU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x19U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 1U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0xaU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0xbU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x1aU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0xcU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x1bU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U])) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 1U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 2U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 3U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 4U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 6U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 7U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 8U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 9U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0xaU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0xbU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0xcU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0xdU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            >> 0x19U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0xeU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0xfU))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x10U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x11U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x12U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x13U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x14U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0 
            = (0xffU & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU]);
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x15U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
                         << 0x18U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
                                      >> 8U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x16U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
                                      >> 0x10U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
          | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc)) 
         & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            >> 0x17U))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0 
            = (0xffU & ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
                         << 8U) | (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
                                   >> 0x18U)));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen) 
         & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc))) {
        __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0 
            = (0x3ffffffffffffULL & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                     >> 0xeU));
        __Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0 
            = (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                >> 6U)));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_addr = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497) {
            VL_EXTEND_WQ(65,64, __Vtemp314, ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__block_rmeta_tag 
                                              << 0xeU) 
                                             | (QData)((IData)(
                                                               (0x3fc0U 
                                                                & (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count) 
                                                                    - (IData)(1U)) 
                                                                   << 6U))))));
            VL_EXTEND_WQ(65,64, __Vtemp315, ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_data 
                                              << 0xeU) 
                                             | (QData)((IData)(
                                                               (0x3fc0U 
                                                                & ((IData)(
                                                                           (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                                            >> 6U)) 
                                                                   << 6U))))));
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_addr 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode)
                    ? __Vtemp314[0U] : __Vtemp315[0U]);
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index 
            = (0x3fU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__clear_index)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id 
        = ((IData)(vlTOPp->rst) ? 0U : (7U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                   >> 2U))
                                                ? (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                      >> 3U))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id)) 
                                              + ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_advance) 
                                                 & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))))));
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jmp_id 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__id_ex 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_id 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__instruction_id;
    __Vtemp319 = ((~ (IData)(vlTOPp->rst)) & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
                                                & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                   >> 2U)) 
                                               | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_result_valid)) 
                                              & (~ 
                                                 ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                      >> 6U) 
                                                     & (((0x1fU 
                                                          & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                              << 0x10U) 
                                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                >> 0x10U))) 
                                                         == 
                                                         (0x1fU 
                                                          & ((4U 
                                                              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage))
                                                              ? 
                                                             ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                               << 0x1dU) 
                                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                 >> 3U))
                                                              : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs1_addr)))) 
                                                        | ((0x1fU 
                                                            & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                << 0x10U) 
                                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                  >> 0x10U))) 
                                                           == 
                                                           (0x1fU 
                                                            & ((4U 
                                                                & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage))
                                                                ? 
                                                               ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                 << 0x18U) 
                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                   >> 8U))
                                                                : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs2_addr))))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_result_valid 
        = __Vtemp319;
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__new_sq_request) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound2 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
        if ((0xbU >= (0xfU & ((IData)(3U) * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids 
                = (((~ ((IData)(7U) << (0xfU & ((IData)(3U) 
                                                * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index))))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__ids)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT____Vlvbound2) 
                      << (0xfU & ((IData)(3U) * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)))));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted_1 = 0U;
    } else {
        if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted_1 
                = (((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
                     << 0x19U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
                                  >> 7U)) << (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta));
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_3 
            = (0x3ffffffffULL & (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted)) 
                                  << 1U) + (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_2 
            = ((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted)) 
               << 1U);
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_1 
            = (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted));
    }
    if ((1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                           >> 4U)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                      >> 4U)))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index;
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor = 0U;
    } else {
        if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor 
                = (((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
                     << 0x19U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
                                  >> 7U)) > ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U] 
                                              << 0x19U) 
                                             | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
                                                >> 7U)));
        }
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__divisor_is_zero 
            = (1U & (~ (IData)((0U != ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
                                        << 0x19U) | 
                                       (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
                                        >> 7U))))));
    } else {
        if ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate)) 
                   & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early))))) {
            vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__divisor_is_zero 
                = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero) 
                   & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign)))));
        }
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
        __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
            = (QData)((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted 
                               >> (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift))));
        __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
            = ((0x1fU >= ((IData)(0x20U) - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift)))
                ? (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted 
                   << ((IData)(0x20U) - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift)))
                : 0U);
    } else {
        if ((1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate)) 
                   & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early))))) {
            __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
                = (0xfffffffcU & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
                                  << 2U));
            __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                = ((7U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign))
                    ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR)) 
                        << 2U) | (QData)((IData)((3U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
                                                     >> 0x1eU)))))
                    : ((6U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign))
                        ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_1)) 
                            << 2U) | (QData)((IData)(
                                                     (3U 
                                                      & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
                                                         >> 0x1eU)))))
                        : ((4U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign))
                            ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_2)) 
                                << 2U) | (QData)((IData)(
                                                         (3U 
                                                          & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
                                                             >> 0x1eU)))))
                            : (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_3)) 
                                << 2U) | (QData)((IData)(
                                                         (3U 
                                                          & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
                                                             >> 0x1eU))))))));
        }
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr 
            = (0x7fU & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U]);
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued_with_rd) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                         << 0x10U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                      >> 0x10U)));
    }
    if ((1U & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                           >> 1U)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                      >> 1U)))) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.addr)) 
                << 0xaU) | (QData)((IData)(((0x380U 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[1U] 
                                                 << 0x19U) 
                                                | (0x1ffff80U 
                                                   & (vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
                                                      >> 7U)))) 
                                            | ((0x70U 
                                                & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                   << 1U)) 
                                               | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__potential_store_conflicts))))));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0 = 1U;
        __Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0 = 1U;
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage2_advance) {
        __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id
            [0U];
        __Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1 = 1U;
    }
    if ((1U & (((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                         >> 0x28U)) | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[0U] 
                                       >> 3U)) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to) 
                                                   >> 5U) 
                                                  & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U])))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_pc 
            = ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception 
                              >> 0x28U))) ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec
                : ((8U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[0U])
                    ? ((IData)(4U) + ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[4U] 
                                       << 0x1aU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                       >> 6U))) : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc));
    }
    vlTOPp->taiga_sim__DOT__tr[1U] = ((0xfffffffbU 
                                       & vlTOPp->taiga_sim__DOT__tr[1U]) 
                                      | (0xfffffffcU 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush) 
                                            << 2U)));
    vlTOPp->taiga_sim__DOT__tr[1U] = ((0xfffffffdU 
                                       & vlTOPp->taiga_sim__DOT__tr[1U]) 
                                      | (0xfffffffeU 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall) 
                                            << 1U)));
    vlTOPp->taiga_sim__DOT__tr[1U] = ((0xfffffffeU 
                                       & vlTOPp->taiga_sim__DOT__tr[1U]) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_unit_stall));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0x7fffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0x80000000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_id_stall) 
                                            << 0x1fU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xbfffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xc0000000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_instruction_stall) 
                                            << 0x1eU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xdfffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0x20000000U 
                                         & (((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                              << 0x1bU) 
                                             & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued)) 
                                                << 0x1dU)) 
                                            & ((~ (
                                                   (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall) 
                                                     | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_unit_stall)) 
                                                    | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_id_stall)) 
                                                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_no_instruction_stall))) 
                                               << 0x1dU))));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xefffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xf0000000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
                                            << 0x1cU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xf7ffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xf8000000U 
                                         & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall) 
                                             << 0x1bU) 
                                            & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                               << 0x15U))));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfbffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfc000000U 
                                         & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall) 
                                              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage)) 
                                             & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                                   >> 6U))) 
                                            << 0x1aU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfdffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfe000000U 
                                         & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall) 
                                             << 0x19U) 
                                            & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                               << 0x18U))));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfeffffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xff000000U 
                                         & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_operand_stall) 
                                             << 0x18U) 
                                            & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                                               << 0x16U))));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xff7fffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xff800000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_alu_op) 
                                            << 0x17U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffbfffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffc00000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_branch_or_jump_op) 
                                            << 0x16U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffdfffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffe00000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_load_op) 
                                            << 0x15U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffefffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfff00000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_store_op) 
                                            << 0x14U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfff7ffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfff80000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_mul_op) 
                                            << 0x13U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffbffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffc0000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_div_op) 
                                            << 0x12U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffdffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffe0000U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_misc_op) 
                                            << 0x11U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffeffffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffff0000U 
                                         & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
                                              & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return))) 
                                             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush))) 
                                            << 0x10U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffff7fffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffff8000U 
                                         & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
                                              & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return))) 
                                             & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush)) 
                                            << 0xfU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffffbfffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffffc000U 
                                         & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
                                              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return)) 
                                             & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush))) 
                                            << 0xeU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffffdfffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffffe000U 
                                         & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
                                              & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return)) 
                                             & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_flush)) 
                                            << 0xdU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffffefffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffff000U 
                                         & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                                              << 9U) 
                                             & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict) 
                                                << 0xcU)) 
                                            & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__ready_for_issue_from_lsq) 
                                               << 0xcU))));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffff7ffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffff800U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_rs1_forwarding_needed) 
                                            << 0xbU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffffbffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffffc00U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_rs2_forwarding_needed) 
                                            << 0xaU)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffffdffU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffffe00U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_rs1_and_rs2_forwarding_needed) 
                                            << 9U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffffe3fU 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xffffffc0U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_num_instructions_completing) 
                                            << 6U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xffffffc7U 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (0xfffffff8U 
                                         & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_num_instructions_in_flight) 
                                            << 3U)));
    vlTOPp->taiga_sim__DOT__tr[0U] = ((0xfffffff8U 
                                       & vlTOPp->taiga_sim__DOT__tr[0U]) 
                                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_num_of_instructions_pending_writeback));
    vlTOPp->taiga_sim__DOT__tr[2U] = ((7U & vlTOPp->taiga_sim__DOT__tr[2U]) 
                                      | (0xfffffff8U 
                                         & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                             << 0xcU) 
                                            | (0xff8U 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                  >> 0x14U)))));
    vlTOPp->taiga_sim__DOT__tr[3U] = (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                            >> 0x14U));
    vlTOPp->taiga_sim__DOT__tr[1U] = ((7U & vlTOPp->taiga_sim__DOT__tr[1U]) 
                                      | (0xfffffff8U 
                                         & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                             << 0xcU) 
                                            | (0xff8U 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                  >> 0x14U)))));
    vlTOPp->taiga_sim__DOT__tr[2U] = ((0xfffffff8U 
                                       & vlTOPp->taiga_sim__DOT__tr[2U]) 
                                      | (7U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                               >> 0x14U)));
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) {
        vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb = 
            (0xfU & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                      << 0x1aU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                   >> 6U)));
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) {
        vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr 
            = ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                << 0x14U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                             >> 0xcU));
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) {
        vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata = 
            ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
              << 0x1dU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
                           >> 3U));
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) {
        vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize 
            = (3U & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                      << 0x1dU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                   >> 3U)));
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) {
        vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr 
            = ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                << 0x14U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                             >> 0xcU));
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) {
        vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize 
            = (3U & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                      << 0x1dU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                   >> 3U)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__awvalid_m__result 
        = ((~ (IData)(vlTOPp->rst)) & (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) 
                                        & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                           >> 0xaU)) 
                                       | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__awvalid_m__result) 
                                          & (~ (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                                                >> 0x17U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__arvalid_m__result 
        = ((~ (IData)(vlTOPp->rst)) & (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) 
                                        & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                           >> 0xbU)) 
                                       | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__arvalid_m__result) 
                                          & (~ (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                                                >> 0x16U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result 
        = ((~ (IData)(vlTOPp->rst)) & (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request) 
                                        & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                           >> 0xaU)) 
                                       | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result) 
                                          & (~ (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__slv_ports_resp_o[1U] 
                                                >> 0x15U)))));
    if (__Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram__v0;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index_fifo__DOT__genblk3__DOT__lut_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__DOT__branch_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__DOT__branch_ram__v0;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter 
        = __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__init_clear_counter__DOT__counter;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter 
        = __Vdly__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__tlb_clear_counter__DOT__counter;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__burst_count 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__burst_count;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__write_index;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count;
    if (__Vdlyvset__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs__v0;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_ready_R;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    }
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_burst_count 
        = __Vdly__taiga_sim__DOT__l2_to_mem__DOT__write_burst_count;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address[__Vdlyvdim0__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0] 
            = __Vdlyvval__taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table__v0;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_3));
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__phys_addr_table__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__DOT__branch_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__DOT__branch_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table__v0;
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__cycleCount = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__cycleCount = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__cycleCount = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__cycleCount = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__cycleCount 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__cycleCount 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__cycleCount 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__cycleCount 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_10;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__uses_rd_table__v0;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_3));
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__ras_block__DOT__lut_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__1__KET____DOT__ram_block__DOT__ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__lut_rams__BRA__0__KET____DOT__ram_block__DOT__ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb[0U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb[1U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v1;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb[2U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v2;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb[3U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb__v3;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released 
        = __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released;
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.done 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__done;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh[0U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh[1U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh__v1;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done[0U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done[1U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done__v1;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr__v0;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids[0U] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids[1U] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids__v1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__1__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__1__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0] 
        = __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__id_inuse_toggle_mem_set__DOT__read_port_gen__BRA__0__KET____DOT__write_port_gen__BRA__0__KET____DOT__mem__DOT__id_toggle_memory__v0;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__1__KET____DOT__reg_group__DOT__register_file_bank__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__register_file_block__DOT__register_file_gen__BRA__0__KET____DOT__reg_group__DOT__register_file_bank__v0;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_7));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_R_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_7));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_R_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_7));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_R_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_12));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__value = 0U;
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
            if ((1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__value 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_62;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_20));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index 
        = __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__architectural_id_to_phys_table__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram__v0;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index 
        = __Vdly__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__clear_index;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_3));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__output_R_enable_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__output_R_enable_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID));
        }
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7__v0;
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__cycleCount = 0U;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_R_taskID = 0U;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__cycleCount 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_R_taskID 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index 
        = __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram__v0;
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vdly__divisor_is_zero;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r 
        = __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__AR_r;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
        = __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_to_id_table__v0;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index 
        = __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__write_index;
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id[0U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v0;
    }
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id[1U] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id__v1;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count 
        = __Vdly__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count;
    if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v 
            = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__mmu_request_complete 
        = (1U & ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                                             >> 5U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__mmu_request_complete 
        = (1U & ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                             >> 5U)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress = 0U;
    } else {
        if ((1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                     >> 5U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                               >> 6U)) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush) 
                                          | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress = 0U;
        } else {
            if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.new_request) 
                 & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tag_hit)))))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress = 1U;
            }
        }
    }
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__new_mem_request) 
               | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__state) 
                  >> 6U)))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next;
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid 
        = ((~ ((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))) 
           & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.new_request));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress = 0U;
    } else {
        if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                    >> 5U) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                              >> 6U)))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress = 0U;
        } else {
            if (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.new_request) 
                 & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tag_hit)))))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__request_in_progress = 1U;
            }
        }
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
         & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
            >> 0xbU))) {
        vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__read_index 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__ras_block__DOT__new_index;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__read_data;
    vlTOPp->ddr_axi_arlen_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_len;
    vlTOPp->ddr_axi_awlen_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_len;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tlb_write 
        = (3U & ((- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tlb_write 
        = (3U & ((- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.valid 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.inv_valid 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.pop 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_data[1U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_data[0U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index];
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.pop 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__state 
        = ((IData)(vlTOPp->rst) ? 0U : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state);
    vlTOPp->ddr_axi_arvalid_muir = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_1 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_2 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_3 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle_next 
        = (0x1ffffffffULL & (1ULL + vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_7 
        = (0xfU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count)));
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rd_data_valid 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_12 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt = 0U;
        } else {
            if (((IData)(vlTOPp->ddr_axi_wready_muir) 
                 & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_w_valid))) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_10;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__address_phase_complete = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__address_phase_complete = 0U;
        } else {
            if (((IData)(vlTOPp->taiga_sim__DOT__axi_arvalid) 
                 & (IData)(vlTOPp->ddr_axi_arready_taiga))) {
                vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__address_phase_complete = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.data_out;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.data_out;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus;
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = ((0xfffffffdU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return) 
               | (2U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                        >> 4U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = (0x20U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = (0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = (0xfffdffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                = ((0xfffffff7U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return) 
                   | (8U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                            >> 4U)));
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                = (0x80U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                = (0xffffe7ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
            if ((3U != (3U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                              >> 0xbU)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                    = (0xfffdffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_16 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege 
        = (3U & ((0x20000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus)
                  ? (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                     >> 0xbU) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_105) 
           & (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_15)
                                        : ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29)) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_15))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_11)
                                        : ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29)) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_11))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_tag = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_tag 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid)
                    ? 0U : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid)
                             ? 1U : 2U));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_data 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R
                    : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_19);
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests 
        = ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests)) 
           | (2U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                    >> 3U)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready 
        = (1U & ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                      >> 4U) & (~ (IData)((0U != (0xfU 
                                                  & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                 & (~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count) 
                        >> 4U) & (~ (IData)((0U != 
                                             (0xfU 
                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count)))))))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests 
        = ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests)) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                    >> 4U)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10 = 0U;
            }
        } else {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_52;
        }
    }
    if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id 
            = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid 
        = ((~ (IData)(vlTOPp->rst)) & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance));
    if (vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
            = vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests
            [vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i];
    }
    vlTOPp->ddr_axi_awvalid_taiga = vlTOPp->taiga_sim__DOT__axi_awvalid;
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress = 0U;
    } else {
        if (vlTOPp->ddr_axi_bvalid_taiga) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress = 0U;
        } else {
            if ((1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                         & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count)) 
                        >> 4U) & ((~ (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                              >> 9U))) 
                                  | (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_write_ready))))) {
                vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress = 1U;
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_3;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19 
        = ((2U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R) 
                  << 1U)) | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R) 
                                   >> 1U)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_1 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_control_R_0 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control;
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_490) {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate))) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state 
                    = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_mask))
                        ? 2U : 1U);
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_493) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state 
                        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate))
                            ? ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_mask))
                                ? 2U : 1U) : 0U);
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 3U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 6U;
                        }
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_499) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 3U;
                        } else {
                            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498) {
                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 6U;
                            }
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_505) {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_wrap_out) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 4U;
                        }
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_506) {
                            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_dme_wr_0_ack) {
                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 5U;
                            }
                        } else {
                            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_507) {
                                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498) {
                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state = 6U;
                                }
                            } else {
                                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_509) {
                                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out) {
                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state 
                                            = ((0U 
                                                != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask))
                                                ? 2U
                                                : 0U);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.push) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__last_unit 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_id = ((IData)(vlTOPp->rst)
                                                   ? 0U
                                                   : 
                                                  (7U 
                                                   & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_fetch_id_base) 
                                                      + 
                                                      ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete) 
                                                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_data = 0ULL;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_data 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid)
                    ? 0ULL : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid)
                               ? 0ULL : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_R_data));
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT__ready) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count 
        = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pre_issue_count) 
                   + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id = ((IData)(vlTOPp->rst)
                                                ? 0U
                                                : (7U 
                                                   & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__next_pc_id_base) 
                                                      + 
                                                      ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__pc_id_assigned) 
                                                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_fetch_flush))))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__rs1_r 
            = (((QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[2U] 
                                  >> 1U) & ((1U == 
                                             (3U & 
                                              vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U])) 
                                            | (2U == 
                                               (3U 
                                                & vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U])))))) 
                << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[2U] 
                                              << 0x1eU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[1U] 
                                                >> 2U)))));
    }
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__stage1_advance) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__rs2_r 
            = (((QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[1U] 
                                  >> 1U) & ((0U == 
                                             (3U & 
                                              vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U])) 
                                            | (1U == 
                                               (3U 
                                                & vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U])))))) 
                << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[1U] 
                                              << 0x1eU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__mul_inputs[0U] 
                                                >> 2U)))));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][4U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r;
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x2fU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r) 
                     << 4U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop = ((0xf00000000ULL 
                                                   & vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop) 
                                                  | (IData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                                                                    [1U])));
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop = ((0x1ffffffffULL 
                                                   & vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop) 
                                                  | ((QData)((IData)(
                                                                     (7U 
                                                                      & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                                                                                [1U] 
                                                                                >> 0x21U))))) 
                                                     << 0x21U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop = ((0xeffffffffULL 
                                                   & vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop) 
                                                  | ((QData)((IData)(
                                                                     (1U 
                                                                      & ((~ (IData)(vlTOPp->rst)) 
                                                                         & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__writeback_block__wb_packet
                                                                                [1U] 
                                                                                >> 0x20U)))))) 
                                                     << 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.data_valid) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)) 
           | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.data_valid));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.quotient 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp;
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc = 0x80000000U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__update_pc) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                = (0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__next_pc);
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]) 
           | (0xff000000U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__.addr 
                             << 0x16U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
        = ((0xffc00000U & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U]) 
           | (0x3fffffU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__.addr 
                           >> 0xaU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0x80000U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0xf00000U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0xfffbffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0xfffc1fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0x800U | (0xffffe7ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = ((0x3fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]) 
           | (0xffffc000U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__.addr 
                             << 0xcU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
        = (0xfffU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__.addr 
                     >> 0x14U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0x200U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0x3c00U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0xffffff07U & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (6U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp = 0U;
    } else {
        if ((1U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[4U])) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr;
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][2U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh
           [1U] ? (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__result 
                           >> 0x20U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__result));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                  & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released)) 
                 >> (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__genblk3__DOT__genblk1__DOT__axi_bus__data_out;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xe001ffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xfffe0000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__ls_offset) 
                             << 0x11U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xffffcfffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xfffff000U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_load_r) 
                              << 0xdU) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_store_r) 
                                          << 0xcU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT____Vcellinp__in_progress_m__set 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0xfffffff3U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (0xfffffffcU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op_r) 
                             << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_shifter_path) 
               << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_slt_path)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x3bU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                     [1U] << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr
        [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest];
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [1U];
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_done = 0U;
    } else {
        if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_done 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___GEN_5;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_taskID 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_10));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__value = 0U;
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
            if ((1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__value 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_62;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__cycleCount 
        = ((IData)(vlTOPp->rst) ? 0U : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_10));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__value = 0U;
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
            if ((1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__value 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_62;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_taskID 
                = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
        }
    }
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0x1ffffffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfe000000U 
                                                     & ((IData)(
                                                                (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                  << 0x23U) 
                                                                 | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                     << 3U) 
                                                                    | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast))))))) 
                                                        << 0x19U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[2U] = ((0x1ffffffU 
                                                   & ((IData)(
                                                              (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                << 0x23U) 
                                                               | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                   << 3U) 
                                                                  | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast))))))) 
                                                      >> 7U)) 
                                                  | (0xfe000000U 
                                                     & ((IData)(
                                                                ((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                   << 0x23U) 
                                                                  | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                      << 3U) 
                                                                     | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast)))))) 
                                                                 >> 0x20U)) 
                                                        << 0x19U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xfffcU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0x1ffffffU 
                                                     & ((IData)(
                                                                ((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                   << 0x23U) 
                                                                  | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                      << 3U) 
                                                                     | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast)))))) 
                                                                 >> 0x20U)) 
                                                        >> 7U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xfffbU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0xfffffffcU 
                                                     & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate) 
                                                        << 2U)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0 = 0U;
    } else {
        if ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0 = 2U;
        } else {
            if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11) 
                 & (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr)))) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0 
                    = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
                        << 0x19U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U] 
                                     >> 7U));
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_1 = 0U;
    } else {
        if ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_1 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles;
        } else {
            if (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_11) 
                 & (4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__waddr)))) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_1 
                    = ((vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[9U] 
                        << 0x19U) | (vlTOPp->taiga_sim__DOT____Vcellout__taiga_axi_bus__mst_ports_req_o[8U] 
                                     >> 7U));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg 
        = ((~ (IData)(vlTOPp->rst)) & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state) 
                                           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0)))) 
                                       & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_34)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0 
        = ((~ (IData)(vlTOPp->rst)) & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state) 
                                           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0)))) 
                                       & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_28)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0 
        = ((~ (IData)(vlTOPp->rst)) & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state) 
                                           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1)))) 
                                       & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_30)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0 
        = ((~ (IData)(vlTOPp->rst)) & ((~ ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state) 
                                           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2)))) 
                                       & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_32)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13) 
            << 0xdU) | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12) 
                         << 0xcU) | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11) 
                                      << 0xbU) | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10) 
                                                   << 0xaU) 
                                                  | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9) 
                                                      << 9U) 
                                                     | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8) 
                                                         << 8U) 
                                                        | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7) 
                                                            << 7U) 
                                                           | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6) 
                                                               << 6U) 
                                                              | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5) 
                                                                  << 5U) 
                                                                 | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4) 
                                                                     << 4U) 
                                                                    | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3) 
                                                                        << 3U) 
                                                                       | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2) 
                                                                           << 2U) 
                                                                          | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1) 
                                                                              << 1U) 
                                                                             | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0))))))))))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5;
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5;
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5;
                    }
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7;
            }
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7;
                }
            } else {
                if ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R = 0U;
                    } else {
                        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R 
                                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7;
                        }
                    }
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7;
                    }
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R 
        = ((~ (IData)(vlTOPp->rst)) & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7)
                                        : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7))
                                            : ((2U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                                                ? (
                                                   (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55)) 
                                                   & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7))
                                                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7)))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_control 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_control = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R = 1U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8) {
                __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_taskID 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_1_bits_taskID;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_taskID = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_control 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_R_control = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R = 1U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8) {
                __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_taskID 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_2_bits_taskID;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_taskID = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_control 
                        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_R_control = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8) {
                if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9)))) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R = 1U;
                }
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R = 0U;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8) {
                __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9) {
                    __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state = 0U;
                }
            }
        }
    }
    if (VL_UNLIKELY(((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_62) 
                     & (~ (IData)(vlTOPp->rst))))) {
        VL_FWRITEF(0x80000002U,"[LOG] [Vadd] [TID: %2#] [COMPUTE] [binaryOp_indvars_iv_next14] [Pred: %1#] [In(0): 0x%x] [In(1) 0x%x] [Out: 0x%x] [OpCode: add] [Cycle: %5#]\n",
                   5,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__taskID,
                   1,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control),
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_R_data,
                   64,vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out,
                   15,(IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__cycleCount));
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1 = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1 
                    = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)));
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1 = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1 
                            = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)));
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1 
                        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)));
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_31;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state = 0U;
                }
            }
        }
    }
    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R 
        = ((IData)(vlTOPp->rst) ? 0ULL : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23)
                                           ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control)
                                               ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out
                                               : 0ULL)
                                           : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)
                                               ? ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47)
                                                   ? 0ULL
                                                   : 
                                                  ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control)
                                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out
                                                    : 0ULL))
                                               : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_control)
                                                   ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out
                                                   : 0ULL))));
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_update) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__rollback_wb_group 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group
            [vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_table_write_index];
    }
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.data_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__read_index];
    vlTOPp->ddr_axi_araddr_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_addr;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->ddr_axi_awaddr_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_addr;
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode = 0U;
    } else {
        if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))) {
            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_408;
        } else {
            if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__block_rmeta_tag 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag
        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag___05FT_447_addr_pipe_0];
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_data_reg_addr;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_init_clear 
        = (2U == vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__next_state);
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table
        [vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__id_ex];
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x38fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_id) 
                     << 0xcU));
    if ((0x20U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__instruction_id 
            = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                      << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                   >> 3U)));
    }
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs1_addr 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                         << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                     >> 0x12U)));
    }
    if ((4U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__issue_to))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk9__DOT__prev_div_rs2_addr 
            = (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                         << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                   >> 0x17U)));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                  >> (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)) 
                 | (0U != ((0xbU >= (0xfU & ((IData)(3U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index))))
                            ? (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                                     >> (0xfU & ((IData)(3U) 
                                                 * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)))))
                            : 0U))));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted = 0U;
    } else {
        if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted 
                = ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
                    << 0x19U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
                                 >> 7U));
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3f1ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram
                     [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__read_index] 
                     << 9U));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate = 0U;
        } else {
            if ((1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count) 
                        >> (0xfU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter))) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_1 
        = (0x7ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                             - vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_2 
        = (0x7ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                             - vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_2));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_3 
        = (0x7ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                             - vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_3));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted = 0U;
    } else {
        if (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted 
                = ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U] 
                    << 0x19U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U] 
                                 >> 7U));
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3ffc7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (0x38U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                              << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_done 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.done) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress) 
              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                 >> 3U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3fe3fU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id
                     [1U] << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec 
        = (0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec);
    if ((0x20U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U])) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec 
            = ((3U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec) 
               | (0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc 
        = (0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc);
    if ((1U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U] 
                >> 1U) | (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                  >> 0x28U))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc 
            = ((3U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc) 
               | (0xfffffffcU & (((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                                 >> 0x28U)))
                                   ? (vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                      [vlTOPp->taiga_sim__DOT__cpu__DOT__exception_id] 
                                      >> 2U) : (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
                                                >> 2U)) 
                                 << 2U)));
    }
    if ((1U & ((((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                  >> 2U) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed_issue_stage) 
                            >> 5U)) & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready) 
                                       >> 5U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_hold))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[0U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U];
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[1U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[1U];
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U];
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U];
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[4U] 
            = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[4U];
    }
    vlTOPp->instruction_pc_dec = ((vlTOPp->taiga_sim__DOT__tr[3U] 
                                   << 0x1dU) | (vlTOPp->taiga_sim__DOT__tr[2U] 
                                                >> 3U));
    vlTOPp->instruction_data_dec = ((vlTOPp->taiga_sim__DOT__tr[2U] 
                                     << 0x1dU) | (vlTOPp->taiga_sim__DOT__tr[1U] 
                                                  >> 3U));
    vlTOPp->instruction_issued = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                        >> 0x1cU));
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[1U] 
                                                 >> 2U));
    vlTOPp->taiga_events[0U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[1U] 
                                                 >> 1U));
    vlTOPp->taiga_events[1U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & vlTOPp->taiga_sim__DOT__tr[1U]);
    vlTOPp->taiga_events[2U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1fU));
    vlTOPp->taiga_events[3U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1eU));
    vlTOPp->taiga_events[4U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1dU));
    vlTOPp->taiga_events[5U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1cU));
    vlTOPp->taiga_events[6U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1bU));
    vlTOPp->taiga_events[7U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1aU));
    vlTOPp->taiga_events[8U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x19U));
    vlTOPp->taiga_events[9U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x18U));
    vlTOPp->taiga_events[0xaU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x17U));
    vlTOPp->taiga_events[0xbU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x16U));
    vlTOPp->taiga_events[0xcU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x15U));
    vlTOPp->taiga_events[0xdU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x14U));
    vlTOPp->taiga_events[0xeU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x13U));
    vlTOPp->taiga_events[0xfU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x12U));
    vlTOPp->taiga_events[0x10U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x11U));
    vlTOPp->taiga_events[0x11U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x10U));
    vlTOPp->taiga_events[0x12U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xfU));
    vlTOPp->taiga_events[0x13U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xeU));
    vlTOPp->taiga_events[0x14U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xdU));
    vlTOPp->taiga_events[0x15U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xcU));
    vlTOPp->taiga_events[0x16U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xbU));
    vlTOPp->taiga_events[0x17U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xaU));
    vlTOPp->taiga_events[0x18U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 9U));
    vlTOPp->taiga_events[0x19U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 8U));
    vlTOPp->taiga_events[0x1aU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 7U));
    vlTOPp->taiga_events[0x1bU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 6U));
    vlTOPp->taiga_events[0x1cU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 5U));
    vlTOPp->taiga_events[0x1dU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 4U));
    vlTOPp->taiga_events[0x1eU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 3U));
    vlTOPp->taiga_events[0x1fU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 2U));
    vlTOPp->taiga_events[0x20U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 1U));
    vlTOPp->taiga_events[0x21U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & vlTOPp->taiga_sim__DOT__tr[0U]);
    vlTOPp->taiga_events[0x22U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_is_idle = 
        (1U & (((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                    >> 3U)) & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid))))) 
               & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid))));
    if ((1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__instruction_is_completing) 
               | (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__is_return 
            = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
                     >> 0x15U));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_misc_op = 
        ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
         & (~ ((((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_alu_op) 
                   | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_branch_or_jump_op)) 
                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_load_op)) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_store_op)) 
                | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_mul_op)) 
               | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_div_op))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__tr_alu_op = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__instruction_issued) 
                                                   & ((((((0xcU 
                                                           == 
                                                           (0x1fU 
                                                            & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                << 0x11U) 
                                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                  >> 0xfU)))) 
                                                          | (4U 
                                                             == 
                                                             (0x1fU 
                                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                  << 0x11U) 
                                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                    >> 0xfU))))) 
                                                         | (5U 
                                                            == 
                                                            (0x1fU 
                                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                 << 0x11U) 
                                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                   >> 0xfU))))) 
                                                        | (0xdU 
                                                           == 
                                                           (0x1fU 
                                                            & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                << 0x11U) 
                                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                                  >> 0xfU))))) 
                                                       & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_mul_op))) 
                                                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__tr_div_op))));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U] 
        = ((0x7ffffU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
           | (0xfff80000U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                       << 0x2dU) | 
                                      (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                        << 0xdU) | (QData)((IData)(
                                                                   ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                    << 2U)))))) 
                             << 0x13U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
        = ((0x7ffffU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                  << 0x2dU) | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                                << 0xdU) 
                                               | (QData)((IData)(
                                                                 ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                  << 2U)))))) 
                        >> 0xdU)) | (0xfff80000U & 
                                     ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                                 << 0x2dU) 
                                                | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                                    << 0xdU) 
                                                   | (QData)((IData)(
                                                                     ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                      << 2U))))) 
                                               >> 0x20U)) 
                                      << 0x13U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0x7ffffU & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                     << 0x2dU) | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                                   << 0xdU) 
                                                  | (QData)((IData)(
                                                                    ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                     << 2U))))) 
                                   >> 0x20U)) >> 0xdU)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
        = ((0x3fU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U]) 
           | (0xffffffc0U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awid)) 
                                       << 0x2dU) | 
                                      (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr)) 
                                        << 0xdU) | (QData)((IData)(
                                                                   ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize) 
                                                                    << 2U)))))) 
                             << 6U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U]) 
           | ((0x3fU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awid)) 
                                  << 0x2dU) | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr)) 
                                                << 0xdU) 
                                               | (QData)((IData)(
                                                                 ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize) 
                                                                  << 2U)))))) 
                        >> 0x1aU)) | (0xffffffc0U & 
                                      ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awid)) 
                                                  << 0x2dU) 
                                                 | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr)) 
                                                     << 0xdU) 
                                                    | (QData)((IData)(
                                                                      ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize) 
                                                                       << 2U))))) 
                                                >> 0x20U)) 
                                       << 6U))));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
        = ((0xffffbfffU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U]) 
           | (0xffffc000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__awvalid_m__result) 
                             << 0xeU)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U] 
        = (1U | ((0xfffffffcU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__arvalid_m__result) 
                    << 1U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
        = ((0xffffff3fU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0xffffffc0U & (0x40U | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result) 
                                      << 7U))));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
        = ((0x1ffU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0xfffffe00U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata)) 
                                       << 5U) | (QData)((IData)(
                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb) 
                                                                  << 1U) 
                                                                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result)))))) 
                             << 9U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
        = ((0xffffc000U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U]) 
           | ((0x1ffU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata)) 
                                   << 5U) | (QData)((IData)(
                                                            (((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb) 
                                                              << 1U) 
                                                             | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result)))))) 
                         >> 0x17U)) | (0xfffffe00U 
                                       & ((IData)((
                                                   (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata)) 
                                                     << 5U) 
                                                    | (QData)((IData)(
                                                                      (((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb) 
                                                                        << 1U) 
                                                                       | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result))))) 
                                                   >> 0x20U)) 
                                          << 9U))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push = 0U;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push 
        = (((~ ((IData)(1U) << (1U & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out 
                                              >> 0x22U))))) 
            & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push)) 
           | ((1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                     >> 4U)) << (1U & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out 
                                               >> 0x22U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate;
    vlTOPp->taiga_sim__DOT__axi_arvalid = __Vdly__taiga_sim__DOT__axi_arvalid;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid 
        = (1U & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                          [0U] >> 3U)) & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                  [0U] 
                                                  >> 6U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid 
        = (((3U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
            | ((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
               & ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                              [0U] >> 4U))) | ((IData)(
                                                       (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                        [0U] 
                                                        >> 4U)) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                  >> 0x12U))))) 
           | ((0U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
              & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                         [0U] >> 4U))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid) 
           & (1U == (3U & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                   [0U] >> 0x20U)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid) 
           & (3U == (3U & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                   [0U] >> 0x20U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid 
        = (((3U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege)) 
            | ((1U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege)) 
               & ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                              [0U] >> 4U))) | ((IData)(
                                                       (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                        [0U] 
                                                        >> 4U)) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                  >> 0x12U))))) 
           | ((0U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege)) 
              & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                         [0U] >> 4U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_3));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[0U] = 0U;
    if ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[0U] = 0U;
    }
    if ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[0U] = 1U;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[1U] = 1U;
    if ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[1U] = 1U;
    }
    if ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[1U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests[0U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests[1U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count 
        = __Vdly__taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_10 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__ready_for_issue_from_lsq 
        = ((3U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_switch_stall)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x3eU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
        = ((1ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry) 
           | (0xfffffffffeULL & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr));
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_40 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_31 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_30 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state 
        = __Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_12 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_62 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__value)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_20 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__cycleCount)));
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group__v0;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[1U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[2U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[3U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[4U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[5U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[6U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[7U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[7U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[8U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[9U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xaU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xbU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xcU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xdU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xeU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xfU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[7U];
    if (__Vdlyvset__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag[__Vdlyvdim0__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0] 
            = __Vdlyvval__taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag__v0;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__cycleCount)));
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_taskID 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_taskID;
                }
            }
        }
    }
    if (vlTOPp->rst) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15) {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control 
                    = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control;
            }
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control = 0U;
                } else {
                    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control 
                            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control;
                    }
                }
            } else {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) {
                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_R_0_control 
                        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_valid_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_fire_R_0 
        = ((~ (IData)(vlTOPp->rst)) & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15)
                                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_6)
                                        : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)
                                            ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0)) 
                                               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_6))
                                            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_6))));
    if (vlTOPp->rst) {
        vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15) {
            vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state 
                = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_10;
        } else {
            if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state) {
                if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0) {
                    vlTOPp->__Vdly__taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state = 0U;
                }
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count 
        = __Vdly__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__shift_count;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign 
        = ((4U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_3 
                           >> 0x22U)) << 2U)) | ((2U 
                                                  & ((IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_2 
                                                              >> 0x22U)) 
                                                     << 1U)) 
                                                 | (1U 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_1 
                                                               >> 0x22U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x3dU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_done) 
                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__done_r)) 
                     << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict 
        = (0U != ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out) 
                  & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid)));
    if (__Vdlyvset__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table[__Vdlyvdim0__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0] 
            = __Vdlyvval__taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table__v0;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
        = ((0xfffffff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ecall) 
               << 2U) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ebreak) 
                          << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ret))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r 
        = __Vdly__taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r;
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xffdfffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xffe00000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_return) 
                             << 0x15U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xffbfffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xffc00000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_call) 
                             << 0x16U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xffe00000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset_r);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o = 0U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = 1U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = 0U;
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o = 0U;
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = 1U;
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = 0U;
    }
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o = 0U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
}
