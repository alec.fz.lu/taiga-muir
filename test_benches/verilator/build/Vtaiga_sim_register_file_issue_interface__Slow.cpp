// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim_register_file_issue_interface.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_CTOR_IMP(Vtaiga_sim_register_file_issue_interface) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void Vtaiga_sim_register_file_issue_interface::__Vconfigure(Vtaiga_sim__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

Vtaiga_sim_register_file_issue_interface::~Vtaiga_sim_register_file_issue_interface() {
}

void Vtaiga_sim_register_file_issue_interface::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+            Vtaiga_sim_register_file_issue_interface::_ctor_var_reset\n"); );
    // Body
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        phys_rs_addr[__Vi0] = VL_RAND_RESET_I(6);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        rs_wb_group[__Vi0] = VL_RAND_RESET_I(1);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        data[__Vi0] = VL_RAND_RESET_I(32);
    }
    for (int __Vi0=0; __Vi0<2; ++__Vi0) {
        inuse[__Vi0] = VL_RAND_RESET_I(1);
    }
}
