// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim_axi_lite_mux__pi5.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_INLINE_OPT void Vtaiga_sim_axi_lite_mux__pi5::_sequent__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__3(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          Vtaiga_sim_axi_lite_mux__pi5::_sequent__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__3\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n));
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n));
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n));
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n));
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__gate_clock)))) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__gate_clock)))) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill) {
            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] 
                = this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] 
                = this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] 
                = this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
        }
    } else {
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] = 0U;
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] = 0U;
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] = 0U;
    }
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill) 
             | (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain))) {
            this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q 
                = this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill) 
             | (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain))) {
            this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q 
                = this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q 
            = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n;
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q 
            = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n;
    } else {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q = 0U;
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill) {
            this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] 
                = this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
            this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] 
                = this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
            this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] 
                = this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
        }
    } else {
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U] = 0U;
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U] = 0U;
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U] = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill) 
             | (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain))) {
            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q 
                = this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill) 
             | (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain))) {
            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q 
                = this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q = 0U;
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__gate_clock)))) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n;
        }
    } else {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q = 0U;
    }
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q 
        = ((IData)(vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes) 
              & (~ (IData)(this->__PVT__gen_mux__DOT__ar_ready))));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q 
            = this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d;
        this->__PVT__gen_mux__DOT__lock_aw_valid_q 
            = (((IData)(this->__PVT__gen_mux__DOT__load_aw_lock)
                 ? (IData)(this->__PVT__gen_mux__DOT__lock_aw_valid_d)
                 : (IData)(this->__PVT__gen_mux__DOT__lock_aw_valid_q)) 
               & 1U);
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q 
            = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n;
        this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q 
            = (((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes) 
                & (~ (IData)(this->__PVT__gen_mux__DOT__aw_ready))) 
               & 1U);
        this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q 
            = this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d;
    } else {
        this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q = 0U;
        this->__PVT__gen_mux__DOT__lock_aw_valid_q = 0U;
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q = 0U;
        this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q = 0U;
        this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q = 0U;
    }
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill) {
            if (this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel) {
                this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                    = ((this->__PVT__gen_mux__DOT__slv_ar_chans[3U] 
                        << 0x1cU) | (this->__PVT__gen_mux__DOT__slv_ar_chans[2U] 
                                     >> 4U));
                this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                    = ((this->__PVT__gen_mux__DOT__slv_ar_chans[4U] 
                        << 0x1cU) | (this->__PVT__gen_mux__DOT__slv_ar_chans[3U] 
                                     >> 4U));
                this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                    = (0xfU & (this->__PVT__gen_mux__DOT__slv_ar_chans[4U] 
                               >> 4U));
            } else {
                this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                    = this->__PVT__gen_mux__DOT__slv_ar_chans[0U];
                this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                    = this->__PVT__gen_mux__DOT__slv_ar_chans[1U];
                this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                    = (0xfU & this->__PVT__gen_mux__DOT__slv_ar_chans[2U]);
            }
        }
    } else {
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] = 0U;
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] = 0U;
        this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] = 0U;
    }
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    this->__PVT__gen_mux__DOT__mst_aw_ready = (1U & 
                                               ((~ (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                                                | (~ (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    if (vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni) {
        if (this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill) {
            if (this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel) {
                this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                    = ((this->__PVT__gen_mux__DOT__slv_aw_chans[3U] 
                        << 0x16U) | (this->__PVT__gen_mux__DOT__slv_aw_chans[2U] 
                                     >> 0xaU));
                this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                    = ((this->__PVT__gen_mux__DOT__slv_aw_chans[4U] 
                        << 0x16U) | (this->__PVT__gen_mux__DOT__slv_aw_chans[3U] 
                                     >> 0xaU));
                this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                    = (0x3ffU & (this->__PVT__gen_mux__DOT__slv_aw_chans[4U] 
                                 >> 0xaU));
            } else {
                this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] 
                    = this->__PVT__gen_mux__DOT__slv_aw_chans[0U];
                this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] 
                    = this->__PVT__gen_mux__DOT__slv_aw_chans[1U];
                this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] 
                    = (0x3ffU & this->__PVT__gen_mux__DOT__slv_aw_chans[2U]);
            }
        }
    } else {
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U] = 0U;
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U] = 0U;
        this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U] = 0U;
    }
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    this->__PVT__gen_mux__DOT__mst_ar_ready = (1U & 
                                               ((~ (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                                                | (~ (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    this->__PVT__gen_mux__DOT__ar_ready = ((1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                           & (IData)(this->__PVT__gen_mux__DOT__mst_ar_ready));
}

VL_INLINE_OPT void Vtaiga_sim_axi_lite_mux__pi5::_combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__5(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          Vtaiga_sim_axi_lite_mux__pi5::_combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__5\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*127:0*/ __Vtemp54[4];
    // Body
    this->slv_resps_o[1U] = ((0xfff007ffU & this->slv_resps_o[1U]) 
                             | (0xff800U & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]));
    this->slv_resps_o[3U] = ((0xf007U & this->slv_resps_o[3U]) 
                             | (0xff8U & ((vlTOPp->taiga_sim__DOT__mst_ports_resp[2U] 
                                           << 0x18U) 
                                          | (0xfffff8U 
                                             & (vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                                >> 8U)))));
    this->slv_resps_o[0U] = (IData)((0x3ffffffffffULL 
                                     & (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                         << 0x20U) 
                                        | (QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__mst_ports_resp[0U])))));
    this->slv_resps_o[1U] = ((0xfffffc00U & this->slv_resps_o[1U]) 
                             | (IData)(((0x3ffffffffffULL 
                                         & (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                             << 0x20U) 
                                            | (QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__mst_ports_resp[0U])))) 
                                        >> 0x20U)));
    this->slv_resps_o[1U] = ((0xffffffU & this->slv_resps_o[1U]) 
                             | (0xff000000U & ((IData)(
                                                       (0x3ffffffffffULL 
                                                        & (((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                            << 0x20U) 
                                                           | (QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__mst_ports_resp[0U]))))) 
                                               << 0x18U)));
    this->slv_resps_o[2U] = ((0xffffffU & ((IData)(
                                                   (0x3ffffffffffULL 
                                                    & (((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                        << 0x20U) 
                                                       | (QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__mst_ports_resp[0U]))))) 
                                           >> 8U)) 
                             | (0xff000000U & ((IData)(
                                                       ((0x3ffffffffffULL 
                                                         & (((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__mst_ports_resp[0U])))) 
                                                        >> 0x20U)) 
                                               << 0x18U)));
    this->slv_resps_o[3U] = ((0xfffcU & this->slv_resps_o[3U]) 
                             | (0xffffffU & ((IData)(
                                                     ((0x3ffffffffffULL 
                                                       & (((QData)((IData)(
                                                                           vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                           << 0x20U) 
                                                          | (QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__mst_ports_resp[0U])))) 
                                                      >> 0x20U)) 
                                             >> 8U)));
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
              >> 0x17U));
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
              >> 0x16U));
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                 >> 0x17U)));
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                 >> 0x16U)));
    this->__PVT__gen_mux__DOT__slv_aw_chans[0U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[4U] 
                                                    << 0x11U) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[3U] 
                                                      >> 0xfU));
    this->__PVT__gen_mux__DOT__slv_aw_chans[1U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
                                                    << 0x11U) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[4U] 
                                                      >> 0xfU));
    this->__PVT__gen_mux__DOT__slv_aw_chans[2U] = (
                                                   (0xfffffc00U 
                                                    & this->__PVT__gen_mux__DOT__slv_aw_chans[2U]) 
                                                   | (0x3ffU 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[6U] 
                                                          << 0x11U) 
                                                         | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
                                                            >> 0xfU))));
    this->__PVT__gen_mux__DOT__slv_aw_chans[2U] = (
                                                   (0x3ffU 
                                                    & this->__PVT__gen_mux__DOT__slv_aw_chans[2U]) 
                                                   | (0xfffffc00U 
                                                      & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[9U] 
                                                         << 2U)));
    this->__PVT__gen_mux__DOT__slv_aw_chans[3U] = (
                                                   (0x3ffU 
                                                    & ((0x3fcU 
                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xaU] 
                                                           << 2U)) 
                                                       | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[9U] 
                                                          >> 0x1eU))) 
                                                   | (0xfffffc00U 
                                                      & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xaU] 
                                                         << 2U)));
    this->__PVT__gen_mux__DOT__slv_aw_chans[4U] = (
                                                   (0x3ffU 
                                                    & ((0x3fcU 
                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xbU] 
                                                           << 2U)) 
                                                       | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xaU] 
                                                          >> 0x1eU))) 
                                                   | (0xffc00U 
                                                      & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xbU] 
                                                         << 2U)));
    this->__PVT__gen_mux__DOT__slv_ar_chans[0U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[1U] 
                                                    << 0x1eU) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0U] 
                                                      >> 2U));
    this->__PVT__gen_mux__DOT__slv_ar_chans[1U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[2U] 
                                                    << 0x1eU) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[1U] 
                                                      >> 2U));
    this->__PVT__gen_mux__DOT__slv_ar_chans[2U] = (
                                                   (0xfffffff0U 
                                                    & this->__PVT__gen_mux__DOT__slv_ar_chans[2U]) 
                                                   | (0xfU 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[3U] 
                                                          << 0x1eU) 
                                                         | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[2U] 
                                                            >> 2U))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[2U] = (
                                                   (0xfU 
                                                    & this->__PVT__gen_mux__DOT__slv_ar_chans[2U]) 
                                                   | (0xfffffff0U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[6U] 
                                                          << 9U) 
                                                         | (0x1f0U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
                                                               >> 0x17U)))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[3U] = (
                                                   (0xfU 
                                                    & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[6U] 
                                                       >> 0x17U)) 
                                                   | (0xfffffff0U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[7U] 
                                                          << 9U) 
                                                         | (0x1f0U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[6U] 
                                                               >> 0x17U)))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[4U] = (
                                                   (0xfU 
                                                    & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[7U] 
                                                       >> 0x17U)) 
                                                   | (0xf0U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[8U] 
                                                          << 9U) 
                                                         | (0x1f0U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[7U] 
                                                               >> 0x17U)))));
    this->__PVT__gen_mux__DOT__slv_ar_valids = ((2U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_ar_valids)) 
                                                | (1U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0U] 
                                                      >> 1U)));
    this->__PVT__gen_mux__DOT__slv_ar_valids = ((1U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_ar_valids)) 
                                                | (2U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
                                                      >> 0x19U)));
    this->__PVT__gen_mux__DOT__slv_aw_valids = ((2U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_aw_valids)) 
                                                | (1U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[3U] 
                                                      >> 0xeU)));
    this->__PVT__gen_mux__DOT__slv_aw_valids = ((1U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_aw_valids)) 
                                                | (2U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[9U] 
                                                      >> 6U)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q)
            ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q)
            : (IData)(this->__PVT__gen_mux__DOT__slv_ar_valids));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q)
            ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q)
            : (IData)(this->__PVT__gen_mux__DOT__slv_aw_valids));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (0xfffffffeU & (((1U <= (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)) 
           | (0xfffffffeU & (((1U > (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel 
        = (1U & ((~ (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d)) 
                 | (((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d) 
                     >> 1U) & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes 
        = (1U & ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d) 
                 | ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d) 
                    >> 1U)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (0xfffffffeU & (((1U <= (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)) 
           | (0xfffffffeU & (((1U > (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel 
        = (1U & ((~ (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d)) 
                 | (((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d) 
                     >> 1U) & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes 
        = (1U & ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d) 
                 | ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d) 
                    >> 1U)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__slv_ar_readies = ((2U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_ar_readies)) 
                                                 | ((IData)(this->__PVT__gen_mux__DOT__ar_ready) 
                                                    & (~ (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel))));
    this->__PVT__gen_mux__DOT__slv_ar_readies = ((1U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_ar_readies)) 
                                                 | (((IData)(this->__PVT__gen_mux__DOT__ar_ready) 
                                                     & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel)) 
                                                    << 1U));
    this->__PVT__gen_mux__DOT__mst_ar_valid = ((1U 
                                                != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                               & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__lock_aw_valid_d = this->__PVT__gen_mux__DOT__lock_aw_valid_q;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
            this->__PVT__gen_mux__DOT__lock_aw_valid_d = 0U;
        }
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__mst_aw_ready)))) {
                this->__PVT__gen_mux__DOT__lock_aw_valid_d = 1U;
            }
        }
    }
    this->__PVT__gen_mux__DOT__load_aw_lock = 0U;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
            this->__PVT__gen_mux__DOT__load_aw_lock = 1U;
        }
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__mst_aw_ready)))) {
                this->__PVT__gen_mux__DOT__load_aw_lock = 1U;
            }
        }
    }
    this->__PVT__gen_mux__DOT__mst_aw_valid = 0U;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        this->__PVT__gen_mux__DOT__mst_aw_valid = 1U;
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            this->__PVT__gen_mux__DOT__mst_aw_valid = 1U;
        }
    }
    this->__PVT__gen_mux__DOT__aw_ready = 0U;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
            this->__PVT__gen_mux__DOT__aw_ready = 1U;
        }
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
                this->__PVT__gen_mux__DOT__aw_ready = 1U;
            }
        }
    }
    this->__PVT__gen_mux__DOT__w_fifo_push = 0U;
    if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__lock_aw_valid_q)))) {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            this->__PVT__gen_mux__DOT__w_fifo_push = 1U;
        }
    }
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes)) 
           | (1U & ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                    | ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                       >> 1U))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->slv_resps_o[1U] = ((0xffbfffffU & this->slv_resps_o[1U]) 
                             | (0x400000U & ((IData)(this->__PVT__gen_mux__DOT__slv_ar_readies) 
                                             << 0x16U)));
    this->slv_resps_o[3U] = ((0xbfffU & this->slv_resps_o[3U]) 
                             | (0x4000U & ((IData)(this->__PVT__gen_mux__DOT__slv_ar_readies) 
                                           << 0xdU)));
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__mst_ar_valid) 
           & (IData)(this->__PVT__gen_mux__DOT__mst_ar_ready));
    this->__PVT__gen_mux__DOT__r_fifo_push = ((IData)(this->__PVT__gen_mux__DOT__mst_ar_valid) 
                                              & (IData)(this->__PVT__gen_mux__DOT__mst_ar_ready));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes)) 
           | (1U & ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                    | ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                       >> 1U))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__mst_aw_valid) 
           & (IData)(this->__PVT__gen_mux__DOT__mst_aw_ready));
    this->__PVT__gen_mux__DOT__slv_aw_readies = ((2U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_aw_readies)) 
                                                 | ((IData)(this->__PVT__gen_mux__DOT__aw_ready) 
                                                    & (~ (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel))));
    this->__PVT__gen_mux__DOT__slv_aw_readies = ((1U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_aw_readies)) 
                                                 | (((IData)(this->__PVT__gen_mux__DOT__aw_ready) 
                                                     & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel)) 
                                                    << 1U));
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->gen_mux__DOT__i_w_fifo__DOT____Vlvbound1 
            = this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
        if ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q))) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q))) 
                    & (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n)) 
                   | ((IData)(this->gen_mux__DOT__i_w_fifo__DOT____Vlvbound1) 
                      << (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q)));
        }
    }
    this->__PVT__gen_mux__DOT__w_fifo_empty = ((0U 
                                                == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
                                               & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_push)));
    this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o 
        = ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q)) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q) 
              >> (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o 
            = this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
    }
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d 
        = (1U & (((IData)(this->__PVT__gen_mux__DOT__ar_ready) 
                  & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes))
                  ? ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes))
                      ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)
                      : (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes))
                  : (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q)));
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->gen_mux__DOT__i_r_fifo__DOT____Vlvbound1 
            = this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
        if ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q))) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q))) 
                    & (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n)) 
                   | ((IData)(this->gen_mux__DOT__i_r_fifo__DOT____Vlvbound1) 
                      << (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q)));
        }
    }
    this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o 
        = ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q)) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q) 
              >> (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o 
            = this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
    }
    this->__PVT__gen_mux__DOT__r_fifo_empty = ((0U 
                                                == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                               & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_push)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d 
        = (1U & (((IData)(this->__PVT__gen_mux__DOT__aw_ready) 
                  & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))
                  ? ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes))
                      ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)
                      : (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes))
                  : (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q)));
    this->slv_resps_o[1U] = ((0xff7fffffU & this->slv_resps_o[1U]) 
                             | (0x800000U & ((IData)(this->__PVT__gen_mux__DOT__slv_aw_readies) 
                                             << 0x17U)));
    this->slv_resps_o[3U] = ((0x7fffU & this->slv_resps_o[3U]) 
                             | (0x8000U & ((IData)(this->__PVT__gen_mux__DOT__slv_aw_readies) 
                                           << 0xeU)));
    this->__PVT__gen_mux__DOT__mst_w_valid = (((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                               & (1U 
                                                  != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                              & ((0x171U 
                                                  >= 
                                                  (0x1ffU 
                                                   & ((IData)(0x47U) 
                                                      + 
                                                      ((IData)(0xb9U) 
                                                       * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                 & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                    (0xfU 
                                                     & (((IData)(0x47U) 
                                                         + 
                                                         ((IData)(0xb9U) 
                                                          * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                        >> 5U))] 
                                                    >> 
                                                    (0x1fU 
                                                     & ((IData)(0x47U) 
                                                        + 
                                                        ((IData)(0xb9U) 
                                                         * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))));
    this->slv_resps_o[1U] = ((0xffdfffffU & this->slv_resps_o[1U]) 
                             | (0xffe00000U & (((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                                 & ((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                    << 0x15U)) 
                                                & ((1U 
                                                    != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
                                                   << 0x15U)) 
                                               & ((~ (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)) 
                                                  << 0x15U))));
    this->slv_resps_o[3U] = ((0xdfffU & this->slv_resps_o[3U]) 
                             | (0xffe000U & ((((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                                >> 8U) 
                                               & ((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                  << 0xdU)) 
                                              & ((1U 
                                                  != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
                                                 << 0xdU)) 
                                             & ((IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o) 
                                                << 0xdU))));
    this->__PVT__gen_mux__DOT__mst_r_ready = ((~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)) 
                                              & ((0x171U 
                                                  >= 
                                                  (0x1ffU 
                                                   & ((IData)(0xb9U) 
                                                      * (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o)))) 
                                                 & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                    (0xfU 
                                                     & (((IData)(0xb9U) 
                                                         * (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o)) 
                                                        >> 5U))] 
                                                    >> 
                                                    (0x1fU 
                                                     & ((IData)(0xb9U) 
                                                        * (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o))))));
    this->slv_resps_o[1U] = ((0xfffffbffU & this->slv_resps_o[1U]) 
                             | (0x400U & ((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                           & ((~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)) 
                                              << 0xaU)) 
                                          & ((~ (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o)) 
                                             << 0xaU))));
    this->slv_resps_o[3U] = ((0xfffbU & this->slv_resps_o[3U]) 
                             | (0xfffffcU & (((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                               >> 8U) 
                                              & ((~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)) 
                                                 << 2U)) 
                                             & ((IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o) 
                                                << 2U))));
    this->__PVT__gen_mux__DOT__w_fifo_pop = ((IData)(this->__PVT__gen_mux__DOT__mst_w_valid) 
                                             & (vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                                >> 0x15U));
    this->__PVT__gen_mux__DOT__r_fifo_pop = ((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                              >> 0xaU) 
                                             & (IData)(this->__PVT__gen_mux__DOT__mst_r_ready));
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
           & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop)) 
          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
            = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__w_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__w_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__w_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->gen_mux__DOT__i_b_fifo__DOT____Vlvbound1 
            = this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o;
        if ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q))) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q))) 
                    & (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n)) 
                   | ((IData)(this->gen_mux__DOT__i_b_fifo__DOT____Vlvbound1) 
                      << (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q)));
        }
    }
    this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o 
        = ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q)) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q) 
              >> (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o 
            = this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o;
    }
    this->__PVT__gen_mux__DOT__b_fifo_empty = ((0U 
                                                == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
                                               & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop)));
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
           & (IData)(this->__PVT__gen_mux__DOT__r_fifo_pop)) 
          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
            = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__r_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__r_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__r_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__mst_b_ready = ((~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)) 
                                              & ((0x171U 
                                                  >= 
                                                  (0x1ffU 
                                                   & ((IData)(0x46U) 
                                                      + 
                                                      ((IData)(0xb9U) 
                                                       * (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o))))) 
                                                 & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                    (0xfU 
                                                     & (((IData)(0x46U) 
                                                         + 
                                                         ((IData)(0xb9U) 
                                                          * (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o))) 
                                                        >> 5U))] 
                                                    >> 
                                                    (0x1fU 
                                                     & ((IData)(0x46U) 
                                                        + 
                                                        ((IData)(0xb9U) 
                                                         * (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o)))))));
    this->slv_resps_o[1U] = ((0xffefffffU & this->slv_resps_o[1U]) 
                             | (0x100000U & ((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                              & ((~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)) 
                                                 << 0x14U)) 
                                             & ((~ (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o)) 
                                                << 0x14U))));
    this->slv_resps_o[3U] = ((0xefffU & this->slv_resps_o[3U]) 
                             | (0xfff000U & (((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                               >> 8U) 
                                              & ((~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)) 
                                                 << 0xcU)) 
                                             & ((IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o) 
                                                << 0xcU))));
    this->__PVT__gen_mux__DOT__b_fifo_pop = ((vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] 
                                              >> 0x14U) 
                                             & (IData)(this->__PVT__gen_mux__DOT__mst_b_ready));
    __Vtemp54[2U] = ((0xffffff00U & ((IData)((((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                               & (1U 
                                                  != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))
                                               ? ((0x171U 
                                                   >= 
                                                   (0x1ffU 
                                                    & ((IData)(0x48U) 
                                                       + 
                                                       ((IData)(0xb9U) 
                                                        * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                   ? 
                                                  (0x3fffffffffULL 
                                                   & (((0U 
                                                        == 
                                                        (0x1fU 
                                                         & ((IData)(0x48U) 
                                                            + 
                                                            ((IData)(0xb9U) 
                                                             * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                        ? 0ULL
                                                        : 
                                                       ((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                        ((IData)(2U) 
                                                                         + 
                                                                         (0xfU 
                                                                          & (((IData)(0x48U) 
                                                                              + 
                                                                              ((IData)(0xb9U) 
                                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                             >> 5U)))])) 
                                                        << 
                                                        ((IData)(0x40U) 
                                                         - 
                                                         (0x1fU 
                                                          & ((IData)(0x48U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))) 
                                                      | (((QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                          ((IData)(1U) 
                                                                           + 
                                                                           (0xfU 
                                                                            & (((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                               >> 5U)))])) 
                                                          << 
                                                          ((0U 
                                                            == 
                                                            (0x1fU 
                                                             & ((IData)(0x48U) 
                                                                + 
                                                                ((IData)(0xb9U) 
                                                                 * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                            ? 0x20U
                                                            : 
                                                           ((IData)(0x20U) 
                                                            - 
                                                            (0x1fU 
                                                             & ((IData)(0x48U) 
                                                                + 
                                                                ((IData)(0xb9U) 
                                                                 * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))) 
                                                         | ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                            (0xfU 
                                                                             & (((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                                >> 5U))])) 
                                                            >> 
                                                            (0x1fU 
                                                             & ((IData)(0x48U) 
                                                                + 
                                                                ((IData)(0xb9U) 
                                                                 * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))
                                                   : 0ULL)
                                               : 0ULL)) 
                                     << 8U)) | ((0xffffff80U 
                                                 & ((IData)(this->__PVT__gen_mux__DOT__mst_w_valid) 
                                                    << 7U)) 
                                                | ((0xffffffc0U 
                                                    & ((IData)(this->__PVT__gen_mux__DOT__mst_b_ready) 
                                                       << 6U)) 
                                                   | ((3U 
                                                       & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                                            ? 
                                                           this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                                            : 
                                                           this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                                          >> 0x1eU)) 
                                                      | (0xfffffffcU 
                                                         & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                                              ? 
                                                             this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U]
                                                              : 
                                                             this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U]) 
                                                            << 2U))))));
    __Vtemp54[3U] = ((0xffU & ((IData)((((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))
                                         ? ((0x171U 
                                             >= (0x1ffU 
                                                 & ((IData)(0x48U) 
                                                    + 
                                                    ((IData)(0xb9U) 
                                                     * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                             ? (0x3fffffffffULL 
                                                & (((0U 
                                                     == 
                                                     (0x1fU 
                                                      & ((IData)(0x48U) 
                                                         + 
                                                         ((IData)(0xb9U) 
                                                          * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                     ? 0ULL
                                                     : 
                                                    ((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                     ((IData)(2U) 
                                                                      + 
                                                                      (0xfU 
                                                                       & (((IData)(0x48U) 
                                                                           + 
                                                                           ((IData)(0xb9U) 
                                                                            * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                          >> 5U)))])) 
                                                     << 
                                                     ((IData)(0x40U) 
                                                      - 
                                                      (0x1fU 
                                                       & ((IData)(0x48U) 
                                                          + 
                                                          ((IData)(0xb9U) 
                                                           * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))) 
                                                   | (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                       ((IData)(1U) 
                                                                        + 
                                                                        (0xfU 
                                                                         & (((IData)(0x48U) 
                                                                             + 
                                                                             ((IData)(0xb9U) 
                                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                            >> 5U)))])) 
                                                       << 
                                                       ((0U 
                                                         == 
                                                         (0x1fU 
                                                          & ((IData)(0x48U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                         ? 0x20U
                                                         : 
                                                        ((IData)(0x20U) 
                                                         - 
                                                         (0x1fU 
                                                          & ((IData)(0x48U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))) 
                                                      | ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                         (0xfU 
                                                                          & (((IData)(0x48U) 
                                                                              + 
                                                                              ((IData)(0xb9U) 
                                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                             >> 5U))])) 
                                                         >> 
                                                         (0x1fU 
                                                          & ((IData)(0x48U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))
                                             : 0ULL)
                                         : 0ULL)) >> 0x18U)) 
                     | (0xffffff00U & ((IData)(((((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                  & (1U 
                                                     != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))
                                                  ? 
                                                 ((0x171U 
                                                   >= 
                                                   (0x1ffU 
                                                    & ((IData)(0x48U) 
                                                       + 
                                                       ((IData)(0xb9U) 
                                                        * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                   ? 
                                                  (0x3fffffffffULL 
                                                   & (((0U 
                                                        == 
                                                        (0x1fU 
                                                         & ((IData)(0x48U) 
                                                            + 
                                                            ((IData)(0xb9U) 
                                                             * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                        ? 0ULL
                                                        : 
                                                       ((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                        ((IData)(2U) 
                                                                         + 
                                                                         (0xfU 
                                                                          & (((IData)(0x48U) 
                                                                              + 
                                                                              ((IData)(0xb9U) 
                                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                             >> 5U)))])) 
                                                        << 
                                                        ((IData)(0x40U) 
                                                         - 
                                                         (0x1fU 
                                                          & ((IData)(0x48U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))) 
                                                      | (((QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                          ((IData)(1U) 
                                                                           + 
                                                                           (0xfU 
                                                                            & (((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                               >> 5U)))])) 
                                                          << 
                                                          ((0U 
                                                            == 
                                                            (0x1fU 
                                                             & ((IData)(0x48U) 
                                                                + 
                                                                ((IData)(0xb9U) 
                                                                 * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                            ? 0x20U
                                                            : 
                                                           ((IData)(0x20U) 
                                                            - 
                                                            (0x1fU 
                                                             & ((IData)(0x48U) 
                                                                + 
                                                                ((IData)(0xb9U) 
                                                                 * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))) 
                                                         | ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                            (0xfU 
                                                                             & (((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))) 
                                                                                >> 5U))])) 
                                                            >> 
                                                            (0x1fU 
                                                             & ((IData)(0x48U) 
                                                                + 
                                                                ((IData)(0xb9U) 
                                                                 * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))
                                                   : 0ULL)
                                                  : 0ULL) 
                                                >> 0x20U)) 
                                       << 8U)));
    this->mst_req_o[0U] = ((0xfffffffcU & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                             : this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                           << 2U)) 
                           | ((((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
                                | (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)) 
                               << 1U) | (IData)(this->__PVT__gen_mux__DOT__mst_r_ready)));
    this->mst_req_o[1U] = ((3U & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                    ? this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                    : this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                  >> 0x1eU)) | (0xfffffffcU 
                                                & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                                     ? 
                                                    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                                     : 
                                                    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                                   << 2U)));
    this->mst_req_o[2U] = __Vtemp54[2U];
    this->mst_req_o[3U] = ((0xffff8000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                             : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                           << 0xfU)) 
                           | ((0xffffc000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
                                               | (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)) 
                                              << 0xeU)) 
                              | __Vtemp54[3U]));
    this->mst_req_o[4U] = ((0x7fffU & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                         ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                         : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                       >> 0x11U)) | 
                           (0xffff8000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                             : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                           << 0xfU)));
    this->mst_req_o[5U] = ((0x7fffU & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                         ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                         : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                       >> 0x11U)) | 
                           (0xffff8000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U]
                                             : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U]) 
                                           << 0xfU)));
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(this->__PVT__gen_mux__DOT__b_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
           & (IData)(this->__PVT__gen_mux__DOT__b_fifo_pop)) 
          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
            = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        if (this->__PVT__gen_mux__DOT__b_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        if (this->__PVT__gen_mux__DOT__b_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__b_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        if (this->__PVT__gen_mux__DOT__b_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q;
        }
    }
}

VL_INLINE_OPT void Vtaiga_sim_axi_lite_mux__pi5::_combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux__6(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+          Vtaiga_sim_axi_lite_mux__pi5::_combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux__6\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*127:0*/ __Vtemp100[4];
    // Body
    this->slv_resps_o[1U] = ((0xfff007ffU & this->slv_resps_o[1U]) 
                             | (0xff800U & (vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                            << 8U)));
    this->slv_resps_o[3U] = ((0xf007U & this->slv_resps_o[3U]) 
                             | (0xff8U & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]));
    this->slv_resps_o[0U] = (IData)((0x3ffffffffffULL 
                                     & (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__mst_ports_resp[3U])) 
                                         << 0x28U) 
                                        | (((QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__mst_ports_resp[2U])) 
                                            << 8U) 
                                           | ((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                              >> 0x18U)))));
    this->slv_resps_o[1U] = ((0xfffffc00U & this->slv_resps_o[1U]) 
                             | (IData)(((0x3ffffffffffULL 
                                         & (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__mst_ports_resp[3U])) 
                                             << 0x28U) 
                                            | (((QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__mst_ports_resp[2U])) 
                                                << 8U) 
                                               | ((QData)((IData)(
                                                                  vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                  >> 0x18U)))) 
                                        >> 0x20U)));
    this->slv_resps_o[1U] = ((0xffffffU & this->slv_resps_o[1U]) 
                             | (0xff000000U & ((IData)(
                                                       (0x3ffffffffffULL 
                                                        & (((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__mst_ports_resp[3U])) 
                                                            << 0x28U) 
                                                           | (((QData)((IData)(
                                                                               vlTOPp->taiga_sim__DOT__mst_ports_resp[2U])) 
                                                               << 8U) 
                                                              | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                                 >> 0x18U))))) 
                                               << 0x18U)));
    this->slv_resps_o[2U] = ((0xffffffU & ((IData)(
                                                   (0x3ffffffffffULL 
                                                    & (((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__mst_ports_resp[3U])) 
                                                        << 0x28U) 
                                                       | (((QData)((IData)(
                                                                           vlTOPp->taiga_sim__DOT__mst_ports_resp[2U])) 
                                                           << 8U) 
                                                          | ((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                             >> 0x18U))))) 
                                           >> 8U)) 
                             | (0xff000000U & ((IData)(
                                                       ((0x3ffffffffffULL 
                                                         & (((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__mst_ports_resp[3U])) 
                                                             << 0x28U) 
                                                            | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__mst_ports_resp[2U])) 
                                                                << 8U) 
                                                               | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                                  >> 0x18U)))) 
                                                        >> 0x20U)) 
                                               << 0x18U)));
    this->slv_resps_o[3U] = ((0xfffcU & this->slv_resps_o[3U]) 
                             | (0xffffffU & ((IData)(
                                                     ((0x3ffffffffffULL 
                                                       & (((QData)((IData)(
                                                                           vlTOPp->taiga_sim__DOT__mst_ports_resp[3U])) 
                                                           << 0x28U) 
                                                          | (((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__mst_ports_resp[2U])) 
                                                              << 8U) 
                                                             | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__mst_ports_resp[1U])) 
                                                                >> 0x18U)))) 
                                                      >> 0x20U)) 
                                             >> 8U)));
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
              >> 0xfU));
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) 
           & (vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
              >> 0xeU));
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                 >> 0xfU)));
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain) 
           & (~ (vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                 >> 0xeU)));
    this->__PVT__gen_mux__DOT__slv_aw_chans[0U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x10U] 
                                                    << 0x1fU) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xfU] 
                                                      >> 1U));
    this->__PVT__gen_mux__DOT__slv_aw_chans[1U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
                                                    << 0x1fU) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x10U] 
                                                      >> 1U));
    this->__PVT__gen_mux__DOT__slv_aw_chans[2U] = (
                                                   (0xfffffc00U 
                                                    & this->__PVT__gen_mux__DOT__slv_aw_chans[2U]) 
                                                   | (0x3ffU 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x12U] 
                                                          << 0x1fU) 
                                                         | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
                                                            >> 1U))));
    this->__PVT__gen_mux__DOT__slv_aw_chans[2U] = (
                                                   (0x3ffU 
                                                    & this->__PVT__gen_mux__DOT__slv_aw_chans[2U]) 
                                                   | (0xfffffc00U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x15U] 
                                                          << 0x10U) 
                                                         | (0xfc00U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x14U] 
                                                               >> 0x10U)))));
    this->__PVT__gen_mux__DOT__slv_aw_chans[3U] = (
                                                   (0x3ffU 
                                                    & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x15U] 
                                                       >> 0x10U)) 
                                                   | (0xfffffc00U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x16U] 
                                                          << 0x10U) 
                                                         | (0xfc00U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x15U] 
                                                               >> 0x10U)))));
    this->__PVT__gen_mux__DOT__slv_aw_chans[4U] = (
                                                   (0x3ffU 
                                                    & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x16U] 
                                                       >> 0x10U)) 
                                                   | (0xffc00U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x17U] 
                                                          << 0x10U) 
                                                         | (0xfc00U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x16U] 
                                                               >> 0x10U)))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[0U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xcU] 
                                                    << 0xcU) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xbU] 
                                                      >> 0x14U));
    this->__PVT__gen_mux__DOT__slv_ar_chans[1U] = (
                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xdU] 
                                                    << 0xcU) 
                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xcU] 
                                                      >> 0x14U));
    this->__PVT__gen_mux__DOT__slv_ar_chans[2U] = (
                                                   (0xfffffff0U 
                                                    & this->__PVT__gen_mux__DOT__slv_ar_chans[2U]) 
                                                   | (0xfU 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xeU] 
                                                          << 0xcU) 
                                                         | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xdU] 
                                                            >> 0x14U))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[2U] = (
                                                   (0xfU 
                                                    & this->__PVT__gen_mux__DOT__slv_ar_chans[2U]) 
                                                   | (0xfffffff0U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x12U] 
                                                          << 0x17U) 
                                                         | (0x7ffff0U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
                                                               >> 9U)))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[3U] = (
                                                   (0xfU 
                                                    & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x12U] 
                                                       >> 9U)) 
                                                   | (0xfffffff0U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x13U] 
                                                          << 0x17U) 
                                                         | (0x7ffff0U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x12U] 
                                                               >> 9U)))));
    this->__PVT__gen_mux__DOT__slv_ar_chans[4U] = (
                                                   (0xfU 
                                                    & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x13U] 
                                                       >> 9U)) 
                                                   | (0xf0U 
                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x14U] 
                                                          << 0x17U) 
                                                         | (0x7ffff0U 
                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x13U] 
                                                               >> 9U)))));
    this->__PVT__gen_mux__DOT__slv_ar_valids = ((2U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_ar_valids)) 
                                                | (1U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xbU] 
                                                      >> 0x13U)));
    this->__PVT__gen_mux__DOT__slv_ar_valids = ((1U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_ar_valids)) 
                                                | (2U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
                                                      >> 0xbU)));
    this->__PVT__gen_mux__DOT__slv_aw_valids = ((2U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_aw_valids)) 
                                                | (1U 
                                                   & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xfU]));
    this->__PVT__gen_mux__DOT__slv_aw_valids = ((1U 
                                                 & (IData)(this->__PVT__gen_mux__DOT__slv_aw_valids)) 
                                                | (2U 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x14U] 
                                                      >> 0x18U)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d 
        = ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q)
            ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q)
            : (IData)(this->__PVT__gen_mux__DOT__slv_ar_valids));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d 
        = ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q)
            ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q)
            : (IData)(this->__PVT__gen_mux__DOT__slv_aw_valids));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (0xfffffffeU & (((1U <= (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)) 
           | (0xfffffffeU & (((1U > (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel 
        = (1U & ((~ (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d)) 
                 | (((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d) 
                     >> 1U) & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes 
        = (1U & ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d) 
                 | ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d) 
                    >> 1U)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)) 
           | (0xfffffffeU & (((1U <= (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)) 
           | (0xfffffffeU & (((1U > (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q)) 
                              << 1U) & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel 
        = (1U & ((~ (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d)) 
                 | (((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d) 
                     >> 1U) & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes 
        = (1U & ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d) 
                 | ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d) 
                    >> 1U)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__slv_ar_readies = ((2U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_ar_readies)) 
                                                 | ((IData)(this->__PVT__gen_mux__DOT__ar_ready) 
                                                    & (~ (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel))));
    this->__PVT__gen_mux__DOT__slv_ar_readies = ((1U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_ar_readies)) 
                                                 | (((IData)(this->__PVT__gen_mux__DOT__ar_ready) 
                                                     & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel)) 
                                                    << 1U));
    this->__PVT__gen_mux__DOT__mst_ar_valid = ((1U 
                                                != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                               & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp 
        = ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp)) 
           | (2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask)));
    this->__PVT__gen_mux__DOT__lock_aw_valid_d = this->__PVT__gen_mux__DOT__lock_aw_valid_q;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
            this->__PVT__gen_mux__DOT__lock_aw_valid_d = 0U;
        }
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__mst_aw_ready)))) {
                this->__PVT__gen_mux__DOT__lock_aw_valid_d = 1U;
            }
        }
    }
    this->__PVT__gen_mux__DOT__load_aw_lock = 0U;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
            this->__PVT__gen_mux__DOT__load_aw_lock = 1U;
        }
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__mst_aw_ready)))) {
                this->__PVT__gen_mux__DOT__load_aw_lock = 1U;
            }
        }
    }
    this->__PVT__gen_mux__DOT__mst_aw_valid = 0U;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        this->__PVT__gen_mux__DOT__mst_aw_valid = 1U;
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            this->__PVT__gen_mux__DOT__mst_aw_valid = 1U;
        }
    }
    this->__PVT__gen_mux__DOT__aw_ready = 0U;
    if (this->__PVT__gen_mux__DOT__lock_aw_valid_q) {
        if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
            this->__PVT__gen_mux__DOT__aw_ready = 1U;
        }
    } else {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            if (this->__PVT__gen_mux__DOT__mst_aw_ready) {
                this->__PVT__gen_mux__DOT__aw_ready = 1U;
            }
        }
    }
    this->__PVT__gen_mux__DOT__w_fifo_push = 0U;
    if ((1U & (~ (IData)(this->__PVT__gen_mux__DOT__lock_aw_valid_q)))) {
        if (((1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))) {
            this->__PVT__gen_mux__DOT__w_fifo_push = 1U;
        }
    }
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes)) 
           | (1U & ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                    | ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                       >> 1U))));
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->slv_resps_o[1U] = ((0xffbfffffU & this->slv_resps_o[1U]) 
                             | (0x400000U & ((IData)(this->__PVT__gen_mux__DOT__slv_ar_readies) 
                                             << 0x16U)));
    this->slv_resps_o[3U] = ((0xbfffU & this->slv_resps_o[3U]) 
                             | (0x4000U & ((IData)(this->__PVT__gen_mux__DOT__slv_ar_readies) 
                                           << 0xdU)));
    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__mst_ar_valid) 
           & (IData)(this->__PVT__gen_mux__DOT__mst_ar_ready));
    this->__PVT__gen_mux__DOT__r_fifo_push = ((IData)(this->__PVT__gen_mux__DOT__mst_ar_valid) 
                                              & (IData)(this->__PVT__gen_mux__DOT__mst_ar_ready));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes)) 
           | (1U & ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                    | ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp) 
                       >> 1U))));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes 
        = ((2U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)) 
           | (1U & ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp))
                     ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut)
                     : ((IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut) 
                        >> 1U))));
    this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((IData)(this->__PVT__gen_mux__DOT__mst_aw_valid) 
           & (IData)(this->__PVT__gen_mux__DOT__mst_aw_ready));
    this->__PVT__gen_mux__DOT__slv_aw_readies = ((2U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_aw_readies)) 
                                                 | ((IData)(this->__PVT__gen_mux__DOT__aw_ready) 
                                                    & (~ (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel))));
    this->__PVT__gen_mux__DOT__slv_aw_readies = ((1U 
                                                  & (IData)(this->__PVT__gen_mux__DOT__slv_aw_readies)) 
                                                 | (((IData)(this->__PVT__gen_mux__DOT__aw_ready) 
                                                     & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel)) 
                                                    << 1U));
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->gen_mux__DOT__i_w_fifo__DOT____Vlvbound1 
            = this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
        if ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q))) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q))) 
                    & (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n)) 
                   | ((IData)(this->gen_mux__DOT__i_w_fifo__DOT____Vlvbound1) 
                      << (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q)));
        }
    }
    this->__PVT__gen_mux__DOT__w_fifo_empty = ((0U 
                                                == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
                                               & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_push)));
    this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o 
        = ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q)) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q) 
              >> (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o 
            = this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
    }
    this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d 
        = (1U & (((IData)(this->__PVT__gen_mux__DOT__ar_ready) 
                  & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes))
                  ? ((1U & (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes))
                      ? (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)
                      : (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes))
                  : (IData)(this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q)));
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->gen_mux__DOT__i_r_fifo__DOT____Vlvbound1 
            = this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
        if ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q))) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q))) 
                    & (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n)) 
                   | ((IData)(this->gen_mux__DOT__i_r_fifo__DOT____Vlvbound1) 
                      << (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q)));
        }
    }
    this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o 
        = ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q)) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q) 
              >> (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o 
            = this->__PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
    }
    this->__PVT__gen_mux__DOT__r_fifo_empty = ((0U 
                                                == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                               & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_push)));
    this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d 
        = (1U & (((IData)(this->__PVT__gen_mux__DOT__aw_ready) 
                  & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes))
                  ? ((1U & (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes))
                      ? (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes)
                      : (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes))
                  : (IData)(this->__PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q)));
    this->slv_resps_o[1U] = ((0xff7fffffU & this->slv_resps_o[1U]) 
                             | (0x800000U & ((IData)(this->__PVT__gen_mux__DOT__slv_aw_readies) 
                                             << 0x17U)));
    this->slv_resps_o[3U] = ((0x7fffU & this->slv_resps_o[3U]) 
                             | (0x8000U & ((IData)(this->__PVT__gen_mux__DOT__slv_aw_readies) 
                                           << 0xeU)));
    this->__PVT__gen_mux__DOT__mst_w_valid = (((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                               & (1U 
                                                  != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                              & ((0x171U 
                                                  >= 
                                                  (0x1ffU 
                                                   & ((IData)(0x47U) 
                                                      + 
                                                      ((IData)(0xb9U) 
                                                       * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                 & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                    (0x1fU 
                                                     & (((IData)(0x172U) 
                                                         + 
                                                         (0x1ffU 
                                                          & ((IData)(0x47U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                        >> 5U))] 
                                                    >> 
                                                    (0x1fU 
                                                     & ((IData)(0x172U) 
                                                        + 
                                                        (0x1ffU 
                                                         & ((IData)(0x47U) 
                                                            + 
                                                            ((IData)(0xb9U) 
                                                             * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))));
    this->slv_resps_o[1U] = ((0xffdfffffU & this->slv_resps_o[1U]) 
                             | (0xffe00000U & ((((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                                  << 8U) 
                                                 & ((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                    << 0x15U)) 
                                                & ((1U 
                                                    != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
                                                   << 0x15U)) 
                                               & ((~ (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)) 
                                                  << 0x15U))));
    this->slv_resps_o[3U] = ((0xdfffU & this->slv_resps_o[3U]) 
                             | (0xffffe000U & (((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                                 & ((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                    << 0xdU)) 
                                                & ((1U 
                                                    != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
                                                   << 0xdU)) 
                                               & ((IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o) 
                                                  << 0xdU))));
    this->__PVT__gen_mux__DOT__mst_r_ready = ((~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)) 
                                              & ((0x171U 
                                                  >= 
                                                  (0x1ffU 
                                                   & ((IData)(0xb9U) 
                                                      * (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o)))) 
                                                 & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                    (0x1fU 
                                                     & (((IData)(0x172U) 
                                                         + 
                                                         (0x1ffU 
                                                          & ((IData)(0xb9U) 
                                                             * (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o)))) 
                                                        >> 5U))] 
                                                    >> 
                                                    (0x1fU 
                                                     & ((IData)(0x172U) 
                                                        + 
                                                        (0x1ffU 
                                                         & ((IData)(0xb9U) 
                                                            * (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o))))))));
    this->slv_resps_o[1U] = ((0xfffffbffU & this->slv_resps_o[1U]) 
                             | (0x400U & (((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                            << 8U) 
                                           & ((~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)) 
                                              << 0xaU)) 
                                          & ((~ (IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o)) 
                                             << 0xaU))));
    this->slv_resps_o[3U] = ((0xfffbU & this->slv_resps_o[3U]) 
                             | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                                & ((~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)) 
                                                   << 2U)) 
                                               & ((IData)(this->__Vcellout__gen_mux__DOT__i_r_fifo__data_o) 
                                                  << 2U))));
    this->__PVT__gen_mux__DOT__w_fifo_pop = ((IData)(this->__PVT__gen_mux__DOT__mst_w_valid) 
                                             & (vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                                >> 0xdU));
    this->__PVT__gen_mux__DOT__r_fifo_pop = ((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                              >> 2U) 
                                             & (IData)(this->__PVT__gen_mux__DOT__mst_r_ready));
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
           & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop)) 
          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
            = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__w_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__w_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n 
        = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__w_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n 
                = this->__PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->gen_mux__DOT__i_b_fifo__DOT____Vlvbound1 
            = this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o;
        if ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q))) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q))) 
                    & (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n)) 
                   | ((IData)(this->gen_mux__DOT__i_b_fifo__DOT____Vlvbound1) 
                      << (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q)));
        }
    }
    this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o 
        = ((0U >= (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q)) 
           & ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q) 
              >> (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o 
            = this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o;
    }
    this->__PVT__gen_mux__DOT__b_fifo_empty = ((0U 
                                                == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
                                               & (~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop)));
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
           & (IData)(this->__PVT__gen_mux__DOT__r_fifo_pop)) 
          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
            = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__r_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_push) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__r_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n 
        = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__r_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__r_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__r_fifo_push))) {
        if (this->__PVT__gen_mux__DOT__r_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n 
                = this->__PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__mst_b_ready = ((~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)) 
                                              & ((0x171U 
                                                  >= 
                                                  (0x1ffU 
                                                   & ((IData)(0x46U) 
                                                      + 
                                                      ((IData)(0xb9U) 
                                                       * (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o))))) 
                                                 & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                    (0x1fU 
                                                     & (((IData)(0x172U) 
                                                         + 
                                                         (0x1ffU 
                                                          & ((IData)(0x46U) 
                                                             + 
                                                             ((IData)(0xb9U) 
                                                              * (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o))))) 
                                                        >> 5U))] 
                                                    >> 
                                                    (0x1fU 
                                                     & ((IData)(0x172U) 
                                                        + 
                                                        (0x1ffU 
                                                         & ((IData)(0x46U) 
                                                            + 
                                                            ((IData)(0xb9U) 
                                                             * (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o)))))))));
    this->slv_resps_o[1U] = ((0xffefffffU & this->slv_resps_o[1U]) 
                             | (0x100000U & (((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                               << 8U) 
                                              & ((~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)) 
                                                 << 0x14U)) 
                                             & ((~ (IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o)) 
                                                << 0x14U))));
    this->slv_resps_o[3U] = ((0xefffU & this->slv_resps_o[3U]) 
                             | (0xfffff000U & ((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                                & ((~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)) 
                                                   << 0xcU)) 
                                               & ((IData)(this->__Vcellout__gen_mux__DOT__i_b_fifo__data_o) 
                                                  << 0xcU))));
    this->__PVT__gen_mux__DOT__b_fifo_pop = ((vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] 
                                              >> 0xcU) 
                                             & (IData)(this->__PVT__gen_mux__DOT__mst_b_ready));
    __Vtemp100[2U] = ((0xffffff00U & ((IData)((((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                & (1U 
                                                   != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))
                                                ? (
                                                   (0x171U 
                                                    >= 
                                                    (0x1ffU 
                                                     & ((IData)(0x48U) 
                                                        + 
                                                        ((IData)(0xb9U) 
                                                         * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                    ? 
                                                   (0x3fffffffffULL 
                                                    & (((0U 
                                                         == 
                                                         (0x1fU 
                                                          & ((IData)(0x172U) 
                                                             + 
                                                             (0x1ffU 
                                                              & ((IData)(0x48U) 
                                                                 + 
                                                                 ((IData)(0xb9U) 
                                                                  * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))
                                                         ? 0ULL
                                                         : 
                                                        ((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                         ((IData)(2U) 
                                                                          + 
                                                                          (0x1fU 
                                                                           & (((IData)(0x172U) 
                                                                               + 
                                                                               (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                              >> 5U)))])) 
                                                         << 
                                                         ((IData)(0x40U) 
                                                          - 
                                                          (0x1fU 
                                                           & ((IData)(0x172U) 
                                                              + 
                                                              (0x1ffU 
                                                               & ((IData)(0x48U) 
                                                                  + 
                                                                  ((IData)(0xb9U) 
                                                                   * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))) 
                                                       | (((QData)((IData)(
                                                                           vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                           ((IData)(1U) 
                                                                            + 
                                                                            (0x1fU 
                                                                             & (((IData)(0x172U) 
                                                                                + 
                                                                                (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                                >> 5U)))])) 
                                                           << 
                                                           ((0U 
                                                             == 
                                                             (0x1fU 
                                                              & ((IData)(0x172U) 
                                                                 + 
                                                                 (0x1ffU 
                                                                  & ((IData)(0x48U) 
                                                                     + 
                                                                     ((IData)(0xb9U) 
                                                                      * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))
                                                             ? 0x20U
                                                             : 
                                                            ((IData)(0x20U) 
                                                             - 
                                                             (0x1fU 
                                                              & ((IData)(0x172U) 
                                                                 + 
                                                                 (0x1ffU 
                                                                  & ((IData)(0x48U) 
                                                                     + 
                                                                     ((IData)(0xb9U) 
                                                                      * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))) 
                                                          | ((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                             (0x1fU 
                                                                              & (((IData)(0x172U) 
                                                                                + 
                                                                                (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                                >> 5U))])) 
                                                             >> 
                                                             (0x1fU 
                                                              & ((IData)(0x172U) 
                                                                 + 
                                                                 (0x1ffU 
                                                                  & ((IData)(0x48U) 
                                                                     + 
                                                                     ((IData)(0xb9U) 
                                                                      * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))))
                                                    : 0ULL)
                                                : 0ULL)) 
                                      << 8U)) | ((0xffffff80U 
                                                  & ((IData)(this->__PVT__gen_mux__DOT__mst_w_valid) 
                                                     << 7U)) 
                                                 | ((0xffffffc0U 
                                                     & ((IData)(this->__PVT__gen_mux__DOT__mst_b_ready) 
                                                        << 6U)) 
                                                    | ((3U 
                                                        & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                                             ? 
                                                            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                                             : 
                                                            this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                                           >> 0x1eU)) 
                                                       | (0xfffffffcU 
                                                          & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                                               ? 
                                                              this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U]
                                                               : 
                                                              this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U]) 
                                                             << 2U))))));
    __Vtemp100[3U] = ((0xffU & ((IData)((((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))
                                          ? ((0x171U 
                                              >= (0x1ffU 
                                                  & ((IData)(0x48U) 
                                                     + 
                                                     ((IData)(0xb9U) 
                                                      * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                              ? (0x3fffffffffULL 
                                                 & (((0U 
                                                      == 
                                                      (0x1fU 
                                                       & ((IData)(0x172U) 
                                                          + 
                                                          (0x1ffU 
                                                           & ((IData)(0x48U) 
                                                              + 
                                                              ((IData)(0xb9U) 
                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))
                                                      ? 0ULL
                                                      : 
                                                     ((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                      ((IData)(2U) 
                                                                       + 
                                                                       (0x1fU 
                                                                        & (((IData)(0x172U) 
                                                                            + 
                                                                            (0x1ffU 
                                                                             & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                           >> 5U)))])) 
                                                      << 
                                                      ((IData)(0x40U) 
                                                       - 
                                                       (0x1fU 
                                                        & ((IData)(0x172U) 
                                                           + 
                                                           (0x1ffU 
                                                            & ((IData)(0x48U) 
                                                               + 
                                                               ((IData)(0xb9U) 
                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))) 
                                                    | (((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                        ((IData)(1U) 
                                                                         + 
                                                                         (0x1fU 
                                                                          & (((IData)(0x172U) 
                                                                              + 
                                                                              (0x1ffU 
                                                                               & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                             >> 5U)))])) 
                                                        << 
                                                        ((0U 
                                                          == 
                                                          (0x1fU 
                                                           & ((IData)(0x172U) 
                                                              + 
                                                              (0x1ffU 
                                                               & ((IData)(0x48U) 
                                                                  + 
                                                                  ((IData)(0xb9U) 
                                                                   * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))
                                                          ? 0x20U
                                                          : 
                                                         ((IData)(0x20U) 
                                                          - 
                                                          (0x1fU 
                                                           & ((IData)(0x172U) 
                                                              + 
                                                              (0x1ffU 
                                                               & ((IData)(0x48U) 
                                                                  + 
                                                                  ((IData)(0xb9U) 
                                                                   * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))) 
                                                       | ((QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                          (0x1fU 
                                                                           & (((IData)(0x172U) 
                                                                               + 
                                                                               (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                              >> 5U))])) 
                                                          >> 
                                                          (0x1fU 
                                                           & ((IData)(0x172U) 
                                                              + 
                                                              (0x1ffU 
                                                               & ((IData)(0x48U) 
                                                                  + 
                                                                  ((IData)(0xb9U) 
                                                                   * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))))
                                              : 0ULL)
                                          : 0ULL)) 
                                >> 0x18U)) | (0xffffff00U 
                                              & ((IData)(
                                                         ((((~ (IData)(this->__PVT__gen_mux__DOT__w_fifo_empty)) 
                                                            & (1U 
                                                               != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))
                                                            ? 
                                                           ((0x171U 
                                                             >= 
                                                             (0x1ffU 
                                                              & ((IData)(0x48U) 
                                                                 + 
                                                                 ((IData)(0xb9U) 
                                                                  * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))
                                                             ? 
                                                            (0x3fffffffffULL 
                                                             & (((0U 
                                                                  == 
                                                                  (0x1fU 
                                                                   & ((IData)(0x172U) 
                                                                      + 
                                                                      (0x1ffU 
                                                                       & ((IData)(0x48U) 
                                                                          + 
                                                                          ((IData)(0xb9U) 
                                                                           * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))
                                                                  ? 0ULL
                                                                  : 
                                                                 ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                                ((IData)(2U) 
                                                                                + 
                                                                                (0x1fU 
                                                                                & (((IData)(0x172U) 
                                                                                + 
                                                                                (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                                >> 5U)))])) 
                                                                  << 
                                                                  ((IData)(0x40U) 
                                                                   - 
                                                                   (0x1fU 
                                                                    & ((IData)(0x172U) 
                                                                       + 
                                                                       (0x1ffU 
                                                                        & ((IData)(0x48U) 
                                                                           + 
                                                                           ((IData)(0xb9U) 
                                                                            * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))) 
                                                                | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                                ((IData)(1U) 
                                                                                + 
                                                                                (0x1fU 
                                                                                & (((IData)(0x172U) 
                                                                                + 
                                                                                (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                                >> 5U)))])) 
                                                                    << 
                                                                    ((0U 
                                                                      == 
                                                                      (0x1fU 
                                                                       & ((IData)(0x172U) 
                                                                          + 
                                                                          (0x1ffU 
                                                                           & ((IData)(0x48U) 
                                                                              + 
                                                                              ((IData)(0xb9U) 
                                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o)))))))
                                                                      ? 0x20U
                                                                      : 
                                                                     ((IData)(0x20U) 
                                                                      - 
                                                                      (0x1fU 
                                                                       & ((IData)(0x172U) 
                                                                          + 
                                                                          (0x1ffU 
                                                                           & ((IData)(0x48U) 
                                                                              + 
                                                                              ((IData)(0xb9U) 
                                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))) 
                                                                   | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[
                                                                                (0x1fU 
                                                                                & (((IData)(0x172U) 
                                                                                + 
                                                                                (0x1ffU 
                                                                                & ((IData)(0x48U) 
                                                                                + 
                                                                                ((IData)(0xb9U) 
                                                                                * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))) 
                                                                                >> 5U))])) 
                                                                      >> 
                                                                      (0x1fU 
                                                                       & ((IData)(0x172U) 
                                                                          + 
                                                                          (0x1ffU 
                                                                           & ((IData)(0x48U) 
                                                                              + 
                                                                              ((IData)(0xb9U) 
                                                                               * (IData)(this->__Vcellout__gen_mux__DOT__i_w_fifo__data_o))))))))))
                                                             : 0ULL)
                                                            : 0ULL) 
                                                          >> 0x20U)) 
                                                 << 8U)));
    this->mst_req_o[0U] = ((0xfffffffcU & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                             : this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                           << 2U)) 
                           | ((((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
                                | (IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)) 
                               << 1U) | (IData)(this->__PVT__gen_mux__DOT__mst_r_ready)));
    this->mst_req_o[1U] = ((3U & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                    ? this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                    : this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                  >> 0x1eU)) | (0xfffffffcU 
                                                & (((IData)(this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                                     ? 
                                                    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                                     : 
                                                    this->__PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                                   << 2U)));
    this->mst_req_o[2U] = __Vtemp100[2U];
    this->mst_req_o[3U] = ((0xffff8000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                             : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                           << 0xfU)) 
                           | ((0xffffc000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
                                               | (IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)) 
                                              << 0xeU)) 
                              | __Vtemp100[3U]));
    this->mst_req_o[4U] = ((0x7fffU & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                         ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U]
                                         : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U]) 
                                       >> 0x11U)) | 
                           (0xffff8000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                             : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                           << 0xfU)));
    this->mst_req_o[5U] = ((0x7fffU & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                         ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U]
                                         : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U]) 
                                       >> 0x11U)) | 
                           (0xffff8000U & (((IData)(this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)
                                             ? this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U]
                                             : this->__PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U]) 
                                           << 0xfU)));
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(this->__PVT__gen_mux__DOT__b_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
           & (IData)(this->__PVT__gen_mux__DOT__b_fifo_pop)) 
          & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
            = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        if (this->__PVT__gen_mux__DOT__b_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__w_fifo_pop) 
         & (1U != (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        if (this->__PVT__gen_mux__DOT__b_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q;
        }
    }
    this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n 
        = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(this->__PVT__gen_mux__DOT__b_fifo_pop) 
         & (~ (IData)(this->__PVT__gen_mux__DOT__b_fifo_empty)))) {
        this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n 
            = ((IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(this->__PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q)) 
         & (IData)(this->__PVT__gen_mux__DOT__w_fifo_pop))) {
        if (this->__PVT__gen_mux__DOT__b_fifo_pop) {
            this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n 
                = this->__PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q;
        }
    }
}
