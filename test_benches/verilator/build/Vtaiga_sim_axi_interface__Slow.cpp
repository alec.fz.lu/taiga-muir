// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim_axi_interface.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_CTOR_IMP(Vtaiga_sim_axi_interface) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void Vtaiga_sim_axi_interface::__Vconfigure(Vtaiga_sim__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

Vtaiga_sim_axi_interface::~Vtaiga_sim_axi_interface() {
}

void Vtaiga_sim_axi_interface::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+              Vtaiga_sim_axi_interface::_ctor_var_reset\n"); );
    // Body
    araddr = VL_RAND_RESET_I(32);
    arsize = VL_RAND_RESET_I(3);
    arid = VL_RAND_RESET_I(6);
    rresp = VL_RAND_RESET_I(2);
    rlast = VL_RAND_RESET_I(1);
    rid = VL_RAND_RESET_I(6);
    awaddr = VL_RAND_RESET_I(32);
    awsize = VL_RAND_RESET_I(3);
    awid = VL_RAND_RESET_I(6);
    wdata = VL_RAND_RESET_I(32);
    wstrb = VL_RAND_RESET_I(4);
    bresp = VL_RAND_RESET_I(2);
    bid = VL_RAND_RESET_I(6);
}
