// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_CTOR_IMP(Vtaiga_sim) {
    Vtaiga_sim__Syms* __restrict vlSymsp = __VlSymsp = new Vtaiga_sim__Syms(this, name());
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    VL_CELL(__PVT__taiga_sim__DOT__m_axi, Vtaiga_sim_axi_interface);
    VL_CELL(__PVT__taiga_sim__DOT__uart_axi, Vtaiga_sim_axi_interface);
    VL_CELL(__PVT__taiga_sim__DOT__muir_axi2, Vtaiga_sim_axi_interface);
    VL_CELL(__PVT__taiga_sim__DOT__l2__BRA__1__KET__, Vtaiga_sim_l2_requester_interface);
    VL_CELL(__PVT__taiga_sim__DOT__l2__BRA__0__KET__, Vtaiga_sim_l2_requester_interface);
    VL_CELL(__PVT__taiga_sim__DOT__mem, Vtaiga_sim_l2_memory_interface);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__arb, Vtaiga_sim_l2_arbitration_interface);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__, Vtaiga_sim_fifo_interface__D2b);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__, Vtaiga_sim_fifo_interface__D2b);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__, Vtaiga_sim_fifo_interface__D20);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__, Vtaiga_sim_fifo_interface__D20);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__, Vtaiga_sim_fifo_interface__D1e);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__, Vtaiga_sim_fifo_interface__D1e);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__, Vtaiga_sim_fifo_interface__D22);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__, Vtaiga_sim_fifo_interface__D22);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo, Vtaiga_sim_fifo_interface__D2c);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo, Vtaiga_sim_fifo_interface__D20);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__data_attributes, Vtaiga_sim_fifo_interface__D7);
    VL_CELL(__PVT__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo, Vtaiga_sim_fifo_interface__D23);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__, Vtaiga_sim_l1_arbiter_request_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__2__KET__, Vtaiga_sim_l1_arbiter_request_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__, Vtaiga_sim_l1_arbiter_request_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__, Vtaiga_sim_l1_arbiter_request_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__, Vtaiga_sim_l1_arbiter_return_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_response__BRA__2__KET__, Vtaiga_sim_l1_arbiter_return_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__, Vtaiga_sim_l1_arbiter_return_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__l1_response__BRA__0__KET__, Vtaiga_sim_l1_arbiter_return_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__bp, Vtaiga_sim_branch_predictor_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__ras, Vtaiga_sim_ras_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__rf_issue, Vtaiga_sim_register_file_issue_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__5__KET__, Vtaiga_sim_unit_writeback_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__4__KET__, Vtaiga_sim_unit_writeback_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__3__KET__, Vtaiga_sim_unit_writeback_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__, Vtaiga_sim_unit_writeback_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__1__KET__, Vtaiga_sim_unit_writeback_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__0__KET__, Vtaiga_sim_unit_writeback_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__immu, Vtaiga_sim_mmu_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__dmmu, Vtaiga_sim_mmu_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__itlb, Vtaiga_sim_tlb_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__dtlb, Vtaiga_sim_tlb_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__decode_rename_interface, Vtaiga_sim_renamer_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__, Vtaiga_sim_fetch_sub_unit_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo, Vtaiga_sim_fifo_interface__D5);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo, Vtaiga_sim_fifo_interface__D3);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list, Vtaiga_sim_fifo_interface__D6);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram, Vtaiga_sim_ls_sub_unit_interface__pi7);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus, Vtaiga_sim_ls_sub_unit_interface__pi8);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes, Vtaiga_sim_fifo_interface__D9);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq, Vtaiga_sim_load_store_queue_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo, Vtaiga_sim_fifo_interface__D2a);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo, Vtaiga_sim_fifo_interface__D3);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core, Vtaiga_sim_unsigned_division_interface);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo, Vtaiga_sim_fifo_interface__D47);
    VL_CELL(__PVT__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__wb_fifo, Vtaiga_sim_fifo_interface__D20);
    VL_CELL(__PVT__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux, Vtaiga_sim_axi_lite_mux__pi5);
    VL_CELL(__PVT__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux, Vtaiga_sim_axi_lite_mux__pi5);
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Vtaiga_sim::__Vconfigure(Vtaiga_sim__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
    Verilated::timeunit(-12);
    Verilated::timeprecision(-12);
}

Vtaiga_sim::~Vtaiga_sim() {
    VL_DO_CLEAR(delete __VlSymsp, __VlSymsp = nullptr);
}

void Vtaiga_sim::_settle__TOP__1(Vtaiga_sim__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Vtaiga_sim::_settle__TOP__1\n"); );
    Vtaiga_sim* const __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    WData/*255:0*/ __Vtemp13[8];
    WData/*255:0*/ __Vtemp14[8];
    WData/*255:0*/ __Vtemp16[8];
    WData/*255:0*/ __Vtemp17[8];
    WData/*255:0*/ __Vtemp20[8];
    WData/*255:0*/ __Vtemp21[8];
    WData/*255:0*/ __Vtemp23[8];
    WData/*255:0*/ __Vtemp100[8];
    WData/*255:0*/ __Vtemp101[8];
    WData/*255:0*/ __Vtemp107[8];
    WData/*95:0*/ __Vtemp193[3];
    WData/*95:0*/ __Vtemp196[3];
    WData/*95:0*/ __Vtemp197[3];
    WData/*95:0*/ __Vtemp198[3];
    WData/*95:0*/ __Vtemp199[3];
    WData/*127:0*/ __Vtemp219[4];
    WData/*95:0*/ __Vtemp220[3];
    WData/*191:0*/ __Vtemp221[6];
    WData/*127:0*/ __Vtemp223[4];
    WData/*95:0*/ __Vtemp224[3];
    WData/*191:0*/ __Vtemp225[6];
    // Body
    vlTOPp->instruction_bram_be = 0U;
    vlTOPp->instruction_bram_data_in = 0U;
    vlTOPp->ddr_axi_awburst_taiga = 1U;
    vlTOPp->ddr_axi_awcache_taiga = 3U;
    vlTOPp->ddr_axi_bready_taiga = 1U;
    vlTOPp->ddr_axi_arburst_taiga = 1U;
    vlTOPp->ddr_axi_arcache_taiga = 3U;
    vlTOPp->ddr_axi_arsize_taiga = 2U;
    vlTOPp->ddr_axi_rready_taiga = 1U;
    vlTOPp->ddr_axi_awid_muir = 0U;
    vlTOPp->ddr_axi_awsize_muir = 2U;
    vlTOPp->ddr_axi_awburst_muir = 1U;
    vlTOPp->ddr_axi_awcache_muir = 3U;
    vlTOPp->ddr_axi_wstrb_muir = 0xfU;
    vlTOPp->ddr_axi_wid_muir = 0U;
    vlTOPp->ddr_axi_arid_muir = 0U;
    vlTOPp->ddr_axi_arsize_muir = 2U;
    vlTOPp->ddr_axi_arburst_muir = 1U;
    vlTOPp->ddr_axi_arcache_muir = 3U;
    vlTOPp->ddr_axi_awid_taiga = vlTOPp->taiga_sim__DOT__axi_awid;
    vlTOPp->ddr_axi_bid_muir = vlTOPp->taiga_sim__DOT__axi_bid_muir;
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)) 
           | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__.request));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__2__KET__.request) 
              << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready 
        = (1U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x37U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_done) 
                     << 3U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][3U] 
        = (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_result);
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[0U][0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT__phys_reg_inuse_set[0U][1U] = 0U;
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xf00fU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0xfffffff0U 
                                                     & (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.bid) 
                                                         << 6U) 
                                                        | ((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.bresp) 
                                                           << 4U))));
    vlTOPp->ddr_axi_bready_muir = vlTOPp->ddr_axi_bvalid_muir;
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__unit_data_array[0U] 
        = vlTOPp->instruction_bram_data_out;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array[0U] 
        = vlTOPp->data_bram_data_out;
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xfffffbffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfffffc00U 
                                                     & ((IData)(vlTOPp->bus_axi_rvalid) 
                                                        << 0xaU)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xfff00fffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfffff000U 
                                                     & (((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.bid) 
                                                         << 0xeU) 
                                                        | ((IData)(vlTOPp->bus_axi_bresp) 
                                                           << 0xcU))));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[0U] = ((1U 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[0U]) 
                                                  | (0xfffffffeU 
                                                     & ((IData)(
                                                                (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rid)) 
                                                                  << 0x23U) 
                                                                 | (((QData)((IData)(vlTOPp->bus_axi_rdata)) 
                                                                     << 3U) 
                                                                    | (QData)((IData)(
                                                                                (((IData)(vlTOPp->bus_axi_rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rlast))))))) 
                                                        << 1U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xfffffc00U 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | ((1U 
                                                      & ((IData)(
                                                                 (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rid)) 
                                                                   << 0x23U) 
                                                                  | (((QData)((IData)(vlTOPp->bus_axi_rdata)) 
                                                                      << 3U) 
                                                                     | (QData)((IData)(
                                                                                (((IData)(vlTOPp->bus_axi_rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rlast))))))) 
                                                         >> 0x1fU)) 
                                                     | (0xfffffffeU 
                                                        & ((IData)(
                                                                   ((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rid)) 
                                                                      << 0x23U) 
                                                                     | (((QData)((IData)(vlTOPp->bus_axi_rdata)) 
                                                                         << 3U) 
                                                                        | (QData)((IData)(
                                                                                (((IData)(vlTOPp->bus_axi_rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__uart_axi.rlast)))))) 
                                                                    >> 0x20U)) 
                                                           << 1U))));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0xff0fffffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfff00000U 
                                                     & (((IData)(vlTOPp->bus_axi_awready) 
                                                         << 0x17U) 
                                                        | (((IData)(vlTOPp->bus_axi_arready) 
                                                            << 0x16U) 
                                                           | (((IData)(vlTOPp->bus_axi_wready) 
                                                               << 0x15U) 
                                                              | ((IData)(vlTOPp->bus_axi_bvalid) 
                                                                 << 0x14U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (1U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (2U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (4U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (8U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x10U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x20U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x40U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x80U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x100U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x1000U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x2000U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask 
        = (0x8000U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__medeleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask 
        = (2U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask 
        = (0x20U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask 
        = (0x200U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mideleg_mask);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[8U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[9U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xaU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xbU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xcU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xdU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xeU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xfU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x10U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x11U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x12U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x13U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x14U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x15U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x16U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x17U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x18U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x19U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x1aU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x1bU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x1cU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x1dU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x1eU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0x1fU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[1U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[2U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[3U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[4U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[5U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[6U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[7U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[8U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[9U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xbU] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xcU] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xdU] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_EXCEPTION_MASKING_ROM[0xfU] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[8U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[9U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xaU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xbU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xcU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xdU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xeU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xfU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x10U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x11U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x12U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x13U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x14U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x15U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x16U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x17U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x18U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x19U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x1aU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x1bU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x1cU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x1dU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x1eU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0x1fU] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[1U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[3U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[5U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[7U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[9U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__MCAUSE_INTERRUPT_MASKING_ROM[0xbU] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__lq_block__rst 
        = ((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__rst 
        = ((IData)(vlTOPp->rst) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_issue_flush));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry 
        = (0x4000000U | vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__ex_entry);
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[1U] 
        = ((0x1fffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [1U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__1__KET__.addr)) 
                     << 0xdU));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[1U] 
        = ((0x7ffffffe1ffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [1U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__1__KET__.be)) 
                     << 9U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[1U] 
        = ((0x7fffffffeffULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [1U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__1__KET__.rnw)) 
                     << 8U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[1U] 
        = ((0x7ffffffff7fULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [1U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__1__KET__.is_amo)) 
                     << 7U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[1U] 
        = ((0x7ffffffff83ULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [1U]) | ((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__1__KET__.amo_type_or_burst_size)) 
                     << 2U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in[1U] 
        = ((0x7fffffffffcULL & vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests_in
            [1U]) | (IData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2__BRA__1__KET__.sub_id)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = (0xffffff00U & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]);
    vlTOPp->ddr_axi_awvalid_taiga = vlTOPp->taiga_sim__DOT__axi_awvalid;
    vlTOPp->ddr_axi_awaddr_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_addr;
    vlTOPp->ddr_axi_awlen_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_len;
    vlTOPp->ddr_axi_arvalid_muir = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->ddr_axi_araddr_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_addr;
    vlTOPp->ddr_axi_arlen_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rd_len;
    vlTOPp->instruction_pc_dec = ((vlTOPp->taiga_sim__DOT__tr[3U] 
                                   << 0x1dU) | (vlTOPp->taiga_sim__DOT__tr[2U] 
                                                >> 3U));
    vlTOPp->instruction_data_dec = ((vlTOPp->taiga_sim__DOT__tr[2U] 
                                     << 0x1dU) | (vlTOPp->taiga_sim__DOT__tr[1U] 
                                                  >> 3U));
    vlTOPp->instruction_issued = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                        >> 0x1cU));
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.inv_valid 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__0__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.pop 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__l2__BRA__0__KET__.rd_data_valid 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.pop 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.valid;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.valid 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk4__BRA__1__KET____DOT__inv_response_fifo__DOT__genblk3__DOT__inflight_count) 
                 >> 4U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle_next 
        = (0x1ffffffffULL & (1ULL + vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.quotient 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_1 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_2 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_3 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_7 
        = (0xfU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_8 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next 
        = (0x1eU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_next));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__tlb_write 
        = (3U & ((- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__replacement_way)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__tlb_write 
        = (3U & ((- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))) 
                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_tlb__DOT__replacement_way)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT____Vcellinp__in_progress_m__set 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_12 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__state));
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[1U] 
                                                 >> 2U));
    vlTOPp->taiga_events[0U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[1U] 
                                                 >> 1U));
    vlTOPp->taiga_events[1U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & vlTOPp->taiga_sim__DOT__tr[1U]);
    vlTOPp->taiga_events[2U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1fU));
    vlTOPp->taiga_events[3U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1eU));
    vlTOPp->taiga_events[4U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1dU));
    vlTOPp->taiga_events[5U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1cU));
    vlTOPp->taiga_events[6U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1bU));
    vlTOPp->taiga_events[7U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x1aU));
    vlTOPp->taiga_events[8U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x19U));
    vlTOPp->taiga_events[9U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x18U));
    vlTOPp->taiga_events[0xaU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x17U));
    vlTOPp->taiga_events[0xbU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x16U));
    vlTOPp->taiga_events[0xcU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x15U));
    vlTOPp->taiga_events[0xdU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x14U));
    vlTOPp->taiga_events[0xeU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x13U));
    vlTOPp->taiga_events[0xfU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x12U));
    vlTOPp->taiga_events[0x10U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x11U));
    vlTOPp->taiga_events[0x11U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0x10U));
    vlTOPp->taiga_events[0x12U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xfU));
    vlTOPp->taiga_events[0x13U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xeU));
    vlTOPp->taiga_events[0x14U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xdU));
    vlTOPp->taiga_events[0x15U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xcU));
    vlTOPp->taiga_events[0x16U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xbU));
    vlTOPp->taiga_events[0x17U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 0xaU));
    vlTOPp->taiga_events[0x18U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 9U));
    vlTOPp->taiga_events[0x19U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 8U));
    vlTOPp->taiga_events[0x1aU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 7U));
    vlTOPp->taiga_events[0x1bU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 6U));
    vlTOPp->taiga_events[0x1cU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 5U));
    vlTOPp->taiga_events[0x1dU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 4U));
    vlTOPp->taiga_events[0x1eU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 3U));
    vlTOPp->taiga_events[0x1fU] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 2U));
    vlTOPp->taiga_events[0x20U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & (vlTOPp->taiga_sim__DOT__tr[0U] 
                                                 >> 1U));
    vlTOPp->taiga_events[0x21U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT____Vlvbound1 = (1U & vlTOPp->taiga_sim__DOT__tr[0U]);
    vlTOPp->taiga_events[0x22U] = vlTOPp->taiga_sim__DOT____Vlvbound1;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_data[0U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_data[1U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__1__KET____DOT__input_data_fifo__DOT__genblk3__DOT__read_index];
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_1 
        = (0x7ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                             - vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_2 
        = (0x7ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                             - vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_2));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_3 
        = (0x7ffffffffULL & (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                             - vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_16 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q)))) {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q)))) {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_13) 
            << 0xdU) | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_12) 
                         << 0xcU) | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_11) 
                                      << 0xbU) | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_10) 
                                                   << 0xaU) 
                                                  | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_9) 
                                                      << 9U) 
                                                     | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_8) 
                                                         << 8U) 
                                                        | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_7) 
                                                            << 7U) 
                                                           | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_6) 
                                                               << 6U) 
                                                              | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_5) 
                                                                  << 5U) 
                                                                 | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_4) 
                                                                     << 4U) 
                                                                    | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_3) 
                                                                        << 3U) 
                                                                       | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_2) 
                                                                           << 2U) 
                                                                          | (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_1) 
                                                                              << 1U) 
                                                                             | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_ready_R_0))))))))))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.data_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list_fifo__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus;
    if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = ((0xfffffffdU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return) 
               | (2U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                        >> 4U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = (0x20U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = (0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
            = (0xfffdffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
    } else {
        if (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                = ((0xfffffff7U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return) 
                   | (8U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                            >> 4U)));
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                = (0x80U | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                = (0xffffe7ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
            if ((3U != (3U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                              >> 0xbU)))) {
                vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return 
                    = (0xfffdffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return);
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__0__KET____DOT__addr_table__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__predicted_pc[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk2__DOT__branch_table_banks__BRA__1__KET____DOT__addr_table__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_is_idle = 
        (1U & (((~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                    >> 3U)) & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid))))) 
               & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.valid))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19 
        = ((2U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R) 
                  << 1U)) | (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_R) 
                                   >> 1U)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__read_index];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege 
        = (3U & ((0x20000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus)
                  ? (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                     >> 0xbU) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q)) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_ex 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__branch_metadata_table
        [vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__id_ex];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__push_ready 
        = (1U & ((~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                      >> 4U) & (~ (IData)((0U != (0xfU 
                                                  & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count))))))) 
                 & (~ (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count) 
                        >> 4U) & (~ (IData)((0U != 
                                             (0xfU 
                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk2__BRA__0__KET____DOT__input_data_fifo__DOT__genblk3__DOT__inflight_count)))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__inflight_count 
        = (0xfU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pre_issue_count) 
                   + (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__post_issue_count)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests 
        = ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests)) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                    >> 4U)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests 
        = ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests)) 
           | (2U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                    >> 3U)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.data_out;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.data_out;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]) 
           | (0xff000000U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__.addr 
                             << 0x16U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U] 
        = ((0xffc00000U & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[2U]) 
           | (0x3fffffU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__.addr 
                           >> 0xaU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0x80000U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0xf00000U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0xfffbffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0xfffc1fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U] 
        = (0x800U | (0xffffe7ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[1U]));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = ((0x3fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]) 
           | (0xffffc000U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__.addr 
                             << 0xcU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[5U] 
        = (0xfffU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__.addr 
                     >> 0x14U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0x200U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0x3c00U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (0xffffff07U & vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U] 
        = (6U | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__l2_requests[4U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__0__KET____DOT__tag_bank__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT____Vcellout__genblk1__DOT__branch_tag_banks__BRA__1__KET____DOT__tag_bank__read_data;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT__ready) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                  & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__released)) 
                 >> (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_attr
        [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0xfffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
                                << 1U) | (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))))) 
              << 0x2cU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0xfffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
                                << 1U) | (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))))) 
              << 0x2cU));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3c3fffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3dffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q) 
                                     >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q))))) 
              << 0x29U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x18000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3bffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x40000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3c3fffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3dffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q) 
                                     >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q))))) 
              << 0x29U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x18000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3bffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x40000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o 
        = ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q))))
            ? (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q) 
                         >> (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)))))
            : 0U);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o 
        = ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q))))
            ? (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q) 
                         >> (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)))))
            : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3ffc7U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (0x38U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                              << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3fe3fU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__id
                     [1U] << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x3bU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                     [1U] << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x38fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_id) 
                     << 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x2fU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r) 
                     << 4U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][4U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r;
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3f1ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__lut_ram
                     [vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__div_input_fifo__DOT__genblk3__DOT__read_index] 
                     << 9U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][2U] 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__mulh
           [1U] ? (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__result 
                           >> 0x20U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__result));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)) 
           | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.data_valid));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)) 
           | ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.data_valid) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_done 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.done) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress) 
              & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                 >> 3U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid) 
                  >> (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)) 
                 | (0U != ((0xbU >= (0xfU & ((IData)(3U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index))))
                            ? (7U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__load_check_count) 
                                     >> (0xfU & ((IData)(3U) 
                                                 * (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_index)))))
                            : 0U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellout__id_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__genblk3__DOT__genblk1__DOT__axi_bus__data_out;
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xe001ffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xfffe0000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__ls_offset) 
                             << 0x11U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xffffcfffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0xfffff000U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_load_r) 
                              << 0xdU) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_store_r) 
                                          << 0xcU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0xfffffff3U & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (0xfffffffcU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op_r) 
                             << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U] 
        = ((0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__alu_inputs[0U]) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_shifter_path) 
               << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_slt_path)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xfffbU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0xfffffffcU 
                                                     & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate) 
                                                        << 2U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[1U] = ((0x1ffffffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[1U]) 
                                                  | (0xfe000000U 
                                                     & ((IData)(
                                                                (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                  << 0x23U) 
                                                                 | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                     << 3U) 
                                                                    | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast))))))) 
                                                        << 0x19U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[2U] = ((0x1ffffffU 
                                                   & ((IData)(
                                                              (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                << 0x23U) 
                                                               | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                   << 3U) 
                                                                  | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast))))))) 
                                                      >> 7U)) 
                                                  | (0xfe000000U 
                                                     & ((IData)(
                                                                ((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                   << 0x23U) 
                                                                  | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                      << 3U) 
                                                                     | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast)))))) 
                                                                 >> 0x20U)) 
                                                        << 0x19U)));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xfffcU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0x1ffffffU 
                                                     & ((IData)(
                                                                ((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rid)) 
                                                                   << 0x23U) 
                                                                  | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rdata)) 
                                                                      << 3U) 
                                                                     | (QData)((IData)(
                                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rresp) 
                                                                                << 1U) 
                                                                                | (IData)(vlSymsp->TOP__taiga_sim__DOT__muir_axi2.rlast)))))) 
                                                                 >> 0x20U)) 
                                                        >> 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
        = ((0xffffbfffU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U]) 
           | (0xffffc000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__awvalid_m__result) 
                             << 0xeU)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
        = ((0xffffff3fU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0xffffffc0U & (0x40U | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result) 
                                      << 7U))));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U] 
        = (1U | ((0xfffffffcU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
                 | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__arvalid_m__result) 
                    << 1U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
        = ((0x3fU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U]) 
           | (0xffffffc0U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awid)) 
                                       << 0x2dU) | 
                                      (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr)) 
                                        << 0xdU) | (QData)((IData)(
                                                                   ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize) 
                                                                    << 2U)))))) 
                             << 6U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U]) 
           | ((0x3fU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awid)) 
                                  << 0x2dU) | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr)) 
                                                << 0xdU) 
                                               | (QData)((IData)(
                                                                 ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize) 
                                                                  << 2U)))))) 
                        >> 0x1aU)) | (0xffffffc0U & 
                                      ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awid)) 
                                                  << 0x2dU) 
                                                 | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awaddr)) 
                                                     << 0xdU) 
                                                    | (QData)((IData)(
                                                                      ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.awsize) 
                                                                       << 2U))))) 
                                                >> 0x20U)) 
                                       << 6U))));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
        = ((0x1ffU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0xfffffe00U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata)) 
                                       << 5U) | (QData)((IData)(
                                                                (((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb) 
                                                                  << 1U) 
                                                                 | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result)))))) 
                             << 9U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
        = ((0xffffc000U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U]) 
           | ((0x1ffU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata)) 
                                   << 5U) | (QData)((IData)(
                                                            (((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb) 
                                                              << 1U) 
                                                             | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result)))))) 
                         >> 0x17U)) | (0xfffffe00U 
                                       & ((IData)((
                                                   (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wdata)) 
                                                     << 5U) 
                                                    | (QData)((IData)(
                                                                      (((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.wstrb) 
                                                                        << 1U) 
                                                                       | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__genblk3__DOT__genblk1__DOT__axi_bus__DOT____Vcellout__wvalid_m__result))))) 
                                                   >> 0x20U)) 
                                          << 9U))));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U] 
        = ((0x7ffffU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
           | (0xfff80000U & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                       << 0x2dU) | 
                                      (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                        << 0xdU) | (QData)((IData)(
                                                                   ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                    << 2U)))))) 
                             << 0x13U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
        = ((0x7ffffU & ((IData)((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                  << 0x2dU) | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                                << 0xdU) 
                                               | (QData)((IData)(
                                                                 ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                  << 2U)))))) 
                        >> 0xdU)) | (0xfff80000U & 
                                     ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                                 << 0x2dU) 
                                                | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                                    << 0xdU) 
                                                   | (QData)((IData)(
                                                                     ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                      << 2U))))) 
                                               >> 0x20U)) 
                                      << 0x13U)));
    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0x7ffffU & ((IData)(((((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arid)) 
                                     << 0x2dU) | (((QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.araddr)) 
                                                   << 0xdU) 
                                                  | (QData)((IData)(
                                                                    ((IData)(vlSymsp->TOP__taiga_sim__DOT__m_axi.arsize) 
                                                                     << 2U))))) 
                                   >> 0x20U)) >> 0xdU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q) 
           | (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q));
    vlTOPp->taiga_sim__DOT____Vcellinp__taiga_axi_bus__rst_ni 
        = (1U & (~ (IData)(vlTOPp->rst)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_20 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_10 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_12 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_62 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__value)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests[0U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests[1U] 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U] 
        = ((0xfffffff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[0U]) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ecall) 
               << 2U) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ebreak) 
                          << 1U) | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_ret))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result 
        = (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid) 
            & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
               >> 2U)) & (~ ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
                             >> 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xffdfffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xffe00000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_return) 
                             << 0x15U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xffbfffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xffc00000U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__is_call) 
                             << 0x16U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xffe00000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset_r);
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[1U] 
        = ((0x3fff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [1U]) | (7U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out) 
                           >> 1U)));
    vlTOPp->ddr_axi_arvalid_taiga = vlTOPp->taiga_sim__DOT__axi_arvalid;
    vlTOPp->ddr_axi_awvalid_muir = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->ddr_axi_wlast_muir = (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_lr 
        = ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                    >> 7U)) & (2U == (0x1fU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                       >> 2U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_10 
        = (0xffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wr_cnt)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_11 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_12 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_13 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT___T_17 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_3 
        = (0xfU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_490 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_493 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_499 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_505 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_506 
        = (4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_507 
        = (5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_509 
        = (6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_10 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_62 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__value)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_10 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_62 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__value)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_7 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((0xeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | ((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                             >> 0x21U))) == (7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | (((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                              >> 0x21U))) == (7U & 
                                              ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed) 
                                               >> 3U))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((0xbU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | (((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                              >> 0x21U))) == (7U & 
                                              ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed) 
                                               >> 6U))) 
              << 2U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match 
        = ((7U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__wb_id_match)) 
           | (((7U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__wb_snoop 
                              >> 0x21U))) == (7U & 
                                              ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__id_needed) 
                                               >> 9U))) 
              << 3U));
    __Vtemp13[0U] = 1U;
    __Vtemp13[1U] = 0U;
    __Vtemp13[2U] = 0U;
    __Vtemp13[3U] = 0U;
    __Vtemp13[4U] = 0U;
    __Vtemp13[5U] = 0U;
    __Vtemp13[6U] = 0U;
    __Vtemp13[7U] = 0U;
    VL_SHIFTL_WWI(256,256,8, __Vtemp14, __Vtemp13, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[0U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[0U] 
           | __Vtemp14[0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[1U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[1U] 
           | __Vtemp14[1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[2U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[2U] 
           | __Vtemp14[2U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[3U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[3U] 
           | __Vtemp14[3U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[4U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[4U] 
           | __Vtemp14[4U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[5U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[5U] 
           | __Vtemp14[5U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[6U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[6U] 
           | __Vtemp14[6U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_266[7U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v[7U] 
           | __Vtemp14[7U]);
    __Vtemp16[0U] = 1U;
    __Vtemp16[1U] = 0U;
    __Vtemp16[2U] = 0U;
    __Vtemp16[3U] = 0U;
    __Vtemp16[4U] = 0U;
    __Vtemp16[5U] = 0U;
    __Vtemp16[6U] = 0U;
    __Vtemp16[7U] = 0U;
    VL_SHIFTL_WWI(256,256,8, __Vtemp17, __Vtemp16, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[0U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U] 
           | __Vtemp17[0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[1U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U] 
           | __Vtemp17[1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[2U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U] 
           | __Vtemp17[2U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[3U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U] 
           | __Vtemp17[3U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[4U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U] 
           | __Vtemp17[4U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[5U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U] 
           | __Vtemp17[5U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[6U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U] 
           | __Vtemp17[6U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_273[7U] 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U] 
           | __Vtemp17[7U]);
    __Vtemp20[0U] = 1U;
    __Vtemp20[1U] = 0U;
    __Vtemp20[2U] = 0U;
    __Vtemp20[3U] = 0U;
    __Vtemp20[4U] = 0U;
    __Vtemp20[5U] = 0U;
    __Vtemp20[6U] = 0U;
    __Vtemp20[7U] = 0U;
    VL_SHIFTL_WWI(256,256,8, __Vtemp21, __Vtemp20, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[0U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[0U]) 
           | __Vtemp21[0U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[1U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[1U]) 
           | __Vtemp21[1U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[2U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[2U]) 
           | __Vtemp21[2U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[3U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[3U]) 
           | __Vtemp21[3U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[4U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[4U]) 
           | __Vtemp21[4U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[5U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[5U]) 
           | __Vtemp21[5U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[6U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[6U]) 
           | __Vtemp21[6U]);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_275[7U] 
        = ((~ vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d[7U]) 
           | __Vtemp21[7U]);
    vlSymsp->TOP__taiga_sim__DOT__mem.wr_data_read 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress) 
            & (IData)(vlTOPp->ddr_axi_wready_taiga)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__new_entry 
        = (((QData)((IData)((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_tlb_flush))))) 
            << 0x23U) | (((QData)((IData)((0x7fffU 
                                           & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                                              >> 0x11U)))) 
                          << 0x14U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__immu.upper_physical_address))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_r_ready 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate)) 
           & (6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)));
    vlTOPp->taiga_sim__DOT__axi_wvalid = (((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress) 
                                           & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                                              >> 4U)) 
                                          & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete)));
    vlTOPp->taiga_sim__DOT__axi_wlast = ((((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__on_last_burst) 
                                           & (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_in_progress)) 
                                          & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                                             >> 4U)) 
                                         & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_transfer_complete)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated 
        = (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_shifted_1 
           > vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted);
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                  >> 4U) & (~ (IData)((0U != (0xfU 
                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__inflight_count)))))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full 
        = (1U & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count) 
                  >> 4U) & (~ (IData)((0U != (0xfU 
                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_data__DOT__genblk3__DOT__inflight_count)))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406 
        = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & ((1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
              & ((2U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                 & (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store 
        = (1U & ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                             >> 8U))) | ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                  >> 7U)) 
                                         & (2U != (0x1fU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                              >> 2U)))))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc 
        = ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                    >> 7U)) & (3U == (0x1fU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                                       >> 2U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__itlb.new_request 
        = (1U & ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT__request_in_progress)) 
                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
                    >> 0x1fU)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T 
        = ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->ddr_axi_rvalid_muir));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
        = (((QData)((IData)(((0x100000U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U])
                              ? (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                           << 0xbU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                             >> 0x15U)))
                              : ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                  << 0x1aU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[1U] 
                                               >> 6U))))) 
            << 0x10U) | (QData)((IData)(((0xfff0U & 
                                          ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                            << 0xaU) 
                                           | (0x3f0U 
                                              & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                 >> 0x16U)))) 
                                         | ((0xcU & 
                                             ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                               << 0x10U) 
                                              | (0xfffcU 
                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                    >> 0x10U)))) 
                                            | (((0U 
                                                 == 
                                                 (0x1fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                                      << 0xbU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                        >> 0x15U)))) 
                                                << 1U) 
                                               | (0U 
                                                  == 
                                                  (0x1fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[3U] 
                                                       << 0x13U) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__stage1[2U] 
                                                         >> 0xdU))))))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_data 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag
        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_addr_pipe_0];
    VL_SHIFTR_WWI(256,256,8, __Vtemp23, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[0U] 
        = __Vtemp23[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[1U] 
        = __Vtemp23[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[2U] 
        = __Vtemp23[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[3U] 
        = __Vtemp23[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[4U] 
        = __Vtemp23[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[5U] 
        = __Vtemp23[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[6U] 
        = __Vtemp23[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[7U] 
        = __Vtemp23[7U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__0__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                    >> 0xcU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_tlb__DOT____Vcellinp__lut_rams__BRA__1__KET____DOT__ram_block__raddr[0U] 
        = (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc 
                    >> 0xcU));
    if ((0x40U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr))) {
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b 
            = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                     >> 4U));
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a 
            = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor)
                ? vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_shifted
                : (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__PR 
                           >> 2U)));
    } else {
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b 
            = (1U & ((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero)) 
                     & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress_attr) 
                        >> 5U)));
        vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a 
            = ((- (IData)((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.divisor_is_zero))) 
               | vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__Q_temp);
    }
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__Vfuncout 
        = (((- (IData)((IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b))) 
            ^ vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__a) 
           + (IData)(vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__b));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__.rd 
        = vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__negate_if__7__Vfuncout;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[2U];
    } else {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[0U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[1U];
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[2U];
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3));
    vlTOPp->taiga_sim__DOT__cpu__DOT__br_exception 
        = (((QData)((IData)((((vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex 
                               >> 1U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_taken_ex)) 
                             & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__branch_issued_r)))) 
            << 0x28U) | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__new_pc_ex)) 
                          << 3U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__branch_unit_block__DOT__jmp_id))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__lut_ram
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__input_fifo__DOT__genblk3__DOT__read_index];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests 
        = ((0xdU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk1__DOT__arb__DOT__requests)) 
           | (2U & ((0xfffffffeU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state)) 
                    | (0x3ffffffeU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__state) 
                                      >> 2U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = (1U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = (0x40U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x77U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (8U & (((~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                      [0U]) | (~ vlTOPp->taiga_sim__DOT__cpu__DOT__genblk7__DOT__mul_unit_block__DOT__done
                               [1U])) << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x6fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__unit_ready) 
               & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.pop))) 
              << 4U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_3 
        = ((IData)(1U) + vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cycles);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_8 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__terminate_early 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__firstCycle)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__greaterDivisor));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.start 
        = (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid) 
            & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__in_progress))) 
           & (~ (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U] 
                 >> 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x100U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 1U : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x200U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 2U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x400U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x800U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 4U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x1000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 5U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x2000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 6U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x4000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 7U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x8000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 8U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x10000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 9U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x20000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xaU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x40000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xbU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x80000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xcU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x100000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xdU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x200000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xeU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x400000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0xfU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x800000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x10U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x1000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x11U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x2000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x12U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x4000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x13U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x8000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x14U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x10000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x15U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x20000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x16U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x40000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x17U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x80000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x18U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((1U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x19U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((2U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1aU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((4U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1bU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((8U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1cU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x10U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1dU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x20U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1eU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB 
        = ((0x40U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[2U])
            ? 0x1fU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x100U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 1U : 0U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x200U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 2U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x400U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x800U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 4U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x1000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 5U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x2000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 6U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x4000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 7U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x8000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 8U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x10000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 9U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x20000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xaU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x40000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xbU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x80000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xcU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x100000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xdU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x200000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xeU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x400000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0xfU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x800000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x10U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x1000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x11U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x2000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x12U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x4000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x13U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x8000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x14U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x10000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x15U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x20000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x16U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x40000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x17U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x80000000U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[0U])
            ? 0x18U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((1U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x19U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((2U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1aU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((4U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1bU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((8U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1cU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x10U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1dU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x20U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1eU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB 
        = ((0x40U & vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.data_out[1U])
            ? 0x1fU : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_R_data 
           + vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_R_data);
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level = 3U;
    if (((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
         | (0U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)))) {
        if ((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg 
                   >> (0x1fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                        >> 0x23U)))))) {
            vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_3___05FT_14_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_2___05FT_14_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_1___05FT_14_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_0___05FT_14_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_7___05FT_14_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_6___05FT_14_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_5___05FT_14_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_0_4___05FT_14_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_3___05FT_24_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_2___05FT_24_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_1___05FT_24_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_0___05FT_24_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_7___05FT_24_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_6___05FT_24_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_5___05FT_24_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_1_4___05FT_24_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_3___05FT_34_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_2___05FT_34_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_1___05FT_34_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_0___05FT_34_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_7___05FT_34_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_6___05FT_34_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_5___05FT_34_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_2_4___05FT_34_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7___05FT_44_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6___05FT_44_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5___05FT_44_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4___05FT_44_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3___05FT_44_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2___05FT_44_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1___05FT_44_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0___05FT_44_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_7___05FT_44_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_6___05FT_44_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_5___05FT_44_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_4___05FT_44_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_3___05FT_44_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_2___05FT_44_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_1___05FT_44_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_3_0___05FT_44_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[0U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_3___05FT_54_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_2___05FT_54_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_1___05FT_54_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_0___05FT_54_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[1U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_7___05FT_54_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_6___05FT_54_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_5___05FT_54_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_4_4___05FT_54_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[2U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_3___05FT_64_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_2___05FT_64_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_1___05FT_64_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_0___05FT_64_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[3U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_7___05FT_64_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_6___05FT_64_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_5___05FT_64_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_5_4___05FT_64_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[4U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_3___05FT_74_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_2___05FT_74_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_1___05FT_74_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_0___05FT_74_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[5U] 
        = ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7
            [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_7___05FT_74_addr_pipe_0] 
            << 0x18U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6
                          [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_6___05FT_74_addr_pipe_0] 
                          << 0x10U) | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_5___05FT_74_addr_pipe_0] 
                                        << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_6_4___05FT_74_addr_pipe_0])));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[6U] 
        = (IData)((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7___05FT_84_addr_pipe_0] 
                                      << 0x18U) | (
                                                   (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                                    [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6___05FT_84_addr_pipe_0] 
                                                    << 0x10U) 
                                                   | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5___05FT_84_addr_pipe_0] 
                                                       << 8U) 
                                                      | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4___05FT_84_addr_pipe_0]))))) 
                    << 0x20U) | (QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                  [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3___05FT_84_addr_pipe_0] 
                                                  << 0x18U) 
                                                 | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                     [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2___05FT_84_addr_pipe_0] 
                                                     << 0x10U) 
                                                    | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1___05FT_84_addr_pipe_0] 
                                                        << 8U) 
                                                       | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0___05FT_84_addr_pipe_0])))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[7U] 
        = (IData)(((((QData)((IData)(((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7
                                       [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_7___05FT_84_addr_pipe_0] 
                                       << 0x18U) | 
                                      ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_6___05FT_84_addr_pipe_0] 
                                        << 0x10U) | 
                                       ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5
                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_5___05FT_84_addr_pipe_0] 
                                         << 8U) | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4
                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_4___05FT_84_addr_pipe_0]))))) 
                     << 0x20U) | (QData)((IData)(((
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3
                                                   [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_3___05FT_84_addr_pipe_0] 
                                                   << 0x18U) 
                                                  | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2
                                                      [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_2___05FT_84_addr_pipe_0] 
                                                      << 0x10U) 
                                                     | ((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1
                                                         [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_1___05FT_84_addr_pipe_0] 
                                                         << 8U) 
                                                        | vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0
                                                        [vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dataMem_7_0___05FT_84_addr_pipe_0])))))) 
                   >> 0x20U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] = 
        ((7U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U]) 
         | (0xfffffff8U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id) 
                           << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] = 
        ((0xfffffffbU & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]) 
         | (0xfffffffcU & ((IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetched_count))) 
                           << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] = 
        ((0xfffffffcU & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]) 
         | vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__fetch_metadata_table
         [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] = 
        ((7U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U]) 
         | (0xfffffff8U & ((IData)((((QData)((IData)(
                                                     vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                                     [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                                     << 0x20U) | (QData)((IData)(
                                                                 vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                                 [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])))) 
                           << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] = 
        ((7U & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                          [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                          << 0x20U) | (QData)((IData)(
                                                      vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                      [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])))) 
                >> 0x1dU)) | (0xfffffff8U & ((IData)(
                                                     ((((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                                                        [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                                                        << 0x20U) 
                                                       | (QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                                         [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id]))) 
                                                      >> 0x20U)) 
                                             << 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U] = 
        ((0x38U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[2U]) 
         | (7U & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__pc_table
                                             [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id])) 
                             << 0x20U) | (QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__instruction_table
                                                         [vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT__decode_id]))) 
                           >> 0x20U)) >> 0x1dU)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_11 
        = (0xffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_22 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__id_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[0U] 
        = (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                  << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                               >> 3U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__toggle_addr[0U] 
        = (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                     << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                  >> 0xaU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U] 
        = ((0x3fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[2U]) 
           | (0xffffffc0U & ((IData)((((QData)((IData)(
                                                       vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                                       << 0x29U) | 
                                      (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                        << 9U) | ((QData)((IData)(
                                                                  vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                                  >> 0x17U)))) 
                             << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[3U] 
        = ((0x3fU & ((IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                               << 0x29U) | (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                             << 9U) 
                                            | ((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                               >> 0x17U)))) 
                     >> 0x1aU)) | (0xffffffc0U & ((IData)(
                                                          ((((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                                                             << 0x29U) 
                                                            | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                                                << 9U) 
                                                               | ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                                                  >> 0x17U))) 
                                                           >> 0x20U)) 
                                                  << 6U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_inputs[4U] 
        = (0x3fU & ((IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U])) 
                               << 0x29U) | (((QData)((IData)(
                                                             vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U])) 
                                             << 9U) 
                                            | ((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U])) 
                                               >> 0x17U))) 
                             >> 0x20U)) >> 0x1aU));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
        = ((0xc7ffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U]) 
           | (0x38000000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0x3ffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xfc000000U & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                                         << 9U) 
                                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                           >> 0x17U)))) 
                                       << 1U) | (QData)((IData)(
                                                                (1U 
                                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                    >> 2U)))))) 
                             << 0x1aU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U] 
        = ((0xf8000000U & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[1U]) 
           | ((0x3ffffffU & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                                         << 9U) 
                                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                           >> 0x17U)))) 
                                       << 1U) | (QData)((IData)(
                                                                (1U 
                                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                    >> 2U)))))) 
                             >> 6U)) | (0xfc000000U 
                                        & ((IData)(
                                                   ((((QData)((IData)(
                                                                      ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[3U] 
                                                                        << 9U) 
                                                                       | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                                                                          >> 0x17U)))) 
                                                      << 1U) 
                                                     | (QData)((IData)(
                                                                       (1U 
                                                                        & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                                                           >> 2U))))) 
                                                    >> 0x20U)) 
                                           << 0x1aU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U] 
        = ((0xfc7fffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__branch_inputs[0U]) 
           | (0xff800000U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__br_use_signed) 
                              << 0x19U) | ((0x1000000U 
                                            & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                               << 8U)) 
                                           | (0x800000U 
                                              & (((~ 
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                    >> 0x10U)) 
                                                  << 0x17U) 
                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                                    << 8U)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id[0U] 
        = ((0x3fff8U & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_instruction_id
            [0U]) | (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                            << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                                         >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U] 
        = ((0xfffe3fffU & vlTOPp->taiga_sim__DOT__cpu__DOT__ls_inputs[0U]) 
           | (0x1c000U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[2U] 
                           << 0x1aU) | (0x3ffc000U 
                                        & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                                           >> 6U)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group[0U] 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 0x15U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.rs_wb_group[1U] 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                 >> 0x16U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr[0U] 
        = (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                     << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                               >> 0x17U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr[1U] 
        = (0x3fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__issue[1U] 
                     << 3U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__issue[0U] 
                               >> 0x1dU)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_dme_wr_0_ack 
        = ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
           & (IData)(vlTOPp->ddr_axi_bvalid_muir));
    VL_SHIFTR_WWI(256,256,8, __Vtemp100, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__v, (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count));
    VL_SHIFTR_WWI(256,256,8, __Vtemp101, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d, (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_block_dirty 
        = (1U & (__Vtemp100[0U] & __Vtemp101[0U]));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_512 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_513 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_514 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_515 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_517 
        = (4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_518 
        = (5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__FU_io_out 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_R_data 
           + vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_R_data);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_17 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_17 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_17 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
           & (0xffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_count)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_38 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_45 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_66 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_44 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_52 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_59 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_44 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_50 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_51 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_44 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_50 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_51 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_56 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_84 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_R_0_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0)) 
           & (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_0_0) 
               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_1_0)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_2_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_valid_R) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_valid_R) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_R_control) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_11 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_3 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_38 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_44 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_47 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_19 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___GEN_8 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_20 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_52 
        = (1U & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_ready_R_0) 
                 | (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__task_id 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_8 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__true_output 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__false_output 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_29 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_true_R_0) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_false_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_41 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen) 
           == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_tag_reg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_13 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_29 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_31 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___T_33 
        = (3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_1 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_2 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_3 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_15 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_22 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_408 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_1_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_2_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_8_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_taskID) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__predecessor_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__state)) 
           & vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__reg_0);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_1 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_33 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_23 
        = (1U & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_7 
        = (0x7fffU & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__cycleCount)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__FU__DOT___T_21 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_R_data 
           == vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_R_data);
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_47 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_R_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_R_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_0_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_in_R_1_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_92 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T 
        = (0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_1 
        = (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT___T_2 
        = (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate));
    vlTOPp->taiga_sim__DOT__mst_ports_resp[3U] = ((0xfffU 
                                                   & vlTOPp->taiga_sim__DOT__mst_ports_resp[3U]) 
                                                  | (0xfffff000U 
                                                     & (((0U 
                                                          == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
                                                         << 0xfU) 
                                                        | ((0x4000U 
                                                            & ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__rstate)) 
                                                               << 0xeU)) 
                                                           | (((1U 
                                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
                                                               << 0xdU) 
                                                              | ((2U 
                                                                  == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dcr__DOT__wstate)) 
                                                                 << 0xcU))))));
    vlTOPp->taiga_sim__DOT__xbar_rule_array[0U] = 0x60002000U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[1U] = 0x60001000U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[2U] = 0U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[0U] = 0x60002000U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[1U] = 0x60001000U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[2U] = 0U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[3U] = 0x60002fffU;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[4U] = 0x60002000U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[5U] = 1U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[3U] = 0x60002fffU;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[4U] = 0x60002000U;
    vlTOPp->taiga_sim__DOT__xbar_rule_array[5U] = 1U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
        = ((0U >= (1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out)))
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__unit_data_array
           [(1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out))]
            : 0U);
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push = 0U;
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push 
        = (((~ ((IData)(1U) << (1U & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out 
                                              >> 0x22U))))) 
            & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_push)) 
           | ((1U & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_returndata__DOT__genblk3__DOT__inflight_count) 
                     >> 4U)) << (1U & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.data_out 
                                               >> 0x22U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_30 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_28)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_start_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_31 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_29)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__active_loop_back_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_40 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_34)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_carry_out_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_3 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_10) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__mask_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_16) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_sign 
        = ((4U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_3 
                           >> 0x22U)) << 2U)) | ((2U 
                                                  & ((IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_2 
                                                              >> 0x22U)) 
                                                     << 1U)) 
                                                 | (1U 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__new_PR_1 
                                                               >> 0x22U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_129 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state) 
           & (0x3fffU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_48)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__processing_csr) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__ls_is_idle));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_3));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_R_1_data
            : 0ULL);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0x7fffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o)) 
              << 0x37U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xbfffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o)) 
              << 0x36U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0x7fffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o)) 
              << 0x37U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o 
        = ((0xbfffffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__slv_resp_o) 
           | ((QData)((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o)) 
              << 0x36U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[0U] = 0U;
    if ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[0U] = 0U;
    }
    if ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[0U] = 1U;
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[1U] = 1U;
    if ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[1U] = 1U;
    }
    if ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests))) {
        vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes[1U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__access_valid 
        = (1U & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                          [0U] >> 3U)) & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                  [0U] 
                                                  >> 6U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__privilege_valid 
        = (((3U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege)) 
            | ((1U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege)) 
               & ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                              [0U] >> 4U))) | ((IData)(
                                                       (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                        [0U] 
                                                        >> 4U)) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                  >> 0x12U))))) 
           | ((0U == (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dmmu.privilege)) 
              & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                         [0U] >> 4U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__privilege_valid 
        = (((3U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
            | ((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
               & ((~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                              [0U] >> 4U))) | ((IData)(
                                                       (vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                                        [0U] 
                                                        >> 4U)) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                  >> 0x12U))))) 
           | ((0U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level)) 
              & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                         [0U] >> 4U))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid) 
           & (1U == (3U & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                   [0U] >> 0x20U)))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.valid) 
           & (3U == (3U & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__return_data
                                   [0U] >> 0x20U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[1U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[2U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[3U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[4U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[5U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[6U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[7U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_199[7U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[8U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[9U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xaU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xbU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xcU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xdU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xeU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xfU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_202[7U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__retire_ids
        [1U];
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__pc 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc;
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__Vfuncout 
        = (0x1fffffU & (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__pc 
                        >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
           | ((0x3fffffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                            [0U] >> 5U)) == (0x200000U 
                                             | vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__0__Vfuncout)));
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__pc 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__pc;
    vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__Vfuncout 
        = (0x1fffffU & (vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__pc 
                        >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)) 
           | (((0x3fffffU & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                             [1U] >> 5U)) == (0x200000U 
                                              | vlTOPp->__Vfunc_taiga_sim__DOT__cpu__DOT__bp_block__DOT__get_tag__1__Vfuncout)) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__ready_for_issue_from_lsq 
        = ((3U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_ready)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_switch_stall)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
        = ((1ULL & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry) 
           | (0xfffffffffeULL & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict 
        = (0U != ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out) 
                  & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__valid)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fe000000000ULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3fefffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o) 
                                     >> 8U)))) << 0x24U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xbadcab1e0ULL | (0x3ff00000000fULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xcULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fdfffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x2000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = ((0x3ffffffffffdULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp) 
               | ((QData)((IData)((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q))))) 
                  << 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fe000000000ULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = ((0x3fefffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
           | ((QData)((IData)((1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o) 
                                     >> 8U)))) << 0x24U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xbadcab1e0ULL | (0x3ff00000000fULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0xcULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x3fdfffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x2000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = ((0x3ffffffffffdULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp) 
               | ((QData)((IData)((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q))))) 
                  << 1U));
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x3eU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | (0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_valid)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done[1U] 
        = ((0x3dU & vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_done
            [1U]) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_done) 
                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__done_r)) 
                     << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready 
        = ((0x7bU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_ready)) 
           | (4U & (((~ (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.valid)) 
                     | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.pop)) 
                    << 2U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_data_array
        [(1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
            >> 0xeU) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U] 
            >> 1U) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U] 
            >> 7U) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill 
        = ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            >> 0x1aU) & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__ready_o));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q)))) {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 0U;
    if ((1U & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q)))) {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_metadata 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result) 
            << 1U) | (1U & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
                            >> 1U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_complete 
        = (((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.valid) 
            & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result))) 
           | (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid));
    vlTOPp->ddr_axi_rready_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_r_ready;
    vlTOPp->ddr_axi_wvalid_taiga = vlTOPp->taiga_sim__DOT__axi_wvalid;
    vlTOPp->ddr_axi_wlast_taiga = vlTOPp->taiga_sim__DOT__axi_wlast;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_7 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__enable_valid_R));
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift 
            = (0x1fU & (0x1eU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated)
                         ? ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                            >> 1U) : ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                                      >> 1U)));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__inital_shift 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated)
                         ? ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                            - (IData)(2U)) : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__num_radix4_iter 
            = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__over_estimated)
                         ? (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                             >> 1U) - (IData)(1U)) : 
                        ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta_r) 
                         >> 1U)));
    }
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__advance = 
        ((0U != (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.requests)) 
         & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 5U) & (~ ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                                >> 6U))) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 5U) & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                             >> 6U)) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.push 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))) 
                 & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__write_done 
        = ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__data_attributes_fifo__DOT__genblk3__DOT__inflight_count) 
             >> 5U) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.full))) 
           & ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__burst_count) 
              == (0x1fU & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.data_out) 
                           >> 1U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_8) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_8) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT__enable_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_8) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT__enable_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
            : ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
                : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
                    : ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                        ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406)
                        : ((4U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
                           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_406))))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__.push 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
            & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.push 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
            & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store)) 
           & (~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id_v) 
                 >> 1U)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match)) 
           | (vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address
              [0U] == (0x3fffffffU & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                              >> 0xdU)))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc) 
              | ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store) 
                 & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match))));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match)) 
           | ((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation_address
               [1U] == (0x3fffffffU & (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                               >> 0xdU)))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__revoke_reservation)) 
           | (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc) 
               << 1U) | (0xfffffffeU & (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_store) 
                                         << 1U) & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_31 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_62 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__state) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_0));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_valid_R_1));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T) 
           & (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_count)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[(7U 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                                >> 4U)) 
                                                                                >> 5U))] 
        = (((~ ((IData)(1U) << (0x1fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
            & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[
            (7U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                            >> 4U)) >> 5U))]) | (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r) 
                                                  & ((3U 
                                                      != 
                                                      (3U 
                                                       & (IData)(
                                                                 (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                  >> 0xeU)))) 
                                                     & (1U 
                                                        == 
                                                        (3U 
                                                         & (IData)(
                                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                    >> 0xcU)))))) 
                                                 << 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
        = ((1U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                 >> 2U)))) ? (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 0x10U))
            : ((2U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                     >> 2U)))) ? (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r 
                                                  | (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 0x10U)))
                : ((3U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                         >> 2U)))) ? 
                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr_r 
                    & (~ (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                  >> 0x10U)))) : (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 0x10U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_regs
        [((0x18U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                             >> 0xcU)) << 3U)) | (7U 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))];
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_ready_to_complete_r) 
           & ((3U != (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                    >> 0xeU)))) & (3U 
                                                   == 
                                                   (3U 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 0xcU))))));
    VL_SHIFTR_WWI(256,256,8, __Vtemp107, vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__d, 
                  (0xffU & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                    >> 6U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty 
        = (1U & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[0U] 
                 & __Vtemp107[0U]));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit 
        = (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_219[0U] 
           & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__metaMem_tag_rmeta_data 
              == (0x3ffffffffffffULL & (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                        >> 0xeU))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__.rd;
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U]) 
           | (0xfffffffeU & ((0xfffffffcU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
        = ((1U & ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                  | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                       & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                      & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                     >> 0x1fU))) | (0xfffffffeU & (
                                                   (2U 
                                                    & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                                                   | (0xfffffffcU 
                                                      & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
              | (0xfffffffeU & ((2U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
                                | (0x3cU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U])))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0x3ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | (0xfc000000U & ((0xf8000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x19U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x1aU))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
        = ((0x3ffffffU & ((0x3ffffffU & ((0x2000000U 
                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                             << 0x19U)) 
                                         | (0x1ffffffU 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                               >> 7U)))) 
                          | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                               & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                              & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                             >> 6U))) | (0xfc000000U 
                                         & ((0x4000000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x19U)) 
                                            | (0xf8000000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U]) 
           | ((0x3ffffffU & ((0x2000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                            << 0x19U)) 
                             | (0x1ffffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                              >> 7U)))) 
              | (0xfc000000U & ((0x4000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                               << 0x19U)) 
                                | (0x78000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                  << 0x19U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0x7ffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | (0xfff80000U & ((0xfff00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x12U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x13U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
        = ((0x7ffffU & ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)) 
                                     | (0x3ffffU & 
                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                         >> 0xeU)))) 
                        | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                            & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                           >> 0xdU))) | (0xfff80000U 
                                         & ((0x80000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x12U)) 
                                            | (0xfff00000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                        << 0x12U)) 
                           | (0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                          >> 0xeU)))) 
              | (0xfff80000U & ((0x80000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                             << 0x12U)) 
                                | (0xf00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                << 0x12U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
        = ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U]) 
           | (0xfffffffeU & ((0xfffffffcU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 1U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
        = ((1U & ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                  | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                       & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                      & (0U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                     >> 0x1fU))) | (0xfffffffeU & (
                                                   (2U 
                                                    & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]) 
                                                   | (0xfffffffcU 
                                                      & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U]))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0xffffffc0U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
              | (0xfffffffeU & ((2U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U]) 
                                | (0x3cU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U])))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0x3ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | (0xfc000000U & ((0xf8000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x19U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x1aU))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
        = ((0x3ffffffU & ((0x3ffffffU & ((0x2000000U 
                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                             << 0x19U)) 
                                         | (0x1ffffffU 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                               >> 7U)))) 
                          | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                               & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                              & (1U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                             >> 6U))) | (0xfc000000U 
                                         & ((0x4000000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x19U)) 
                                            | (0xf8000000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
        = ((0x80000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U]) 
           | ((0x3ffffffU & ((0x2000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                            << 0x19U)) 
                             | (0x1ffffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                              >> 7U)))) 
              | (0xfc000000U & ((0x4000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                               << 0x19U)) 
                                | (0x78000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                  << 0x19U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0x7ffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | (0xfff80000U & ((0xfff00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                             << 0x12U)) 
                             | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                                  & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                                 & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                                << 0x13U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
        = ((0x7ffffU & ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)) 
                                     | (0x3ffffU & 
                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U] 
                                         >> 0xeU)))) 
                        | ((((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_r_fifo__DOT__status_cnt_q)) 
                             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_valid)) 
                            & (2U == (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[0U]))) 
                           >> 0xdU))) | (0xfff80000U 
                                         & ((0x80000U 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                << 0x12U)) 
                                            | (0xfff00000U 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                                  << 0x12U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0xff000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | ((0x7ffffU & ((0x40000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                        << 0x12U)) 
                           | (0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[1U] 
                                          >> 0xeU)))) 
              | (0xfff80000U & ((0x80000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                             << 0x12U)) 
                                | (0xf00000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_ar_spill_reg__data_o[2U] 
                                                << 0x12U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1 = 1U;
        if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1) 
                      << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2 = 1U;
            if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                    = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                        & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                       | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2) 
                          << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__lock_aw_valid_q) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1 = 1U;
        if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound1) 
                      << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
        }
    } else {
        if (((1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
             & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_valid))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2 = 1U;
            if ((2U >= (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids 
                    = (((~ ((IData)(1U) << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]))) 
                        & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids)) 
                       | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vlvbound2) 
                          << (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U])));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__left_valid_R));
    vlTOPp->ddr_axi_awaddr_taiga = (0xfffffffcU & ((IData)(
                                                           (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                            >> 0xeU)) 
                                                   << 2U));
    vlTOPp->ddr_axi_araddr_taiga = (0xfffffff0U & ((IData)(
                                                           (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                            >> 0x10U)) 
                                                   << 4U));
    vlTOPp->ddr_axi_arid_taiga = (7U & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out));
    vlTOPp->ddr_axi_arlen_taiga = (0x1fU & (IData)(
                                                   (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                    >> 3U)));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
        = ((0xffffffe0U & ((IData)((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                                     << 0x20U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out)))) 
                           << 5U)) | (0x1fU & (IData)(
                                                      (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                       >> 3U))));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
        = ((0x1fU & ((IData)((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                               << 0x20U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out)))) 
                     >> 0x1bU)) | (0xffffffe0U & ((IData)(
                                                          ((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                                                             << 0x20U) 
                                                            | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out))) 
                                                           >> 0x20U)) 
                                                  << 5U)));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
        = (0x1fU & ((IData)(((((QData)((IData)(vlTOPp->ddr_axi_rdata_taiga)) 
                               << 0x20U) | (QData)((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out))) 
                             >> 0x20U)) >> 0x1bU));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write 
        = ((IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                    >> 8U)) & ((2U != (0x1fU & (IData)(
                                                       (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                        >> 3U)))) 
                               | (3U != (0x1fU & (IData)(
                                                         (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                          >> 3U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__MSB_delta 
        = (0x1fU & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__A_MSB) 
                    - (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__divider_block__DOT__genblk14__DOT__div_block__DOT__B_MSB)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_19 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_31)
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__FU_io_out
            : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_data_R);
    if (VL_LIKELY(VL_ONEHOT0_I(((4U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                                >> 0x28U)) 
                                       << 2U)) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                                                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret)))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__next_privilege_level 
            = (3U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                      | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret))
                      ? ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret)
                          ? (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                             >> 0xbU) : (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                               >> 8U)))
                      : ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                        >> 0x28U)))
                          ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__exception_privilege_level)
                          : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))));
    } else {
        if (VL_UNLIKELY(Verilated::assertOn())) {
            VL_WRITEF("[%0t] %%Error: csr_regs.sv:222: Assertion failed in %Ntaiga_sim.cpu.gc_unit_block.csr_registers.genblk1: 'unique if' statement violated\n",
                      64,VL_TIME_UNITED_Q(1),vlSymsp->name());
            VL_STOP_MT("/localssd/taiga-project/taiga/core/csr_regs.sv", 222, "");
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[1U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[2U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[3U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[4U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[5U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[6U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[7U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_94[7U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[8U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[0U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[9U] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xaU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[2U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xbU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[3U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xcU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[4U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xdU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[5U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xeU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[6U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xfU] 
        = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_97[7U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op 
        = ((0x20000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
            ? ((0x10000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                ? ((0x8000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                    ? 2U : 1U) : ((0x8000U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                                   ? 3U : 0U)) : 3U);
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op 
        = ((0x20U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
            ? 3U : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__alu_logic_op));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rs1_link 
        = ((1U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                             << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                         >> 0x12U)))) 
           | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 0x12U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__rd_link 
        = ((1U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                             << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                          >> 0xaU)))) 
           | (5U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0x16U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xaU)))));
    if (VL_LIKELY(VL_ONEHOT0_I(((2U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 5U)) | (1U 
                                                  & ((~ 
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 6U)) 
                                                     & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 5U))))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__pc_offset 
            = (0x1fffffU & ((1U & ((~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 6U)) & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U)))
                             ? VL_EXTENDS_II(21,12, 
                                             (0xfffU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 9U) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 0x17U))))
                             : ((0x40U & vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U])
                                 ? ((0x100000U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x12U)) 
                                    | ((0xff000U & 
                                        ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1dU) 
                                         | (0x1ffff000U 
                                            & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 3U)))) 
                                       | ((0x800U & 
                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0xcU)) 
                                          | (0x7feU 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 9U) 
                                                | (0x1feU 
                                                   & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 0x17U)))))))
                                 : VL_EXTENDS_II(21,13, 
                                                 ((0x1000U 
                                                   & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0xaU)) 
                                                  | ((0x800U 
                                                      & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         << 1U)) 
                                                     | ((0x7e0U 
                                                         & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                             << 9U) 
                                                            | (0x1e0U 
                                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                                  >> 0x17U)))) 
                                                        | (0x1eU 
                                                           & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                               << 0x16U) 
                                                              | (0x3ffffeU 
                                                                 & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                                    >> 0xaU)))))))))));
    } else {
        if (VL_UNLIKELY(Verilated::assertOn())) {
            VL_WRITEF("[%0t] %%Error: decode_and_issue.sv:437: Assertion failed in %Ntaiga_sim.cpu.decode_and_issue_block: 'unique if' statement violated\n",
                      64,VL_TIME_UNITED_Q(1),vlSymsp->name());
            VL_STOP_MT("/localssd/taiga-project/taiga/core/decode_and_issue.sv", 437, "");
        }
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match 
        = ((0U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                              << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 0x17U))))
            ? (1U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
            : ((1U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                  << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 0x17U))))
                ? (2U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                : ((2U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                      << 9U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x17U))))
                    ? (4U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                    : ((0x102U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 9U) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x17U))))
                        ? (8U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                        : ((0x302U == (0xfffU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 9U) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 0x17U))))
                            ? (0x10U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                            : ((0x120U == (0xfe0U & 
                                           ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                             << 9U) 
                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 0x17U))))
                                ? (0x20U | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__sys_op_match))
                                : 0U))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__genblk11__DOT__illegal_op_check__DOT__base_legal 
        = (((((((((((((((((((((((((((((((((((((((((
                                                   ((((0x63U 
                                                       == 
                                                       (0x707fU 
                                                        & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                            << 0x1dU) 
                                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                              >> 3U)))) 
                                                      | (0x1063U 
                                                         == 
                                                         (0x707fU 
                                                          & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                              << 0x1dU) 
                                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                                >> 3U))))) 
                                                     | (0x4063U 
                                                        == 
                                                        (0x707fU 
                                                         & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                             << 0x1dU) 
                                                            | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                               >> 3U))))) 
                                                    | (0x5063U 
                                                       == 
                                                       (0x707fU 
                                                        & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                            << 0x1dU) 
                                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                              >> 3U))))) 
                                                   | (0x6063U 
                                                      == 
                                                      (0x707fU 
                                                       & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                           << 0x1dU) 
                                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                             >> 3U))))) 
                                                  | (0x7063U 
                                                     == 
                                                     (0x707fU 
                                                      & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                          << 0x1dU) 
                                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                            >> 3U))))) 
                                                 | (0x67U 
                                                    == 
                                                    (0x707fU 
                                                     & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                         << 0x1dU) 
                                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                           >> 3U))))) 
                                                | (0x6fU 
                                                   == 
                                                   (0x7fU 
                                                    & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                        << 0x1dU) 
                                                       | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                          >> 3U))))) 
                                               | (0x37U 
                                                  == 
                                                  (0x7fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U))))) 
                                              | (0x17U 
                                                 == 
                                                 (0x7fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1dU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 3U))))) 
                                             | (0x13U 
                                                == 
                                                (0x707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                            | (0x1013U 
                                               == (0xfc00707fU 
                                                   & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                       << 0x1dU) 
                                                      | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                         >> 3U))))) 
                                           | (0x2013U 
                                              == (0x707fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1dU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 3U))))) 
                                          | (0x3013U 
                                             == (0x707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                         | (0x4013U 
                                            == (0x707fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                                        | (0x5013U 
                                           == (0xfc00707fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))))) 
                                       | (0x40005013U 
                                          == (0xfc00707fU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                                      | (0x6013U == 
                                         (0x707fU & 
                                          ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                            << 0x1dU) 
                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 3U))))) 
                                     | (0x7013U == 
                                        (0x707fU & 
                                         ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
                                    | (0x33U == (0xfe00707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                   | (0x40000033U == 
                                      (0xfe00707fU 
                                       & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
                                  | (0x1033U == (0xfe00707fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1dU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 3U))))) 
                                 | (0x2033U == (0xfe00707fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                                | (0x3033U == (0xfe00707fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))))) 
                               | (0x4033U == (0xfe00707fU 
                                              & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                              | (0x5033U == (0xfe00707fU 
                                             & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x1dU) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 3U))))) 
                             | (0x40005033U == (0xfe00707fU 
                                                & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                            | (0x6033U == (0xfe00707fU 
                                           & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1dU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U))))) 
                           | (0x7033U == (0xfe00707fU 
                                          & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1dU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 3U))))) 
                          | (3U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                << 0x1dU) 
                                               | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 3U))))) 
                         | (0x1003U == (0x707fU & (
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                    << 0x1dU) 
                                                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                      >> 3U))))) 
                        | (0x2003U == (0x707fU & ((
                                                   vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1dU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 3U))))) 
                       | (0x4003U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                  << 0x1dU) 
                                                 | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                    >> 3U))))) 
                      | (0x5003U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                 << 0x1dU) 
                                                | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                   >> 3U))))) 
                     | (0x23U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1dU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 3U))))) 
                    | (0x1023U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                               << 0x1dU) 
                                              | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U))))) 
                   | (0x2023U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                              << 0x1dU) 
                                             | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 3U))))) 
                  | (0xfU == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1dU) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 3U))))) 
                 | (0x100fU == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                            << 0x1dU) 
                                           | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 3U))))) 
                | (0x1073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1dU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 3U))))) 
               | (0x2073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1dU) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 3U))))) 
              | (0x3073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 0x1dU) 
                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 3U))))) 
             | (0x5073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                        << 0x1dU) | 
                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                        >> 3U))))) 
            | (0x6073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x1dU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 3U))))) | 
           (0x7073U == (0x707fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1dU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 3U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x3fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((0x18U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U)))) 
                | (0x1bU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                       << 0x1bU) | 
                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                       >> 5U))))) | 
               (0x19U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U))))) 
              << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x7eU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | (((0xcU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U)))) 
               & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                     >> 0x1cU))) | (((((4U == (0x1fU 
                                               & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                     >> 5U)))) 
                                       | (5U == (0x1fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 5U))))) 
                                      | (0xdU == (0x1fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 5U))))) 
                                     | (0x1bU == (0x1fU 
                                                  & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                        >> 5U))))) 
                                    | (0x19U == (0x1fU 
                                                 & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                       >> 5U)))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x7dU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((0U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                  << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 5U)))) 
                | (8U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U))))) 
               | (0xbU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                     << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 5U))))) 
              << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x5fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | (((0x1cU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                 >> 5U)))) 
               | (3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                   << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 5U))))) 
              << 5U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x6fU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((2U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                  << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                               >> 5U)))) 
                & (2U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                 << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                              >> 0xfU))))) 
               & (0x40U == (0x7fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                      << 4U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x1cU))))) 
              << 4U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr 
        = ((0x3e0U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr)) 
           | (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                        << 0xeU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                    >> 0x12U))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr 
        = ((0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr)) 
           | (0x3e0U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                         << 0xeU) | (0x3fe0U & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                >> 0x12U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op 
        = ((0xcU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                               << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 5U)))) 
           & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
              >> 0x1cU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op 
        = ((0x1cU == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                << 0x1bU) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 5U)))) 
           & (0U == (7U & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                            << 0x11U) | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                         >> 0xfU)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_24 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_47 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_29) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__0__KET____DOT__reg_group__read_addr[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__register_file_gen__BRA__1__KET____DOT__reg_group__read_addr[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[0U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__register_file_block__DOT____Vcellinp__id_inuse_toggle_mem_set__read_addr[1U] 
        = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__rf_issue.phys_rs_addr
        [1U];
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__left_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_11) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__base_addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_11) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__base_addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_13) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__idx_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT___GEN_5 
        = (((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
            & ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
               & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__set_wrap))) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_done));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_34 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_35 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_31) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_36 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_37 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_32) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_38 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_valid_R_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__base_addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_39 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_33) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__out_live_in_fire_R_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_16 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_18 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_20 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_55 
        = (((((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_0) 
              | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20)) 
             & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_1) 
                | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21))) 
            & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_2) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22))) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__fire_R_3) 
              | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_82 
        = ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state)) 
             & (1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) 
            & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_25));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_82 
        = ((((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state)) 
             & (1U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) 
            & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_25));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_log_value_ready 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_42 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_20)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_43 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_21)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_1)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_44 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_22)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_2)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_45 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_34) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_23)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__out_valid_R_3)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_21) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_21 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_23) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_25 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_25) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_live_in_valid_R_2));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx7__DOT___T_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_14) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx29__DOT___T_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_14) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_293 
        = (((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state)) 
            & (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__state))) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_50));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT___T_22 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Gep_arrayidx412__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_18) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__addr_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__state)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_15) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__enable_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__state)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_15) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__enable_R_control)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_32 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__in_data_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_33 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_30) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_exit_fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_12 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_15) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_19)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_R_control)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_13 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_true_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__fire_false_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_9 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_valid_R_0));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_17) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_6 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_taskID;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_7 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_R_control;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_6 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_taskID;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_7 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_back_R_0_control;
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_valid_R)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__enable_R_taskID)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_8_bits_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_valid_R)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__enable_R_taskID)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_11_bits_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__taskID 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_valid_R)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__enable_R_taskID)
            : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_12_bits_taskID));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT___T_7)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__out_valid_R)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_39 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___T_19))
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_R_0_taskID)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_valid_R)
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT__enable_R_taskID)
                : (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5_io_Out_0_bits_taskID)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_19 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_17)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_true_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_18 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_18)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__output_false_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_80 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_21));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel_io_in_valid) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_24 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_47 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_29) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___T_7) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__in_data_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_24) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_25) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__predicate_valid_R_1));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o = 0U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = 1U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = 0U;
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
            << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                        >> 0x13U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
              << 0xdU) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                          >> 0x13U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o = 0U;
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = 1U;
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = 0U;
    }
    if (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
          >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U] 
            < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o = 0U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = 1U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = 0U;
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
            << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                         >> 0xcU)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
              << 0x14U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                           >> 0xcU)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o = 0U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[2U]);
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o 
            = (1U & vlTOPp->taiga_sim__DOT__xbar_rule_array[5U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = 1U;
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[1U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[0U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = 0U;
    }
    if (((((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
            << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                      >> 0x19U)) >= vlTOPp->taiga_sim__DOT__xbar_rule_array[4U]) 
         & (((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
              << 7U) | (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                        >> 0x19U)) < vlTOPp->taiga_sim__DOT__xbar_rule_array[3U]))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump 
        = (((0x1bU == (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
                                >> 2U))) | (0x19U == 
                                            (0x1fU 
                                             & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
                                                >> 2U)))) 
           | (0x18U == (0x1fU & (vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_instruction 
                                 >> 2U))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_138 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30
            : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__state))
                ? 0ULL : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_30));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__muxes
        [vlTOPp->taiga_sim__DOT__l2_arb__DOT__rr__DOT__state];
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__delayed_abort_complete 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk4__DOT__d_mmu__DOT__abort_tracking) 
            >> 2U) & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.data_valid));
    vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__delayed_abort_complete 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__genblk2__DOT__i_mmu__DOT__abort_tracking) 
            >> 2U) & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.data_valid));
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_0;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_1;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_2;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_4;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_5;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_6;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_7;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_8;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_9;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_10;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_11;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_12;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_13;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU] 
            = (IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_15)) 
                        << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU] 
            = (IData)(((((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_15)) 
                         << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))) 
                       >> 0x20U));
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[1U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[1U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[2U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[2U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[3U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[3U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[4U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[4U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[5U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[5U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[6U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[6U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[7U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[7U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[8U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[8U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[9U]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[9U]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xaU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xaU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xbU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xbU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xcU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xcU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xdU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xdU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xeU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xeU]);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU] 
            = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__ren_reg)
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata[0xfU]
                : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__rdata_buf[0xfU]);
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT____Vcellinp__load_store_unit_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[1U] = 0U;
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[1U] = 1U;
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array[0U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__genblk4__DOT__hit_way_conv__DOT__int_array
           [1U]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__output_attr))
            ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_wb
           [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest]
            : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__store_data_from_issue
           [vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest]);
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xffffff00U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xffff00ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xff00U & (((1U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                >> 8U))))
                           ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment
                           : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                              >> 8U)) << 8U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xff00ffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xff0000U & (((2U == (3U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                  >> 8U))))
                             ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment
                             : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                                >> 0x10U)) << 0x10U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data) 
           | (0xff000000U & (((2U == (3U & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                    >> 8U))))
                               ? (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                                  >> 8U) : ((3U == 
                                             (3U & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                            >> 8U))))
                                             ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment
                                             : (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__data_for_alignment 
                                                >> 0x18U))) 
                             << 0x18U)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request 
        = (((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
              >> 3U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict))) 
            | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__ready_for_issue_from_lsq));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_queue_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 3U) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_conflict))) 
                 & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_full) 
                       & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_output_valid)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
        = ((0xffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data) 
           | (0xffff0000U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
        = ((0xffff0000U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data) 
           | (0xffffU & ((0x20U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                          ? (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data 
                             >> 0x10U) : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__unit_muxed_load_data)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
        = ((0xffffff00U & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data) 
           | (0xffU & ((0x10U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                        ? (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data 
                           >> 8U) : vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n)) 
               | ((3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
            = (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n 
            = (((~ ((IData)(3U) << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                                          << 1U)))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_n)) 
               | ((3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]) 
                  << (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__write_pointer_q) 
                            << 1U))));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
        = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__mem_q) 
                 >> (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__read_pointer_q) 
                           << 1U))));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o 
            = (3U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U]);
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0__DOT___T_9 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__phiindvars_iv6__DOT__in_data_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const0_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1__DOT___T_9 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_15 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const1_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2__DOT___T_9 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_14 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__const2_io_Out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_w_valid 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_4 
        = (((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
            & (IData)(vlTOPp->ddr_axi_wready_muir)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_data_valid));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort 
        = ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_sc) 
           & ((~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation) 
                  >> (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id))) 
              | (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__reservation) 
                  >> (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id)) 
                 & (~ ((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv__DOT__address_match) 
                       >> (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_12 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14_io_Out_1_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc 
        = ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read_wrap_out));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__selected_csr 
        = (((((((((0x301U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                >> 4U)))) 
                  | (0xf11U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                  >> 4U))))) 
                 | (0xf12U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
                | (0xf13U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                >> 4U))))) 
               | (0xf14U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                               >> 4U))))) 
              | (0x300U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                              >> 4U))))) 
             | (0x302U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                             >> 4U))))) 
            | (0x303U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                            >> 4U)))))
            ? ((0x301U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                             >> 4U))))
                ? 0x40141100U : ((0xf11U == (0xfffU 
                                             & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U))))
                                  ? 0U : ((0xf12U == 
                                           (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))
                                           ? 0U : (
                                                   (0xf13U 
                                                    == 
                                                    (0xfffU 
                                                     & (IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                >> 4U))))
                                                    ? 0U
                                                    : 
                                                   ((0xf14U 
                                                     == 
                                                     (0xfffU 
                                                      & (IData)(
                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                 >> 4U))))
                                                     ? 0U
                                                     : 
                                                    ((0x300U 
                                                      == 
                                                      (0xfffU 
                                                       & (IData)(
                                                                 (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                  >> 4U))))
                                                      ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus
                                                      : 
                                                     ((0x302U 
                                                       == 
                                                       (0xfffU 
                                                        & (IData)(
                                                                  (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                   >> 4U))))
                                                       ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__medeleg
                                                       : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mideleg)))))))
            : (((((((((0x304U == (0xfffU & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                    >> 4U)))) 
                      | (0x305U == (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U))))) 
                     | (0x306U == (0xfffU & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                     >> 4U))))) 
                    | (0x340U == (0xfffU & (IData)(
                                                   (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                    >> 4U))))) 
                   | (0x341U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                   >> 4U))))) 
                  | (0x342U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                  >> 4U))))) 
                 | (0x343U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
                | (0x344U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                >> 4U)))))
                ? ((0x304U == (0xfffU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))
                    ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg
                    : ((0x305U == (0xfffU & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                     >> 4U))))
                        ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtvec
                        : ((0x306U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))
                            ? 0U : ((0x340U == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))
                                     ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out
                                     : ((0x341U == 
                                         (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                         ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mepc
                                         : ((0x342U 
                                             == (0xfffU 
                                                 & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                             ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcause
                                             : ((0x343U 
                                                 == 
                                                 (0xfffU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                                 ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mtval
                                                 : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip)))))))
                : ((((((((((0x3efU <= (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U)))) 
                           & (0x3a0U >= (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                          | (0xb00U == (0xfffU & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                         | (0xb02U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                        | ((0xb03U <= (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U)))) 
                           & (0xb1fU >= (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U)))))) 
                       | (0xb80U == (0xfffU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))) 
                      | (0xb82U == (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U))))) 
                     | ((0xb83U <= (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U)))) 
                        & (0xb9fU >= (0xfffU & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U)))))) 
                    | ((0x320U <= (0xfffU & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                     >> 4U)))) 
                       & (0x33fU >= (0xfffU & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))))
                    ? (((0x3efU <= (0xfffU & (IData)(
                                                     (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                      >> 4U)))) 
                        & (0x3a0U >= (0xfffU & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U)))))
                        ? 0U : ((0xb00U == (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))
                                 ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle)
                                 : ((0xb02U == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))
                                     ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret)
                                     : (((0xb03U <= 
                                          (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U)))) 
                                         & (0xb1fU 
                                            >= (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U)))))
                                         ? 0U : ((0xb80U 
                                                  == 
                                                  (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                                  ? 
                                                 (1U 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
                                                             >> 0x20U)))
                                                  : 
                                                 ((0xb82U 
                                                   == 
                                                   (0xfffU 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 4U))))
                                                   ? 
                                                  (1U 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
                                                              >> 0x20U)))
                                                   : 0U))))))
                    : (((((((((0x100U == (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U)))) 
                              | (0x102U == (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U))))) 
                             | (0x103U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))) 
                            | (0x104U == (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))) 
                           | (0x105U == (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                          | (0x106U == (0xfffU & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                         | (0x140U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                        | (0x141U == (0xfffU & (IData)(
                                                       (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                        >> 4U)))))
                        ? ((0x100U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))
                            ? (0xc0122U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus)
                            : ((0x102U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                ? 0U : ((0x103U == 
                                         (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                         ? 0U : ((0x104U 
                                                  == 
                                                  (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                                  ? 
                                                 (0x222U 
                                                  & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mie_reg)
                                                  : 
                                                 ((0x105U 
                                                   == 
                                                   (0xfffU 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 4U))))
                                                   ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__stvec
                                                   : 
                                                  ((0x106U 
                                                    == 
                                                    (0xfffU 
                                                     & (IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                >> 4U))))
                                                    ? 0U
                                                    : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out))))))
                        : (((((((((0x142U == (0xfffU 
                                              & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U)))) 
                                  | (0x143U == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                                 | (0x144U == (0xfffU 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                                | (0x180U == (0xfffU 
                                              & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                               | (1U == (0xfffU & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))) 
                              | (2U == (0xfffU & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))) 
                             | (3U == (0xfffU & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                         >> 4U))))) 
                            | (0xc00U == (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U)))))
                            ? ((0x142U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out
                                : ((0x143U == (0xfffU 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))
                                    ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__scratch_out
                                    : ((0x144U == (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                        ? (0x222U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mip)
                                        : ((0x180U 
                                            == (0xfffU 
                                                & (IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                           >> 4U))))
                                            ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp
                                            : ((1U 
                                                == 
                                                (0xfffU 
                                                 & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U))))
                                                ? 0U
                                                : (
                                                   (2U 
                                                    == 
                                                    (0xfffU 
                                                     & (IData)(
                                                               (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                >> 4U))))
                                                    ? 0U
                                                    : 
                                                   ((3U 
                                                     == 
                                                     (0xfffU 
                                                      & (IData)(
                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                 >> 4U))))
                                                     ? 0U
                                                     : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle))))))))
                            : ((0xc01U == (0xfffU & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle)
                                : ((0xc02U == (0xfffU 
                                               & (IData)(
                                                         (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                          >> 4U))))
                                    ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret)
                                    : (((0xc03U <= 
                                         (0xfffU & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                            >> 4U)))) 
                                        & (0xc1fU >= 
                                           (0xfffU 
                                            & (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                       >> 4U)))))
                                        ? 0U : ((0xc80U 
                                                 == 
                                                 (0xfffU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U))))
                                                 ? 
                                                (1U 
                                                 & (IData)(
                                                           (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
                                                            >> 0x20U)))
                                                 : 
                                                ((0xc81U 
                                                  == 
                                                  (0xfffU 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                              >> 4U))))
                                                  ? 
                                                 (1U 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mcycle 
                                                             >> 0x20U)))
                                                  : 
                                                 ((0xc82U 
                                                   == 
                                                   (0xfffU 
                                                    & (IData)(
                                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                               >> 4U))))
                                                   ? 
                                                  (1U 
                                                   & (IData)(
                                                             (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__minst_ret 
                                                              >> 0x20U)))
                                                   : 0U)))))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[1U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[2U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[3U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[4U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[5U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[6U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[7U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[(7U 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                                                >> 4U)) 
                                                                                >> 5U))] 
        = (((~ ((IData)(1U) << (0x1fU & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                 >> 4U))))) 
            & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[
            (7U & ((IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                            >> 4U)) >> 5U))]) | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write) 
                                                 << 
                                                 (0x1fU 
                                                  & (IData)(
                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_inputs 
                                                             >> 4U)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224 
        = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0x3fffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | (0xffffc000U & ((0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 0xdU)) 
                             | (0x4000U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                           << 0xeU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
        = ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                   << 0xdU)) | (0x1fffU 
                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x13U)))) 
           | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                         << 0xdU)) 
                             | (0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                               << 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                      << 0xdU)) | (0x1fffU 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                      >> 0x13U)))) 
              | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                            << 0xdU)) 
                                | (0x1ff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                                 << 0xdU))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (0xffffff80U & ((0xffffff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 6U)) 
                             | (0x80U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                         << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
        = ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                               << 6U)) | (0x3fU & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x1aU)))) 
           | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                       << 6U)) | (0xffffff00U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                     << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0xfffc0000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                  << 6U)) | (0x3fU 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                >> 0x1aU)))) 
              | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                          << 6U)) | 
                                (0x3ff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 6U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
        = ((0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                            << 0x1fU) | (0x7ffffffeU 
                                         & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                            >> 1U)))) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                    >> 2U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                  >> 1U)) | (0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 0x1fU) 
                                            | (0x7ffffffeU 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                  >> 1U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                  >> 1U)) | (0x7feU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                       >> 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0x3fffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | (0xffffc000U & ((0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 0xdU)) 
                             | (0x4000U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                           << 0xeU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
        = ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                   << 0xdU)) | (0x1fffU 
                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x13U)))) 
           | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                         << 0xdU)) 
                             | (0xffff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                               << 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U]) 
           | ((0x3fffU & ((0x2000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                      << 0xdU)) | (0x1fffU 
                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                      >> 0x13U)))) 
              | (0xffffc000U & ((0x4000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                            << 0xdU)) 
                                | (0x1ff8000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                                 << 0xdU))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (0xffffff80U & ((0xffffff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                             << 6U)) 
                             | (0x80U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                                         << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
        = ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                               << 6U)) | (0x3fU & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                                   >> 0x1aU)))) 
           | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                       << 6U)) | (0xffffff00U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                     << 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
        = ((0xfffc0000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU]) 
           | ((0x7fU & ((0x40U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                  << 6U)) | (0x3fU 
                                             & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                >> 0x1aU)))) 
              | (0xffffff80U & ((0x80U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                          << 6U)) | 
                                (0x3ff00U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 6U))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
        = ((0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                            << 0x1fU) | (0x7ffffffeU 
                                         & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[0U] 
                                            >> 1U)))) 
           | (1U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__mst_aw_valids) 
                    >> 2U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                  >> 1U)) | (0xfffffffeU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                             << 0x1fU) 
                                            | (0x7ffffffeU 
                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[1U] 
                                                  >> 1U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
        = ((1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                  >> 1U)) | (0x7feU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_aw_spill_reg__data_o[2U] 
                                       >> 1U)));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2 
        = VL_LTS_IQQ(1,33,33, (((QData)((IData)((1U 
                                                 & ((~ 
                                                     (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                      >> 4U)) 
                                                    & (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                       >> 4U))))) 
                                << 0x20U) | (QData)((IData)(
                                                            ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                              << 0x1bU) 
                                                             | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                                >> 5U))))), 
                     (((QData)((IData)((1U & ((~ (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                  >> 4U)) 
                                              & (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                 >> 4U))))) 
                       << 0x20U) | (QData)((IData)(
                                                   ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                       >> 5U))))));
    if (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write) {
        vlTOPp->ddr_axi_wstrb_taiga = 0xfU;
        vlTOPp->ddr_axi_wdata_taiga = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result_r;
    } else {
        vlTOPp->ddr_axi_wstrb_taiga = (0xfU & (IData)(
                                                      (vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                                       >> 0xaU)));
        vlTOPp->ddr_axi_wdata_taiga = vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.data_out;
    }
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__pop = (
                                                   (((IData)(vlTOPp->taiga_sim__DOT__axi_arvalid) 
                                                     & (IData)(vlTOPp->ddr_axi_arready_taiga)) 
                                                    & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write))) 
                                                   | ((IData)(vlTOPp->taiga_sim__DOT__axi_awvalid) 
                                                      & (IData)(vlTOPp->ddr_axi_awready_taiga)));
    vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count 
        = ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__read_modify_write)
            ? 0U : (0x1fU & (IData)((vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.data_out 
                                     >> 3U))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus;
    if ((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__next_privilege_level))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xffffffdfU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | (((1U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                    ? (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                             >> 1U)) : 0U) << 5U));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = (0xfffffffdU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xfffffeffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | (0x100U & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level) 
                            << 8U)));
    } else {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xffffff7fU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | (0x80U & (((3U == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                             ? (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                      >> 3U)) : ((1U 
                                                  == (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level))
                                                  ? 
                                                 (1U 
                                                  & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                                                     >> 1U))
                                                  : 0U)) 
                           << 7U)));
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = (0xfffffff7U & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception);
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception 
            = ((0xffffe7ffU & vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception) 
               | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__privilege_level) 
                  << 0xbU));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_352 
        = ((0xdU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xdU]
            : ((0xcU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xcU]
                : ((0xbU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xbU]
                    : ((0xaU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xaU]
                        : ((9U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[9U]
                            : ((8U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[8U]
                                : ((7U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[7U]
                                    : ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[6U]
                                        : ((5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[5U]
                                            : ((4U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[4U]
                                                : (
                                                   (3U 
                                                    == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                    ? 
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[3U]
                                                    : 
                                                   ((2U 
                                                     == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                     ? 
                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[2U]
                                                     : 
                                                    ((1U 
                                                      == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                      ? 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[1U]
                                                      : 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0U])))))))))))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group 
        = ((2U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group)) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group
           [(0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr))]);
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group 
        = ((1U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_wb_group)) 
           | (vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__spec_wb_group
              [(0x1fU & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr) 
                         >> 5U))] << 1U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table
        [(0x1fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr))];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr 
        = ((0xfc0U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1));
    vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT__speculative_rd_to_phys_table
        [(0x1fU & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.rs_addr) 
                   >> 5U))];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr 
        = ((0x3fU & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.phys_rs_addr)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__renamer_block__DOT____Vlvbound1) 
              << 6U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed 
        = ((0x73U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__unit_needed)) 
           | ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op) 
                & (~ (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                      >> 0x11U))) << 3U) | (0x1fffcU 
                                            & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__mult_div_op) 
                                                << 2U) 
                                               & (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                                  >> 0xfU)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__uses_rd 
        = (1U & (~ ((((0x18U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                           << 0x1bU) 
                                          | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                             >> 5U)))) 
                      | (8U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                          << 0x1bU) 
                                         | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                            >> 5U))))) 
                     | (3U == (0x1fU & ((vlTOPp->taiga_sim__DOT__cpu__DOT__decode[1U] 
                                         << 0x1bU) 
                                        | (vlTOPp->taiga_sim__DOT__cpu__DOT__decode[0U] 
                                           >> 5U))))) 
                    | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__decode_and_issue_block__DOT__environment_op))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb__DOT___T 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8_io_MemReq_valid) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10_io_MemReq_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_76 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__loop_finish_R_0_control) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_32));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_10 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_5) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_5 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_15) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__enable_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_81 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_80) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__enable_R_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_0 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_2 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_3) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__inputReg_enable_control));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_28 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_0_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_30 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_1_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_32 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__outputPtrsValidReg_2_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___GEN_34 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ArgSplitter__DOT__enableValidReg));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_10 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__state)
            ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0)
            : ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_5) 
               | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_26) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_27));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[0U] 
        = ((0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                            << 0x13U) | (0x7fffcU & 
                                         (vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U] 
                                          >> 0xdU)))) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_aw_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_aw_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[1U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                  >> 0xdU)) | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                               << 0x13U) 
                                              | (0x7fffcU 
                                                 & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U] 
                                                    >> 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[2U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                  >> 0xdU)) | (0xffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                                          << 0x13U) 
                                         | (0x7fffcU 
                                            & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                               >> 0xdU)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[0U] 
        = ((0xfffffffcU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[0U]) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__dec_ar_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_ar_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[1U] 
        = ((3U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U]) 
           | (0xfffffffcU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[1U]));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[2U] 
        = ((3U & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]) 
           | (0x3cU & vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U]));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[0U] 
        = ((0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                            << 0x1aU) | (0x3fffffcU 
                                         & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U] 
                                            >> 6U)))) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_aw_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_aw_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[1U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                  >> 6U)) | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
                                             << 0x1aU) 
                                            | (0x3fffffcU 
                                               & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xaU] 
                                                  >> 6U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_aw_inp[2U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
                  >> 6U)) | (0xffcU & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[0xbU] 
                                       >> 6U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[0U] 
        = ((0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                            << 7U) | (0x7cU & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[5U] 
                                               >> 0x19U)))) 
           | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__dec_ar_error)
               ? 2U : (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_ar_decode__idx_o)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[1U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                  >> 0x19U)) | (0xfffffffcU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                                                << 7U) 
                                               | (0x7cU 
                                                  & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[6U] 
                                                     >> 0x19U)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__slv_ar_inp[2U] 
        = ((3U & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                  >> 0x19U)) | (0x3cU & ((vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U] 
                                          << 7U) | 
                                         (0x7cU & (
                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[7U] 
                                                   >> 0x19U)))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__early_branch_flush 
        = ((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__valid_fetch_result) 
             & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.data_valid)) 
            & ((IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.data_out) 
               >> 4U)) & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__fetch_block__DOT__is_branch_or_jump)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__arb_request 
        = vlTOPp->taiga_sim__DOT__l2_arb__DOT__requests
        [vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i];
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v = 0U;
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v 
        = ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v) 
           | ((IData)(1U) << (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_368 
        = ((0xdU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU]
            : ((0xcU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU]
                : ((0xbU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU]
                    : ((0xaU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU]
                        : ((9U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U]
                            : ((8U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U]
                                : ((7U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                    ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U]
                                    : ((6U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                        ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U]
                                        : ((5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                            ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U]
                                            : ((4U 
                                                == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U]
                                                : (
                                                   (3U 
                                                    == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                    ? 
                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U]
                                                    : 
                                                   ((2U 
                                                     == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                     ? 
                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U]
                                                     : 
                                                    ((1U 
                                                      == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                                      ? 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U]
                                                      : 
                                                     vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U])))))))))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_bits_data 
        = ((7U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                 >> 3U)))) ? (((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU])) 
                                               << 0x20U) 
                                              | (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU])))
            : ((6U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                     >> 3U)))) ? (((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xdU])) 
                                                   << 0x20U) 
                                                  | (QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xcU])))
                : ((5U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                         >> 3U)))) ? 
                   (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xbU])) 
                     << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xaU])))
                    : ((4U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                             >> 3U))))
                        ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[9U])) 
                            << 0x20U) | (QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[8U])))
                        : ((3U == (7U & (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                 >> 3U))))
                            ? (((QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[7U])) 
                                << 0x20U) | (QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[6U])))
                            : ((2U == (7U & (IData)(
                                                    (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                     >> 3U))))
                                ? (((QData)((IData)(
                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[5U])) 
                                    << 0x20U) | (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[4U])))
                                : ((1U == (7U & (IData)(
                                                        (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                                         >> 3U))))
                                    ? (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[3U])) 
                                        << 0x20U) | (QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[2U])))
                                    : (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[1U])) 
                                        << 0x20U) | (QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0U]))))))))));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids[0U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT____Vcellinp__sq_block__retire_ids[1U] 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellinp__lsq_block__retire_ids
        [1U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__branch_metadata_if 
        = ((0x18U & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                     [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                     << 3U)) | (((IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))) 
                                 << 2U) | ((0U != (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches))
                                            ? (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__tag_matches)
                                            : (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__replacement_way))));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_branch 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                 [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                 >> 4U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__bp.is_return 
        = (1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__if_entry
                 [vlTOPp->taiga_sim__DOT__cpu__DOT__bp_block__DOT__hit_way] 
                 >> 3U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__dtlb.new_request 
        = ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__satp 
            >> 0x1fU) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_ack 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_ack 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)));
    __Vtemp193[0U] = (IData)((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)) 
                               << 0x2bU) | (((QData)((IData)(
                                                             (1U 
                                                              & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected))))) 
                                             << 0x2aU) 
                                            | (((QData)((IData)(
                                                                ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                  ? 0U
                                                                  : 
                                                                 (0xfU 
                                                                  & (IData)(
                                                                            (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                             >> 4U)))))) 
                                                << 0x26U) 
                                               | (((QData)((IData)(
                                                                   (7U 
                                                                    & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                        ? (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 7U))
                                                                        : (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                                >> 1U)))))) 
                                                   << 0x23U) 
                                                  | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data)) 
                                                      << 3U) 
                                                     | (QData)((IData)(
                                                                       (7U 
                                                                        & (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 4U)))))))))));
    __Vtemp193[1U] = ((0xfffff000U & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                        ? (IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                   >> 0xaU))
                                        : (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                   >> 8U))) 
                                      << 0xcU)) | (IData)(
                                                          ((((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)) 
                                                             << 0x2bU) 
                                                            | (((QData)((IData)(
                                                                                (1U 
                                                                                & (~ (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected))))) 
                                                                << 0x2aU) 
                                                               | (((QData)((IData)(
                                                                                ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                                 ? 0U
                                                                                 : 
                                                                                (0xfU 
                                                                                & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                                >> 4U)))))) 
                                                                   << 0x26U) 
                                                                  | (((QData)((IData)(
                                                                                (7U 
                                                                                & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                                                                                 ? (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 7U))
                                                                                 : (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                                                >> 1U)))))) 
                                                                      << 0x23U) 
                                                                     | (((QData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_data)) 
                                                                         << 3U) 
                                                                        | (QData)((IData)(
                                                                                (7U 
                                                                                & (IData)(
                                                                                (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                                                                >> 4U)))))))))) 
                                                           >> 0x20U)));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
        = __Vtemp193[0U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
        = __Vtemp193[1U];
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
        = (0xfffU & (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__load_selected)
                       ? (IData)((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.data_out 
                                  >> 0xaU)) : (IData)(
                                                      (vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_entry 
                                                       >> 8U))) 
                     >> 0x14U));
    vlTOPp->taiga_sim__DOT__cpu__DOT__writeback_block__DOT__unit_rd[1U][0U] 
        = ((0x100U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
            ? ((0x80U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data
                : ((0x40U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                    ? (0xffffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)
                    : (0xffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)))
            : ((0x80U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                ? vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data
                : ((0x40U & (IData)(vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.data_out))
                    ? VL_EXTENDS_II(32,16, (0xffffU 
                                            & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data))
                    : VL_EXTENDS_II(32,8, (0xffU & vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__aligned_load_data)))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | (0xffffff80U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                           << 0x39U) 
                                          | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                              << 0x19U) 
                                             | (0x1fffffffffffffeULL 
                                                & ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                   >> 7U))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                             >> 7U)) 
                                                         & (0U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0xffffc000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | ((0x7fU & ((IData)(((0x7ffffffffeULL & 
                                  (((QData)((IData)(
                                                    vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                    << 0x39U) | (((QData)((IData)(
                                                                  vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                                  << 0x19U) 
                                                 | (0x1fffffffffffffeULL 
                                                    & ((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                       >> 7U))))) 
                                 | (QData)((IData)(
                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                      & (1U 
                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                     & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                        >> 7U)) 
                                                    & (0U 
                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                        >> 0x19U)) | (0xffffff80U & 
                                      ((IData)((((0x7ffffffffeULL 
                                                  & (((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                                      << 0x39U) 
                                                     | (((QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                                         << 0x19U) 
                                                        | (0x1fffffffffffffeULL 
                                                           & ((QData)((IData)(
                                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                              >> 7U))))) 
                                                 | (QData)((IData)(
                                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                      & (1U 
                                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                     & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                                        >> 7U)) 
                                                                    & (0U 
                                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                                >> 0x20U)) 
                                       << 7U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
        = (IData)(((0x7ffffffffeULL & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                        << 0x39U) | 
                                       (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                         << 0x19U) 
                                        | (0x1fffffffffffffeULL 
                                           & ((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                              >> 7U))))) 
                   | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                        & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                       & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                          >> 7U)) & 
                                      (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0xffffff80U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (IData)((((0x7ffffffffeULL & (((QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                            << 0x39U) 
                                           | (((QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                               << 0x19U) 
                                              | (0x1fffffffffffffeULL 
                                                 & ((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                    >> 7U))))) 
                       | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                            & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                           & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                              >> 7U)) 
                                          & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                      >> 0x20U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | (0xfe000000U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                           << 0x39U) 
                                          | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                              << 0x19U) 
                                             | (0x1fffffffffffffeULL 
                                                & ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                   >> 7U))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                             >> 7U)) 
                                                         & (2U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
        = ((0x1ffffffU & ((IData)(((0x7ffffffffeULL 
                                    & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                        << 0x39U) | 
                                       (((QData)((IData)(
                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                         << 0x19U) 
                                        | (0x1fffffffffffffeULL 
                                           & ((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                              >> 7U))))) 
                                   | (QData)((IData)(
                                                     ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                        & (1U 
                                                           != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                       & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                          >> 7U)) 
                                                      & (2U 
                                                         == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                          >> 7U)) | (0xfe000000U & 
                                     ((IData)((((0x7ffffffffeULL 
                                                 & (((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__slv_ports_req_array[4U])) 
                                                     << 0x39U) 
                                                    | (((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[3U])) 
                                                        << 0x19U) 
                                                       | (0x1fffffffffffffeULL 
                                                          & ((QData)((IData)(
                                                                             vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U])) 
                                                             >> 7U))))) 
                                                | (QData)((IData)(
                                                                  ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                     & (1U 
                                                                        != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                    & (vlTOPp->taiga_sim__DOT__slv_ports_req_array[2U] 
                                                                       >> 7U)) 
                                                                   & (2U 
                                                                      == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                               >> 0x20U)) 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
        = ((0x7fU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U]) 
           | (0xffffff80U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                           << 0x20U) 
                                          | (0xfffffffffffffffeULL 
                                             & (QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                         & (0U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
        = ((0xffffc000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U]) 
           | ((0x7fU & ((IData)(((0x7ffffffffeULL & 
                                  (((QData)((IData)(
                                                    vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                    << 0x20U) | (0xfffffffffffffffeULL 
                                                 & (QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                 | (QData)((IData)(
                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                      & (1U 
                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                     & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                    & (0U 
                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                        >> 0x19U)) | (0xffffff80U & 
                                      ((IData)((((0x7ffffffffeULL 
                                                  & (((QData)((IData)(
                                                                      vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                                      << 0x20U) 
                                                     | (0xfffffffffffffffeULL 
                                                        & (QData)((IData)(
                                                                          vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                                 | (QData)((IData)(
                                                                   ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                      & (1U 
                                                                         != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                     & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                                    & (0U 
                                                                       == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                                >> 0x20U)) 
                                       << 7U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
        = (IData)(((0x7ffffffffeULL & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                        << 0x20U) | 
                                       (0xfffffffffffffffeULL 
                                        & (QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                   | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                        & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                       & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                      & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
        = ((0xffffff80U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U]) 
           | (IData)((((0x7ffffffffeULL & (((QData)((IData)(
                                                            vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                            << 0x20U) 
                                           | (0xfffffffffffffffeULL 
                                              & (QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                       | (QData)((IData)(((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                            & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                           & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                          & (1U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                      >> 0x20U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU]) 
           | (0xfe000000U & ((IData)(((0x7ffffffffeULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                           << 0x20U) 
                                          | (0xfffffffffffffffeULL 
                                             & (QData)((IData)(
                                                               vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                      | (QData)((IData)(
                                                        ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                           & (1U 
                                                              != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                          & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                         & (2U 
                                                            == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
        = ((0x1ffffffU & ((IData)(((0x7ffffffffeULL 
                                    & (((QData)((IData)(
                                                        vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                        << 0x20U) | 
                                       (0xfffffffffffffffeULL 
                                        & (QData)((IData)(
                                                          vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                   | (QData)((IData)(
                                                     ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                        & (1U 
                                                           != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                       & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                      & (2U 
                                                         == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o))))))) 
                          >> 7U)) | (0xfe000000U & 
                                     ((IData)((((0x7ffffffffeULL 
                                                 & (((QData)((IData)(
                                                                     vlTOPp->taiga_sim__DOT__slv_ports_req_array[9U])) 
                                                     << 0x20U) 
                                                    | (0xfffffffffffffffeULL 
                                                       & (QData)((IData)(
                                                                         vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]))))) 
                                                | (QData)((IData)(
                                                                  ((((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__w_fifo_empty)) 
                                                                     & (1U 
                                                                        != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT__gen_demux__DOT__i_b_fifo__DOT__status_cnt_q))) 
                                                                    & vlTOPp->taiga_sim__DOT__slv_ports_req_array[8U]) 
                                                                   & (2U 
                                                                      == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__DOT____Vcellout__gen_demux__DOT__i_w_fifo__data_o)))))) 
                                               >> 0x20U)) 
                                      << 0x19U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_15) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__right_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_15 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_14) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__right_valid_R));
    vlTOPp->ddr_axi_wvalid_muir = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem_io_mem_w_valid;
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_wrap_out 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_4) 
           & (0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__new_attr = 
        (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_id) 
          << 6U) | ((0x3eU & ((IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                                       >> 2U)) << 1U)) 
                    | (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort)));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.push 
        = (((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_valid) 
            & (~ (IData)((vlTOPp->taiga_sim__DOT__l2_arb__DOT__reserv_request 
                          >> 8U)))) & (~ (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT____Vcellout__reserv__abort)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___GEN_29 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT___T_27) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__Loop_0__DOT__in_carry_in_valid_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___GEN_11 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_12) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__left_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_47 
        = (((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_0) 
            | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_1)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT__out_ready_R_1) 
              | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_indvars_iv_next14__DOT___T_2)));
    VL_EXTEND_WI(71,8, __Vtemp196, (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask));
    VL_SHIFTL_WWI(71,71,6, __Vtemp197, __Vtemp196, 
                  (0x38U & ((IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__addr_reg 
                                     >> 3U)) << 3U)));
    __Vtemp198[0U] = __Vtemp197[0U];
    __Vtemp198[1U] = __Vtemp197[1U];
    __Vtemp198[2U] = (0x7fU & __Vtemp197[2U]);
    VL_EXTEND_WW(72,71, __Vtemp199, __Vtemp198);
    if (vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] = 0xffffffffU;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] = 0xffffffffU;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[2U] = 0xffU;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_0;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_1;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_2;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_3;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_4;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_5;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_6;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_7;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_8;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_9;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_10;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_11;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_12;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_13;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
            = (IData)((((QData)((IData)(vlTOPp->ddr_axi_rdata_muir)) 
                        << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
            = (IData)(((((QData)((IData)(vlTOPp->ddr_axi_rdata_muir)) 
                         << 0x20U) | (QData)((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__refill_buf_14))) 
                       >> 0x20U));
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[0U] 
            = __Vtemp199[0U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[1U] 
            = __Vtemp199[1U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wmask[2U] 
            = __Vtemp199[2U];
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[1U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[2U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[3U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[4U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[5U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[6U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[7U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[8U] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[9U] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xaU] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xbU] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xcU] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xdU] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xeU] 
            = (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data);
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wdata[0xfU] 
            = (IData)((vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_data 
                       >> 0x20U));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100 
        = ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404 
        = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))
               ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit)) 
                  & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty))
               : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                  & ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99)) 
                     & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405 
        = ((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           & ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))
               ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__hit)) 
                  & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty)))
               : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))
                   ? ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_99)) 
                      & (~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_dirty)))
                   : ((3U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                      & ((4U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
                         & (5U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)))))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_req_ready 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid 
        = (((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
            | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_224)) 
           | ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc_reg) 
              & (~ (IData)((0U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__cpu_mask))))));
    if (((((((((1U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U])) 
               | (0U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
              | (4U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
             | (0xcU == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
            | (8U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
           | (0x10U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
          | (0x14U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) 
         | (0x18U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U])))) {
        vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result 
            = ((1U == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                ? ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                    << 0x1bU) | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                 >> 5U)) : ((0U == 
                                             (0x1fU 
                                              & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                             ? (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                  << 0x1bU) 
                                                 | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                    >> 5U)) 
                                                + (
                                                   (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                      >> 5U)))
                                             : ((4U 
                                                 == 
                                                 (0x1fU 
                                                  & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                 ? 
                                                (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                     >> 5U)) 
                                                 ^ 
                                                 ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                   << 0x1bU) 
                                                  | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                     >> 5U)))
                                                 : 
                                                ((0xcU 
                                                  == 
                                                  (0x1fU 
                                                   & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                  ? 
                                                 (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                    << 0x1bU) 
                                                   | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                      >> 5U)) 
                                                  & ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                        >> 5U)))
                                                  : 
                                                 ((8U 
                                                   == 
                                                   (0x1fU 
                                                    & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                   ? 
                                                  (((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                     << 0x1bU) 
                                                    | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       >> 5U)) 
                                                   | ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                         >> 5U)))
                                                   : 
                                                  ((0x10U 
                                                    == 
                                                    (0x1fU 
                                                     & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                    ? 
                                                   ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                                                     ? 
                                                    ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                        >> 5U))
                                                     : 
                                                    ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                      << 0x1bU) 
                                                     | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                        >> 5U)))
                                                    : 
                                                   ((0x14U 
                                                     == 
                                                     (0x1fU 
                                                      & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))
                                                     ? 
                                                    ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                                                      ? 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                         >> 5U))
                                                      : 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                         >> 5U)))
                                                     : 
                                                    ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                                                      ? 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                         >> 5U))
                                                      : 
                                                     ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                       << 0x1bU) 
                                                      | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                                         >> 5U))))))))));
    } else {
        if ((0x1cU == (0x1fU & vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U]))) {
            vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_result 
                = ((IData)(vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_unit__DOT__rs1_smaller_than_rs2)
                    ? ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                        << 0x1bU) | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[0U] 
                                     >> 5U)) : ((vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[2U] 
                                                 << 0x1bU) 
                                                | (vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__amo_alu_inputs[1U] 
                                                   >> 5U)));
        }
    }
    vlTOPp->ddr_axi_awlen_taiga = vlTOPp->taiga_sim__DOT__l2_to_mem__DOT__write_reference_burst_count;
    if (VL_LIKELY(VL_ONEHOT0_I(((((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                                  | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret)) 
                                 << 2U) | ((2U & ((IData)(
                                                          (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                                                           >> 0x28U)) 
                                                  << 1U)) 
                                           | (1U & 
                                              (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U] 
                                               | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U]))))))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_new 
            = ((1U & (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mwrite_decoder[0U] 
                      | vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__swrite_decoder[0U]))
                ? ((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus 
                    & (~ ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write)
                           ? 0xe19aaU : 0xc0122U))) 
                   | (vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__updated_csr 
                      & ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__machine_write)
                          ? 0xe19aaU : 0xc0122U))) : 
               ((1U & (IData)((vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__gc_exception_r 
                               >> 0x28U))) ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_exception
                 : (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__mret) 
                     | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__sret))
                     ? vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__genblk1__DOT__mstatus_return
                     : vlTOPp->taiga_sim__DOT__cpu__DOT__gc_unit_block__DOT__csr_registers__DOT__mstatus)));
    } else {
        if (VL_UNLIKELY(Verilated::assertOn())) {
            VL_WRITEF("[%0t] %%Error: csr_regs.sv:278: Assertion failed in %Ntaiga_sim.cpu.gc_unit_block.csr_registers.genblk1: 'unique if' statement violated\n",
                      64,VL_TIME_UNITED_Q(1),vlSymsp->name());
            VL_STOP_MT("/localssd/taiga-project/taiga/core/csr_regs.sv", 278, "");
        }
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT___T_39 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__binaryOp_add11__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_17 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_19) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__data_valid_R));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_io_out_valid 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb__DOT___T) 
           | ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__state)) 
              & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__enable_valid_R) 
                 & ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_40) 
                    & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_41)))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ret_4__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_39 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT__out_ready_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__icmp_exitcond15__DOT___T_1));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___GEN_4 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT___T_10) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_16__DOT__cmp_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT__enable_valid_R)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1_io_Out_0_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_57 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__state));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_42 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_2)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_43 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_3)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_1)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_44 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_4)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_2)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_45 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_5)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_3)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_46 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_6)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_4)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_47 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_6)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_5)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_48 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_8)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_6)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_49 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_6)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_7)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_50 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_10)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_8)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_51 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_11)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_9)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_52 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_12)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_10)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_53 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_13)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_11)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_54 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_14)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_12)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_55 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_15)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__out_valid_R_13)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___GEN_56 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__start) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT___T_16)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_body5__DOT__mask_valid_R_0)));
    vlTOPp->taiga_sim__DOT__l2_arb__DOT__mem_request 
        = ((0xffffffffff8ULL & (vlTOPp->taiga_sim__DOT__l2_arb__DOT__arb_request 
                                << 1U)) | (QData)((IData)(
                                                          (((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_i) 
                                                            << 2U) 
                                                           | (3U 
                                                              & (IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__arb_request))))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__0__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 4U) & (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v)) 
                 & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full))));
    vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__.pop 
        = (1U & ((((IData)(vlTOPp->taiga_sim__DOT__l2_arb__DOT__genblk1__BRA__1__KET____DOT__input_fifo__DOT__genblk3__DOT__inflight_count) 
                   >> 4U) & ((IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__arb.grantee_v) 
                             >> 1U)) & (~ (IData)(vlSymsp->TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.full))));
    vlTOPp->ddr_axi_wdata_muir = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_mode)
                                   ? ((0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                       ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xfU]
                                       : ((0xeU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                           ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__dirty_cache_block[0xeU]
                                           : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_352))
                                   : ((0xfU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                       ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xfU]
                                       : ((0xeU == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__write_count))
                                           ? vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__read[0xeU]
                                           : vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_368)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot) 
           | ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__issued_one_hot 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__sq_block__DOT__sq_oldest_one_hot) 
           & (- (IData)((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__store_ack))));
    vlTOPp->data_bram_addr = (0x3fffffffU & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                                              << 0x12U) 
                                             | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                                >> 0xeU)));
    vlTOPp->data_bram_be = (0xfU & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                                     << 0x1aU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                                  >> 6U)));
    vlTOPp->data_bram_data_in = ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                  << 0x1dU) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
                                               >> 3U));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.push 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request) 
           & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
              >> 0xbU));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in)) 
           | ((0x1c0U & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                         << 3U)) | ((0x30U & ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
                                               << 0x18U) 
                                              | (0xfffff0U 
                                                 & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                                                    >> 8U)))) 
                                    | (0xeU & (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[0U] 
                                               << 1U)))));
    vlTOPp->__Vfunc_address_range_check__3__addr = 
        ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
          << 0x14U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                       >> 0xcU));
    vlTOPp->__Vfunc_address_range_check__3__Vfuncout 
        = (8U == (0xfU & (vlTOPp->__Vfunc_address_range_check__3__addr 
                          >> 0x1cU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match 
        = ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match)) 
           | (IData)(vlTOPp->__Vfunc_address_range_check__3__Vfuncout));
    vlTOPp->__Vfunc_address_range_check__4__addr = 
        ((vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[2U] 
          << 0x14U) | (vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.transaction_out[1U] 
                       >> 0xcU));
    vlTOPp->__Vfunc_address_range_check__4__Vfuncout 
        = (6U == (0xfU & (vlTOPp->__Vfunc_address_range_check__4__addr 
                          >> 0x1cU)));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match 
        = ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match)) 
           | ((IData)(vlTOPp->__Vfunc_address_range_check__4__Vfuncout) 
              << 1U));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[1U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[2U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[3U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[4U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[5U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
        = ((0xfffff800U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U]) 
           | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U]);
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
        = ((0x7ffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U]) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
                             << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[1U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[2U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[3U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[4U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[5U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[6U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[7U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[8U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[9U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xaU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xbU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xcU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xdU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xeU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0xfU] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
        = ((0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x10U] 
                      >> 0x15U)) | (0xfffff800U & (
                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_lite_demux__mst_reqs_o[0x11U] 
                                                   << 0xbU)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_100) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__is_alloc));
    if ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))) {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404;
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid 
            = vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405;
    } else {
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid 
            = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404)
                : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404)
                    : ((3U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
                       | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_404))));
        vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid 
            = ((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405)
                : ((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state))
                    ? (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405)
                    : ((3U != (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__flush_state)) 
                       & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___GEN_405))));
    }
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid 
        = ((((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid)) 
           & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid 
        = ((((1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid)) 
           & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_wr_mem_0_MemResp_valid 
        = ((((2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_chosen)) 
             & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T_4)) 
            & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_cpu_resp_valid)) 
           & (2U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT___T 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__mstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache__DOT__in_arb_io_out_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___GEN_6 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__out_fire_mask_0 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT__output_fire_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_for_cond_cleanup3__DOT___T_8));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT___GEN_6 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_fire_R_0));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__out_fire_mask_0 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__bb_entry1__DOT__output_fire_R_0) 
           | (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__br_2__DOT___T_6));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.new_request 
        = (((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match) 
            >> 1U) & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request));
    vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.new_request 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match) 
           & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__issue_request));
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[0U] = 0U;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[1U] = 0U;
    if ((2U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[1U] = 1U;
    }
    if ((1U & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_address_match))) {
        vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array[0U] = 0U;
    }
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out 
        = vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array
        [0U];
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out 
        = ((IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out) 
           | vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__sub_unit_select__DOT__int_array
           [1U]);
    __Vtemp219[3U] = ((0xffU & ((0xffU & ((IData)((QData)((IData)(
                                                                  ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                    << 0x1bU) 
                                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                      >> 5U))))) 
                                          >> 0x13U)) 
                                | ((IData)(((QData)((IData)(
                                                            (0x8000000U 
                                                             | ((0x70000U 
                                                                 & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                | (1U 
                                                                   & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]))))) 
                                            >> 0x20U)) 
                                   >> 0x18U))) | (0xffffff00U 
                                                  & ((0x1f00U 
                                                      & ((IData)((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                                << 0x1bU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                                >> 5U))))) 
                                                         >> 0x13U)) 
                                                     | (0xffffe000U 
                                                        & ((IData)(
                                                                   ((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                                << 0x1bU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                                >> 5U)))) 
                                                                    >> 0x20U)) 
                                                           << 0xdU)))));
    __Vtemp220[1U] = ((1U & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                         << 0xeU) 
                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                           >> 0x12U)))) 
                                       << 0x1fU) | (QData)((IData)(
                                                                   (0x200000U 
                                                                    | ((0x1c00U 
                                                                        & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                            << 0xdU) 
                                                                           | (0x1c00U 
                                                                              & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U)))) 
                                                                       | (1U 
                                                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                             >> 0x13U)))))))) 
                             >> 0x1fU)) | (0xfffffffeU 
                                           & ((IData)(
                                                      ((((QData)((IData)(
                                                                         ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                           << 0xeU) 
                                                                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                             >> 0x12U)))) 
                                                         << 0x1fU) 
                                                        | (QData)((IData)(
                                                                          (0x200000U 
                                                                           | ((0x1c00U 
                                                                               & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                                << 0xdU) 
                                                                                | (0x1c00U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U)))) 
                                                                              | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U))))))) 
                                                       >> 0x20U)) 
                                              << 1U)));
    __Vtemp221[2U] = ((0xfffffffeU & ((IData)(((0xfffffffff0ULL 
                                                & (((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                                    << 0x28U) 
                                                   | (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                                       << 8U) 
                                                      | (0xfffffffff0ULL 
                                                         & ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                            >> 0x18U))))) 
                                               | (QData)((IData)(
                                                                 (8U 
                                                                  | (3U 
                                                                     & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                         << 8U) 
                                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                           >> 0x18U)))))))) 
                                      << 1U)) | (1U 
                                                 & ((IData)(
                                                            ((((QData)((IData)(
                                                                               ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                                << 0xeU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                                >> 0x12U)))) 
                                                               << 0x1fU) 
                                                              | (QData)((IData)(
                                                                                (0x200000U 
                                                                                | ((0x1c00U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                                << 0xdU) 
                                                                                | (0x1c00U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U)))) 
                                                                                | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                                >> 0x13U))))))) 
                                                             >> 0x20U)) 
                                                    >> 0x1fU)));
    __Vtemp221[3U] = ((1U & ((IData)(((0xfffffffff0ULL 
                                       & (((QData)((IData)(
                                                           vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                           << 0x28U) 
                                          | (((QData)((IData)(
                                                              vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                              << 8U) 
                                             | (0xfffffffff0ULL 
                                                & ((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                   >> 0x18U))))) 
                                      | (QData)((IData)(
                                                        (8U 
                                                         | (3U 
                                                            & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                << 8U) 
                                                               | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                  >> 0x18U)))))))) 
                             >> 0x1fU)) | (0xfffffffeU 
                                           & ((0xfffffe00U 
                                               & ((IData)((QData)((IData)(
                                                                          (0x8000000U 
                                                                           | ((0x70000U 
                                                                               & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                              | (1U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])))))) 
                                                  << 9U)) 
                                              | ((IData)(
                                                         (((0xfffffffff0ULL 
                                                            & (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                                                << 0x28U) 
                                                               | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                                                   << 8U) 
                                                                  | (0xfffffffff0ULL 
                                                                     & ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                                        >> 0x18U))))) 
                                                           | (QData)((IData)(
                                                                             (8U 
                                                                              | (3U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                                << 8U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                                >> 0x18U))))))) 
                                                          >> 0x20U)) 
                                                 << 1U))));
    __Vtemp221[4U] = ((1U & ((1U & ((IData)((QData)((IData)(
                                                            (0x8000000U 
                                                             | ((0x70000U 
                                                                 & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                | (1U 
                                                                   & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])))))) 
                                    >> 0x17U)) | ((IData)(
                                                          (((0xfffffffff0ULL 
                                                             & (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])) 
                                                                 << 0x28U) 
                                                                | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU])) 
                                                                    << 8U) 
                                                                   | (0xfffffffff0ULL 
                                                                      & ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU])) 
                                                                         >> 0x18U))))) 
                                                            | (QData)((IData)(
                                                                              (8U 
                                                                               | (3U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xeU] 
                                                                                << 8U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                                                >> 0x18U))))))) 
                                                           >> 0x20U)) 
                                                  >> 0x1fU))) 
                      | (0xfffffffeU & ((0x1feU & ((IData)((QData)((IData)(
                                                                           (0x8000000U 
                                                                            | ((0x70000U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                               | (1U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU])))))) 
                                                   >> 0x17U)) 
                                        | (0xfffffe00U 
                                           & ((0xffffc000U 
                                               & ((IData)((QData)((IData)(
                                                                          ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                                            << 0x1bU) 
                                                                           | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                                              >> 5U))))) 
                                                  << 0xeU)) 
                                              | ((IData)(
                                                         ((QData)((IData)(
                                                                          (0x8000000U 
                                                                           | ((0x70000U 
                                                                               & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                              | (1U 
                                                                                & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]))))) 
                                                          >> 0x20U)) 
                                                 << 9U))))));
    __Vtemp221[5U] = ((1U & (((IData)((QData)((IData)(
                                                      ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                                                        << 0x1bU) 
                                                       | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x10U] 
                                                          >> 5U))))) 
                              >> 0x12U) | ((IData)(
                                                   ((QData)((IData)(
                                                                    (0x8000000U 
                                                                     | ((0x70000U 
                                                                         & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]) 
                                                                        | (1U 
                                                                           & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xfU]))))) 
                                                    >> 0x20U)) 
                                           >> 0x17U))) 
                      | (0xfffffffeU & (__Vtemp219[3U] 
                                        << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
        = ((0xfffffffeU & ((IData)((((QData)((IData)(
                                                     ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xdU] 
                                                       << 0xeU) 
                                                      | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                         >> 0x12U)))) 
                                     << 0x1fU) | (QData)((IData)(
                                                                 (0x200000U 
                                                                  | ((0x1c00U 
                                                                      & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xcU] 
                                                                          << 0xdU) 
                                                                         | (0x1c00U 
                                                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                               >> 0x13U)))) 
                                                                     | (1U 
                                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                                                           >> 0x13U)))))))) 
                           << 1U)) | (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                                            >> 0x12U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
        = __Vtemp220[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
        = __Vtemp221[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
        = __Vtemp221[3U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[4U] 
        = __Vtemp221[4U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
        = __Vtemp221[5U];
    __Vtemp223[1U] = ((0xffffff00U & ((IData)((QData)((IData)(
                                                              (0x8000000U 
                                                               | ((0x70000U 
                                                                   & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                       << 0x15U) 
                                                                      | (0x1f0000U 
                                                                         & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                            >> 0xbU)))) 
                                                                  | (1U 
                                                                     & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                        >> 0xbU))))))) 
                                      << 8U)) | (IData)(
                                                        (((0xfffffffff0ULL 
                                                           & (((QData)((IData)(
                                                                               vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U])) 
                                                               << 0x3dU) 
                                                              | (((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U])) 
                                                                  << 0x1dU) 
                                                                 | (0x1ffffffffffffff0ULL 
                                                                    & ((QData)((IData)(
                                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU])) 
                                                                       >> 3U))))) 
                                                          | (QData)((IData)(
                                                                            (8U 
                                                                             | (3U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                                << 0x1dU) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
                                                                                >> 3U))))))) 
                                                         >> 0x20U)));
    __Vtemp223[2U] = ((0xffU & ((IData)((QData)((IData)(
                                                        (0x8000000U 
                                                         | ((0x70000U 
                                                             & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                 << 0x15U) 
                                                                | (0x1f0000U 
                                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                      >> 0xbU)))) 
                                                            | (1U 
                                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                  >> 0xbU))))))) 
                                >> 0x18U)) | (0xffffff00U 
                                              & ((0xffffe000U 
                                                  & ((IData)((QData)((IData)(
                                                                             ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                               << 0x10U) 
                                                                              | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                >> 0x10U))))) 
                                                     << 0xdU)) 
                                                 | ((IData)(
                                                            ((QData)((IData)(
                                                                             (0x8000000U 
                                                                              | ((0x70000U 
                                                                                & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                << 0x15U) 
                                                                                | (0x1f0000U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                                >> 0xbU)))) 
                                                                                | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                                >> 0xbU)))))) 
                                                             >> 0x20U)) 
                                                    << 8U))));
    __Vtemp223[3U] = ((0xffU & ((0xffU & ((IData)((QData)((IData)(
                                                                  ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                    << 0x10U) 
                                                                   | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                      >> 0x10U))))) 
                                          >> 0x13U)) 
                                | ((IData)(((QData)((IData)(
                                                            (0x8000000U 
                                                             | ((0x70000U 
                                                                 & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                     << 0x15U) 
                                                                    | (0x1f0000U 
                                                                       & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                          >> 0xbU)))) 
                                                                | (1U 
                                                                   & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                      >> 0xbU)))))) 
                                            >> 0x20U)) 
                                   >> 0x18U))) | (0xffffff00U 
                                                  & ((0x1f00U 
                                                      & ((IData)((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                                << 0x10U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                >> 0x10U))))) 
                                                         >> 0x13U)) 
                                                     | (0xffffe000U 
                                                        & ((IData)(
                                                                   ((QData)((IData)(
                                                                                ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x22U] 
                                                                                << 0x10U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U] 
                                                                                >> 0x10U)))) 
                                                                    >> 0x20U)) 
                                                           << 0xdU)))));
    __Vtemp224[1U] = ((1U & ((IData)((((QData)((IData)(
                                                       ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                         << 3U) 
                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                           >> 0x1dU)))) 
                                       << 0x1fU) | (QData)((IData)(
                                                                   (0x200000U 
                                                                    | ((0x1c00U 
                                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                           << 2U)) 
                                                                       | (1U 
                                                                          & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                             >> 0x1eU)))))))) 
                             >> 0x1fU)) | (0xfffffffeU 
                                           & ((IData)(
                                                      ((((QData)((IData)(
                                                                         ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                                           << 3U) 
                                                                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                             >> 0x1dU)))) 
                                                         << 0x1fU) 
                                                        | (QData)((IData)(
                                                                          (0x200000U 
                                                                           | ((0x1c00U 
                                                                               & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                                << 2U)) 
                                                                              | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                                >> 0x1eU))))))) 
                                                       >> 0x20U)) 
                                              << 1U)));
    __Vtemp225[2U] = ((0xfffffffeU & ((IData)(((0xfffffffff0ULL 
                                                & (((QData)((IData)(
                                                                    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U])) 
                                                    << 0x3dU) 
                                                   | (((QData)((IData)(
                                                                       vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U])) 
                                                       << 0x1dU) 
                                                      | (0x1ffffffffffffff0ULL 
                                                         & ((QData)((IData)(
                                                                            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU])) 
                                                            >> 3U))))) 
                                               | (QData)((IData)(
                                                                 (8U 
                                                                  | (3U 
                                                                     & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                                         << 0x1dU) 
                                                                        | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
                                                                           >> 3U)))))))) 
                                      << 1U)) | (1U 
                                                 & ((IData)(
                                                            ((((QData)((IData)(
                                                                               ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                                                << 3U) 
                                                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                                >> 0x1dU)))) 
                                                               << 0x1fU) 
                                                              | (QData)((IData)(
                                                                                (0x200000U 
                                                                                | ((0x1c00U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                                << 2U)) 
                                                                                | (1U 
                                                                                & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                                >> 0x1eU))))))) 
                                                             >> 0x20U)) 
                                                    >> 0x1fU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
        = ((0xfffffffeU & ((IData)((((QData)((IData)(
                                                     ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1eU] 
                                                       << 3U) 
                                                      | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                         >> 0x1dU)))) 
                                     << 0x1fU) | (QData)((IData)(
                                                                 (0x200000U 
                                                                  | ((0x1c00U 
                                                                      & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                                                                         << 2U)) 
                                                                     | (1U 
                                                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                                                           >> 0x1eU)))))))) 
                           << 1U)) | (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                            >> 0x1dU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
        = __Vtemp224[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
        = __Vtemp225[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
        = ((1U & ((IData)(((0xfffffffff0ULL & (((QData)((IData)(
                                                                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x21U])) 
                                                << 0x3dU) 
                                               | (((QData)((IData)(
                                                                   vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U])) 
                                                   << 0x1dU) 
                                                  | (0x1ffffffffffffff0ULL 
                                                     & ((QData)((IData)(
                                                                        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU])) 
                                                        >> 3U))))) 
                           | (QData)((IData)((8U | 
                                              (3U & 
                                               ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x20U] 
                                                 << 0x1dU) 
                                                | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1fU] 
                                                   >> 3U)))))))) 
                  >> 0x1fU)) | (0xfffffffeU & (__Vtemp223[1U] 
                                               << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[4U] 
        = ((1U & (__Vtemp223[1U] >> 0x1fU)) | (0xfffffffeU 
                                               & (__Vtemp223[2U] 
                                                  << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
        = ((1U & (__Vtemp223[2U] >> 0x1fU)) | (0xfffffffeU 
                                               & (__Vtemp223[3U] 
                                                  << 1U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[1U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[1U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[2U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[2U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[3U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[3U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[4U] 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[4U];
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
        = ((0xfe000000U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U]) 
           | (0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[5U]));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U] 
        = ((0x1ffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[5U]) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                             << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[6U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x11U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[7U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x12U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[8U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x13U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[9U] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x14U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xaU] 
        = ((0x1ffffffU & ((0x1ffc000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
                                         << 0xeU)) 
                          | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x15U] 
                             >> 0x12U))) | (0xfe000000U 
                                            & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
                                               << 0xeU)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xbU] 
        = ((0x1ffffffU & ((0x1fc0000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[5U] 
                                         >> 7U)) | 
                          (0x3ffffU & ((0x1ffc000U 
                                        & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
                                           << 0xeU)) 
                                       | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x16U] 
                                          >> 0x12U))))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xcU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[6U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xdU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[7U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xeU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[8U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0xfU] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[9U] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x10U] 
        = ((0x1ffffffU & ((0x3ffffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
                                       >> 7U)) | (0x1fc0000U 
                                                  & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xaU] 
                                                     >> 7U)))) 
           | (0xfe000000U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                             << 0x19U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
        = ((0xfffff800U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U]) 
           | (0x7ffU & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0xbU] 
                        >> 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U] 
        = ((0x7ffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x11U]) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x12U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x17U] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x13U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x18U] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x14U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x19U] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x15U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1aU] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x16U] 
        = ((0x7ffU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                 << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1bU] 
                                            >> 0x19U))) 
           | (0xfffff800U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                             << 7U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__mst_reqs[0x17U] 
        = (0xfU & ((0x780U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1dU] 
                              << 7U)) | (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_reqs[0x1cU] 
                                         >> 0x19U)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_105 
        = ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__wen)) 
           & ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state)) 
              | (1U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT__state))));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_497 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__wstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_wr_cmd_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache__DOT___T_498 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__dmem__DOT__rstate)) 
           & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__cache_io_mem_rd_cmd_valid));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___GEN_25 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_0_MemResp_valid) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT___T_4)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_8__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___GEN_25 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_rd_mem_1_MemResp_valid) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT___T_4)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__ld_10__DOT__out_valid_R_0)));
    vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___GEN_38 
        = ((IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__mem_ctrl_cache_io_wr_mem_0_MemResp_valid) 
           | ((~ (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT___T_5)) 
              & (IData)(vlTOPp->taiga_sim__DOT__cus_acc__DOT__shell__DOT__accel__DOT__vadd__DOT__st_13__DOT__succ_valid_R_0)));
    vlTOPp->data_bram_en = vlSymsp->TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.new_request;
    vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in 
        = ((0x1feU & (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes_in)) 
           | (IData)(vlTOPp->taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT____Vcellout__sub_unit_select__int_out));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 0U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp 
        = ((0x100U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                      << 8U)) | (0xffU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
                                           << 8U) | 
                                          (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
                                           >> 0x18U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop = 0U;
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                     >> 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
            >> 1U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
            >> 9U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 0U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_d = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
            }
        }
    } else {
        if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_load = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp 
        = ((0x100U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                      << 8U)) | (0xffU & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[1U] 
                                           << 8U) | 
                                          (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
                                           >> 0x18U))));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop = 0U;
    if ((0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                     >> 1U));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 0U;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_busy_q) {
        if ((1U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U])) {
            if ((0U == (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q)))) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop = 1U;
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[0U] 
            >> 1U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push 
        = ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[3U] 
            >> 9U) & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_load) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                = (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o));
        } else {
            if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_cnt_en) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                    = (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q) 
                                 - (IData)(1U)));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp;
        if ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
                = (((~ ((IData)(0x1ffU) << (0xfU & 
                                            ((IData)(9U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1) 
                      << (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)))));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
        if ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1) 
                      << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
        = ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q) 
              >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q;
    if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_clear) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d = 0U;
    } else {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_load) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                = (0xffU & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_r_fifo__data_o));
        } else {
            if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_cnt_en) {
                vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_d 
                    = (0x1ffU & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_counter__DOT__i_counter__DOT__counter_q) 
                                 - (IData)(1U)));
            }
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__read_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__r_fifo_inp;
        if ((8U >= (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n 
                = (((~ ((IData)(0x1ffU) << (0xfU & 
                                            ((IData)(9U) 
                                             * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q))))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT____Vlvbound1) 
                      << (0xfU & ((IData)(9U) * (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_r_fifo__DOT__write_pointer_q)))));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
        if ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n 
                = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q))) 
                    & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_n)) 
                   | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT____Vlvbound1) 
                      << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
        = ((0U >= (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)) 
           & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__mem_q) 
              >> (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o 
            = (1U & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[5U] 
                     >> 0xeU));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty 
        = ((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
           & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__0__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x37ffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x80000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 0U;
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        if ((1U & ((vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                    >> 2U) & (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT____Vcellout__gen_slv_port_demux__BRA__1__KET____DOT__i_dec_err_conv__mst_req_o[2U] 
                              >> 4U)))) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push = 1U;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
        = (0x37ffffffffffULL & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    if (((~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
            = (0x80000000000ULL | vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp);
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop)) 
          & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
            = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o) 
                  << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U] 
        = ((0xffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[3U]) 
           | (0xffff0000U & ((IData)((((QData)((IData)(
                                                       ((0x3c00U 
                                                         & ((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x2aU)) 
                                                            << 0xaU)) 
                                                        | ((0xcU 
                                                            & ((IData)(
                                                                       (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                        >> 0x27U)) 
                                                               << 2U)) 
                                                           | (1U 
                                                              & (IData)(
                                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                         >> 0x25U))))))) 
                                       << 0x2aU) | 
                                      (((QData)((IData)(
                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                         >> 4U))) 
                                        << 4U) | (QData)((IData)(
                                                                 (0xcU 
                                                                  & ((IData)(
                                                                             (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                              >> 2U)) 
                                                                     << 2U))))))) 
                             << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[4U] 
        = ((0xffffU & ((IData)((((QData)((IData)(((0x3c00U 
                                                   & ((IData)(
                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                               >> 0x2aU)) 
                                                      << 0xaU)) 
                                                  | ((0xcU 
                                                      & ((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 0x27U)) 
                                                         << 2U)) 
                                                     | (1U 
                                                        & (IData)(
                                                                  (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                   >> 0x25U))))))) 
                                 << 0x2aU) | (((QData)((IData)(
                                                               (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                >> 4U))) 
                                               << 4U) 
                                              | (QData)((IData)(
                                                                (0xcU 
                                                                 & ((IData)(
                                                                            (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                             >> 2U)) 
                                                                    << 2U))))))) 
                       >> 0x10U)) | (0xffff0000U & 
                                     ((IData)(((((QData)((IData)(
                                                                 ((0x3c00U 
                                                                   & ((IData)(
                                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                               >> 0x2aU)) 
                                                                      << 0xaU)) 
                                                                  | ((0xcU 
                                                                      & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x27U)) 
                                                                         << 2U)) 
                                                                     | (1U 
                                                                        & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x25U))))))) 
                                                 << 0x2aU) 
                                                | (((QData)((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 4U))) 
                                                    << 4U) 
                                                   | (QData)((IData)(
                                                                     (0xcU 
                                                                      & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 2U)) 
                                                                         << 2U)))))) 
                                               >> 0x20U)) 
                                      << 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U] 
        = ((0xffffff00U & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[5U]) 
           | (0xffffU & ((IData)(((((QData)((IData)(
                                                    ((0x3c00U 
                                                      & ((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 0x2aU)) 
                                                         << 0xaU)) 
                                                     | ((0xcU 
                                                         & ((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x27U)) 
                                                            << 2U)) 
                                                        | (1U 
                                                           & (IData)(
                                                                     (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                      >> 0x25U))))))) 
                                    << 0x2aU) | (((QData)((IData)(
                                                                  (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                   >> 4U))) 
                                                  << 4U) 
                                                 | (QData)((IData)(
                                                                   (0xcU 
                                                                    & ((IData)(
                                                                               (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__0__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 2U)) 
                                                                       << 2U)))))) 
                                  >> 0x20U)) >> 0x10U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop)) 
          & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q))) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push) 
         & (1U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__write_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) 
         & (~ (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_empty)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
            = ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n) 
               & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q)));
    }
    if (((0U == (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__status_cnt_q)) 
         & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_push))) {
        if (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__w_fifo_pop) {
            vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_n 
                = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_w_fifo__DOT__read_pointer_q;
        }
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 1U;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__gate_clock = 0U;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)));
    }
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = (3U & ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q) 
                     - (IData)(1U)));
    }
    if (((((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
           & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_pop)) 
          & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q))) 
         & (0U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_n 
            = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q;
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_n 
            = (1U & ((IData)(1U) + (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
        = vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_q;
    if (((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__b_fifo_push) 
         & (2U != (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__status_cnt_q)))) {
        vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n 
            = (((~ ((IData)(1U) << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q))) 
                & (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__mem_n)) 
               | ((IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT____Vcellout__i_w_fifo__data_o) 
                  << (IData)(vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__i_b_fifo__DOT__write_pointer_q)));
    }
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U] 
        = ((0xffffffU & vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[8U]) 
           | (0xff000000U & ((IData)((((QData)((IData)(
                                                       ((0x3c00U 
                                                         & ((IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x2aU)) 
                                                            << 0xaU)) 
                                                        | ((0xcU 
                                                            & ((IData)(
                                                                       (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                        >> 0x27U)) 
                                                               << 2U)) 
                                                           | (1U 
                                                              & (IData)(
                                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                         >> 0x25U))))))) 
                                       << 0x2aU) | 
                                      (((QData)((IData)(
                                                        (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                         >> 4U))) 
                                        << 4U) | (QData)((IData)(
                                                                 (0xcU 
                                                                  & ((IData)(
                                                                             (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                              >> 2U)) 
                                                                     << 2U))))))) 
                             << 0x18U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[9U] 
        = ((0xffffffU & ((IData)((((QData)((IData)(
                                                   ((0x3c00U 
                                                     & ((IData)(
                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                 >> 0x2aU)) 
                                                        << 0xaU)) 
                                                    | ((0xcU 
                                                        & ((IData)(
                                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                    >> 0x27U)) 
                                                           << 2U)) 
                                                       | (1U 
                                                          & (IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x25U))))))) 
                                   << 0x2aU) | (((QData)((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 4U))) 
                                                 << 4U) 
                                                | (QData)((IData)(
                                                                  (0xcU 
                                                                   & ((IData)(
                                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                               >> 2U)) 
                                                                      << 2U))))))) 
                         >> 8U)) | (0xff000000U & ((IData)(
                                                           ((((QData)((IData)(
                                                                              ((0x3c00U 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x2aU)) 
                                                                                << 0xaU)) 
                                                                               | ((0xcU 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x27U)) 
                                                                                << 2U)) 
                                                                                | (1U 
                                                                                & (IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 0x25U))))))) 
                                                              << 0x2aU) 
                                                             | (((QData)((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 4U))) 
                                                                 << 4U) 
                                                                | (QData)((IData)(
                                                                                (0xcU 
                                                                                & ((IData)(
                                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                                >> 2U)) 
                                                                                << 2U)))))) 
                                                            >> 0x20U)) 
                                                   << 0x18U)));
    vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__slv_resps[0xaU] 
        = (0xffffffU & ((IData)(((((QData)((IData)(
                                                   ((0x3c00U 
                                                     & ((IData)(
                                                                (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                 >> 0x2aU)) 
                                                        << 0xaU)) 
                                                    | ((0xcU 
                                                        & ((IData)(
                                                                   (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                    >> 0x27U)) 
                                                           << 2U)) 
                                                       | (1U 
                                                          & (IData)(
                                                                    (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                     >> 0x25U))))))) 
                                   << 0x2aU) | (((QData)((IData)(
                                                                 (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                  >> 4U))) 
                                                 << 4U) 
                                                | (QData)((IData)(
                                                                  (0xcU 
                                                                   & ((IData)(
                                                                              (vlTOPp->taiga_sim__DOT__taiga_axi_bus__DOT__gen_slv_port_demux__BRA__1__KET____DOT__i_axi_err_slv__DOT__err_resp 
                                                                               >> 2U)) 
                                                                      << 2U)))))) 
                                 >> 0x20U)) >> 8U));
}
