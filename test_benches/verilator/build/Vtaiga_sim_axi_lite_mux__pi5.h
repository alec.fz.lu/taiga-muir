// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtaiga_sim.h for the primary calling header

#ifndef _VTAIGA_SIM_AXI_LITE_MUX__PI5_H_
#define _VTAIGA_SIM_AXI_LITE_MUX__PI5_H_  // guard

#include "verilated_heavy.h"

//==========

class Vtaiga_sim__Syms;

//----------

VL_MODULE(Vtaiga_sim_axi_lite_mux__pi5) {
  public:
    
    // PORTS
    VL_IN8(clk_i,0,0);
    VL_IN8(rst_ni,0,0);
    VL_IN8(test_i,0,0);
    VL_INW(slv_reqs_i,369,0,12);
    VL_OUTW(slv_resps_o,111,0,4);
    VL_OUTW(mst_req_o,184,0,6);
    VL_IN64(mst_resp_i,55,0);
    
    // LOCAL SIGNALS
    // Anonymous structures to workaround compiler member-count bugs
    struct {
        CData/*1:0*/ __PVT__gen_mux__DOT__slv_aw_valids;
        CData/*1:0*/ __PVT__gen_mux__DOT__slv_aw_readies;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_aw_valid;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_aw_ready;
        CData/*0:0*/ __PVT__gen_mux__DOT__aw_ready;
        CData/*0:0*/ __PVT__gen_mux__DOT__lock_aw_valid_d;
        CData/*0:0*/ __PVT__gen_mux__DOT__lock_aw_valid_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__load_aw_lock;
        CData/*0:0*/ __PVT__gen_mux__DOT__w_fifo_empty;
        CData/*0:0*/ __PVT__gen_mux__DOT__w_fifo_push;
        CData/*0:0*/ __PVT__gen_mux__DOT__w_fifo_pop;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_w_valid;
        CData/*0:0*/ __PVT__gen_mux__DOT__b_fifo_empty;
        CData/*0:0*/ __PVT__gen_mux__DOT__b_fifo_pop;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_b_ready;
        CData/*1:0*/ __PVT__gen_mux__DOT__slv_ar_valids;
        CData/*1:0*/ __PVT__gen_mux__DOT__slv_ar_readies;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_ar_valid;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_ar_ready;
        CData/*0:0*/ __PVT__gen_mux__DOT__ar_ready;
        CData/*0:0*/ __PVT__gen_mux__DOT__r_fifo_empty;
        CData/*0:0*/ __PVT__gen_mux__DOT__r_fifo_push;
        CData/*0:0*/ __PVT__gen_mux__DOT__r_fifo_pop;
        CData/*0:0*/ __PVT__gen_mux__DOT__mst_r_ready;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_nodes;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__rr_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__req_d;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_aw_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__gate_clock;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__read_pointer_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__write_pointer_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_n;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__status_cnt_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__mem_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_w_fifo__DOT__mem_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_full_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_drain;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_full_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_drain;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__gate_clock;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__read_pointer_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__write_pointer_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_n;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__status_cnt_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__mem_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_b_fifo__DOT__mem_q;
    };
    struct {
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_nodes;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__rr_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__req_d;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__rr_d;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__lock_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_lock__DOT__req_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__upper_mask;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__lower_mask;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_levels__BRA__0__KET____DOT__gen_level__BRA__0__KET____DOT__sel;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_lut;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__sel_nodes;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__index_nodes;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_upper__DOT__gen_lzc__DOT__in_tmp;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_lut;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__index_nodes;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_ar_arbiter__DOT__gen_arbiter__DOT__gen_int_rr__DOT__gen_fair_arb__DOT__i_lzc_lower__DOT__gen_lzc__DOT__in_tmp;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__gate_clock;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__read_pointer_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__write_pointer_q;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_n;
        CData/*1:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__status_cnt_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__mem_n;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_r_fifo__DOT__mem_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_full_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_fill;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_drain;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_full_q;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_fill;
        CData/*0:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_drain;
        WData/*147:0*/ __PVT__gen_mux__DOT__slv_aw_chans[5];
        WData/*135:0*/ __PVT__gen_mux__DOT__slv_ar_chans[5];
        WData/*73:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[3];
        WData/*73:0*/ __PVT__gen_mux__DOT__i_aw_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[3];
        WData/*67:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__a_data_q[3];
        WData/*67:0*/ __PVT__gen_mux__DOT__i_ar_spill_reg__DOT__gen_spill_reg__DOT__b_data_q[3];
    };
    
    // LOCAL VARIABLES
    CData/*0:0*/ __Vcellout__gen_mux__DOT__i_w_fifo__data_o;
    CData/*0:0*/ __Vcellout__gen_mux__DOT__i_b_fifo__data_o;
    CData/*0:0*/ __Vcellout__gen_mux__DOT__i_r_fifo__data_o;
    CData/*0:0*/ gen_mux__DOT__i_w_fifo__DOT____Vlvbound1;
    CData/*0:0*/ gen_mux__DOT__i_b_fifo__DOT____Vlvbound1;
    CData/*0:0*/ gen_mux__DOT__i_r_fifo__DOT____Vlvbound1;
    
    // INTERNAL VARIABLES
  private:
    Vtaiga_sim__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Vtaiga_sim_axi_lite_mux__pi5);  ///< Copying not allowed
  public:
    Vtaiga_sim_axi_lite_mux__pi5(const char* name = "TOP");
    ~Vtaiga_sim_axi_lite_mux__pi5();
    
    // INTERNAL METHODS
    void __Vconfigure(Vtaiga_sim__Syms* symsp, bool first);
    void _combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__5(Vtaiga_sim__Syms* __restrict vlSymsp);
    void _combo__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux__6(Vtaiga_sim__Syms* __restrict vlSymsp);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
  public:
    void _sequent__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__3(Vtaiga_sim__Syms* __restrict vlSymsp);
    void _settle__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux__1(Vtaiga_sim__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    void _settle__TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux__2(Vtaiga_sim__Syms* __restrict vlSymsp) VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
