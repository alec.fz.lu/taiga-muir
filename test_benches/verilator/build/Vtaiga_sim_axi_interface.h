// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design internal header
// See Vtaiga_sim.h for the primary calling header

#ifndef _VTAIGA_SIM_AXI_INTERFACE_H_
#define _VTAIGA_SIM_AXI_INTERFACE_H_  // guard

#include "verilated_heavy.h"

//==========

class Vtaiga_sim__Syms;

//----------

VL_MODULE(Vtaiga_sim_axi_interface) {
  public:
    
    // LOCAL SIGNALS
    CData/*2:0*/ arsize;
    CData/*5:0*/ arid;
    CData/*1:0*/ rresp;
    CData/*0:0*/ rlast;
    CData/*5:0*/ rid;
    CData/*2:0*/ awsize;
    CData/*5:0*/ awid;
    CData/*3:0*/ wstrb;
    CData/*1:0*/ bresp;
    CData/*5:0*/ bid;
    IData/*31:0*/ araddr;
    IData/*31:0*/ awaddr;
    IData/*31:0*/ wdata;
    
    // INTERNAL VARIABLES
  private:
    Vtaiga_sim__Syms* __VlSymsp;  // Symbol table
  public:
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Vtaiga_sim_axi_interface);  ///< Copying not allowed
  public:
    Vtaiga_sim_axi_interface(const char* name = "TOP");
    ~Vtaiga_sim_axi_interface();
    
    // INTERNAL METHODS
    void __Vconfigure(Vtaiga_sim__Syms* symsp, bool first);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
