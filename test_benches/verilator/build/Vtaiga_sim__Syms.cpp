// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Symbol table implementation internals

#include "Vtaiga_sim__Syms.h"
#include "Vtaiga_sim.h"
#include "Vtaiga_sim___024unit.h"
#include "Vtaiga_sim_axi_lite_mux__pi5.h"
#include "Vtaiga_sim_l2_memory_interface.h"
#include "Vtaiga_sim_l2_arbitration_interface.h"
#include "Vtaiga_sim_l2_requester_interface.h"
#include "Vtaiga_sim_branch_predictor_interface.h"
#include "Vtaiga_sim_unit_writeback_interface.h"
#include "Vtaiga_sim_ras_interface.h"
#include "Vtaiga_sim_tlb_interface.h"
#include "Vtaiga_sim_renamer_interface.h"
#include "Vtaiga_sim_register_file_issue_interface.h"
#include "Vtaiga_sim_mmu_interface.h"
#include "Vtaiga_sim_ls_sub_unit_interface__pi8.h"
#include "Vtaiga_sim_ls_sub_unit_interface__pi7.h"
#include "Vtaiga_sim_fetch_sub_unit_interface.h"
#include "Vtaiga_sim_axi_interface.h"
#include "Vtaiga_sim_l1_arbiter_request_interface.h"
#include "Vtaiga_sim_l1_arbiter_return_interface.h"
#include "Vtaiga_sim_load_store_queue_interface.h"
#include "Vtaiga_sim_unsigned_division_interface.h"
#include "Vtaiga_sim_fifo_interface__D2a.h"
#include "Vtaiga_sim_fifo_interface__D47.h"
#include "Vtaiga_sim_fifo_interface__D9.h"
#include "Vtaiga_sim_fifo_interface__D6.h"
#include "Vtaiga_sim_fifo_interface__D3.h"
#include "Vtaiga_sim_fifo_interface__D5.h"
#include "Vtaiga_sim_fifo_interface__D23.h"
#include "Vtaiga_sim_fifo_interface__D7.h"
#include "Vtaiga_sim_fifo_interface__D2c.h"
#include "Vtaiga_sim_fifo_interface__D22.h"
#include "Vtaiga_sim_fifo_interface__D1e.h"
#include "Vtaiga_sim_fifo_interface__D20.h"
#include "Vtaiga_sim_fifo_interface__D2b.h"



// FUNCTIONS
Vtaiga_sim__Syms::Vtaiga_sim__Syms(Vtaiga_sim* topp, const char* namep)
    // Setup locals
    : __Vm_namep(namep)
    , __Vm_didInit(false)
    // Setup submodule names
    , TOP__taiga_sim__DOT__cpu__DOT__bp(Verilated::catName(topp->name(), "taiga_sim.cpu.bp"))
    , TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface(Verilated::catName(topp->name(), "taiga_sim.cpu.decode_rename_interface"))
    , TOP__taiga_sim__DOT__cpu__DOT__dmmu(Verilated::catName(topp->name(), "taiga_sim.cpu.dmmu"))
    , TOP__taiga_sim__DOT__cpu__DOT__dtlb(Verilated::catName(topp->name(), "taiga_sim.cpu.dtlb"))
    , TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo(Verilated::catName(topp->name(), "taiga_sim.cpu.fetch_block.fetch_attr_fifo"))
    , TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.fetch_block.fetch_sub[0]"))
    , TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo(Verilated::catName(topp->name(), "taiga_sim.cpu.genblk6.alec_unit_block.input_fifo"))
    , TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core(Verilated::catName(topp->name(), "taiga_sim.cpu.genblk8.div_unit_block.div_core"))
    , TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo(Verilated::catName(topp->name(), "taiga_sim.cpu.genblk8.div_unit_block.input_fifo"))
    , TOP__taiga_sim__DOT__cpu__DOT__immu(Verilated::catName(topp->name(), "taiga_sim.cpu.immu"))
    , TOP__taiga_sim__DOT__cpu__DOT__itlb(Verilated::catName(topp->name(), "taiga_sim.cpu.itlb"))
    , TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.l1_request[0]"))
    , TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.l1_request[1]"))
    , TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__2__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.l1_request[2]"))
    , TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.l1_request[3]"))
    , TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.l1_response[1]"))
    , TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.l1_response[3]"))
    , TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram(Verilated::catName(topp->name(), "taiga_sim.cpu.load_store_unit_block.bram"))
    , TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus(Verilated::catName(topp->name(), "taiga_sim.cpu.load_store_unit_block.bus"))
    , TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes(Verilated::catName(topp->name(), "taiga_sim.cpu.load_store_unit_block.load_attributes"))
    , TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq(Verilated::catName(topp->name(), "taiga_sim.cpu.load_store_unit_block.lsq"))
    , TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo(Verilated::catName(topp->name(), "taiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_fifo"))
    , TOP__taiga_sim__DOT__cpu__DOT__ras(Verilated::catName(topp->name(), "taiga_sim.cpu.ras"))
    , TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo(Verilated::catName(topp->name(), "taiga_sim.cpu.ras_block.ri_fifo"))
    , TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list(Verilated::catName(topp->name(), "taiga_sim.cpu.renamer_block.free_list"))
    , TOP__taiga_sim__DOT__cpu__DOT__rf_issue(Verilated::catName(topp->name(), "taiga_sim.cpu.rf_issue"))
    , TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__(Verilated::catName(topp->name(), "taiga_sim.cpu.unit_wb[2]"))
    , TOP__taiga_sim__DOT__l2__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.l2[0]"))
    , TOP__taiga_sim__DOT__l2__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.l2[1]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__arb(Verilated::catName(topp->name(), "taiga_sim.l2_arb.arb"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes(Verilated::catName(topp->name(), "taiga_sim.l2_arb.data_attributes"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.input_data_fifos[0]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.input_data_fifos[1]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.input_fifos[0]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.input_fifos[1]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.inv_response_fifos[0]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.inv_response_fifos[1]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo(Verilated::catName(topp->name(), "taiga_sim.l2_arb.mem_addr_fifo"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo(Verilated::catName(topp->name(), "taiga_sim.l2_arb.mem_data_fifo"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo(Verilated::catName(topp->name(), "taiga_sim.l2_arb.mem_returndata_fifo"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.returndata_fifos[0]"))
    , TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__(Verilated::catName(topp->name(), "taiga_sim.l2_arb.returndata_fifos[1]"))
    , TOP__taiga_sim__DOT__m_axi(Verilated::catName(topp->name(), "taiga_sim.m_axi"))
    , TOP__taiga_sim__DOT__mem(Verilated::catName(topp->name(), "taiga_sim.mem"))
    , TOP__taiga_sim__DOT__muir_axi2(Verilated::catName(topp->name(), "taiga_sim.muir_axi2"))
    , TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux(Verilated::catName(topp->name(), "taiga_sim.taiga_axi_bus.gen_mst_port_mux[0].i_axi_lite_mux"))
    , TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux(Verilated::catName(topp->name(), "taiga_sim.taiga_axi_bus.gen_mst_port_mux[1].i_axi_lite_mux"))
    , TOP__taiga_sim__DOT__uart_axi(Verilated::catName(topp->name(), "taiga_sim.uart_axi"))
{
    // Pointer to top level
    TOPp = topp;
    // Setup each module's pointers to their submodules
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__bp = &TOP__taiga_sim__DOT__cpu__DOT__bp;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__decode_rename_interface = &TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__dmmu = &TOP__taiga_sim__DOT__cpu__DOT__dmmu;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__dtlb = &TOP__taiga_sim__DOT__cpu__DOT__dtlb;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo = &TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo = &TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core = &TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo = &TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__immu = &TOP__taiga_sim__DOT__cpu__DOT__immu;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__itlb = &TOP__taiga_sim__DOT__cpu__DOT__itlb;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__2__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__2__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram = &TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus = &TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes = &TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq = &TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo = &TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__ras = &TOP__taiga_sim__DOT__cpu__DOT__ras;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo = &TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list = &TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__rf_issue = &TOP__taiga_sim__DOT__cpu__DOT__rf_issue;
    TOPp->__PVT__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__ = &TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2__BRA__0__KET__ = &TOP__taiga_sim__DOT__l2__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2__BRA__1__KET__ = &TOP__taiga_sim__DOT__l2__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__arb = &TOP__taiga_sim__DOT__l2_arb__DOT__arb;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__data_attributes = &TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo = &TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo = &TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo = &TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__;
    TOPp->__PVT__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__ = &TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__;
    TOPp->__PVT__taiga_sim__DOT__m_axi = &TOP__taiga_sim__DOT__m_axi;
    TOPp->__PVT__taiga_sim__DOT__mem = &TOP__taiga_sim__DOT__mem;
    TOPp->__PVT__taiga_sim__DOT__muir_axi2 = &TOP__taiga_sim__DOT__muir_axi2;
    TOPp->__PVT__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux = &TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux;
    TOPp->__PVT__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux = &TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux;
    TOPp->__PVT__taiga_sim__DOT__uart_axi = &TOP__taiga_sim__DOT__uart_axi;
    // Setup each module's pointer back to symbol table (for public functions)
    TOPp->__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__bp.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__decode_rename_interface.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__dmmu.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__dtlb.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_attr_fifo.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__fetch_block__DOT__fetch_sub__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__genblk6__DOT__alec_unit_block__DOT__input_fifo.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__div_core.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__genblk8__DOT__div_unit_block__DOT__input_fifo.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__immu.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__itlb.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__1__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__2__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__l1_request__BRA__3__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__1__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__l1_response__BRA__3__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bram.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__bus.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__load_attributes.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__load_store_unit_block__DOT__lsq_block__DOT__lq_block__DOT__load_fifo.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__ras.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__ras_block__DOT__ri_fifo.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__cpu__DOT__renamer_block__DOT__free_list.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__rf_issue.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__cpu__DOT__unit_wb__BRA__2__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2__BRA__1__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__l2_arb__DOT__arb.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__data_attributes.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__input_data_fifos__BRA__1__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__input_fifos__BRA__1__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__inv_response_fifos__BRA__1__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__l2_arb__DOT__mem_addr_fifo.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__mem_data_fifo.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__l2_arb__DOT__mem_returndata_fifo.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__0__KET__.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__l2_arb__DOT__returndata_fifos__BRA__1__KET__.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__m_axi.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__mem.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__muir_axi2.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__0__KET____DOT__i_axi_lite_mux.__Vconfigure(this, true);
    TOP__taiga_sim__DOT__taiga_axi_bus__DOT__gen_mst_port_mux__BRA__1__KET____DOT__i_axi_lite_mux.__Vconfigure(this, false);
    TOP__taiga_sim__DOT__uart_axi.__Vconfigure(this, false);
    // Setup scopes
    __Vscope_taiga_sim__cpu__decode_and_issue_block.configure(this, name(), "taiga_sim.cpu.decode_and_issue_block", "decode_and_issue_block", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__decode_and_issue_block__invalid_fetch_address_assertion.configure(this, name(), "taiga_sim.cpu.decode_and_issue_block.invalid_fetch_address_assertion", "invalid_fetch_address_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__fetch_block.configure(this, name(), "taiga_sim.cpu.fetch_block", "fetch_block", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__fetch_block__attributes_fifo.configure(this, name(), "taiga_sim.cpu.fetch_block.attributes_fifo", "attributes_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__fetch_block__attributes_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.fetch_block.attributes_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__fetch_block__attributes_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.cpu.fetch_block.attributes_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__fetch_block__attributes_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.fetch_block.attributes_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__fetch_block__spurious_fetch_complete_assertion.configure(this, name(), "taiga_sim.cpu.fetch_block.spurious_fetch_complete_assertion", "spurious_fetch_complete_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__gc_unit_block__csr_registers.configure(this, name(), "taiga_sim.cpu.gc_unit_block.csr_registers", "csr_registers", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__gc_unit_block__csr_registers__genblk1.configure(this, name(), "taiga_sim.cpu.gc_unit_block.csr_registers.genblk1", "genblk1", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk2__i_mmu.configure(this, name(), "taiga_sim.cpu.genblk2.i_mmu", "i_mmu", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk2__i_mmu__mmu_spurious_l1_response.configure(this, name(), "taiga_sim.cpu.genblk2.i_mmu.mmu_spurious_l1_response", "mmu_spurious_l1_response", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk2__i_mmu__mmu_tlb_state_mismatch.configure(this, name(), "taiga_sim.cpu.genblk2.i_mmu.mmu_tlb_state_mismatch", "mmu_tlb_state_mismatch", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk2__i_tlb.configure(this, name(), "taiga_sim.cpu.genblk2.i_tlb", "i_tlb", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk2__i_tlb__multiple_tag_hit_in_tlb.configure(this, name(), "taiga_sim.cpu.genblk2.i_tlb.multiple_tag_hit_in_tlb", "multiple_tag_hit_in_tlb", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk4__d_mmu.configure(this, name(), "taiga_sim.cpu.genblk4.d_mmu", "d_mmu", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk4__d_mmu__mmu_spurious_l1_response.configure(this, name(), "taiga_sim.cpu.genblk4.d_mmu.mmu_spurious_l1_response", "mmu_spurious_l1_response", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk4__d_mmu__mmu_tlb_state_mismatch.configure(this, name(), "taiga_sim.cpu.genblk4.d_mmu.mmu_tlb_state_mismatch", "mmu_tlb_state_mismatch", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk4__d_tlb.configure(this, name(), "taiga_sim.cpu.genblk4.d_tlb", "d_tlb", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk4__d_tlb__multiple_tag_hit_in_tlb.configure(this, name(), "taiga_sim.cpu.genblk4.d_tlb.multiple_tag_hit_in_tlb", "multiple_tag_hit_in_tlb", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk6__alec_unit_block__div_input_fifo.configure(this, name(), "taiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo", "div_input_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk6__alec_unit_block__div_input_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk6__alec_unit_block__div_input_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk6__alec_unit_block__div_input_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.genblk6.alec_unit_block.div_input_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk8__div_unit_block__div_input_fifo.configure(this, name(), "taiga_sim.cpu.genblk8.div_unit_block.div_input_fifo", "div_input_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk8__div_unit_block__div_input_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.genblk8.div_unit_block.div_input_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk8__div_unit_block__div_input_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.cpu.genblk8.div_unit_block.div_input_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__genblk8__div_unit_block__div_input_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.genblk8.div_unit_block.div_input_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__id_block.configure(this, name(), "taiga_sim.cpu.id_block", "id_block", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__id_block__decode_advanced_without_id_assertion.configure(this, name(), "taiga_sim.cpu.id_block.decode_advanced_without_id_assertion", "decode_advanced_without_id_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__id_block__pc_id_assigned_without_pc_id_available_assertion.configure(this, name(), "taiga_sim.cpu.id_block.pc_id_assigned_without_pc_id_available_assertion", "pc_id_assigned_without_pc_id_available_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block.configure(this, name(), "taiga_sim.cpu.load_store_unit_block", "load_store_unit_block", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__attributes_fifo.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.attributes_fifo", "attributes_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__attributes_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.attributes_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__attributes_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.attributes_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__attributes_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.attributes_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__invalid_ls_address_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.invalid_ls_address_assertion", "invalid_ls_address_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__lsq_block__lq_block__load_queue_fifo.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo", "load_queue_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__lsq_block__lq_block__load_queue_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__lsq_block__lq_block__load_queue_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__lsq_block__lq_block__load_queue_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.lsq_block.lq_block.load_queue_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__load_store_unit_block__spurious_load_complete_assertion.configure(this, name(), "taiga_sim.cpu.load_store_unit_block.spurious_load_complete_assertion", "spurious_load_complete_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__ras_block__read_index_fifo.configure(this, name(), "taiga_sim.cpu.ras_block.read_index_fifo", "read_index_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__ras_block__read_index_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.ras_block.read_index_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__ras_block__read_index_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.cpu.ras_block.read_index_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__ras_block__read_index_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.ras_block.read_index_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block.configure(this, name(), "taiga_sim.cpu.register_file_block", "register_file_block", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block__register_file_gen__BRA__0__KET____reg_group.configure(this, name(), "taiga_sim.cpu.register_file_block.register_file_gen[0].reg_group", "reg_group", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block__register_file_gen__BRA__0__KET____reg_group__write_to_zero_reg_assertion.configure(this, name(), "taiga_sim.cpu.register_file_block.register_file_gen[0].reg_group.write_to_zero_reg_assertion", "write_to_zero_reg_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block__register_file_gen__BRA__1__KET____reg_group.configure(this, name(), "taiga_sim.cpu.register_file_block.register_file_gen[1].reg_group", "reg_group", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block__register_file_gen__BRA__1__KET____reg_group__write_to_zero_reg_assertion.configure(this, name(), "taiga_sim.cpu.register_file_block.register_file_gen[1].reg_group.write_to_zero_reg_assertion", "write_to_zero_reg_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block__write_to_rd_zero_assertion__BRA__0__KET__.configure(this, name(), "taiga_sim.cpu.register_file_block.write_to_rd_zero_assertion[0]", "write_to_rd_zero_assertion[0]", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__register_file_block__write_to_rd_zero_assertion__BRA__1__KET__.configure(this, name(), "taiga_sim.cpu.register_file_block.write_to_rd_zero_assertion[1]", "write_to_rd_zero_assertion[1]", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block.configure(this, name(), "taiga_sim.cpu.renamer_block", "renamer_block", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block__free_list_fifo.configure(this, name(), "taiga_sim.cpu.renamer_block.free_list_fifo", "free_list_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block__free_list_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.cpu.renamer_block.free_list_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block__free_list_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.cpu.renamer_block.free_list_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block__rename_rd_zero_assertion.configure(this, name(), "taiga_sim.cpu.renamer_block.rename_rd_zero_assertion", "rename_rd_zero_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block__rename_rs_zero_assertion__BRA__0__KET__.configure(this, name(), "taiga_sim.cpu.renamer_block.rename_rs_zero_assertion[0]", "rename_rs_zero_assertion[0]", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__cpu__renamer_block__rename_rs_zero_assertion__BRA__1__KET__.configure(this, name(), "taiga_sim.cpu.renamer_block.rename_rs_zero_assertion[1]", "rename_rs_zero_assertion[1]", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__data_attributes_fifo.configure(this, name(), "taiga_sim.l2_arb.data_attributes_fifo", "data_attributes_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__data_attributes_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.data_attributes_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__data_attributes_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.data_attributes_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__data_attributes_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.data_attributes_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk1__BRA__0__KET____input_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk1[0].input_fifo", "input_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk1__BRA__0__KET____input_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk1[0].input_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk1__BRA__0__KET____input_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk1[0].input_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk1__BRA__0__KET____input_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk1[0].input_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk1__BRA__1__KET____input_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk1[1].input_fifo", "input_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk1__BRA__1__KET____input_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk1[1].input_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk2__BRA__0__KET____input_data_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk2[0].input_data_fifo", "input_data_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk2__BRA__0__KET____input_data_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk2[0].input_data_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk2__BRA__1__KET____input_data_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk2[1].input_data_fifo", "input_data_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk2__BRA__1__KET____input_data_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk2[1].input_data_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__0__KET____inv_response_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk4[0].inv_response_fifo", "inv_response_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__0__KET____inv_response_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk4[0].inv_response_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__0__KET____inv_response_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk4[0].inv_response_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__0__KET____inv_response_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk4[0].inv_response_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__1__KET____inv_response_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk4[1].inv_response_fifo", "inv_response_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__1__KET____inv_response_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk4[1].inv_response_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__1__KET____inv_response_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk4[1].inv_response_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk4__BRA__1__KET____inv_response_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk4[1].inv_response_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__0__KET____returndata_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk5[0].returndata_fifo", "returndata_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__0__KET____returndata_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk5[0].returndata_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__0__KET____returndata_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk5[0].returndata_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__0__KET____returndata_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk5[0].returndata_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__1__KET____returndata_fifo.configure(this, name(), "taiga_sim.l2_arb.genblk5[1].returndata_fifo", "returndata_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__1__KET____returndata_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk5[1].returndata_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__1__KET____returndata_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk5[1].returndata_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__genblk5__BRA__1__KET____returndata_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.genblk5[1].returndata_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__input_fifo.configure(this, name(), "taiga_sim.l2_arb.input_fifo", "input_fifo", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__input_fifo__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.input_fifo.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__input_fifo__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.input_fifo.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__input_fifo__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.input_fifo.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_data.configure(this, name(), "taiga_sim.l2_arb.mem_data", "mem_data", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_data__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.mem_data.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_data__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.mem_data.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_data__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.mem_data.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_returndata.configure(this, name(), "taiga_sim.l2_arb.mem_returndata", "mem_returndata", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_returndata__fifo_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.mem_returndata.fifo_overflow_assertion", "fifo_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_returndata__fifo_potenial_push_overflow_assertion.configure(this, name(), "taiga_sim.l2_arb.mem_returndata.fifo_potenial_push_overflow_assertion", "fifo_potenial_push_overflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
    __Vscope_taiga_sim__l2_arb__mem_returndata__fifo_underflow_assertion.configure(this, name(), "taiga_sim.l2_arb.mem_returndata.fifo_underflow_assertion", "fifo_underflow_assertion", -12, VerilatedScope::SCOPE_OTHER);
}
