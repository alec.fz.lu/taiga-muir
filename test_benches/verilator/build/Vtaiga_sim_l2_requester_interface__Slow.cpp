// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vtaiga_sim.h for the primary calling header

#include "Vtaiga_sim_l2_requester_interface.h"
#include "Vtaiga_sim__Syms.h"

//==========

VL_CTOR_IMP(Vtaiga_sim_l2_requester_interface) {
    // Reset internal values
    // Reset structure values
    _ctor_var_reset();
}

void Vtaiga_sim_l2_requester_interface::__Vconfigure(Vtaiga_sim__Syms* vlSymsp, bool first) {
    if (false && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
    if (false && this->__VlSymsp) {}  // Prevent unused
}

Vtaiga_sim_l2_requester_interface::~Vtaiga_sim_l2_requester_interface() {
}

void Vtaiga_sim_l2_requester_interface::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+            Vtaiga_sim_l2_requester_interface::_ctor_var_reset\n"); );
    // Body
    addr = VL_RAND_RESET_I(30);
    be = VL_RAND_RESET_I(4);
    rnw = VL_RAND_RESET_I(1);
    is_amo = VL_RAND_RESET_I(1);
    amo_type_or_burst_size = VL_RAND_RESET_I(5);
    sub_id = VL_RAND_RESET_I(2);
    request_push = VL_RAND_RESET_I(1);
    inv_valid = VL_RAND_RESET_I(1);
    rd_data_valid = VL_RAND_RESET_I(1);
}
