/*
 * Copyright © 2017 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */

import taiga_config::*;
import taiga_types::*;
import l2_config_and_types::*;
import axi_pkg::*;

module taiga_sim # (
        parameter MEMORY_FILE = "/home/ematthew/Research/RISCV/software/riscv-tools/riscv-tests/benchmarks/dhrystone.riscv.hw_init" //change this to appropriate location "/home/ematthew/Downloads/dhrystone.riscv.sim_init"
        )
        (
        input logic clk,
        input logic rst,

        //-------------------------------------------------------------------------
        //DDR AXI - muIR
        //-------------------------------------------------------------------------
        //--------------------------------------------- 
        // 'aw'
        //---------------------------------------------
        output logic        ddr_axi_awvalid_muir,
        input logic         ddr_axi_awready_muir,
        output logic [31:0] ddr_axi_awaddr_muir,
        output logic [5:0]  ddr_axi_awid_muir,
        //axi_awuser
        output logic [7:0]  ddr_axi_awlen_muir,
        output logic [2:0]  ddr_axi_awsize_muir,
        output logic [1:0]  ddr_axi_awburst_muir,
        //output logic [0:0]ddr_axi_awlock,
        output logic [3:0]  ddr_axi_awcache_muir,
        //output logic [2:0]ddr_axi_awprot,
        //output logic [3:0]ddr_axi_awqos,
        //output logic [3:0]ddr_axi_awregion,
        //--------------------------------------------- 
        // 'w'
        //---------------------------------------------
        output logic        ddr_axi_wvalid_muir,
        input logic         ddr_axi_wready_muir,
        output logic [31:0] ddr_axi_wdata_muir,
        output logic [3:0]  ddr_axi_wstrb_muir,
        output logic        ddr_axi_wlast_muir,
        output logic [5:0]  ddr_axi_wid_muir,
        //--------------------------------------------- 
        // 'b'
        //---------------------------------------------
        input logic         ddr_axi_bvalid_muir,
        output logic        ddr_axi_bready_muir,
        input logic [1:0]   ddr_axi_bresp_muir,
        output logic [5:0]  ddr_axi_bid_muir,
        //axi_buser
        //--------------------------------------------- 
        // 'ar'
        //---------------------------------------------        
        output logic        ddr_axi_arvalid_muir,
        input logic         ddr_axi_arready_muir,
        output logic [31:0] ddr_axi_araddr_muir,
        output logic [5:0]  ddr_axi_arid_muir,
        //axi_aruser
        output logic [7:0]  ddr_axi_arlen_muir,
        output logic [2:0]  ddr_axi_arsize_muir,
        output logic [1:0]  ddr_axi_arburst_muir,
        //output logic [0:0]ddr_axi_arlock,
        output logic [3:0]  ddr_axi_arcache_muir,
        //output logic [2:0]ddr_axi_arprot,
        //output logic [3:0]ddr_axi_arqos,
        //output logic [3:0]ddr_axi_arregion,
        //--------------------------------------------- 
        // 'r'
        //---------------------------------------------
        input logic         ddr_axi_rvalid_muir,
        output logic        ddr_axi_rready_muir,
        input logic [31:0]  ddr_axi_rdata_muir,
        input logic [1:0]   ddr_axi_rresp_muir,
        input logic         ddr_axi_rlast_muir,
        input logic [5:0]   ddr_axi_rid_muir,
        //axi_ruser

        //-------------------------------------------------------------------------
        //DDR AXI - Taiga
        //-------------------------------------------------------------------------
        //--------------------------------------------- 
        // 'aw'
        //---------------------------------------------
        output logic        ddr_axi_awvalid_taiga,
        input logic         ddr_axi_awready_taiga,
        output logic [31:0] ddr_axi_awaddr_taiga,
        output logic [5:0]  ddr_axi_awid_taiga,
        //axi_awuser
        output logic [7:0]  ddr_axi_awlen_taiga,
        output logic [2:0]  ddr_axi_awsize_taiga,
        output logic [1:0]  ddr_axi_awburst_taiga,
        //output logic [0:0]ddr_axi_awlock,
        output logic [3:0]  ddr_axi_awcache_taiga,
        //output logic [2:0]ddr_axi_awprot,
        //output logic [3:0]ddr_axi_awqos,
        //output logic [3:0]ddr_axi_awregion,
        //--------------------------------------------- 
        // 'w'
        //---------------------------------------------
        output logic        ddr_axi_wvalid_taiga,
        input logic         ddr_axi_wready_taiga,
        output logic [31:0] ddr_axi_wdata_taiga/*verilator sc_bv*/,
        output logic [3:0]  ddr_axi_wstrb_taiga,
        output logic        ddr_axi_wlast_taiga,
        output logic [5:0]  ddr_axi_wid_taiga,
        //--------------------------------------------- 
        // 'b'
        //---------------------------------------------
        input logic         ddr_axi_bvalid_taiga,
        output logic        ddr_axi_bready_taiga,
        input logic [1:0]   ddr_axi_bresp_taiga,
        output logic [5:0]  ddr_axi_bid_taiga,
        //axi_buser
        //--------------------------------------------- 
        // 'ar'
        //---------------------------------------------        
        output logic        ddr_axi_arvalid_taiga,
        input logic         ddr_axi_arready_taiga,
        output logic [31:0] ddr_axi_araddr_taiga,
        output logic [5:0]  ddr_axi_arid_taiga,
        //axi_aruser
        output logic [7:0]  ddr_axi_arlen_taiga,
        output logic [2:0]  ddr_axi_arsize_taiga,
        output logic [1:0]  ddr_axi_arburst_taiga,
        //output logic [0:0]ddr_axi_arlock,
        output logic [3:0]  ddr_axi_arcache_taiga,
        //output logic [2:0]ddr_axi_arprot,
        //output logic [3:0]ddr_axi_arqos,
        //output logic [3:0]ddr_axi_arregion,
        //--------------------------------------------- 
        // 'r'
        //---------------------------------------------
        input logic         ddr_axi_rvalid_taiga,
        output logic        ddr_axi_rready_taiga,
        input logic [31:0]  ddr_axi_rdata_taiga/*verilator sc_bv*/,
        input logic [1:0]   ddr_axi_rresp_taiga,
        input logic         ddr_axi_rlast_taiga,
        input logic [5:0]   ddr_axi_rid_taiga,
        //axi_ruser

        // UART AXI
        output logic [31:0]bus_axi_araddr,
        output logic [1:0]bus_axi_arburst,
        output logic [3:0]bus_axi_arcache,
        output logic [5:0]bus_axi_arid,
        output logic [7:0]bus_axi_arlen,
        output logic [0:0]bus_axi_arlock,
        output logic [2:0]bus_axi_arprot,
        output logic [3:0]bus_axi_arqos,
        input logic bus_axi_arready,
        output logic [3:0]bus_axi_arregion,
        output logic [2:0]bus_axi_arsize,
        output logic bus_axi_arvalid,
        output logic [31:0]bus_axi_awaddr,
        output logic [1:0]bus_axi_awburst,
        output logic [3:0]bus_axi_awcache,
        output logic [5:0]bus_axi_awid,
        output logic [7:0]bus_axi_awlen,
        output logic [0:0]bus_axi_awlock,
        output logic [2:0]bus_axi_awprot,
        output logic [3:0]bus_axi_awqos,
        input logic bus_axi_awready,
        output logic [3:0]bus_axi_awregion,
        output logic [2:0]bus_axi_awsize,
        output logic bus_axi_awvalid,
        output logic [5:0]bus_axi_bid,
        output logic bus_axi_bready,
        input logic [1:0]bus_axi_bresp,
        input logic bus_axi_bvalid,
        input logic [31:0]bus_axi_rdata,
        output logic [5:0]bus_axi_rid,
        output logic bus_axi_rlast,
        output logic bus_axi_rready,
        input logic [1:0]bus_axi_rresp,
        input logic bus_axi_rvalid,
        output logic [31:0]bus_axi_wdata,
        output logic bus_axi_wlast,
        input logic bus_axi_wready,
        output logic [3:0]bus_axi_wstrb,
        output logic bus_axi_wvalid,
        output logic [5:0]bus_axi_wid,

        //L2 interface
        input logic [29:0] addr,
        input logic [3:0] be,
        input logic rnw,
        input logic is_amo,
        input logic [4:0] amo_type_or_burst_size,
        input logic [L2_SUB_ID_W-1:0] sub_id,

        input logic request_push,
        output logic request_full,

        output logic [31:2] inv_addr,
        output logic inv_valid,
        input logic inv_ack,

        output logic con_result,
        output logic con_valid,

        input logic [31:0] wr_data,
        input logic wr_data_push,
        output logic data_full,

        output logic [31:0] rd_data,
        output logic [L2_SUB_ID_W-1:0] rd_sub_id,
        output logic rd_data_valid,
        input logic rd_data_ack,

        //Local Memory
        output logic [29:0] instruction_bram_addr,
        output logic instruction_bram_en,
        output logic [3:0] instruction_bram_be,
        output logic [31:0] instruction_bram_data_in,
        input logic [31:0] instruction_bram_data_out,

        output logic [29:0] data_bram_addr,
        output logic data_bram_en,
        output logic [3:0] data_bram_be,
        output logic [31:0] data_bram_data_in,
        input logic [31:0] data_bram_data_out,

        //Used by verilator
        output logic write_uart,
        output logic [7:0] uart_byte,

        //Trace Interface
        output logic instruction_issued,
        output logic taiga_events [0:$bits(taiga_trace_events_t)-1],
        output logic [31:0] instruction_pc_dec,
        output logic [31:0] instruction_data_dec
    );

    logic [3:0] WRITE_COUNTER_MAX;
    logic [3:0] READ_COUNTER_MAX;
    assign READ_COUNTER_MAX = 4'b0101;
    assign WRITE_COUNTER_MAX = 4'b0101;

    //-------------------------------------------------------------------------
    //AXI DDR - Taiga
    //-------------------------------------------------------------------------
    //---------------------------------------------
    // 'aw'
    //---------------------------------------------
    logic [31:0]    axi_awaddr;
    logic [1:0]     axi_awburst;
    logic [3:0]     axi_awcache;
    logic [5:0]     axi_awid;
    logic [7:0]     axi_awlen;
    logic [0:0]     axi_awlock;
    logic [2:0]     axi_awprot;
    logic [3:0]     axi_awqos;
    logic           axi_awready;
    logic [3:0]     axi_awregion;
    logic [2:0]     axi_awsize;
    logic           axi_awvalid;
    //--------------------------------------------- 
    // 'w'
    //---------------------------------------------    
    logic [31:0]    axi_wdata;
    logic           axi_wlast;
    logic           axi_wready;
    logic [3:0]     axi_wstrb;
    logic           axi_wvalid;
    logic [5:0]     axi_wid;    
    //--------------------------------------------- 
    // 'b'
    //---------------------------------------------    
    logic [5:0]     axi_bid;
    logic           axi_bready;
    logic [1:0]     axi_bresp;
    logic           axi_bvalid;
    //--------------------------------------------- 
    // 'ar'
    //---------------------------------------------    
    logic [31:0]    axi_araddr;
    logic [1:0]     axi_arburst;
    logic [3:0]     axi_arcache;
    logic [5:0]     axi_arid;
    logic [7:0]     axi_arlen;
    logic [0:0]     axi_arlock;
    logic [2:0]     axi_arprot;
    logic [3:0]     axi_arqos;
    logic           axi_arready;
    logic [3:0]     axi_arregion;
    logic [2:0]     axi_arsize;
    logic           axi_arvalid;    
    //--------------------------------------------- 
    // 'r'
    //---------------------------------------------    
    logic [31:0]    axi_rdata;
    logic [5:0]     axi_rid;
    logic           axi_rlast;
    logic           axi_rready;
    logic [1:0]     axi_rresp;
    logic           axi_rvalid;
    //-------------------------------------------------------------------------
    //AXI DDR - muIR
    //-------------------------------------------------------------------------
    //--------------------------------------------- 
    // 'aw'
    //---------------------------------------------
    logic           axi_awvalid_muir;
    logic           axi_awready_muir;
    logic [31:0]    axi_awaddr_muir;
    logic [5:0]     axi_awid_muir;
    logic [9:0]     axi_awuser_muir;
    logic [7:0]     axi_awlen_muir;
    logic [2:0]     axi_awsize_muir;
    logic [1:0]     axi_awburst_muir;
    logic [1:0]     axi_awlock_muir;
    logic [3:0]     axi_awcache_muir;
    logic [2:0]     axi_awprot_muir;
    logic [3:0]     axi_awqos_muir;
    logic [3:0]     axi_awregion_muir;
    //--------------------------------------------- 
    // 'w'
    //---------------------------------------------    
    logic           axi_wvalid_muir;
    logic           axi_wready_muir;
    logic [31:0]    axi_wdata_muir;
    logic [3:0]     axi_wstrb_muir;
    logic           axi_wlast_muir;
    logic [5:0]     axi_wid_muir;  
    logic [9:0]     axi_wuser_muir;
    //--------------------------------------------- 
    // 'b'
    //---------------------------------------------
    logic           axi_bvalid_muir;
    logic           axi_bready_muir;
    logic [1:0]     axi_bresp_muir;
    logic [5:0]     axi_bid_muir;
    logic [9:0]     axi_buser_muir;
    //--------------------------------------------- 
    // 'ar'
    //---------------------------------------------    
    logic           axi_arvalid_muir;    
    logic           axi_arready_muir;
    logic [31:0]    axi_araddr_muir;
    logic [5:0]     axi_arid_muir;
    logic [9:0]     axi_aruser_muir;
    logic [7:0]     axi_arlen_muir;
    logic [2:0]     axi_arsize_muir;
    logic [1:0]     axi_arburst_muir;
    logic [1:0]     axi_arlock_muir;
    logic [3:0]     axi_arcache_muir;
    logic [2:0]     axi_arprot_muir;
    logic [3:0]     axi_arqos_muir;
    logic [3:0]     axi_arregion_muir;
    //--------------------------------------------- 
    // 'r'
    //---------------------------------------------    
    logic           axi_rvalid_muir;
    logic           axi_rready_muir;
    logic [31:0]    axi_rdata_muir;
    logic [1:0]     axi_rresp_muir;
    logic           axi_rlast_muir;
    logic [5:0]     axi_rid_muir;
    logic [9:0]     axi_ruser_muir;

    parameter SCRATCH_MEM_KB = 128;
    parameter MEM_LINES = (SCRATCH_MEM_KB*1024)/4;

    logic interrupt;
    logic timer_interrupt;

    assign interrupt = 0;

    axi_interface m_axi();
    axi_interface uart_axi();
    axi_interface muir_axi2();
    avalon_interface m_avalon();
    wishbone_interface m_wishbone();

    trace_outputs_t tr;

    l2_requester_interface l2[L2_NUM_PORTS-1:0]();
    l2_memory_interface mem();

    local_memory_interface instruction_bram();
    local_memory_interface data_bram();

    assign l2[1].request_push = 0;
    assign l2[1].wr_data_push = 0;
    assign l2[1].inv_ack = l2[1].inv_valid;
    assign l2[1].rd_data_ack = l2[1].rd_data_valid;

    axi_to_arb l2_to_mem (.*, .l2(mem));
    l2_arbiter l2_arb (.*, .request(l2));

    assign instruction_bram_addr = instruction_bram.addr;
    assign instruction_bram_en = instruction_bram.en;
    assign instruction_bram_be = instruction_bram.be;
    assign instruction_bram_data_in = instruction_bram.data_in;
    assign instruction_bram.data_out = instruction_bram_data_out;

    assign data_bram_addr = data_bram.addr;
    assign data_bram_en = data_bram.en;
    assign data_bram_be = data_bram.be;
    assign data_bram_data_in = data_bram.data_in;
    assign data_bram.data_out = data_bram_data_out;

    taiga cpu(.*, .m_axi(m_axi), .l2(l2[0]));

    //AXI BUS
    //configuration:
    localparam xbar_cfg_t taiga_axi_bus_cfg = '{
        NoSlvPorts:         2,
        NoMstPorts:         2,
        MaxMstTrans:        1,
        MaxSlvTrans:        1,
        FallThrough:        1'b1,
        LatencyMode:        CUT_ALL_AX,
        AxiIdWidthSlvPorts: 6,
        AxiIdUsedSlvPorts:  6,
        AxiAddrWidth:       32,
        AxiDataWidth:       32,
        NoAddrRules:        2
    }; 
    //UART Peripheral MAP
    xbar_rule_32_t uart_rule_map;
    assign uart_rule_map.idx = 0;
    assign uart_rule_map.start_addr =   32'h60001000; //32'h60000000;
    assign uart_rule_map.end_addr =     32'h60002000;

    //MUIR Peripheral MAP
    xbar_rule_32_t muir_rule_map;
    assign muir_rule_map.idx = 1;
    assign muir_rule_map.start_addr = 32'h60002000; //32'h61000000;
    assign muir_rule_map.end_addr = 32'h60002FFF;

    xbar_rule_32_t[taiga_axi_bus_cfg.NoMstPorts-1:0] xbar_rule_array;
    assign xbar_rule_array[0] = uart_rule_map;
    assign xbar_rule_array[1] = muir_rule_map;
    //Helper Functions ///////////////////////////////////////////////////
    //Function: unit_to_bus_translate
    //Input: Taiga's AXI Interface
    //Output: Bus Compatible Interace
    typedef logic [31:0]   addr_t;
    typedef logic [31:0]   data_t;
    typedef logic [32/8-1:0] strb_t;
    typedef logic [5:0] id_t;
    `AXI_TYPEDEF_AW_CHAN_T(aw_chan_t, addr_t, id_t, logic)
    `AXI_TYPEDEF_W_CHAN_T(w_chan_t, data_t, strb_t, logic)
    `AXI_TYPEDEF_B_CHAN_T(b_chan_t, id_t, logic)
    `AXI_TYPEDEF_AR_CHAN_T(ar_chan_t, addr_t, id_t, logic)
    `AXI_TYPEDEF_R_CHAN_T(r_chan_t, data_t, id_t, logic)
    `AXI_TYPEDEF_REQ_T(req_t, aw_chan_t, w_chan_t, ar_chan_t)
    `AXI_TYPEDEF_RESP_T(resp_t, b_chan_t, r_chan_t)

    `define unit_to_bus_translate(m_axi, output_req, output_resp)\
        assign output_req.aw_valid = m_axi.awvalid;\
        assign output_req.w_valid =  m_axi.wvalid;\
        assign output_req.b_ready = m_axi.bready;\
        assign output_req.ar_valid = m_axi.arvalid;\
        assign output_req.r_ready = m_axi.rready;\
        //AW \
        assign output_req.aw.id = m_axi.awid;\
        assign output_req.aw.addr = m_axi.awaddr;\
        assign output_req.aw.len = m_axi.awlen;\
        assign output_req.aw.size = m_axi.awsize;\
        assign output_req.aw.burst = m_axi.awburst;\
        //assign output_req.aw.lock = m_axi.awlock;\
        //assign output_req.aw.cache = m_axi.awcache;\
        // assign output_req.aw.prot = m_axi.awprot;\
        // assign output_req.aw.qos = m_axi.awqos;\
        // assign output_req.aw.region = m_axi.awregion;\
        // assign output_req.aw.atop = m_axi.awatop;\
        // assign output_req.aw.user = m_axi.awuser;\
        //W\
        assign output_req.w.data = m_axi.wdata;\
        assign output_req.w.strb = m_axi.wstrb;\
        assign output_req.w.last = m_axi.wlast;\
        //assign output_req.w.user = m_axi.wuser;\
        //AR\
        assign output_req.ar.id = m_axi.arid;\
        assign output_req.ar.addr = m_axi.araddr;\
        assign output_req.ar.len = m_axi.arlen;\
        assign output_req.ar.size = m_axi.arsize;\
        assign output_req.ar.burst = m_axi.arburst;\
        // assign output_req.ar.lock = m_axi.arlock;\
        // assign output_req.ar.cache = m_axi.arcache;\
        // assign output_req.ar.prot = m_axi.arprot;\
        // assign output_req.ar.qos = m_axi.arqos;\
        // assign output_req.ar.region = m_axi.arregion;\
        // assign output_req.ar.user = m_axi.aruser;\
        //BRESP\
        assign m_axi.bid = output_resp.b.id;\
        assign m_axi.bresp = output_resp.b.resp;\
        //assign m_axi.buser = output_resp.b.user;\
        assign m_axi.rid = output_resp.r.id;\
        assign m_axi.rdata = output_resp.r.data;\
        assign m_axi.rresp = output_resp.r.resp;\
        assign m_axi.rlast = output_resp.r.last;\
        //assign m_axi.ruser = output_resp.r.user;\
        assign m_axi.awready = output_resp.aw_ready;\
        assign m_axi.arready = output_resp.ar_ready;\
        assign m_axi.wready = output_resp.w_ready;\
        assign m_axi.bvalid = output_resp.b_valid;\
        assign m_axi.rvalid = output_resp.r_valid;
    
    `define bus_to_unit_translate(output_req, output_resp, m_axi)\
        assign m_axi.awvalid = output_req.aw_valid;\
        assign m_axi.wvalid = output_req.w_valid;\
        assign m_axi.bready = output_req.b_ready;\
        assign m_axi.arvalid = output_req.ar_valid;\
        assign m_axi.rready = output_req.r_ready;\
        //AW \
        assign m_axi.awid = output_req.aw.id;\
        assign m_axi.awaddr = output_req.aw.addr;\
        assign m_axi.awlen = output_req.aw.len;\
        assign m_axi.awsize = output_req.aw.size;\
        assign m_axi.awburst = output_req.aw.burst;\
        //assign m_axi.awlock = output_req.aw.lock;\
        //assign m_axi.awcache = output_req.aw.cache;\
        //assign m_axi.awprot = output_req.aw.prot;\
        //assign m_axi.awqos = output_req.aw.qos;\
        //assign m_axi.awregion = output_req.aw.region;\
        //assign m_axi.awatop = output_req.aw.atop;\
        //assign m_axi.awuser = output_req.aw.user;\
        //W\
        assign m_axi.wdata = output_req.w.data;\
        assign m_axi.wstrb = output_req.w.strb;\
        assign m_axi.wlast = output_req.w.last;\
        //assign m_axi.wuser = output_req.w.user;\
        //AR\
        assign m_axi.arid = output_req.ar.id;\
        assign m_axi.araddr = output_req.ar.addr;\
        assign m_axi.arlen = output_req.ar.len;\
        assign m_axi.arsize = output_req.ar.size;\
        assign m_axi.arburst = output_req.ar.burst;\
        //assign m_axi.arlock = output_req.ar.lock;\
        //assign m_axi.arcache = output_req.ar.cache;\
        //assign m_axi.arprot = output_req.ar.prot;\
        //assign m_axi.arqos = output_req.ar.qos;\
        //assign m_axi.arregion = output_req.ar.region;\
        //assign m_axi.aruser = output_req.ar.user;\
        //BRESP\
        assign output_resp.b.id = m_axi.bid;\
        assign output_resp.b.resp = m_axi.bresp;\
        //assign output_resp.b.user = m_axi.buser;\
        assign output_resp.r.id = m_axi.rid;\
        assign output_resp.r.data = m_axi.rdata;\
        assign output_resp.r.resp = m_axi.rresp;\
        assign output_resp.r.last = m_axi.rlast;\
        //assign output_resp.r.user = m_axi.ruser;\
        assign output_resp.aw_ready = m_axi.awready;\
        assign output_resp.ar_ready = m_axi.arready;\
        assign output_resp.w_ready = m_axi.wready;\
        assign output_resp.b_valid = m_axi.bvalid;\
        assign output_resp.r_valid = m_axi.rvalid;

    req_t [taiga_axi_bus_cfg.NoSlvPorts-1:0] slv_ports_req_array;
    resp_t [taiga_axi_bus_cfg.NoSlvPorts-1:0] slv_ports_resp_array;
    req_t [taiga_axi_bus_cfg.NoMstPorts-1:0]   mst_ports_req;
    resp_t [taiga_axi_bus_cfg.NoMstPorts-1:0]  mst_ports_resp;
    `unit_to_bus_translate(m_axi, slv_ports_req_array[0], slv_ports_resp_array[0]);
    `bus_to_unit_translate(mst_ports_req[0], mst_ports_resp[0], uart_axi);
    `bus_to_unit_translate(mst_ports_req[1], mst_ports_resp[1], muir_axi2);
    assign uart_axi.arready = bus_axi_arready;
    assign bus_axi_arvalid = uart_axi.arvalid;
    assign bus_axi_araddr = uart_axi.araddr;

    //read data
    assign bus_axi_rready = uart_axi.rready;
    assign uart_axi.rvalid = bus_axi_rvalid;
    assign uart_axi.rdata = bus_axi_rdata;
    assign uart_axi.rresp = bus_axi_rresp;

    //Write channel
    //write address
    assign uart_axi.awready = bus_axi_awready;
    assign bus_axi_awaddr = uart_axi.awaddr;
    assign bus_axi_awvalid = uart_axi.awvalid;


    //write data
    assign uart_axi.wready = bus_axi_wready;
    assign bus_axi_wvalid = uart_axi. wvalid;
    assign bus_axi_wdata = uart_axi.wdata;
    assign bus_axi_wstrb = uart_axi.wstrb;

    //write response
    assign bus_axi_bready = uart_axi.bready;
    assign uart_axi.bvalid = bus_axi_bvalid;
    assign uart_axi.bresp = bus_axi_bresp;

    ////////////////////////////////////////////////////////////////////
    axi_lite_xbar #(
    .Cfg      ( taiga_axi_bus_cfg ),
    .aw_chan_t( aw_chan_t ),
    .w_chan_t ( w_chan_t     ),
    .b_chan_t ( b_chan_t ),
    .ar_chan_t( ar_chan_t ),
    .r_chan_t ( r_chan_t ),
    .req_t    ( req_t     ),
    .resp_t   ( resp_t    ),
    .rule_t   ( xbar_rule_32_t )
    ) taiga_axi_bus(
        .clk_i(clk),
        .rst_ni(~rst),
        .test_i(0),
        .slv_ports_req_i(slv_ports_req_array),
        .slv_ports_resp_o(slv_ports_resp_array),
        .mst_ports_req_o(mst_ports_req),
        .mst_ports_resp_i(mst_ports_resp),
        .addr_map_i(xbar_rule_array),
        .en_default_mst_port_i(0),
        .default_mst_port_i(0)   
     );

/*
    //read channel
    logic[3:0] read_counter;
    logic begin_read_counter;

    always_ff @(posedge clk) begin
        if (rst) begin
            m_axi.rvalid <= 0;
            m_axi.arready <= 1; //You want it to start at ready
            m_axi.rresp <= 0;
            read_counter <= READ_COUNTER_MAX;
        end
        else begin
            if(m_axi.arready == 1 && m_axi.arvalid == 1) begin
                m_axi.arready <= 0;
                begin_read_counter <= 1;
                m_axi.rdata <= 32'hFFFFFF21;
            end

            if(begin_read_counter) begin
                if(read_counter == 0) begin
                    m_axi.rvalid <= 1;
                    m_axi.arready <= 1;
                    read_counter <= READ_COUNTER_MAX;
                    begin_read_counter <= 0;
                end
                else begin
                    read_counter <= read_counter - 1;
                    m_axi.rvalid <= 0;
                end
            end

            if(m_axi.rvalid &&  m_axi.rready) begin
                m_axi.rvalid <= 0;
            end

        end
    end

    //Write channel
    //write address
    logic[3:0] write_counter;
    logic begin_write_counter;

    always_ff @(posedge clk) begin
        if (rst) begin
            m_axi.wready <= 0;
            m_axi.awready <= 1; //You want it to start at ready
            m_axi.bresp <= 0;
            write_counter <= WRITE_COUNTER_MAX;
        end
        else begin
            if(m_axi.awready == 1 && m_axi.awvalid == 1) begin
                m_axi.awready <= 0;
                begin_write_counter <= 1;
            end

            if(begin_write_counter) begin
                if(write_counter == 0) begin
                    m_axi.awready <= 1;
                    m_axi.wready <= 1;
                    write_counter <= WRITE_COUNTER_MAX;
                    begin_write_counter <= 0;
                end
                else begin
                    write_counter <= write_counter - 1;
                    m_axi.wready <= 0;
                end
            end

            if(m_axi.bready == 1 && m_axi.wready) begin
                m_axi.bvalid <= 1;
                m_axi.bresp <= 0;
            end
            else begin
                m_axi.bvalid <= 0;
                m_axi.bresp <= 0;
            end

            if(m_axi.wready & m_axi.wvalid) begin
                m_axi.wready <= 0;
            end
        end
    end

    initial begin
        write_uart = 0;
        uart_byte = 0;
    end
    //Capture writes to UART
    always_ff @(posedge clk) begin
        write_uart <= (m_axi.wvalid && m_axi.wready && m_axi.awaddr[13:0] == 4096);
        uart_byte <= m_axi.wdata[7:0];
    end
*/

    TaigaShell cus_acc(
        .ap_clk(clk),
        .ap_rst_n(rst),
        
        .cl_axi_mstr_bus_AWVALID(axi_awvalid_muir),
        .cl_axi_mstr_bus_AWREADY(axi_awready_muir),
        .cl_axi_mstr_bus_AWADDR(axi_awaddr_muir),
        .cl_axi_mstr_bus_AWID(axi_awid_muir),
        .cl_axi_mstr_bus_AWUSER(axi_awuser_muir),
        .cl_axi_mstr_bus_AWLEN(axi_awlen_muir),
        .cl_axi_mstr_bus_AWSIZE(axi_awsize_muir),
        .cl_axi_mstr_bus_AWBURST(axi_awburst_muir),
        .cl_axi_mstr_bus_AWLOCK(axi_awlock_muir),
        .cl_axi_mstr_bus_AWCACHE(axi_awcache_muir),
        .cl_axi_mstr_bus_AWPROT(axi_awprot_muir),
        .cl_axi_mstr_bus_AWQOS(axi_awqos_muir),
        .cl_axi_mstr_bus_AWREGION(axi_awregion_muir),
        .cl_axi_mstr_bus_WVALID(axi_wvalid_muir),
        .cl_axi_mstr_bus_WREADY(axi_wready_muir),
        .cl_axi_mstr_bus_WDATA(axi_wdata_muir),
        .cl_axi_mstr_bus_WSTRB(axi_wstrb_muir),
        .cl_axi_mstr_bus_WLAST(axi_wlast_muir),
        .cl_axi_mstr_bus_WID(axi_wid_muir),
        .cl_axi_mstr_bus_WUSER(axi_wuser_muir),
        .cl_axi_mstr_bus_BVALID(axi_bvalid_muir),
        .cl_axi_mstr_bus_BREADY(axi_bready_muir),
        .cl_axi_mstr_bus_BRESP(axi_bresp_muir),
        .cl_axi_mstr_bus_BID(axi_bid_muir),
        .cl_axi_mstr_bus_BUSER(axi_buser_muir),
        .cl_axi_mstr_bus_ARVALID(axi_arvalid_muir),
        .cl_axi_mstr_bus_ARREADY(axi_arready_muir),
        .cl_axi_mstr_bus_ARADDR(axi_araddr_muir),
        .cl_axi_mstr_bus_ARID(axi_arid_muir),
        .cl_axi_mstr_bus_ARUSER(axi_aruser_muir),
        .cl_axi_mstr_bus_ARLEN(axi_arlen_muir),
        .cl_axi_mstr_bus_ARSIZE(axi_arsize_muir),
        .cl_axi_mstr_bus_ARBURST(axi_arburst_muir),
        .cl_axi_mstr_bus_ARLOCK(axi_arlock_muir),
        .cl_axi_mstr_bus_ARCACHE(axi_arcache_muir),
        .cl_axi_mstr_bus_ARPROT(axi_arprot_muir),
        .cl_axi_mstr_bus_ARQOS(axi_arqos_muir),
        .cl_axi_mstr_bus_ARREGION(axi_arregion_muir),
        .cl_axi_mstr_bus_RVALID(axi_rvalid_muir),
        .cl_axi_mstr_bus_RREADY(axi_rready_muir),
        .cl_axi_mstr_bus_RDATA(axi_rdata_muir),
        .cl_axi_mstr_bus_RRESP(axi_rresp_muir),
        .cl_axi_mstr_bus_RLAST(axi_rlast_muir),
        .cl_axi_mstr_bus_RID(axi_rid_muir),
        .cl_axi_mstr_bus_RUSER(axi_ruser_muir),
        
        .axi_mstr_cfg_bus_AWVALID(muir_axi2.awvalid),
        .axi_mstr_cfg_bus_AWREADY(muir_axi2.awready),
        .axi_mstr_cfg_bus_AWADDR({8'b0, muir_axi2.awaddr[7:0]}),
        .axi_mstr_cfg_bus_WVALID(muir_axi2.wvalid),
        .axi_mstr_cfg_bus_WREADY(muir_axi2.wready),
        .axi_mstr_cfg_bus_WDATA(muir_axi2.wdata),
        .axi_mstr_cfg_bus_BVALID(muir_axi2.bvalid),
        .axi_mstr_cfg_bus_BREADY(muir_axi2.bready),
        .axi_mstr_cfg_bus_ARVALID(muir_axi2.arvalid),
        .axi_mstr_cfg_bus_ARREADY(muir_axi2.arready),
        .axi_mstr_cfg_bus_ARADDR({8'b0, muir_axi2.araddr[7:0]}),
        .axi_mstr_cfg_bus_RVALID(muir_axi2.rvalid),
        .axi_mstr_cfg_bus_RREADY(muir_axi2.rready),
        .axi_mstr_cfg_bus_RDATA(muir_axi2.rdata)
    );

    ////////////////////////////////////////////////////
    //DDR AXI interface - Taiga
    //--------------------------------------------- 
    // 'aw'
    //---------------------------------------------    
    assign ddr_axi_awaddr_taiga = axi_awaddr;
    assign ddr_axi_awburst_taiga = axi_awburst;
    assign ddr_axi_awcache_taiga = axi_awcache;
    assign ddr_axi_awid_taiga = axi_awid;
    assign ddr_axi_awlen_taiga =  axi_awlen;
    assign axi_awready = ddr_axi_awready_taiga;
    assign ddr_axi_awvalid_taiga = axi_awvalid;
    //--------------------------------------------- 
    // 'w'
    //---------------------------------------------
    assign ddr_axi_wdata_taiga = axi_wdata;
    assign ddr_axi_wlast_taiga = axi_wlast;
    assign axi_wready = ddr_axi_wready_taiga;
    assign ddr_axi_wstrb_taiga = axi_wstrb;
    assign ddr_axi_wvalid_taiga = axi_wvalid;
    //--------------------------------------------- 
    // 'b'
    //---------------------------------------------    
    assign axi_bid = ddr_axi_bid_taiga;
    assign ddr_axi_bready_taiga = axi_bready;
    assign axi_bresp = ddr_axi_bresp_taiga;
    assign axi_bvalid = ddr_axi_bvalid_taiga;
    //--------------------------------------------- 
    // 'ar'
    //---------------------------------------------
    assign ddr_axi_araddr_taiga = axi_araddr;
    assign ddr_axi_arburst_taiga = axi_arburst;
    assign ddr_axi_arcache_taiga = axi_arcache;
    assign ddr_axi_arid_taiga = axi_arid;
    assign ddr_axi_arlen_taiga = axi_arlen;
    assign axi_arready = ddr_axi_arready_taiga;
    assign ddr_axi_arsize_taiga = axi_arsize;
    assign ddr_axi_arvalid_taiga = axi_arvalid;
    //--------------------------------------------- 
    // 'r'
    //---------------------------------------------
    assign axi_rdata = ddr_axi_rdata_taiga;
    assign axi_rid = ddr_axi_rid_taiga;
    assign axi_rlast = ddr_axi_rlast_taiga;
    assign ddr_axi_rready_taiga = axi_rready;
    assign axi_rresp = ddr_axi_rresp_taiga;
    assign axi_rvalid = ddr_axi_rvalid_taiga;
    ////////////////////////////////////////////////////
    //DDR AXI interface - muIR
    //--------------------------------------------- 
    // 'aw'
    //---------------------------------------------
    assign ddr_axi_awvalid_muir = axi_awvalid_muir;
    assign axi_awready_muir = ddr_axi_awready_muir;
    assign ddr_axi_awaddr_muir = axi_awaddr_muir;
    assign ddr_axi_awid_muir = axi_awid_muir;
    assign ddr_axi_awlen_muir = axi_awlen_muir;
    assign ddr_axi_awsize_muir = axi_awsize_muir;
    assign ddr_axi_awburst_muir = axi_awburst_muir;
    assign ddr_axi_awcache_muir = axi_awcache_muir;
    //--------------------------------------------- 
    // 'w'
    //---------------------------------------------
    assign ddr_axi_wvalid_muir = axi_wvalid_muir;
    assign axi_wready_muir = ddr_axi_wready_muir;
    assign ddr_axi_wdata_muir = axi_wdata_muir;
    assign ddr_axi_wstrb_muir = axi_wstrb_muir;
    assign ddr_axi_wlast_muir = axi_wlast_muir;
    assign ddr_axi_wid_muir = axi_wid_muir;
    //--------------------------------------------- 
    // 'b'
    //---------------------------------------------
    assign axi_bvalid_muir = ddr_axi_bvalid_muir;
    assign ddr_axi_bready_muir = axi_bready_muir;
    assign axi_bresp_muir = ddr_axi_bresp_muir;
    assign ddr_axi_bid_muir = axi_bid_muir;    
    //--------------------------------------------- 
    // 'ar'
    //---------------------------------------------
    assign ddr_axi_arvalid_muir = axi_arvalid_muir;
    assign axi_arready_muir = ddr_axi_arready_muir;
    assign ddr_axi_araddr_muir = axi_araddr_muir;
    assign ddr_axi_arid_muir = axi_arid_muir;
    assign ddr_axi_arlen_muir = axi_arlen_muir;
    assign ddr_axi_arsize_muir = axi_arsize_muir;
    assign ddr_axi_arburst_muir = axi_arburst_muir;
    assign ddr_axi_arcache_muir = axi_arcache_muir;
    //--------------------------------------------- 
    // 'r'
    //---------------------------------------------
    assign axi_rvalid_muir = ddr_axi_rvalid_muir;
    assign ddr_axi_rready_muir = axi_rready_muir;
    assign axi_rdata_muir = ddr_axi_rdata_muir;
    assign axi_rresp_muir = ddr_axi_rresp_muir;
    assign axi_rlast_muir = ddr_axi_rlast_muir;
    assign axi_rid_muir = ddr_axi_rid_muir;


    ////////////////////////////////////////////////////
    //Trace Interface
    assign instruction_pc_dec = tr.instruction_pc_dec;
    assign instruction_data_dec = tr.instruction_data_dec;
    assign instruction_issued = tr.events.instruction_issued_dec;
    logic [$bits(taiga_trace_events_t)-1:0] taiga_events_packed;
    assign taiga_events_packed = tr.events;
    always_comb begin
        foreach(taiga_events_packed[i])
            taiga_events[$bits(taiga_trace_events_t)-1-i] = taiga_events_packed[i];
    end

endmodule
