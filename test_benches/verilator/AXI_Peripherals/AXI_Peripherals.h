/*
 * Copyright © 2019 Eric Matthews,Zavier Aguila  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 *			   Zavier Aguila <zaguila@sfu.ca>
 */

#ifndef AXI_PERIPHERAL_H
#define AXI_PERIPHERAL_H
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <random>
#include <queue>
#include "../AXI_DDR_simulation/axi_interface.h"
using namespace std;

#define UART_ADDRESS 0x60000000
#define UART_REG_ADDRESS (UART_ADDRESS + 0x1000)

#define UART_RBR_OFFSET 0 /**< Receive buffer, read only */
#define UART_THR_OFFSET 1 /**< Transmit holding register */
#define UART_IER_OFFSET 2 /**< Interrupt enable */
#define UART_IIR_OFFSET 3 /**< Interrupt id, read only */
#define UART_FCR_OFFSET 3 /**< Fifo control, write only */
#define UART_LCR_OFFSET 4 /**< Line Control Register */
#define UART_MCR_OFFSET 5 /**< Modem Control Register */
#define UART_LSR_OFFSET 6 /**< Line Status Register */
#define UART_MSR_OFFSET 7 /**< Modem Status Regsiter */
#define UART_SCR_OFFSET 8 /**< Screatch Register */

#define UART_LSR_TX_EMPTY                0x00000040 /**< Transmitter is empty */
#define UART_LSR_TX_BUFFER_EMPTY         0x00000020 /**< Transmit holding reg empty */
#define UART_LSR_DATA_READY              0x00000001 /**< Receive data ready */

template <class TB>
 class axi_peripheral_sim{
 	public:
	//Functions--------------------------------------------------------------
 	axi_peripheral_sim();
 	axi_peripheral_sim(TB * tb);
 	axi_peripheral_sim(TB * tb, string filename);
 	void open_new_uart_file(string filename);
	void step();
 	int get_data(uint32_t data_address);
 	void set_data(uint32_t data_address, uint32_t set_data, uint32_t byte_enable);

	private:
		default_random_engine generator;
		TB *tb;
		fstream input_file;
		bool from_file;
 		void parse_output_signals();
 		void parse_input_signals();
		void update_current_read_parameters();
		void update_current_write_parameters();
		void handle_write_req();
 		void handle_read_req();
		void handle_UART();
 		addr_calculation_parameters current_read_parameters{0,0,0,0};
 		addr_calculation_parameters current_write_parameters{0,0,0,0};
		//Read Request Queue
		queue<AXI_read_address_channel_signals> rd_ad_channel_queue;
		//Write Request Queue
		queue<AXI_write_address_channel_signals> wd_ad_channel_queue;
		//Read Data Queue
		queue<AXI_read_data_channel_signals> r_data_channel_queue;
		//Write Data Queue
		queue<AXI_write_data_channel_signals> w_data_channel_queue;
		//Write Response Queue
		queue<AXI_write_response_channel_signals> w_res_channel_queue;

		//Peripheral: UART
		//[0]: RBR
		//[1]: THR
		//[2]: Interrupt
		//[3]: FIFO COntrol
		//[4]: Line Control Register
		//[5]: Modem Control Register
		//[6]: Line Status Register
		//[7]: Modem Status Register
		//[8]: Scratch Regiser
		uint32_t uart_registers[9] = {0, 0x000000FF, 0, 1, 0, 0, 0x00000060, 0, 0};
};

#include "AXI_Peripherals.cc"
#endif
