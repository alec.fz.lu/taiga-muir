#include <stdint.h>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <assert.h>
using namespace	 std;

template <class TB>
axi_peripheral_sim<TB>::axi_peripheral_sim(){
    this->tb = NULL;
    from_file = false;
}
template <class TB>
axi_peripheral_sim<TB>::axi_peripheral_sim(TB * tb){
    this->tb = tb;
    from_file = false;
}

template <class TB>
axi_peripheral_sim<TB>::axi_peripheral_sim(TB * tb, string filename){
    this->tb = tb;
    input_file.open(filename);
    from_file = true;
}

template <class TB>
void axi_peripheral_sim<TB>::open_new_uart_file(string filename){
    input_file.open(filename);
    from_file = true;
}

template <class TB>
int axi_peripheral_sim<TB>::get_data(uint32_t data_address){
	if(data_address >= UART_REG_ADDRESS && data_address <= UART_REG_ADDRESS + 0x1C){ //UART
        if(data_address == UART_REG_ADDRESS){
            uart_registers[UART_LSR_OFFSET] = (uart_registers[UART_LSR_OFFSET] & ~UART_LSR_DATA_READY) | (0 & UART_LSR_DATA_READY); //Unset Data ready because we have now read the data
            return uart_registers[(data_address%UART_REG_ADDRESS)/4]; //When the AXI Reads from here it is from the RBR
        }
        else if (data_address == (UART_REG_ADDRESS + 0x14)) { //Lines Status Register
            return uart_registers[(data_address%UART_REG_ADDRESS)/4 + 1];
        }
        else{
            cerr << "[ERROR] Read: ADDRESS " << data_address <<" UART Register not implemented" << endl;
            return 0;
        }
    }
    //else if (In Range of Peripheral)
    //  Read from Peripheral Registers
	else{
		cerr << "[ERROR] Read: ADDRESS " << data_address <<"  WAS NOT WITHIN USED RANGE" << endl;
        return 0;
    }
}

template <class TB>
void  axi_peripheral_sim<TB>::set_data(uint32_t data_address, uint32_t set_data, uint32_t byte_enable){
    if(data_address >= UART_REG_ADDRESS && data_address <= UART_REG_ADDRESS + 0x1C){ //UART
        uint32_t data;
        data = uart_registers[(data_address%UART_REG_ADDRESS)/4+1];
    	data = (data & ~byte_enable) | (set_data & byte_enable);
    	uart_registers[(data_address%UART_REG_ADDRESS)/4+1] = set_data;

        if(data_address == UART_REG_ADDRESS)
            uart_registers[UART_LSR_OFFSET] = (uart_registers[UART_LSR_OFFSET] & ~UART_LSR_TX_BUFFER_EMPTY) | (0 & UART_LSR_TX_BUFFER_EMPTY);
    }
    //else if (In Range of Peripheral)
    //  Read from Peripheral Registers
    else{
        cerr << "[ERROR] Write: ADDRESS " << data_address <<"  WAS NOT WITHIN USED RANGE" << endl;
    }
};

template <class TB>
void axi_peripheral_sim<TB>::parse_input_signals(){
    //If the master has a write requests
    if(tb->bus_axi_awvalid){
        //cout << "AW REQ" << endl;
        AXI_write_address_channel_signals elem{tb->bus_axi_awaddr, tb->bus_axi_awlen, tb->bus_axi_awsize, tb->bus_axi_awburst,tb->bus_axi_awcache,tb->bus_axi_awid};
        wd_ad_channel_queue.push(elem);
    }
    //If the master has write data
    if(tb->bus_axi_wvalid){
        //cout << "W REQ" << endl;
        AXI_write_data_channel_signals elem{tb->bus_axi_wid, tb->bus_axi_wdata, tb->bus_axi_wstrb, tb->bus_axi_wlast};
        w_data_channel_queue.push(elem);
    }
    //If the master has a read request
     if(tb->bus_axi_arvalid){
        //cout << "AR REQ" << endl;
        AXI_read_address_channel_signals elem{tb->bus_axi_araddr, tb->bus_axi_arlen, tb->bus_axi_arsize, tb->bus_axi_arburst, tb->bus_axi_arcache, tb->bus_axi_arid};
        rd_ad_channel_queue.push(elem);
     }
}
template <class TB>
void axi_peripheral_sim<TB>::parse_output_signals(){
    tb->bus_axi_wready = 1;

    //Write Req
    tb->bus_axi_awready = 1;  
    //Read Req
    tb->bus_axi_arready = 1;

    //If we the write_response
    if(w_res_channel_queue.size() > 0){
        AXI_write_response_channel_signals elem = w_res_channel_queue.front();
        if(tb->bus_axi_bready)
            w_res_channel_queue.pop();
        
        tb->bus_axi_bid = elem.bid;
        tb->bus_axi_bresp = elem.bresp;
        tb->bus_axi_bvalid = 1;
    }
    else{
        tb->bus_axi_bid = rand();
        tb->bus_axi_bresp = rand();
        //tb->bus_axi_buser = rand();
        tb->bus_axi_bvalid = 0;
    }
    //If we have the read data
    if(r_data_channel_queue.size() > 0){
        AXI_read_data_channel_signals elem = r_data_channel_queue.front();
        if(tb->bus_axi_rready){
            //cout << "Before: " << r_data_channel_queue.size() << endl;
            r_data_channel_queue.pop();
            //cout << "After: " << r_data_channel_queue.size() << endl;
        }
        tb->bus_axi_rid = elem.rid;
        tb->bus_axi_rdata = elem.rdata[0];
        tb->bus_axi_rresp = elem.rresp;
        tb->bus_axi_rlast = elem.rlast;
        //tb->bus_axi_ruser = elem.ruser;
        tb->bus_axi_rvalid = 1;
    }
    else{
        tb->bus_axi_rid = rand();
        tb->bus_axi_rdata = rand();
        tb->bus_axi_rresp = 0;
        tb->bus_axi_rlast = 0;
        //tb->bus_axi_ruser = rand();
        tb->bus_axi_rvalid = 0;
    }
}
template <class TB>
void axi_peripheral_sim<TB>::handle_read_req(){
    if(rd_ad_channel_queue.size() > 0 ){
        if(current_read_parameters.delay_cycles_left == 0){
            AXI_read_data_channel_signals elem;
            
            elem.rid = rd_ad_channel_queue.front().arid;
            elem.rdata[0] =  get_data(current_read_parameters.address);
            current_read_parameters.number_of_bursts_left--;

            if(rd_ad_channel_queue.front().arburst == 0 ){//FIXED
                //do nothing
            }
            else if(rd_ad_channel_queue.front().arburst == 1){ //INCR
                //Increment Address by number of bytes in a burst(arsize)
                current_read_parameters.address += current_read_parameters.increment;

            }
            else if(rd_ad_channel_queue.front().arburst == 2){ //WRAP
                current_read_parameters.address += current_read_parameters.increment;
                if(current_read_parameters.address == current_read_parameters.wrap_boundary + current_read_parameters.number_bytes * current_read_parameters.burst_length){
                    current_read_parameters.address = current_read_parameters.wrap_boundary;
                }
            }
            elem.rresp = 0; //OKAY bx00
            //elem.ruser = rd_ad_channel_queue.front().aruser;
            if(current_read_parameters.number_of_bursts_left == 0){
                elem.rlast = 1;
                rd_ad_channel_queue.pop();
            }
            else
                elem.rlast = 0;
            r_data_channel_queue.push(elem);
        }
        else{
            current_read_parameters.delay_cycles_left--;
        }
    }

}
template <class TB>
void axi_peripheral_sim<TB>::handle_write_req(){
    if(w_data_channel_queue.size() > 0 && current_write_parameters.number_of_bursts_left > 0){
            if(current_write_parameters.delay_cycles_left == 0){
            AXI_write_data_channel_signals elem = w_data_channel_queue.front();
            w_data_channel_queue.pop();
            //Calculate Byte Enable
            uint32_t byte_enable = 0;
            if(elem.wstrb >= 8){
                byte_enable = byte_enable | 0x000000FF;
                elem.wstrb -= 8;
            }
            if(elem.wstrb >= 4){
                byte_enable = byte_enable | 0x0000FF00;
                elem.wstrb -= 4;
            }
            if(elem.wstrb >= 2){
                byte_enable = byte_enable | 0x00FF0000;
                elem.wstrb -= 2;
            }
            if(elem.wstrb == 1){
                byte_enable = byte_enable | 0xFF000000;
                elem.wstrb -= 1;
            }
            //cout << "current_write_parameters.address: " << hex << current_write_parameters.address << endl;
            //cout << "elem.wdata: " << (char)elem.wdata << endl;
            set_data(current_write_parameters.address, elem.wdata[0], byte_enable);
            current_write_parameters.number_of_bursts_left--;

            if(wd_ad_channel_queue.front().awburst == 0 ){//FIXED
                //do nothing
            }
            else if(wd_ad_channel_queue.front().awburst == 1){ //INCR
                //Increment Address by number of bytes in a burst(arsize)
                current_write_parameters.address += current_write_parameters.increment;

            }
            else if(wd_ad_channel_queue.front().awburst == 2){ //WRAP
                current_write_parameters.address += current_write_parameters.increment;
                if(current_write_parameters.address == current_write_parameters.wrap_boundary + current_write_parameters.number_bytes * current_write_parameters.burst_length){
                    current_write_parameters.address = current_write_parameters.wrap_boundary;
                }
            }
            //If the Write is done
            if(current_write_parameters.number_of_bursts_left == 0){
                AXI_write_response_channel_signals resp_elem;
                resp_elem.bid = elem.wid;
                resp_elem.bresp = 0;
                wd_ad_channel_queue.pop();
                w_res_channel_queue.push(resp_elem);
            }
        }
        else{
            current_write_parameters.delay_cycles_left--;
        }
    }
}

template <class TB>
void axi_peripheral_sim<TB>::update_current_read_parameters(){
    //If I can serve a new read request
    if(rd_ad_channel_queue.size() > 0 && current_read_parameters.number_of_bursts_left == 0){
        current_read_parameters.address = rd_ad_channel_queue.front().araddr;
        current_read_parameters.number_of_bursts_left = rd_ad_channel_queue.front().arlen +1;
        current_read_parameters.delay_cycles_left = 1; //One cycle delay
        if(rd_ad_channel_queue.front().arburst == 0 ){//FIXED
            current_read_parameters.increment = 0;
        }
        else if(rd_ad_channel_queue.front().arburst == 1){ //INCR
            //Increment Address by number of bytes in a burst(arsize)
            current_read_parameters.increment = pow(2,rd_ad_channel_queue.front().arsize);

        }
        else if(rd_ad_channel_queue.front().arburst == 2){ //WRAP
            current_read_parameters.increment = pow(2,rd_ad_channel_queue.front().arsize);
            current_read_parameters.number_bytes = pow(2,rd_ad_channel_queue.front().arsize);
            current_read_parameters.burst_length = rd_ad_channel_queue.front().arlen +1;
            current_read_parameters.wrap_boundary =  (int)(current_read_parameters.address/(current_read_parameters.number_bytes * current_read_parameters.burst_length)) * (current_read_parameters.number_bytes * current_read_parameters.burst_length);
        }
    }
}
template <class TB>
void axi_peripheral_sim<TB>::update_current_write_parameters(){
    //If I can serve a new read request
    if(wd_ad_channel_queue.size() > 0 && current_write_parameters.number_of_bursts_left == 0){
        current_write_parameters.address = wd_ad_channel_queue.front().awaddr;
        current_write_parameters.number_of_bursts_left = wd_ad_channel_queue.front().awlen +1;
        current_write_parameters.delay_cycles_left = 1; //One cycle delay
        if(wd_ad_channel_queue.front().awburst == 0 ){//FIXED
            current_write_parameters.increment = 0;
        }
        else if(wd_ad_channel_queue.front().awburst == 1){ //INCR
            //Increment Address by number of bytes in a burst(arsize)
            current_write_parameters.increment = pow(2,wd_ad_channel_queue.front().awsize);

        }
        else if(wd_ad_channel_queue.front().awburst == 2){ //WRAP
            current_write_parameters.increment = pow(2,wd_ad_channel_queue.front().awsize);
            current_write_parameters.number_bytes = pow(2,wd_ad_channel_queue.front().awsize);
            current_write_parameters.burst_length = wd_ad_channel_queue.front().awlen +1;
            current_write_parameters.wrap_boundary =  (int)(current_write_parameters.address/(current_write_parameters.number_bytes * current_write_parameters.burst_length)) * (current_write_parameters.number_bytes * current_write_parameters.burst_length);
        }
    }
}

template <class TB>
void axi_peripheral_sim<TB>::handle_UART(){
    if(!(uart_registers[UART_LSR_OFFSET] & UART_LSR_TX_BUFFER_EMPTY)){
        //Print Out Sent Character Terminal
        cout << (char)uart_registers[UART_THR_OFFSET] << flush;    
        //Set Data Ready to 0 (i.e. data has been recieved)
        uint32_t data = uart_registers[UART_LSR_OFFSET];
        data = (data & ~UART_LSR_TX_BUFFER_EMPTY) | (0x20 & UART_LSR_TX_BUFFER_EMPTY);
        uart_registers[UART_LSR_OFFSET] = data;
    }

    if(from_file){
        if(!(uart_registers[UART_LSR_OFFSET] & UART_LSR_DATA_READY)){
            if(!(input_file.peek() == EOF)){
                //if fstream is not empty
                char * buffer = new char();
                input_file.read(buffer, 1);
                //Assign DATA
                uart_registers[UART_RBR_OFFSET] = *buffer;
                //cout << "UART: " << (char) uart_registers[UART_RBR_OFFSET] << endl;
                delete buffer;
                //Set THR Empty to false (i.e. we are sending data)
                uint32_t data = uart_registers[UART_LSR_OFFSET];
                data = (data & ~UART_LSR_DATA_READY) | (0x1 & UART_LSR_DATA_READY);
                uart_registers[UART_LSR_OFFSET] = data;
            }
        }
    }
    else{
        
    }
};

template <class TB>
void axi_peripheral_sim<TB>::step(){
    parse_input_signals();
    update_current_read_parameters();
    update_current_write_parameters();
    handle_read_req();
    handle_write_req();
    handle_UART();
    parse_output_signals();
}
