#include "axi_ddr_sim.h"
#include "ddr_page.h"
#include <stdint.h>
#include <iostream>
#include <cstdlib>
#include <assert.h>
using namespace  std;

template <class TB>
axi_ddr_sim<TB>::axi_ddr_sim(TB * tb){
    this->tb = tb;
}

template <class TB>
void axi_ddr_sim<TB>::init_signals_taiga(){
    tb->ddr_axi_bresp_taiga = 0;
    tb->ddr_axi_bvalid_taiga = 0;
    tb->ddr_axi_rvalid_taiga = 0;
    #if DATA_WIDTH <= 64
        tb->ddr_axi_rdata_taiga = 0;
    #else
        for(int i ; i < sizeof(tb->ddr_axi_rdata_taiga)/sizeof(tb->ddr_axi_rdata_taiga[0]);i++)
        tb->ddr_axi_rdata_taiga[i] = 0;
    #endif
    tb->ddr_axi_rid_taiga = 0;
    tb->ddr_axi_rlast_taiga = 0;
    tb->ddr_axi_rresp_taiga = 0;
    tb->ddr_axi_rvalid_taiga = 0;
}

template <class TB>
void axi_ddr_sim<TB>::init_signals_muir(){
    tb->ddr_axi_bresp_muir = 0;
    tb->ddr_axi_bvalid_muir = 0;
    tb->ddr_axi_rvalid_muir = 0;
    #if DATA_WIDTH <= 64
        tb->ddr_axi_rdata_muir = 0;
    #else
        for(int i ; i < sizeof(tb->ddr_axi_rdata_muir)/sizeof(tb->ddr_axi_rdata_muir[0]);i++)
        tb->ddr_axi_rdata_muir[i] = 0;
    #endif
    tb->ddr_axi_rid_muir = 0;
    tb->ddr_axi_rlast_muir = 0;
    tb->ddr_axi_rresp_muir = 0;
    tb->ddr_axi_rvalid_muir = 0;
}

template <class TB>
axi_ddr_sim<TB>::axi_ddr_sim(string filepath, uint32_t starting_memory_location, int number_of_bytes, TB * tb){
    ifstream input_memory_file;
    input_memory_file.open(filepath);
    string line;


    uint32_t max_pages = starting_memory_location/PAGE_SIZE + number_of_bytes/PAGE_SIZE;
    //Parse the uniform pages
    uint32_t page_index = starting_memory_location/PAGE_SIZE;
    for (; page_index < max_pages; page_index++){
     ddr_page page;
        for(int data_index = 0; data_index < PAGE_SIZE/4; data_index++){
            getline(input_memory_file, line);
            //Read 32-bit number represented through hexidecimals
            page.write_data(data_index, stoul(line, 0, 16));
        }
        ddr_pages_taiga.insert(pair<uint32_t,ddr_page>((uint32_t)(page_index*PAGE_SIZE), page));
    }
       generator_taiga = default_random_engine(DELAY_SEED);
       read_distribution_taiga = uniform_int_distribution<int>(MIN_DELAY_RD,MAX_DELAY_RD);
       write_distribution_taiga = uniform_int_distribution<int>(MIN_DELAY_WR,MAX_DELAY_WR);
    this->tb = tb;
    init_signals_taiga();
    //printf("Done AXI Initialization. %d Pages intialized\n", page_index);
    fflush(stdout);
}

template <class TB>
axi_ddr_sim<TB>::axi_ddr_sim(ifstream & input_memory_file, TB * tb, unsigned starting_location){
    this->tb = tb;
    init_mem_taiga(input_memory_file, starting_location);
    init_mem_muir(input_memory_file, 0x00000000);
}

template <class TB>
void axi_ddr_sim<TB>::init_mem_taiga(ifstream & input_memory_file, unsigned starting_location){
    string line;

    uint32_t max_pages = DDR_SIZE/PAGE_SIZE;
    //Parse the uniform pages
    bool not_finished = true;
    uint32_t page_index = starting_location/PAGE_SIZE;
    for (; page_index < max_pages; page_index++){
     ddr_page page;

        for(int data_index = 0; data_index < PAGE_SIZE/4; data_index++){
            not_finished = (bool)getline(input_memory_file, line);
            if(!not_finished)
                break;
            //Read 32-bit number represented through hexidecimals
            page.write_data(data_index, stoul(line, 0, 16));
        }
        if(!not_finished)
            break;
        ddr_pages_taiga.insert(pair<uint32_t,ddr_page>((uint32_t)(page_index*PAGE_SIZE), page));
        fflush(stdout);
    }

       generator_taiga = default_random_engine(DELAY_SEED);
       read_distribution_taiga = uniform_int_distribution<int>(MIN_DELAY_RD,MAX_DELAY_RD);
       write_distribution_taiga = uniform_int_distribution<int>(MIN_DELAY_WR,MAX_DELAY_WR);
    
    init_signals_taiga();
    //printf("Done AXI Initialization. Started from: %u\n", starting_location);
    fflush(stdout);
}

template <class TB>
void axi_ddr_sim<TB>::init_mem_muir(ifstream & input_memory_file, unsigned starting_location){
    string line;

    uint32_t max_pages = DDR_SIZE/PAGE_SIZE;
    //Parse the uniform pages
    bool not_finished = true;
    uint32_t page_index = starting_location/PAGE_SIZE;
    for (; page_index < max_pages; page_index++){
     ddr_page page;

        for(int data_index = 0; data_index < PAGE_SIZE/4; data_index++){
            not_finished = (bool)getline(input_memory_file, line);
            if(!not_finished)
                break;
            //Read 32-bit number represented through hexidecimals
            page.write_data(data_index, stoul(line, 0, 16));
        }
        if(!not_finished)
            break;
        ddr_pages_muir.insert(pair<uint32_t,ddr_page>((uint32_t)(page_index*PAGE_SIZE), page));
        fflush(stdout);
    }

       generator_muir = default_random_engine(DELAY_SEED);
       read_distribution_muir = uniform_int_distribution<int>(MIN_DELAY_RD,MAX_DELAY_RD);
       write_distribution_muir = uniform_int_distribution<int>(MIN_DELAY_WR,MAX_DELAY_WR);
    
    init_signals_muir();
    //printf("Done AXI Initialization. Started from: %u\n", starting_location);
    fflush(stdout);
}

template <class TB>
int axi_ddr_sim<TB>::get_data_taiga(uint32_t data_address){
    uint32_t starting_address = (data_address / PAGE_SIZE) * PAGE_SIZE;

    if(ddr_pages_taiga.count(starting_address)){ //If page exists
        return ddr_pages_taiga[starting_address].return_data(data_address%PAGE_SIZE/4);
    }
    else{//If it doesn't, instantiate a new page
        ddr_page page;
        ddr_pages_taiga.insert(pair<uint32_t,ddr_page>(starting_address, page));
        assert(ddr_pages_taiga.count(starting_address)); //Check if it was intialized
        return ddr_pages_taiga[starting_address].return_data(data_address%PAGE_SIZE/4);
    }
}
template <class TB>
int axi_ddr_sim<TB>::get_data_muir(uint32_t data_address){
    uint32_t starting_address = (data_address / PAGE_SIZE) * PAGE_SIZE;

    if(ddr_pages_muir.count(starting_address)){ //If page exists
        return ddr_pages_muir[starting_address].return_data(data_address%PAGE_SIZE/4);
    }
    else{//If it doesn't, instantiate a new page
        ddr_page page;
        ddr_pages_muir.insert(pair<uint32_t,ddr_page>(starting_address, page));
        assert(ddr_pages_muir.count(starting_address)); //Check if it was intialized
        return ddr_pages_muir[starting_address].return_data(data_address%PAGE_SIZE/4);
    }
}

template <class TB>
void  axi_ddr_sim<TB>::set_data_taiga(uint32_t data_address, uint32_t set_data_taiga, uint32_t byte_enable){
    uint32_t data = get_data_taiga(data_address);
    uint32_t starting_address = (data_address / PAGE_SIZE) * PAGE_SIZE;
    data = (data & ~byte_enable) | (set_data_taiga & byte_enable);
    ddr_pages_taiga[starting_address].write_data(data_address%PAGE_SIZE/4, data);

};
template <class TB>
void  axi_ddr_sim<TB>::set_data_muir(uint32_t data_address, uint32_t set_data_muir, uint32_t byte_enable){
    uint32_t data = get_data_muir(data_address);
    uint32_t starting_address = (data_address / PAGE_SIZE) * PAGE_SIZE;
    data = (data & ~byte_enable) | (set_data_muir & byte_enable);
    ddr_pages_muir[starting_address].write_data(data_address%PAGE_SIZE/4, data);

};

template <class TB>
ddr_page axi_ddr_sim<TB>::get_page_taiga(uint32_t page_address){
    return ddr_pages_taiga[page_address];
}
template <class TB>
ddr_page axi_ddr_sim<TB>::get_page_muir(uint32_t page_address){
    return ddr_pages_muir[page_address];
}

template <class TB>
void axi_ddr_sim<TB>::parse_input_signals(){
    parse_input_signals_taiga();
    parse_input_signals_muir();
}
template <class TB>
void axi_ddr_sim<TB>::parse_input_signals_taiga(){
    //If the master has a write requests
    if(tb->ddr_axi_awvalid_taiga && wd_ad_channel_queue_taiga.size() < MAX_INFLIGHT_WD_REQ){
        AXI_write_address_channel_signals elem{tb->ddr_axi_awaddr_taiga, tb->ddr_axi_awlen_taiga, tb->ddr_axi_awsize_taiga, tb->ddr_axi_awburst_taiga,tb->ddr_axi_awcache_taiga,tb->ddr_axi_awid_taiga};
        wd_ad_channel_queue_taiga.push(elem);
    }
    //If the master has write data
    if(tb->ddr_axi_wvalid_taiga){
        AXI_write_data_channel_signals elem{tb->ddr_axi_wid_taiga, {0}, tb->ddr_axi_wstrb_taiga, tb->ddr_axi_wlast_taiga};

        #if DATA_WIDTH <= 64
        for(int i = 0; i < pow(2,tb->ddr_axi_awsize_taiga)/4; i++){
            elem.wdata[i] =  uint64_t (tb->ddr_axi_wdata_taiga) >> 32*i;
        }
        #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_taiga)/sizeof(tb->ddr_axi_rdata_taiga[0]);i++)
               elem.wdata[i] =  tb->ddr_axi_wdata_taiga[i];
         #endif
        w_data_channel_queue_taiga.push(elem);
    }
    //If the master has a read request
     if(tb->ddr_axi_arvalid_taiga && rd_ad_channel_queue_taiga.size() < MAX_INFLIGHT_RD_REQ){
        AXI_read_address_channel_signals elem{tb->ddr_axi_araddr_taiga, tb->ddr_axi_arlen_taiga, tb->ddr_axi_arsize_taiga, tb->ddr_axi_arburst_taiga, tb->ddr_axi_arcache_taiga, tb->ddr_axi_arid_taiga};
        rd_ad_channel_queue_taiga.push(elem);
     }
}
template <class TB>
void axi_ddr_sim<TB>::parse_input_signals_muir(){
    //If the master has a write requests
    if(tb->ddr_axi_awvalid_muir && wd_ad_channel_queue_muir.size() < MAX_INFLIGHT_WD_REQ){
        AXI_write_address_channel_signals elem{tb->ddr_axi_awaddr_muir, tb->ddr_axi_awlen_muir, tb->ddr_axi_awsize_muir, tb->ddr_axi_awburst_muir,tb->ddr_axi_awcache_muir,tb->ddr_axi_awid_muir};
        wd_ad_channel_queue_muir.push(elem);
    }
    //If the master has write data
    if(tb->ddr_axi_wvalid_muir){
        AXI_write_data_channel_signals elem{tb->ddr_axi_wid_muir, {0}, tb->ddr_axi_wstrb_muir, tb->ddr_axi_wlast_muir};

        #if DATA_WIDTH <= 64
        for(int i = 0; i < pow(2,tb->ddr_axi_awsize_muir)/4; i++){
            elem.wdata[i] =  uint64_t (tb->ddr_axi_wdata_muir) >> 32*i;
        }
        #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_muir)/sizeof(tb->ddr_axi_rdata_muir[0]);i++)
               elem.wdata[i] =  tb->ddr_axi_wdata_muir[i];
         #endif
        w_data_channel_queue_muir.push(elem);
    }
    //If the master has a read request
     if(tb->ddr_axi_arvalid_muir && rd_ad_channel_queue_muir.size() < MAX_INFLIGHT_RD_REQ){
        AXI_read_address_channel_signals elem{tb->ddr_axi_araddr_muir, tb->ddr_axi_arlen_muir, tb->ddr_axi_arsize_muir, tb->ddr_axi_arburst_muir, tb->ddr_axi_arcache_muir, tb->ddr_axi_arid_muir};
        rd_ad_channel_queue_muir.push(elem);
     }
}

template <class TB>
void axi_ddr_sim<TB>::parse_output_signals(){
    parse_output_signals_taiga();
    parse_output_signals_muir();
}
template <class TB>
void axi_ddr_sim<TB>::parse_output_signals_taiga(){
    if(tb->rst ==1){
        tb->ddr_axi_wready_taiga = 0;
        tb->ddr_axi_arready_taiga = 0;

        tb->ddr_axi_bid_taiga = 0;
        tb->ddr_axi_bresp_taiga = 0;
        tb->ddr_axi_bvalid_taiga = 0;
        tb->ddr_axi_rid_taiga = 0;
        tb->ddr_axi_rresp_taiga = 0;
        tb->ddr_axi_rlast_taiga = 0;
        //tb->ddr_axi_ruser_taiga = 0;
        tb->ddr_axi_rvalid_taiga = 0;
        #if DATA_WIDTH <= 64
        tb->ddr_axi_rdata_taiga = 0;
        #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_taiga)/sizeof(tb->ddr_axi_rdata_taiga[0]);i++)
            tb->ddr_axi_rdata_taiga[i] = 0;
        #endif

    }
    else { 
    tb->ddr_axi_wready_taiga = 1;

    //Write Req
    if(wd_ad_channel_queue_taiga.size() < MAX_INFLIGHT_WD_REQ)
        tb->ddr_axi_awready_taiga = 1;
    else
        tb->ddr_axi_awready_taiga = 0;
    
    //Read Req
    if(rd_ad_channel_queue_taiga.size() < MAX_INFLIGHT_RD_REQ)
        tb->ddr_axi_arready_taiga = 1;
    else
        tb->ddr_axi_arready_taiga = 0;

    //If we the write_response
    if(w_res_channel_queue_taiga.size() > 0){
        AXI_write_response_channel_signals elem = w_res_channel_queue_taiga.front();
        if(tb->ddr_axi_bready_taiga)
            w_res_channel_queue_taiga.pop();
        
        tb->ddr_axi_bid_taiga = elem.bid;
        tb->ddr_axi_bresp_taiga = elem.bresp;
        tb->ddr_axi_bvalid_taiga = 1;
    }
    else{
        tb->ddr_axi_bid_taiga = rand();
        tb->ddr_axi_bresp_taiga = rand();
        //tb->ddr_axi_buser_taiga = rand();
        tb->ddr_axi_bvalid_taiga = 0;
    }

    //If we have the read data
    if(r_data_channel_queue_taiga.size() > 0){
        AXI_read_data_channel_signals elem = r_data_channel_queue_taiga.front();
        if(tb->ddr_axi_rready_taiga){
            //cout << "Before: " << r_data_channel_queue_taiga.size() << endl;
            r_data_channel_queue_taiga.pop();
            //cout << "After: " << r_data_channel_queue_taiga.size() << endl;
        }
        tb->ddr_axi_rid_taiga = elem.rid;
        #if DATA_WIDTH <= 64
            tb->ddr_axi_rdata_taiga = 0;
            for(int i = 0; i < current_read_parameters_taiga.number_bytes/4; i++){
                tb->ddr_axi_rdata_taiga += (uint64_t)elem.rdata[i] << (32*i);
                //cout << "Value: " << hex << tb->ddr_axi_rdata_taiga << endl;
        }
         #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_taiga)/sizeof(tb->ddr_axi_rdata_taiga[0]);i++)
            tb->ddr_axi_rdata_taiga[i] = elem.rdata[i];
        #endif

        //cout << endl;
        tb->ddr_axi_rresp_taiga = elem.rresp;
        tb->ddr_axi_rlast_taiga = elem.rlast;
        //tb->ddr_axi_ruser_taiga = elem.ruser;
        tb->ddr_axi_rvalid_taiga = 1;
    }
    else{
        tb->ddr_axi_rid_taiga = rand();
        tb->ddr_axi_rresp_taiga = rand();
        tb->ddr_axi_rlast_taiga = 0;
        //tb->ddr_axi_ruser_taiga = rand();
        #if DATA_WIDTH <= 64
            tb->ddr_axi_rdata_taiga = 0;
        for(int i = 0; i < current_read_parameters_taiga.number_bytes/4; i++){
                tb->ddr_axi_rdata_taiga += tb->ddr_axi_rdata_taiga = rand() << (32*i);
                //cout << "Value: " << hex << tb->ddr_axi_rdata_taiga << endl;
        }
         #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_taiga)/sizeof(tb->ddr_axi_rdata_taiga[0]);i++)
            tb->ddr_axi_rdata_taiga[i] = rand();
        #endif
        tb->ddr_axi_rvalid_taiga = 0;
    }
    }
}
template <class TB>
void axi_ddr_sim<TB>::parse_output_signals_muir(){
    if(tb->rst ==1){
        tb->ddr_axi_wready_muir = 0;
        tb->ddr_axi_arready_muir = 0;
        tb->ddr_axi_bid_muir = 0;
        tb->ddr_axi_bresp_muir = 0;
        tb->ddr_axi_bvalid_muir = 0;
        tb->ddr_axi_rid_muir = 0;
        tb->ddr_axi_rresp_muir = 0;
        tb->ddr_axi_rlast_muir = 0;
        //tb->ddr_axi_ruser_muir = 0;
        tb->ddr_axi_rvalid_muir = 0;
        #if DATA_WIDTH <= 64
        tb->ddr_axi_rdata_muir = 0;
        #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_muir)/sizeof(tb->ddr_axi_rdata_muir[0]);i++)
            tb->ddr_axi_rdata_muir[i] = 0;
        #endif

    }
    else { 
    tb->ddr_axi_wready_muir = 1;

    //Write Req
    if(wd_ad_channel_queue_muir.size() < MAX_INFLIGHT_WD_REQ)
        tb->ddr_axi_awready_muir = 1;
    else
        tb->ddr_axi_awready_muir = 0;
    
    //Read Req
    if(rd_ad_channel_queue_muir.size() < MAX_INFLIGHT_RD_REQ)
        tb->ddr_axi_arready_muir = 1;
    else
        tb->ddr_axi_arready_muir = 0;

    //If we the write_response
    if(w_res_channel_queue_muir.size() > 0){
        AXI_write_response_channel_signals elem = w_res_channel_queue_muir.front();
        if(tb->ddr_axi_bready_muir)
            w_res_channel_queue_muir.pop();
        
        tb->ddr_axi_bid_muir = elem.bid;
        tb->ddr_axi_bresp_muir = elem.bresp;
        tb->ddr_axi_bvalid_muir = 1;
    }
    else{
        tb->ddr_axi_bid_muir = rand();
        tb->ddr_axi_bresp_muir = rand();
        //tb->ddr_axi_buser_muir = rand();
        tb->ddr_axi_bvalid_muir = 0;
    }

    //If we have the read data
    if(r_data_channel_queue_muir.size() > 0){
        AXI_read_data_channel_signals elem = r_data_channel_queue_muir.front();
        if(tb->ddr_axi_rready_muir){
            //cout << "Before: " << r_data_channel_queue_muir.size() << endl;
            r_data_channel_queue_muir.pop();
            //cout << "After: " << r_data_channel_queue_muir.size() << endl;
        }
        tb->ddr_axi_rid_muir = elem.rid;
        #if DATA_WIDTH <= 64
            tb->ddr_axi_rdata_muir = 0;
            for(int i = 0; i < current_read_parameters_muir.number_bytes/4; i++){
                tb->ddr_axi_rdata_muir += (uint64_t)elem.rdata[i] << (32*i);
                //cout << "Value: " << hex << tb->ddr_axi_rdata_muir << endl;
        }
         #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_muir)/sizeof(tb->ddr_axi_rdata_muir[0]);i++)
            tb->ddr_axi_rdata_muir[i] = elem.rdata[i];
        #endif

        //cout << endl;
        tb->ddr_axi_rresp_muir = elem.rresp;
        tb->ddr_axi_rlast_muir = elem.rlast;
        //tb->ddr_axi_ruser_muir = elem.ruser;
        tb->ddr_axi_rvalid_muir = 1;
    }
    else{
        tb->ddr_axi_rid_muir = rand();
        tb->ddr_axi_rresp_muir = rand();
        tb->ddr_axi_rlast_muir = 0;
        //tb->ddr_axi_ruser_muir = rand();
        #if DATA_WIDTH <= 64
            tb->ddr_axi_rdata_muir = 0;
        for(int i = 0; i < current_read_parameters_muir.number_bytes/4; i++){
                tb->ddr_axi_rdata_muir += tb->ddr_axi_rdata_muir = rand() << (32*i);
                //cout << "Value: " << hex << tb->ddr_axi_rdata_muir << endl;
        }
         #else
            for(int i ; i < sizeof(tb->ddr_axi_rdata_muir)/sizeof(tb->ddr_axi_rdata_muir[0]);i++)
            tb->ddr_axi_rdata_muir[i] = rand();
        #endif
        tb->ddr_axi_rvalid_muir = 0;
    }
    }
}

template <class TB>
void axi_ddr_sim<TB>::handle_read_req(){
    handle_read_req_taiga();
    handle_read_req_muir();
}
template <class TB>
void axi_ddr_sim<TB>::handle_read_req_taiga(){
    if(rd_ad_channel_queue_taiga.size() > 0 ){
        if(current_read_parameters_taiga.delay_cycles_left == 0){
            AXI_read_data_channel_signals elem;
            elem.rid = rd_ad_channel_queue_taiga.front().arid;
            for(int i = 0; i < current_read_parameters_taiga.number_bytes/4 ;i++){
              // cout << hex << current_read_parameters_taiga.address + 4*i << ": ";
                elem.rdata[i] =  get_data_taiga(current_read_parameters_taiga.address + 4*i);
              // cout << hex << elem.rdata[i] << endl;
            }
            //cout << endl;
            current_read_parameters_taiga.number_of_bursts_left--;

            if(rd_ad_channel_queue_taiga.front().arburst == 0 ){//FIXED
                //do nothing
            }
            else if(rd_ad_channel_queue_taiga.front().arburst == 1){ //INCR
                //Increment Address by number of bytes in a burst(arsize)
                current_read_parameters_taiga.address += current_read_parameters_taiga.increment;

            }
            else if(rd_ad_channel_queue_taiga.front().arburst == 2){ //WRAP
                current_read_parameters_taiga.address += current_read_parameters_taiga.increment;
                if(current_read_parameters_taiga.address == current_read_parameters_taiga.wrap_boundary + current_read_parameters_taiga.number_bytes * current_read_parameters_taiga.burst_length){
                    current_read_parameters_taiga.address = current_read_parameters_taiga.wrap_boundary;
                }
            }
            elem.rresp = 0; //OKAY bx00
            //elem.ruser = rd_ad_channel_queue_taiga.front().aruser;
            if(current_read_parameters_taiga.number_of_bursts_left == 0){
                elem.rlast = 1;
                rd_ad_channel_queue_taiga.pop();
            }
            else
                elem.rlast = 0;
            r_data_channel_queue_taiga.push(elem);
        }
        else{
            current_read_parameters_taiga.delay_cycles_left--;
        }
    }

}
template <class TB>
void axi_ddr_sim<TB>::handle_read_req_muir(){
    if(rd_ad_channel_queue_muir.size() > 0 ){
        if(current_read_parameters_muir.delay_cycles_left == 0){
            AXI_read_data_channel_signals elem;
            elem.rid = rd_ad_channel_queue_muir.front().arid;
            for(int i = 0; i < current_read_parameters_muir.number_bytes/4 ;i++){
              // cout << hex << current_read_parameters_muir.address + 4*i << ": ";
                elem.rdata[i] =  get_data_muir(current_read_parameters_muir.address + 4*i);
              // cout << hex << elem.rdata[i] << endl;
            }
            //cout << endl;
            current_read_parameters_muir.number_of_bursts_left--;

            if(rd_ad_channel_queue_muir.front().arburst == 0 ){//FIXED
                //do nothing
            }
            else if(rd_ad_channel_queue_muir.front().arburst == 1){ //INCR
                //Increment Address by number of bytes in a burst(arsize)
                current_read_parameters_muir.address += current_read_parameters_muir.increment;

            }
            else if(rd_ad_channel_queue_muir.front().arburst == 2){ //WRAP
                current_read_parameters_muir.address += current_read_parameters_muir.increment;
                if(current_read_parameters_muir.address == current_read_parameters_muir.wrap_boundary + current_read_parameters_muir.number_bytes * current_read_parameters_muir.burst_length){
                    current_read_parameters_muir.address = current_read_parameters_muir.wrap_boundary;
                }
            }
            elem.rresp = 0; //OKAY bx00
            //elem.ruser = rd_ad_channel_queue_muir.front().aruser;
            if(current_read_parameters_muir.number_of_bursts_left == 0){
                elem.rlast = 1;
                rd_ad_channel_queue_muir.pop();
            }
            else
                elem.rlast = 0;
            r_data_channel_queue_muir.push(elem);
        }
        else{
            current_read_parameters_muir.delay_cycles_left--;
        }
    }

}

template <class TB>
void axi_ddr_sim<TB>::handle_write_req(){
    handle_write_req_taiga();
    handle_write_req_muir();
}
template <class TB>
void axi_ddr_sim<TB>::handle_write_req_taiga(){
    //cout << "w_data_channel_queue_taiga size: " << w_data_channel_queue_taiga.size() << endl;
    //cout << "current_write_parameters_taiga.number_of_bursts_left: " << current_write_parameters_taiga.number_of_bursts_left << endl;
    if(w_data_channel_queue_taiga.size() > 0 && current_write_parameters_taiga.number_of_bursts_left > 0){
            if(current_write_parameters_taiga.delay_cycles_left == 0){
            AXI_write_data_channel_signals elem = w_data_channel_queue_taiga.front();
            w_data_channel_queue_taiga.pop();
            //Calculate Byte Enable
            uint32_t byte_enable = 0;
            //Z: TO-DO ASSUMING ALL WRITES HAVE BYTE ENABLE SET
            // for(int i = 0; i < current_read_parameters_taiga.number_bytes;i++){
            //     if(elem.wstrb >= (current_read_parameters_taiga.number_bytes)/2*(i-1) ){
            //         byte_enable = byte_enable | 0xFF000000;
            //         elem.wstrb -= 8;
            //     }
            //     if(elem.wstrb >= 4){
            //         byte_enable = byte_enable | 0x00FF0000;
            //         elem.wstrb -= 4;
            //     }
            //     if(elem.wstrb >= 2){
            //         byte_enable = byte_enable | 0x0000FF00;
            //         elem.wstrb -= 2;
            //     }
            //     if(elem.wstrb >= 1){
            //         byte_enable = byte_enable | 0x000000FF;
            //         elem.wstrb -= 1;
            //     }
            // }
            if(elem.wstrb >= 8){
                byte_enable = byte_enable | 0xFF000000;
                elem.wstrb -= 8;
            }
            if(elem.wstrb >= 4){
                byte_enable = byte_enable | 0x00FF0000;
                elem.wstrb -= 4;
            }
            if(elem.wstrb >= 2){
                byte_enable = byte_enable | 0x0000FF00;
                elem.wstrb -= 2;
            }
            if(elem.wstrb >= 1){
                byte_enable = byte_enable | 0x000000FF;
                elem.wstrb -= 1;
            }

            for(int i = 0; i < current_read_parameters_taiga.number_bytes/4 ;i++){
                set_data_taiga(current_write_parameters_taiga.address + 4*i, elem.wdata[i], byte_enable);
            }
            //cout << endl;
            current_write_parameters_taiga.number_of_bursts_left--;

            if(wd_ad_channel_queue_taiga.front().awburst == 0 ){//FIXED
                //do nothing
            }
            else if(wd_ad_channel_queue_taiga.front().awburst == 1){ //INCR
                //Increment Address by number of bytes in a burst(arsize)
                current_write_parameters_taiga.address += current_write_parameters_taiga.increment;

            }
            else if(wd_ad_channel_queue_taiga.front().awburst == 2){ //WRAP
                current_write_parameters_taiga.address += current_write_parameters_taiga.increment;
                if(current_write_parameters_taiga.address == current_write_parameters_taiga.wrap_boundary + current_write_parameters_taiga.number_bytes * current_write_parameters_taiga.burst_length){
                    current_write_parameters_taiga.address = current_write_parameters_taiga.wrap_boundary;
                }
            }
            //If the Write is done
            if(current_write_parameters_taiga.number_of_bursts_left == 0){
                AXI_write_response_channel_signals resp_elem;
                resp_elem.bid = elem.wid;
                resp_elem.bresp = 0;
                wd_ad_channel_queue_taiga.pop();
                w_res_channel_queue_taiga.push(resp_elem);
            }
        }
        else{
            current_write_parameters_taiga.delay_cycles_left--;
        }
    }
}
template <class TB>
void axi_ddr_sim<TB>::handle_write_req_muir(){
    //cout << "w_data_channel_queue_muir size: " << w_data_channel_queue_muir.size() << endl;
    //cout << "current_write_parameters_muir.number_of_bursts_left: " << current_write_parameters_muir.number_of_bursts_left << endl;
    if(w_data_channel_queue_muir.size() > 0 && current_write_parameters_muir.number_of_bursts_left > 0){
            if(current_write_parameters_muir.delay_cycles_left == 0){
            AXI_write_data_channel_signals elem = w_data_channel_queue_muir.front();
            w_data_channel_queue_muir.pop();
            //Calculate Byte Enable
            uint32_t byte_enable = 0;
            //Z: TO-DO ASSUMING ALL WRITES HAVE BYTE ENABLE SET
            // for(int i = 0; i < current_read_parameters_muir.number_bytes;i++){
            //     if(elem.wstrb >= (current_read_parameters_muir.number_bytes)/2*(i-1) ){
            //         byte_enable = byte_enable | 0xFF000000;
            //         elem.wstrb -= 8;
            //     }
            //     if(elem.wstrb >= 4){
            //         byte_enable = byte_enable | 0x00FF0000;
            //         elem.wstrb -= 4;
            //     }
            //     if(elem.wstrb >= 2){
            //         byte_enable = byte_enable | 0x0000FF00;
            //         elem.wstrb -= 2;
            //     }
            //     if(elem.wstrb >= 1){
            //         byte_enable = byte_enable | 0x000000FF;
            //         elem.wstrb -= 1;
            //     }
            // }
            if(elem.wstrb >= 8){
                byte_enable = byte_enable | 0xFF000000;
                elem.wstrb -= 8;
            }
            if(elem.wstrb >= 4){
                byte_enable = byte_enable | 0x00FF0000;
                elem.wstrb -= 4;
            }
            if(elem.wstrb >= 2){
                byte_enable = byte_enable | 0x0000FF00;
                elem.wstrb -= 2;
            }
            if(elem.wstrb >= 1){
                byte_enable = byte_enable | 0x000000FF;
                elem.wstrb -= 1;
            }

            for(int i = 0; i < current_read_parameters_muir.number_bytes/4 ;i++){
                set_data_muir(current_write_parameters_muir.address + 4*i, elem.wdata[i], byte_enable);
            }
            //cout << endl;
            current_write_parameters_muir.number_of_bursts_left--;

            if(wd_ad_channel_queue_muir.front().awburst == 0 ){//FIXED
                //do nothing
            }
            else if(wd_ad_channel_queue_muir.front().awburst == 1){ //INCR
                //Increment Address by number of bytes in a burst(arsize)
                current_write_parameters_muir.address += current_write_parameters_muir.increment;

            }
            else if(wd_ad_channel_queue_muir.front().awburst == 2){ //WRAP
                current_write_parameters_muir.address += current_write_parameters_muir.increment;
                if(current_write_parameters_muir.address == current_write_parameters_muir.wrap_boundary + current_write_parameters_muir.number_bytes * current_write_parameters_muir.burst_length){
                    current_write_parameters_muir.address = current_write_parameters_muir.wrap_boundary;
                }
            }
            //If the Write is done
            if(current_write_parameters_muir.number_of_bursts_left == 0){
                AXI_write_response_channel_signals resp_elem;
                resp_elem.bid = elem.wid;
                resp_elem.bresp = 0;
                wd_ad_channel_queue_muir.pop();
                w_res_channel_queue_muir.push(resp_elem);
            }
        }
        else{
            current_write_parameters_muir.delay_cycles_left--;
        }
    }
}

template <class TB>
void axi_ddr_sim<TB>::update_current_read_parameters(){
    update_current_read_parameters_taiga();
    update_current_read_parameters_muir();
}
template <class TB>
void axi_ddr_sim<TB>::update_current_read_parameters_taiga(){
    //If I can serve a new read request
    if(rd_ad_channel_queue_taiga.size() > 0 && current_read_parameters_taiga.number_of_bursts_left == 0){
        current_read_parameters_taiga.address = rd_ad_channel_queue_taiga.front().araddr;
        current_read_parameters_taiga.number_of_bursts_left = rd_ad_channel_queue_taiga.front().arlen +1;
        current_read_parameters_taiga.delay_cycles_left = read_distribution_taiga(generator_taiga);
        if(rd_ad_channel_queue_taiga.front().arburst == 0 ){//FIXED
            current_read_parameters_taiga.increment = 0;
            current_read_parameters_taiga.number_bytes = pow(2,rd_ad_channel_queue_taiga.front().arsize);
        }
        else if(rd_ad_channel_queue_taiga.front().arburst == 1){ //INCR
            //Increment Address by number of bytes in a burst(arsize)
            current_read_parameters_taiga.increment = pow(2,rd_ad_channel_queue_taiga.front().arsize);
            current_read_parameters_taiga.number_bytes = pow(2,rd_ad_channel_queue_taiga.front().arsize);

        }
        else if(rd_ad_channel_queue_taiga.front().arburst == 2){ //WRAP
            current_read_parameters_taiga.increment = pow(2,rd_ad_channel_queue_taiga.front().arsize);
            current_read_parameters_taiga.number_bytes = pow(2,rd_ad_channel_queue_taiga.front().arsize);
            current_read_parameters_taiga.burst_length = rd_ad_channel_queue_taiga.front().arlen +1;
            current_read_parameters_taiga.wrap_boundary =  (int)(current_read_parameters_taiga.address/(current_read_parameters_taiga.number_bytes * current_read_parameters_taiga.burst_length)) * (current_read_parameters_taiga.number_bytes * current_read_parameters_taiga.burst_length);
        }
    }
}
template <class TB>
void axi_ddr_sim<TB>::update_current_read_parameters_muir(){
    //If I can serve a new read request
    if(rd_ad_channel_queue_muir.size() > 0 && current_read_parameters_muir.number_of_bursts_left == 0){
        current_read_parameters_muir.address = rd_ad_channel_queue_muir.front().araddr;
        current_read_parameters_muir.number_of_bursts_left = rd_ad_channel_queue_muir.front().arlen +1;
        current_read_parameters_muir.delay_cycles_left = read_distribution_muir(generator_muir);
        if(rd_ad_channel_queue_muir.front().arburst == 0 ){//FIXED
            current_read_parameters_muir.increment = 0;
            current_read_parameters_muir.number_bytes = pow(2,rd_ad_channel_queue_muir.front().arsize);
        }
        else if(rd_ad_channel_queue_muir.front().arburst == 1){ //INCR
            //Increment Address by number of bytes in a burst(arsize)
            current_read_parameters_muir.increment = pow(2,rd_ad_channel_queue_muir.front().arsize);
            current_read_parameters_muir.number_bytes = pow(2,rd_ad_channel_queue_muir.front().arsize);

        }
        else if(rd_ad_channel_queue_muir.front().arburst == 2){ //WRAP
            current_read_parameters_muir.increment = pow(2,rd_ad_channel_queue_muir.front().arsize);
            current_read_parameters_muir.number_bytes = pow(2,rd_ad_channel_queue_muir.front().arsize);
            current_read_parameters_muir.burst_length = rd_ad_channel_queue_muir.front().arlen +1;
            current_read_parameters_muir.wrap_boundary =  (int)(current_read_parameters_muir.address/(current_read_parameters_muir.number_bytes * current_read_parameters_muir.burst_length)) * (current_read_parameters_muir.number_bytes * current_read_parameters_muir.burst_length);
        }
    }
}

template <class TB>
void axi_ddr_sim<TB>::update_current_write_parameters(){
    update_current_write_parameters_taiga();
    update_current_write_parameters_muir();
}
template <class TB>
void axi_ddr_sim<TB>::update_current_write_parameters_taiga(){
    //If I can serve a new read request
    if(wd_ad_channel_queue_taiga.size() > 0 && current_write_parameters_taiga.number_of_bursts_left == 0){
        current_write_parameters_taiga.address = wd_ad_channel_queue_taiga.front().awaddr;
        current_write_parameters_taiga.number_of_bursts_left = wd_ad_channel_queue_taiga.front().awlen +1;
        current_write_parameters_taiga.delay_cycles_left = write_distribution_taiga(generator_taiga);
        if(wd_ad_channel_queue_taiga.front().awburst == 0 ){//FIXED
            current_write_parameters_taiga.increment = 0;
            current_write_parameters_taiga.number_bytes = pow(2,wd_ad_channel_queue_taiga.front().awsize);
        }
        else if(wd_ad_channel_queue_taiga.front().awburst == 1){ //INCR
            //Increment Address by number of bytes in a burst(arsize)
            current_write_parameters_taiga.increment = pow(2,wd_ad_channel_queue_taiga.front().awsize);
            current_write_parameters_taiga.number_bytes = pow(2,wd_ad_channel_queue_taiga.front().awsize);

        }
        else if(wd_ad_channel_queue_taiga.front().awburst == 2){ //WRAP
            current_write_parameters_taiga.increment = pow(2,wd_ad_channel_queue_taiga.front().awsize);
            current_write_parameters_taiga.number_bytes = pow(2,wd_ad_channel_queue_taiga.front().awsize);
            current_write_parameters_taiga.burst_length = wd_ad_channel_queue_taiga.front().awlen +1;
            current_write_parameters_taiga.wrap_boundary =  (int)(current_write_parameters_taiga.address/(current_write_parameters_taiga.number_bytes * current_write_parameters_taiga.burst_length)) * (current_write_parameters_taiga.number_bytes * current_write_parameters_taiga.burst_length);
        }
    }
}
template <class TB>
void axi_ddr_sim<TB>::update_current_write_parameters_muir(){
    //If I can serve a new read request
    if(wd_ad_channel_queue_muir.size() > 0 && current_write_parameters_muir.number_of_bursts_left == 0){
        current_write_parameters_muir.address = wd_ad_channel_queue_muir.front().awaddr;
        current_write_parameters_muir.number_of_bursts_left = wd_ad_channel_queue_muir.front().awlen +1;
        current_write_parameters_muir.delay_cycles_left = write_distribution_muir(generator_muir);
        if(wd_ad_channel_queue_muir.front().awburst == 0 ){//FIXED
            current_write_parameters_muir.increment = 0;
            current_write_parameters_muir.number_bytes = pow(2,wd_ad_channel_queue_muir.front().awsize);
        }
        else if(wd_ad_channel_queue_muir.front().awburst == 1){ //INCR
            //Increment Address by number of bytes in a burst(arsize)
            current_write_parameters_muir.increment = pow(2,wd_ad_channel_queue_muir.front().awsize);
            current_write_parameters_muir.number_bytes = pow(2,wd_ad_channel_queue_muir.front().awsize);

        }
        else if(wd_ad_channel_queue_muir.front().awburst == 2){ //WRAP
            current_write_parameters_muir.increment = pow(2,wd_ad_channel_queue_muir.front().awsize);
            current_write_parameters_muir.number_bytes = pow(2,wd_ad_channel_queue_muir.front().awsize);
            current_write_parameters_muir.burst_length = wd_ad_channel_queue_muir.front().awlen +1;
            current_write_parameters_muir.wrap_boundary =  (int)(current_write_parameters_muir.address/(current_write_parameters_muir.number_bytes * current_write_parameters_muir.burst_length)) * (current_write_parameters_muir.number_bytes * current_write_parameters_muir.burst_length);
        }
    }
}

template <class TB>
void axi_ddr_sim<TB>::step(){
    parse_input_signals();
    update_current_read_parameters();
    update_current_write_parameters();
    handle_read_req();
    handle_write_req();
    parse_output_signals();
}