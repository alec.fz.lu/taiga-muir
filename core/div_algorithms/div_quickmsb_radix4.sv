/*
 * Copyright © 2017-2019 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
  *             Alec Lu <alec_lu@sfu.ca>
 */
 

module div_quickmsb_radix_4
    (
        input logic clk,
        input logic rst,
        unsigned_division_interface.divider div
    );
    
    logic terminate;
    logic [div.DATA_WIDTH/2-1:0] shift_count;
    
    logic [div.DATA_WIDTH+1:0] PR;
    logic [2:0] new_PR_sign;
    logic [div.DATA_WIDTH+2:0] new_PR_1;
    logic [div.DATA_WIDTH+2:0] new_PR_2;
    logic [div.DATA_WIDTH+2:0] new_PR_3;
    logic [div.DATA_WIDTH+1:0] B_1;
    logic [div.DATA_WIDTH+1:0] B_2;
    logic [div.DATA_WIDTH+1:0] B_3;
    
    logic [div.DATA_WIDTH-1:0] AR_r;
    logic [div.DATA_WIDTH-1:0] Q_temp;
    logic [5:0] shift_num_R;
    logic [6:0] shift_num_R_normalized;
    logic [5:0] shift_num_Q;
    logic [div.DATA_WIDTH*2+1:0] combined;
    logic [div.DATA_WIDTH*2+1:0] combined_normalized;
    logic terminate_early;
    
    localparam CLZ_W = $clog2(div.DATA_WIDTH);
    logic [CLZ_W-1:0] A_CLZ;
    logic [CLZ_W-1:0] B_CLZ;
    logic [CLZ_W-1:0] A_CLZ_r;
    logic [CLZ_W-1:0] B_CLZ_r;
    logic [CLZ_W-1:0] CLZ_delta;
    
    logic firstCycle;    
    logic greaterDivisor;
    logic [div.DATA_WIDTH-1:0] A_shifted;
    logic [div.DATA_WIDTH-1:0] B_shifted;
    logic [div.DATA_WIDTH-1:0] B_shifted_1;
    logic [div.DATA_WIDTH-1:0] B_shifted_2;
    logic [div.DATA_WIDTH-1:0] R_shifted;
    logic [div.DATA_WIDTH-1:0] B_shifted_r;

    localparam MSB_W = $clog2(div.DATA_WIDTH);
    logic [MSB_W-1:0] A_MSB;
    logic [MSB_W-1:0] B_MSB;
    logic [MSB_W-1:0] MSB_delta;
    logic [MSB_W-1:0] A_MSB_r;
    logic [MSB_W-1:0] B_MSB_r;
    logic [MSB_W-1:0] MSB_delta_r;
    logic [MSB_W-1:0] num_radix4_iter;
    logic [MSB_W-1:0] inital_shift;
    
    logic over_estimated;
    logic odd_MSB_delta;
    
    //implementation
    ////////////////////////////////////////////////////
    clz clz_r (.clz_input(div.dividend), .clz(A_CLZ));
    clz clz_b (.clz_input(div.divisor), .clz(B_CLZ));
    
    msb_naive msb_a (.msb_input(div.dividend), .msb(A_MSB));
    msb_naive msb_b (.msb_input(div.divisor), .msb(B_MSB));
    assign MSB_delta = A_MSB - B_MSB;

    always_ff @ (posedge clk) begin
        if (rst) begin
          firstCycle <= 0;
          A_CLZ_r <= 0;
          B_CLZ_r <= 0;
          A_MSB_r <= 0;
          B_MSB_r <= 0;
          greaterDivisor <= 0;
          A_shifted <= 0;
          B_shifted <= 0;
          B_shifted_1 <= 0;
          MSB_delta_r <= 0;
        end else begin
          if (div.start) begin
            firstCycle <= 1;
	        A_CLZ_r <= A_CLZ;
	        B_CLZ_r <= B_CLZ;
            A_MSB_r <= A_MSB;
            B_MSB_r <= B_MSB;
	        greaterDivisor <= div.divisor > div.dividend;
            A_shifted <= div.dividend;
            B_shifted <= div.divisor;
            B_shifted_1 <= div.divisor << MSB_delta;
            MSB_delta_r <= MSB_delta;
          end else begin
            firstCycle <= 0;
          end 
        end 
    end
    
    assign odd_MSB_delta = MSB_delta_r[0];
    assign over_estimated = B_shifted_1 > A_shifted;
    
    assign inital_shift = odd_MSB_delta ?  (MSB_delta_r & 5'b11110) : (over_estimated ? (MSB_delta_r-2) : MSB_delta_r);
    assign num_radix4_iter = odd_MSB_delta ?  (over_estimated ? (MSB_delta_r>>1) : (MSB_delta_r>>1)) : (over_estimated ? ((MSB_delta_r>>1) - 1) : (MSB_delta_r>>1));
        
    assign new_PR_1 = {1'b0, PR} - {1'b0, B_1};
    assign new_PR_2 = {1'b0, PR} - {1'b0, B_2};
    assign new_PR_3 = {1'b0, PR} - {1'b0, B_3};
    assign new_PR_sign = {new_PR_3[div.DATA_WIDTH+2], new_PR_2[div.DATA_WIDTH+2], new_PR_1[div.DATA_WIDTH+2]};
    
    //Shift reg for
    always_ff @ (posedge clk) begin
      if (rst) begin
        shift_count <= 0;
      end else if (firstCycle) begin
        shift_count <= 1;
      end else if (terminate) begin
        shift_count <= 0;
      end else begin
        shift_count <= {shift_count[14:0], firstCycle};
      end  
    end
    
    assign terminate_early = ~firstCycle & greaterDivisor;
    
    assign div.remainder = greaterDivisor ? A_shifted : PR[div.DATA_WIDTH+1:2];
    assign div.quotient = Q_temp;
    
    always_ff @ (posedge clk) begin
        if (firstCycle) begin
            Q_temp <= '0;
            PR <= {{2{1'b0}}, A_shifted >> inital_shift};
            AR_r <= A_shifted << (div.DATA_WIDTH - inital_shift);
            B_shifted_r <= B_shifted;
            B_1 <= {2'b0, B_shifted};           //1xB
            B_2 <= {1'b0, B_shifted, 1'b0};     //2xB
            B_3 <= {1'b0, B_shifted, 1'b0} + {2'b0, B_shifted}; //3xB
        end else if (~terminate & ~terminate_early) begin
            AR_r <= {AR_r[div.DATA_WIDTH-3:0], 2'b00};
            case (new_PR_sign)
                3'b111 : begin
                    PR <= {PR[div.DATA_WIDTH-1:0], AR_r[div.DATA_WIDTH-1:div.DATA_WIDTH-2]};
                    Q_temp <= {div.quotient[div.DATA_WIDTH-3:0], 2'b00};
                end
                3'b110 : begin
                    PR <= {new_PR_1[div.DATA_WIDTH-1:0], AR_r[div.DATA_WIDTH-1:div.DATA_WIDTH-2]};
                    Q_temp <= {div.quotient[div.DATA_WIDTH-3:0], 2'b01};
                end
                3'b100 : begin
                    PR <= {new_PR_2[div.DATA_WIDTH-1:0], AR_r[div.DATA_WIDTH-1:div.DATA_WIDTH-2]};
                    Q_temp <= {div.quotient[div.DATA_WIDTH-3:0], 2'b10};
                end
                default : begin //3'b000 : begin
                    PR <= {new_PR_3[div.DATA_WIDTH-1:0], AR_r[div.DATA_WIDTH-1:div.DATA_WIDTH-2]};
                    Q_temp <= {div.quotient[div.DATA_WIDTH-3:0], 2'b11};
                end
            endcase
        end
    end

    always_ff @ (posedge clk) begin
        if (div.start)
            div.divisor_is_zero <= ~(|div.divisor);
        else  if (~terminate & ~terminate_early)
            div.divisor_is_zero <= div.divisor_is_zero & ~(|new_PR_sign);
    end

    always_ff @ (posedge clk) begin
        if (rst)
            terminate <= 0;
        else begin
            if (firstCycle)
                terminate <= 0;
            else if (shift_count[num_radix4_iter[3:0]] | terminate_early)
                terminate <= 1;
        end
    end
    
    always_ff @ (posedge clk) begin
        if (rst)
            div.done <= 0;
        else begin
            if (firstCycle)
                div.done <= 0;
            else if ((shift_count[num_radix4_iter[3:0]] | terminate_early) & ~div.done & ~terminate) 
                div.done <= 1;
            else if (div.done)
                div.done <= 0;
        end
    end

endmodule
