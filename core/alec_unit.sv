/*
 * Copyright © 2017-2019 Eric Matthews,  Lesley Shannon
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Initial code developed under the supervision of Dr. Lesley Shannon,
 * Reconfigurable Computing Lab, Simon Fraser University.
 *
 * Author(s):
 *             Eric Matthews <ematthew@sfu.ca>
 */

import taiga_config::*;
import riscv_types::*;
import taiga_types::*;

module alec_unit(
        input logic clk,
        input logic rst,

        input alec_inputs_t alec_inputs,
        unit_issue_interface.unit issue,
        unit_writeback_interface.unit wb
    );

    logic [63:0] unit_result;
    logic [63:0] rs1_r, rs2_r;

    logic unit_ready;
    logic unit_start;
    logic unit_ack;
    logic unit_done;
    id_t id;

    assign rs1_r = {32'b0, alec_inputs.rs1};
    assign rs2_r = {32'b0, alec_inputs.rs2};

//    assign issue.ready = unit_ready;
    assign issue.ready = unit_ready & ~input_fifo.pop;
    assign unit_start = issue.new_request;
    assign unit_ack = wb.ack;
    assign wb.rd = unit_result[31:0];
    assign wb.done = unit_done;
    assign wb.id = id;
    
    fifo_interface #(.DATA_WIDTH($bits(id_t))) input_fifo();

    taiga_fifo #(.DATA_WIDTH($bits(id_t)), .FIFO_DEPTH(MAX_IDS))
        div_input_fifo (
            .clk    (clk),
            .rst    (rst),
            .fifo   (input_fifo)
        );

    assign input_fifo.data_in = issue.id;
    assign input_fifo.push = unit_start;
    assign input_fifo.potential_push = issue.possible_issue;
    assign input_fifo.pop = input_fifo.valid & unit_ack;
    assign id = input_fifo.data_out;
    
    /*always_ff @ (posedge clk) begin
        if (rst) begin
            id <= '0;
        end else begin
            if (unit_start) begin
                id <= issue.id;
            end
        end 
    end*/
/*
    test01RootDF custom_unit(
        .clock(clk),
        .reset(rst),
        .io_in_ready(unit_ready),
        .io_in_valid(unit_start),
        .io_in_bits_dataVals_field1_data(rs2_r),
        .io_in_bits_dataVals_field0_data(rs1_r),
        .io_out_ready(unit_ack),
        .io_out_valid(unit_done),
        .io_out_bits_data_field0_data(unit_result)
    );*/

endmodule

//module alec_unit(
//        input logic clk,
//        input logic rst,

//        input alec_inputs_t alec_inputs,
//        unit_issue_interface.unit issue,
//        unit_writeback_interface.unit wb
//        );

//    logic signed [63:0] result;
//    logic mulh [2];
//    logic done [2];
//    id_t id [2];

//    logic rs1_signed, rs2_signed;
//    logic signed [32:0] rs1_ext, rs2_ext;
//    logic signed [32:0] rs1_r, rs2_r;

//    logic stage1_advance;
//    logic stage2_advance;
//    ////////////////////////////////////////////////////
//    //Implementation
//    assign rs1_ext = {1'b0, alec_inputs.rs1};
//    assign rs2_ext = {1'b0, alec_inputs.rs2};

//    assign issue.ready = (~done[0] | ~done[1]);//stage1_advance;
//    assign stage1_advance = ~done[0] | stage2_advance;
//    assign stage2_advance = ~done[1] | wb.ack;

//    //Input and output registered Multiply
//    always_ff @ (posedge clk) begin
//        if (stage1_advance) begin
//            rs1_r <= rs1_ext;
//            rs2_r <= rs2_ext;
//        end
//        if (stage2_advance) begin
//            result <= 64'(rs1_r * rs2_r);
//        end
//    end

//    always_ff @ (posedge clk) begin
//        if (stage1_advance) begin
//            id[0] <= issue.id;
//            done[0] <= issue.new_request;
//        end
//        if (stage2_advance) begin
//            id[1] <= id[0];
//            done[1] <= done[0];
//        end
//    end

//    //Issue/write-back handshaking
//    ////////////////////////////////////////////////////
//    assign wb.rd = result[31:0];
//    assign wb.done = done[1];
//    assign wb.id = id[1];
//    ////////////////////////////////////////////////////
//    //End of Implementation
//    ////////////////////////////////////////////////////

//    ////////////////////////////////////////////////////
//    //Assertions

//endmodule
